theory abc2

imports Main abc

begin

(*
value "let pair=(''cc aCb AABBaCb'',
   [[(False, [1::nat]), (True, [4]), (True, [6])], [(False, [1]), (False, [4]), (False, [6])],
    [(False, [1]), (True, []), (False, [6, 8])], [(False, [1]), (False, []), (True, [6, 8])],
    [(False, []), (True, [3]), (True, [6])]]) in 
listerate (\<lambda> X. filter (\<lambda>x. x\<in>set X)) ((map (concat o (map (snd))) o snd) pair)"

value "map (\<lambda> (string, indices). (string, let compactString=removeNonLetters string in 
remdups [ i>0 & i<size compactString & compactString!(i-1)=compactString!i. 
  i<-listerate (\<lambda> X. filter (\<lambda>x. x\<in>set X)) ((map (concat o (map (snd)))) indices)])) 
counterexs"
*)

abbreviation "counterexsize' == 654" definition "counterexsize=counterexsize'"

abbreviation "csv2stringList' separators string == 
let removeSeparatorsFrom=breakListPositions (filterpositions2 (\<lambda>x. x\<in>separators) string) string in 
(take 1 removeSeparatorsFrom)@(map (filter (\<lambda> x. x\<notin>separators)) (tl removeSeparatorsFrom))"
definition "csv2stringList=csv2stringList'"
abbreviation "partialListSums' numlist == [listsum (take (Suc i) numlist). i<-[0..<size numlist]]"
definition "partialListSums=partialListSums'"

(*
value "let pair=(''cc aCb AABBaCb'',
   [[(False, [1::nat]), (True, [4]), (True, [6])], [(False, [1]), (False, [4]), (False, [6])],
    [(False, [1]), (True, []), (False, [6, 8])], [(False, [1]), (False, []), (True, [6, 8])],
    [(False, []), (True, [3]), (True, [6])]]) in 
(\<lambda> (string, indices). let stringList=csv2stringList {CHR '' ''} string in (stringList,  
(partialListSums o map size) stringList
, let sizeList=0#(partialListSums o map size) stringList in
[ let index=the (findFirstIndex (\<lambda>x. x=i) (insort i sizeList)) - 1 in (i, index , i-sizeList!(index)). 
  i<-listerate (\<lambda> X. filter (\<lambda>x. x\<in>set X)) ((map (concat o (map (snd)))) indices)])) 
pair"
*)

abbreviation "Conj' == (evalListRel o symCl) [(CHR ''a'', CHR ''A''), (CHR ''b'', CHR ''B''), (CHR ''c'', CHR ''C'')]"
definition "Conj = Conj'"
fun bumpsOld where
"bumpsOld []=[]"|
"bumpsOld (x#xs) = (let I=findFirstIndex (\<lambda>y. y=Conj x) xs in 
(if I\<noteq>None & bumpsOld (take (the I) xs)=[] then [(0::nat, 1 + the I)] else []))@
                                              (map (\<lambda> (i,j). (i+1, j+1)) (bumpsOld xs))"

value "let pair=([''cc'', ''aCb'', ''AABBaCb''], [(6::nat, 2::nat, 1::nat)]) in 
(\<lambda> (stringList, indicesList). 
remdups [let stringNum=(fst o snd) indexList in let pos=(snd o snd) indexList in ( 
(pos\<in>(set o (map fst)) (bumpsOld (stringList!stringNum))) \<or> (pos-1\<in>(set o (map snd)) (bumpsOld (stringList!stringNum)))
). indexList <- indicesList]) pair"

(*
value "sublist counterexs
(set (filterpositions2 (\<lambda>X. False\<in>set X)
(map (
(\<lambda> (stringList, indicesList). 
remdups [let stringNum=(fst o snd) indexList in let pos=(snd o snd) indexList in 
((pos\<in>(set o (map fst)) (bumpsOld (stringList!stringNum))) \<or> (pos-1\<in>(set o (map snd)) (bumpsOld (stringList!stringNum)))). indexList <- indicesList]
) o 
(\<lambda> (string, indices). let stringList=csv2stringList {CHR '' ''} string in (stringList,  
let sizeList=0#(partialListSums o map size) stringList in
[ let index=the (findFirstIndex (\<lambda>x. x=i) (insort i sizeList)) - 1 in (i, index, (i-(sizeList!(index)))). 
  i<-listerate (\<lambda> X. filter (\<lambda>x. x\<in>set X)) ((map (concat o (map (snd)))) indices)])) 
  )
(counterexs)
)))"
*)

abbreviation "allBalancedStringsOfIndexEq' n == 
(concat o (map (\<lambda> (x,y,z). (concat o (map (orderPreservingShuffles x))) (orderPreservingShuffles y z))) o concat) 
[(List.product (orderPreservingShufflesFirst (replicate i (CHR ''a'')) (replicate i (CHR ''A''))) 
(List.product (orderPreservingShufflesFirst (replicate j (CHR ''b'')) (replicate j (CHR ''B''))) 
(orderPreservingShufflesFirst (replicate n (CHR ''c'')) (replicate n (CHR ''C''))))). 
i<-[0..<n], j<-[0..<n], i\<le>j & j\<le>n]"
definition "allBalancedStringsOfIndexEq=allBalancedStringsOfIndexEq'"

(*
value "List.filter 
(\<lambda> string. 
size string > 4 &
List.find (\<lambda> fixedIndices. let (n, s)=checkSubSplits string fixedIndices in 
n=0 \<or> s\<noteq>{}
) (allFixedIndices string) \<noteq> None) 
(allBalancedStringsOfIndexEq 3)"
*)

definition "allBalancedStringsOfIndexEq3=allBalancedStringsOfIndexEq 3"

(*
value "size ((allBalancedStringsOfIndexEq 3))"*)
abbreviation "sizeallBalancedStringsOfIndexEq3'==(19353890::nat)"

(*
value "List.filter (\<lambda> string. 
                    size string > 4 &
                    List.find (\<lambda> fixedIndices. let (n, s)=checkSubSplits string fixedIndices in n=0 \<or> s\<noteq>{}) 
                              (allFixedIndices string) \<noteq> None) 
(take 500000 (drop 500000 (allBalancedStringsOfIndexEq 3)))"
*)



abbreviation "condition2' string fixedIndices alternation == List.find (\<lambda> (flag,indices). size indices > 1)"
abbreviation "mbc07 == ''cbaabbAcBBCBCA''" abbreviation "mbc08 == [0::nat, 5, 7, 14]"
abbreviation "represent01' string fixedIndexTuple == 
(printFixedSplit string fixedIndexTuple, map (subStrings string fixedIndexTuple) (allowedSplits string fixedIndexTuple))"
definition "represent01=represent01'"
abbreviation "represent00' string fixedIndexTuple == 
(printFixedSplit string fixedIndexTuple, (allowedSplits string fixedIndexTuple))"
definition "represent00=represent00'"

abbreviation "compact2reducedFixedSplit' string==
[(size o removeNonLetters o (take n)) string. n<-filterpositions2 (%x. x=hd '' '') string]"
definition "compact2reducedFixedSplit=compact2reducedFixedSplit'"
abbreviation "compact2fullFixedSplit' spacedString==0#compact2reducedFixedSplit spacedString@[size (removeNonLetters spacedString)]"
definition "compact2fullFixedSplit=compact2fullFixedSplit'"

abbreviation "represent02' spacedString == let String=removeNonLetters spacedString in 
(represent00 String (compact2fullFixedSplit spacedString))"
definition "represent02=represent02'"

abbreviation "isShort' string == (Conj ` (set string)) \<inter> set string={}" definition "isShort=isShort'"
fun bumps where "bumps []=[]"|
"bumps (x#xs) = (let I=findFirstIndex (\<lambda>y. y=Conj x) xs in 
(if I\<noteq>None & 
(* bumps (take (1+the I) xs)=[] &*)
(*set (map Conj (take (1+the I) xs)) \<inter> set (take (1+the I) xs)={} & *)
isShort (take (1+the I) xs) then [(0::nat, 1 + the I)] else []))@
(map (\<lambda> (i,j). (i+1, j+1)) (bumps xs))"

abbreviation "filterShortSplits' string == 
filter (%l. False \<notin> set [bumps (drop (l!(i-1)) (take (l!i) string))=[]. i<-[1..<size l] ])"
definition "filterShortSplits=filterShortSplits'"
term "allBalancedStringsOfIndexEq 3"

value "let string=''cb cBB aCCCAbc'' in let String=removeNonLetters string in 
(represent00 String
(0#(compact2reducedFixedSplit string)@[size String]))"
value "represent00' mbc05 mbc06"
value "represent00' mbc07 mbc08"

abbreviation "convertVarIndices' string fixedIndices varIndices entry == 
let flag= fst (varIndices!entry) in let list=(fixedIndices!entry)#(snd (varIndices!entry))@[fixedIndices!(entry+1)] in 
[(1=(i+bool2nat flag) mod 2, list!i, list!(i+1)). i<- [0..<size list-1]]"
definition "convertVarIndices=convertVarIndices'"
(*example: *)
abbreviation "mbc12 == removeNonLetters ''aaaa bacBbAb aaAAABACAAAABaa''" abbreviation "mbc13 == [4, 11::nat]"
value "let mbc13'= 0#mbc13@[size mbc12] in convertVarIndices mbc12 mbc13' [(False, []), (True, []), (True, [19, 23])] 2"

abbreviation "splitIndices' string fixedFullIndices alternation == 
(concat o (map (convertVarIndices string fixedFullIndices alternation))) [0..<size fixedFullIndices-1]"
definition "splitIndices=splitIndices'"
value "let mbc13'= 0#mbc13@[size mbc12] in splitIndices mbc12 mbc13' [(False, []), (True, []), (True, [19, 23])]"

abbreviation "evenChunks' string fixedFullIndices alternation == 
[sublist string {s..<t}. (f,s,t) <- (splitIndices string fixedFullIndices alternation), f=False]"
abbreviation "oddChunks' string fixedFullIndices alternation == 
[sublist string {s..<t}. (f,s,t) <- (splitIndices string fixedFullIndices alternation), f]"
definition "evenChunks=evenChunks'" definition "oddChunks=oddChunks'"
value "let mbc13'= 0#mbc13@[size mbc12] in oddChunks mbc12 mbc13' [(False, []), (True, []), (True, [19, 23])]"

abbreviation "balancedSubSplits' string fixedIndicesFull == let fixedIndices=fixedIndicesFull in  
filter (\<lambda> l. balanced (concat (evenChunks string fixedIndices l))) 
       (allFeasibleAlternations fixedIndices)"
definition "balancedSubSplits=balancedSubSplits'"

abbreviation "commonSubSplitPoints' subSplitList == 
(Inter o set) ((map (\<lambda>L. (Union o set) (map (set o snd) L)) subSplitList))"
definition "commonSubSplitPoints=commonSubSplitPoints'"
value "let string=removeNonLetters ''cb bCC CBaAaccBA'' in 
commonSubSplitPoints (snd (represent00 string [0,2,5,size string]))"

abbreviation "nonSplitPoints' string fixedIndicesReducedOrFull subSplitsList == {1..<size string} - 
(set fixedIndicesReducedOrFull \<union> (set o concat) (map (concat o (map snd)) subSplitsList))"
definition "nonSplitPoints=nonSplitPoints'"

text{*370919*}
(*value "size (allSimpleClosedStringsOfIndexEq 3)"*)



abbreviation "list2Rel' l == (\<lambda> i. (i,l!i))`{0..<size l}" definition "list2Rel=list2Rel'"
lemma lm00: "(list2Rel l) \<subseteq> set (zip [0..<size l] l)" 
unfolding list2Rel_def
using imageE length_map length_zip map_nth 
mem_Collect_eq min_less_iff_conj nth_map nth_zip set_conv_nth set_upt subsetI
by (smt )
lemma lm01: "size (zip [0..<size l] l) = size l & distinct (zip [0..<size l] l)" 
using distinct_upt distinct_zipI1 by fastforce
lemma lm02: "card (set (zip [0..<size l] l)) = size l" using lm01 using comp_def distinct_card by fastforce
lemma lm03: "inj_on (\<lambda> i. (i,l!i)) {0..<size l}" using inj_on_convol_ident by blast
lemma lm04: "card (list2Rel l) = size l" unfolding list2Rel_def using lm02 lm03 card_image distinct_card distinct_upt 
image_cong length_map map_nth set_upt by (metis)
lemma lm05: "finite (set (zip [0..<size l] l))" by blast
lemma "(list2Rel l) = set (zip [0..<size l] l)" using lm00 lm01 lm02 lm03 lm04 lm05 
card_subset_eq by metis

abbreviation "rel2List' L == [L,,i. i<-[0..<card L]]" definition "rel2List=rel2List'"

abbreviation "swapLetters' letter1 letter2 string ==rel2List ((list2Rel string) O 
(Id +* {(hd letter1, hd ''z'')}) O (Id +* {(hd letter2, hd letter1)}) O (Id +* {(hd ''z'', hd letter2)}))"
definition "sw = swapLetters'"
abbreviation "bumpness' string == (min (countOccurs (CHR ''a'') string) (countOccurs (CHR ''A'') string))
+(min (countOccurs (CHR ''b'') string) (countOccurs (CHR ''B'') string))+
(min (countOccurs (CHR ''c'') string) (countOccurs (CHR ''C'') string))"
definition "bumpness=bumpness'"

end