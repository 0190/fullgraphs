theory les3a

imports Main les 
"~/afp/Transitive-Closure/Transitive_Closure_List_Impl"
(* "~/afp/Vickrey_Clarke_Groves/Argmax"*)

begin
abbreviation "optionHd' l==if l=[] then None else (Some o hd) l" definition "optionHd=optionHd'"
abbreviation "optEval' R x == let Y=R``{x} in if card Y=1 then (Some o the_elem) Y else None" definition "optEval=optEval'"
abbreviation "optComp' g f x == if f x=None then None else (Some o g o the o f) x" definition "optComp=optComp'"
abbreviation "optComp2' g f x == if f x=None then None else (g o the o f) x" definition "optComp2=optComp2'"
abbreviation "symCl' relList == remdups (relList@(map flip relList))" definition "symCl=symCl'"
abbreviation "Strict' listRel == filter (% (x,y). x\<noteq>y) listRel"
definition "Strict = Strict'"
term filterpositions2
fun filterPositionsUpto where
"filterPositionsUpto P [] n = []" | 
"filterPositionsUpto P (x#xs) n = (if (P x) then (0#map Suc (filterPositionsUpto P xs (n-1))) else 
(map Suc (filterPositionsUpto P xs n)))"

(*value "filterPositionsUpto (%x. x<3) [1::nat,2,7,1,9,11,0] 10"*)

find_consts "('a => bool) => ('a list) => nat list"

abbreviation "filterFirst' P l == (let match=List.find P l in 
  (if (Option.is_none match) then [] else [the match]))" definition "filterFirst=filterFirst'" 

abbreviation "TrClOver' rel Dom == concat [List.product [x] (rtrancl_list_impl rel [x]). x<-Dom]"
definition "TrClOver = TrClOver'"
abbreviation "TrCl' rel == TrClOver rel (FieldList rel)" definition "TrCl = TrCl'"

abbreviation "compId' X == {(x,x). x\<in>X}"
abbreviation "antisym2' P == ( (% (x,y). x=y) ` (P \<inter> (P^-1))) \<subseteq> {True}"
definition "antisym2 = antisym2'"
abbreviation "antisym3' listRel == (set (Strict listRel) \<inter> set (map flip (Strict listRel)) = {})"
definition "antisym3 = antisym3'"
(*value "antisym3 [(0::nat,1),(1,2),(2,1)]"
value "map flip (Strict [(0::nat,1),(1,0)])"
value "configurationsList' [(1::nat,2::nat)] []"
*)

section{*Vandrager 2.5*}
abbreviation "derivedRelation' Ca1 Ca2 pairlist == 
[ (pair1, pair2). (pair1,pair2) <- List.product pairlist pairlist, 
                  (fst pair1, fst pair2) \<in> set Ca1 \<or> (snd pair1, snd pair2) \<in> set Ca2]"
definition "derivedRelation = derivedRelation'"

abbreviation "preconfigurations01' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == 
[X. X<- sublists (List.product (fresh1 # FieldList Ca1) (fresh2 # FieldList Ca2)),
set (map fst X) - {fresh1} \<in> set (map set (configurationsList Ca1 Co1)),
set (map snd X) - {fresh2} \<in> set (map set (configurationsList Ca2 Co2)),
antisym3 (TrCl (derivedRelation Ca1 Ca2 X))]" definition "preconfigurations01 = preconfigurations01'"

abbreviation "preconfigurations' Ca1 Co1 Ca2 Co2 fresh1 fresh2 ==
let confs1=configurationsList Ca1 Co1 in let confs2=configurationsList Ca2 Co2 in
 (remdups o concat) [[X. X <- sublists (List.product (fresh1#C1) (fresh2#C2)), 
 set (map fst X) - {fresh1} \<in> set (map set confs1)
 , set (map snd X) - {fresh2} \<in> set (map set confs2)
   , antisym3 (TrCl (derivedRelation Ca1 Ca2 X))
 ]. C1 <- confs1, C2 <- confs2]"
definition "preconfigurations=preconfigurations'"
abbreviation "vgCa1 == [(1::nat,2::nat), (1,3), (2,4), (3,5), (1,1), (2,2), (3,3), (4,4), (5,5)]"
abbreviation "Successors' listRel elem == ((map snd) o (filter (%(x,y). x=elem)) o Strict) listRel"
abbreviation "Predecessors' listRel elem == ((map fst) o (filter (%(x,y). y=elem)) o Strict) listRel"
definition "Successors=Successors'" definition "Predecessors = Predecessors'"
(*value "Successors vgCa1 1"*)
abbreviation "maximals2' rel == filter (%x. Successors rel x=[]) ((remdups o map snd) rel)"
definition "maximals2=maximals2'"
(* abbreviation "preconfigurations' Ca1 Co1 Ca2 Co2 fresh == 
filter (antisym2 o trancl o set o (derivedRelation Ca1 Ca2)) (prepreconfigurations Ca1 Co1 Ca2 Co2 fresh)" *)
(*abbreviation "preconfigurations' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == 
filter (antisym3 o myTrCl o (derivedRelation Ca1 Ca2)) (prepreconfigurations Ca1 Co1 Ca2 Co2 fresh1 fresh2)" 
definition "preconfigurations = preconfigurations'"*)

abbreviation "completePrimes' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == 
filter (%X. (size o maximals2  o (derivedRelation Ca1 Ca2)) X  = 1) (preconfigurations Ca1 Co1 Ca2 Co2 fresh1 fresh2)"
definition "completePrimes = completePrimes'"

abbreviation "preproductCa' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == 
[ ((X,Y), maximals2 (derivedRelation Ca1 Ca2 X)). (X,Y) <- 
  (List.product (completePrimes Ca1 Co1 Ca2 Co2 fresh1 fresh2) 
                (completePrimes Ca1 Co1 Ca2 Co2 fresh1 fresh2)),
  set X \<subseteq> set Y]"

definition "preproductCa=preproductCa'"
abbreviation "productCa' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == map fst (preproductCa Ca1 Co1 Ca2 Co2 fresh1 fresh2)"
definition "productCa = productCa'"
section{* Projectors: vanDraager, 3.5 *}
(* NB: hd to be applied to this, once non-emptiness assumed *)
subsection{* Projectors, Vaandrager 3.5 and 3.6 *}
abbreviation "projector1' Ca1 Co1 Ca2 Co2 fresh1 fresh2 preconf == (hd o map (%((X,Y),MX). (fst o hd) MX)) 
  (filterFirst (\<lambda>((X,Y),MX). X=preconf) (preproductCa Ca1 Co1 Ca2 Co2 fresh1 fresh2))"
definition "projector1=projector1'"
abbreviation "projector2' Ca1 Co1 Ca2 Co2 fresh1 fresh2 preconf == (hd o map (%((X,Y),MX). (snd o hd) MX)) 
  (filterFirst (%((X,Y),MX). X=preconf) (preproductCa Ca1 Co1 Ca2 Co2 fresh1 fresh2))"
definition "projector2=projector2'"

abbreviation "productCo' Ca1 Co1 Ca2 Co2 fresh1 fresh2 == [(X,Y). (X,Y)<- 
 List.product (completePrimes Ca1 Co1 Ca2 Co2 fresh1 fresh2) (completePrimes Ca1 Co1 Ca2 Co2 fresh1 fresh2),
set X \<union> (set Y) \<notin> set ` (set (preconfigurations Ca1 Co1 Ca2 Co2 fresh1 fresh2))]"
definition "productCo=productCo'"

(* needs trancl to be computed outside via SMT for performance reasons *)
(* value "(map (antisym2 o trancl o set) o  remdups o  (map
( (derivedRelation [(1::nat,2::nat)]  [(3::nat,3::nat)]))))
 ((preconfigurations [(1::nat,2::nat)] [] [(3::nat,3::nat)] [] 0))" *)
value "(configurationsList [(1::nat, 2::nat), (2,3),(4,5),(6,7), (7,8)] [(3::nat, 4),(4,5)])"

(* Canonical equalizer, Proposition 4.21 *)
abbreviation "equalizerEventList' Ca f g == 
[e. e<-FieldList Ca, map f (Predecessors Ca e) = map g (Predecessors Ca e)]"
(* MBC: can we gain any performance by using immediate predecessors instead of predecessors? No! *)
definition "equalizerEventList=equalizerEventList'"
abbreviation "equalizerCa' Ca f g == filter 
  (%(x,y). x\<in> set (equalizerEventList Ca f g) \<and> y \<in> set (equalizerEventList Ca f g)) Ca"
definition "equalizerCa=equalizerCa'"
abbreviation "equalizerCo Ca1 Co1 f g == 
filter (%(x,y). x\<in>set (equalizerEventList Ca1 f g) \<and> y \<in> set (equalizerEventList Ca1 f g)) Co1"

(* NB: hd below *)
abbreviation "pullbackCa' Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2 == 
equalizerCa (productCa Ca1 Co1 Ca2 Co2 fresh1 fresh2) (morph1 o (projector1 Ca1 Co1 Ca2 Co2 fresh1 fresh2))
(morph2 o (projector2 Ca1 Co1 Ca2 Co2 fresh1 fresh2))" definition "pullbackCa=pullbackCa'"
abbreviation "pullbackCo' Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2 == 
equalizerCo (productCa Ca1 Co1 Ca2 Co2 fresh1 fresh2) (productCo Ca1 Co1 Ca2 Co2 fresh1 fresh2) 
(morph1 o (projector1 Ca1 Co1 Ca2 Co2 fresh1 fresh2)) (morph2 o (projector2 Ca1 Co1 Ca2 Co2 fresh1 fresh2))"
definition "pullbackCo=pullbackCo'"
abbreviation "listgraph' f list == zip list (map f list)" definition "listgraph = listgraph'" 
abbreviation "pullbackArrow1' Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2 == listgraph 
((projector1 Ca1 Co1 Ca2 Co2 fresh1 fresh2)) 
(FieldList (pullbackCa Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2))" 
definition "pullbackArrow1=pullbackArrow1'"
abbreviation "pullbackArrow2' Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2 == listgraph 
((projector2 Ca1 Co1 Ca2 Co2 fresh1 fresh2)) 
(FieldList (pullbackCa Ca1 Co1 morph1 Ca2 Co2 morph2 fresh1 fresh2))" 
definition "pullbackArrow2=pullbackArrow2'"

find_consts "'a => 'b => 'a list"
abbreviation "rangeList' == remdups o (map snd)" definition "rangeList = rangeList'"

(* Coequalizers (proposition 4.26 ). *)
(* To compute a coequalizer, you need as input: 
1) An event structure ES2=(Ca2,Co2).
2) two communication morphisms defined on an ES ES1, mapping into ES2=(Ca2,Co2). 
This definition makes sense only when 
the two communication morphisms have disjoint ranges and are both injective.
Note that the details of ES1 do not matter, since only the set of its events are needed 
to compute the coequalizer; and those are computable from either of the two communication morphisms, 
being those total by definition.
*)
(*
abbreviation "coequalizerEventList' Ca1 commorph1 Ca2 Co2 commorph2 == (let images1=
remdups (map commorph1 (FieldList Ca1)) in let images2=remdups (map commorph2 (FieldList Ca1)) in 
  ((List.product images1 images2), ((filter (%x. x \<notin> set (images1@images2)) (FieldList Ca2)))))"
*)  
abbreviation "coequalizerEventList' commorph1 commorph2 Ca2 == (let images1=
rangeList commorph1 in let images2=rangeList commorph2 in 
  ((List.product images1 images2), ((filter (\<lambda>x. x \<notin> set (images1@images2)) (FieldList Ca2)))))"
(* Note that, _here_, Ca2 is only needed to compute FieldList Ca2. *)
definition "coequalizerEventList=coequalizerEventList'"

abbreviation "coequalizerCa1' commorph1 commorph2 Ca2 == 
filter (\<lambda>(x,y). {x,y} \<subseteq> (set o snd) (coequalizerEventList commorph1 commorph2 Ca2)) Ca2"
abbreviation "coequalizerCa2' commorph1 commorph2 Ca2 ==  
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
[(e, (e1, e2)). e<-snd allEvents, (e1,e2)<-fst allEvents, ((e,e1) \<in> set Ca2 \<or> (e,e2)\<in>set Ca2)]" 
abbreviation "coequalizerCa3' commorph1 commorph2 Ca2 == 
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
[((e1,e2), e). e<-snd allEvents, (e1,e2)<-fst allEvents, (e1,e)\<in>set Ca2 \<or> (e2,e)\<in>set Ca2]"
abbreviation "coequalizerCa4' commorph1 commorph2 Ca2 == 
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
[((e1,e2),(e3,e4)). (e1,e2)<-fst allEvents, (e3,e4)<-fst allEvents, (e1,e3)\<in>set Ca2, (e2,e4)\<in>set Ca2]"
definition "coequalizerCa1=coequalizerCa1'" definition "coequalizerCa2=coequalizerCa2'"
definition "coequalizerCa3=coequalizerCa3'" definition "coequalizerCa4=coequalizerCa4'"
(* `fresh' is used to work around the HOL impossibility of union of differently-typed set.
Remdups not needed due to freshness of fresh. *)
abbreviation "coequalizerCa' commorph1 commorph2 Ca2 fresh == (
map (\<lambda> (x,y). ((x,fresh),(y,fresh))) (coequalizerCa1 commorph1 commorph2 Ca2) @ 
map (\<lambda> (x,(y,z)). ((x,fresh),(y,z))) (coequalizerCa2 commorph1 commorph2 Ca2) @
map (\<lambda> ((x,y),z). ((x,y),(z,fresh))) (coequalizerCa3 commorph1 commorph2 Ca2) @
(coequalizerCa4 commorph1 commorph2 Ca2))" definition "coequalizerCa = coequalizerCa'"

abbreviation "coequalizerCo1' commorph1 commorph2 Ca2 Co2 == 
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
filter (\<lambda> (x,y). {x,y} \<subseteq> (set o snd) allEvents) Co2"
abbreviation "coequalizerCo2' commorph1 commorph2 Ca2 Co2 == 
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
[(e,(e1,e2)). e<-snd allEvents, (e1,e2)<-fst allEvents, (e,e1)\<in>set Co2 \<or> ((e,e2)\<in>set Co2)]"
abbreviation "coequalizerCo3' commorph1 commorph2 Ca2 Co2 == 
let allEvents=coequalizerEventList commorph1 commorph2 Ca2 in
[((e1,e2),(e3,e4)). (e1,e2)<-fst allEvents, (e3,e4)<-fst allEvents, {(e1,e3),(e2,e4)} \<subseteq> set Co2]"
definition "coequalizerCo1=coequalizerCo1'" definition "coequalizerCo2=coequalizerCo2'"
definition "coequalizerCo3=coequalizerCo3'"
abbreviation "coequalizerCo' commorph1 commorph2 Ca2 Co2 fresh == (
map (\<lambda>(x,y). ((x,fresh),(y,fresh))) (coequalizerCo1 commorph1 commorph2 Ca2 Co2)@
map (\<lambda>(x,(y,z)). ((x,fresh),(y,z))) (coequalizerCo2 commorph1 commorph2 Ca2 Co2)@
(coequalizerCo3 commorph1 commorph2 Ca2 Co2))" definition "coequalizerCo = coequalizerCo'"

abbreviation "evalListRel' listRel point == (snd o hd) (filterFirst (%(x,y). x=point) listRel)"
definition "evalListRel = evalListRel'"
notation evalListRel (infix "!," 90)

(*abbreviation "coequalizerMorph' morph1 morph2 fresh event == 
let preimage1=(filterFirst (\<lambda>(x,y). y=event) morph1) in 
let preimage2=(filterFirst (\<lambda>(x,y). y=event) morph2) in
(if preimage1\<noteq>[] then (event, morph2!,((fst o hd) preimage1)) else if 
    preimage2\<noteq>[] then (morph1!,((fst o hd) preimage2), event) 
else (event,fresh))" 
(* this should really be `event` alone, but HOL is typed *) 
definition "coequalizerMorph = coequalizerMorph'"
*)

(* NB: FieldList Ca1 and FieldList Ca2 MUST be disjoint (as a side effect, you don't need remdups) *)
(* Proposition 4.20 vs 4.25. We use 4.25 here. *)
(* abbreviation "coproductEventList' Ca1 Ca2 == FieldList Ca1 @ (FieldList Ca2)" *)
(* abbreviation "coproductCa' Ca1 Ca2 == Ca1@Ca2" *)
abbreviation "coproductCa' Ca1 Ca2 fresh1 fresh2 == 
[((x1,fresh2),(x2,fresh2)). (x1,x2) <- Ca1]@[((fresh1,y1),(fresh1,y2)). (y1,y2) <- Ca2]"
definition "coproductCa = coproductCa'"
(* abbreviation "coproductCo' Co1 Co2 == Co1@Co2" *) 
(* abbreviation "coproductCo' Co1 Co2 fresh1 fresh2 == 
[((x1,fresh2),(x2,fresh2)). (x1,x2) <- Co1]@[((fresh1,y1),(fresh1,y2)). (y1,y2) <- Ca2]" *)
abbreviation "coproductCo' Co1 Co2 fresh1 fresh2 == 
[((x1,fresh2),(x2,fresh2)). (x1,x2) <- Co1]@[((fresh1,y1),(fresh1,y2)). (y1,y2) <- Co2]"
definition "coproductCo = coproductCo'"

(* NB: FieldList Ca1 and FieldList Ca2 MUST be disjoint *)
(* The canonical pushout takes as input 
1) two communication morphisms defined over the same ES
2) two ESs being the codomain of the two commorphs above, respectively.
You need to specify 2) because communication morphisms are not, in general, surjective.
*)
abbreviation "pushoutCa' Ca1 commorph1 Ca2 commorph2 fresh1 fresh2 fresh3 == 
coequalizerCa 
  (map (\<lambda> (x,y). (x,(y,fresh2))) commorph1) 
  (map (\<lambda>(x,y). (x,(fresh1, y))) commorph2) 
  (coproductCa Ca1 Ca2 fresh1 fresh2) 
  fresh3" 
definition "pushoutCa=pushoutCa'" 
abbreviation "pushoutCo' Ca1 Co1 commorph1 Ca2 Co2 commorph2 fresh1 fresh2 fresh3 == 
(coequalizerCo (map (\<lambda> (x,y). (x,(y,fresh2))) commorph1) 
  (map (\<lambda>(x,y). (x,(fresh1, y))) commorph2) 
  (coproductCa Ca1 Ca2 fresh1 fresh2) 
  (coproductCo Co1 Co2 fresh1 fresh2) 
  fresh3
)"
definition "pushoutCo=pushoutCo'"

(*abbreviation "pushoutMorph' commorph1 commorph2 fresh == coequalizerMorph commorph1 commorph2 fresh"*)











section {* Putting the pieces together *}

abbreviation "pullBackTop' SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 fresh1 fresh2 == (
pullbackCa
(filter (\<lambda>(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (\<lambda>(x,y). x\<in>(Domain SDmorph1)) Co1) (eval_rel SDmorph1)
(filter (\<lambda>(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (\<lambda>(x,y). x\<in>(Domain SDmorph2)) Co2) (eval_rel SDmorph2) fresh1 fresh2,
pullbackCo
(filter (\<lambda>(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (\<lambda>(x,y). x\<in>(Domain SDmorph1)) Co1) (eval_rel SDmorph1)
(filter (\<lambda>(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (\<lambda>(x,y). x\<in>(Domain SDmorph2)) Co2) (eval_rel SDmorph2) fresh1 fresh2
)"
definition "pullBackTop = pullBackTop'"
abbreviation "pullBackLeft' SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2 == (
pullbackCa Ca1 Co1 id 
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(projector1 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) 
            (filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2)
freshLeft1 freshLeft2, 
pullbackCo Ca1 Co1 id 
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
((projector1 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) 
  (filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2))
freshLeft1 freshLeft2,
pullbackArrow2 Ca1 Co1 id 
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
((projector1 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) 
(filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2))
freshLeft1 freshLeft2
)" 
abbreviation "trd' == snd o snd" definition "trd=trd'" 
definition "pullBackLeft = pullBackLeft'"

abbreviation "pullBackRight' SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2 == (
pullbackCa  
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
((projector2 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) (filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2))
Ca2 Co2 id
freshRight1 freshRight2 ,
pullbackCo
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
((projector2 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) (filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2))
Ca2 Co2 id freshRight1 freshRight2,
pullbackArrow1
(fst (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
(snd (pullBackTop SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2))
((projector2 (filter (%(x,y). x\<in>(Domain SDmorph1)) Ca1) (filter (%(x,y). x\<in>(Domain SDmorph1)) Co1) (filter (%(x,y). x\<in>(Domain SDmorph2)) Ca2) (filter (%(x,y). x\<in>(Domain SDmorph2)) Co2) freshTop1 freshTop2))
Ca2 Co2 id freshRight1 freshRight2)" 

definition "pullBackRight = pullBackRight'"

subsection {* The final pushout *}
(* We need four pieces of input to trigger the final pushout:
1) two ESs (M1 and M2) in the diagram
2) two communication morphisms (the arrows entering M1 and M2, respectively, in the diagram)

1) is easy: M1 and M2 are the results of pullBackLeft and pullBackRight above, respectively.
To face task 2) (to obtain the morphism into M1, for example): 
thanks to Propositions 5.2 (thesis 2) and Proposition 5.3 together, we only need to take the rightmost
of the output morphisms obtained from pullBackLeft, and invert it to obtain a communication morphism.
Similarly for the morphism into M2.
*)
term "pushoutCa"
term "fst (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2)"
term "(map flip (trd (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2)))"
term coequalizerCa
term "
pushoutCa 
(fst (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2))
(map flip (trd (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2)))
(fst (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2))
(map flip (trd (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2)))
freshBottom1 freshBottom2 freshBottom3"

(* ('a \<times> ('a \<times> 'b) list) list \<Rightarrow> (('a \<times> 'b) list \<times> 'b) list \<Rightarrow> ('a \<times> ('a \<times> 'b) list) list \<times> (('a \<times> 'b) list \<times> 'b) list*)
abbreviation "categoricalConstruction' SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 
freshTop1 freshTop2 freshLeft1 freshLeft2 freshRight1 freshRight2 
freshBottom1 freshBottom2 freshBottom3 
==(pushoutCa 
(fst (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2))
(map flip (trd (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2)))
(fst (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2))
(map flip (trd (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2)))
freshBottom1 freshBottom2 freshBottom3,
pushoutCo 
(fst (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2))
((fst o snd) (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2))
(map flip (trd (pullBackLeft SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshLeft1 freshLeft2)))
(fst (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2))
((fst o snd) (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2))
(map flip (trd (pullBackRight SDmorph1 SDmorph2 Ca1 Co1 Ca2 Co2 freshTop1 freshTop2 freshRight1 freshRight2)))
freshBottom1 freshBottom2 freshBottom3)"

definition "categoricalConstruction = categoricalConstruction'"































abbreviation "mbc01Ca'==[(0::nat,1::nat),(1,2),(2,3),(2,5)]"
definition "mbc01Ca = TrCl mbc01Ca'"
abbreviation "mbc01Co'==[(3::nat,4::nat),(4,3)]" definition "mbc01Co=mbc01Co'"
abbreviation "mbc01SDmorp1' == {(3::nat,5::nat)}" definition "mbc01SDmorp1 = mbc01SDmorp1'"
value "mbc01Ca"
definition "mbc01Temp = sublists (List.product (hd (configurationsList mbc01Ca mbc01Co)) 
(hd (configurationsList mbc01Ca mbc01Co)))"


abbreviation "isDownwardClosed' Ca C == (C \<subseteq> events Ca & (\<forall>e f. e \<in> C & (f, e) \<in> Ca \<longrightarrow> f \<in> C))"
definition "isDownwardClosed = isDownwardClosed'"

theorem "IsLes causality conflict \<longleftrightarrow> (isLes (pred2set causality) (pred2set conflict))"
using lm12 lm13 lm14 lm15 lm16 lm17 lm18 lm19 reflex_def by smt

(*unused*)
abbreviation "preconfigurations02' Ca1 Co1 Ca2 Co2 fresh1 fresh2 ==
let confs1=configurationsList Ca1 Co1 in let confs2=configurationsList Ca2 Co2 in
 (concat) [[X. X <-  (sublists (List.product (fresh1#C1) (fresh2#C2))), True 
 , set (map fst X) - {fresh1} \<in> set (map set (confs1))
 , set (map snd X) - {fresh2} \<in> set (map set (confs2))
  ,  antisym3 (TrCl (derivedRelation Ca1 Ca2 X))
 ]. C1 <- confs1, C2 <- confs2]"
definition "preconfigurations02=preconfigurations02'"


end
