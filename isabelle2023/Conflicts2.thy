theory Conflicts2

imports Conflicts

begin

section{*Possible optimisations*}

(* Consider turning ReducedRelation3 to (\<times>) option, as suggested by CICM reviewer *)
abbreviation "ReducedRelation3' P ==
(let minRel=List.filter (%(x,y). x \<notin> (snd`((set P)-{(x,x)}))) P in let Minimals=remdups (map fst minRel) in
let Multiplicities=map (%z. (size (nextForPartialOrder P [z]),z)) Minimals
in let suitableVals=Maxs(set (map fst Multiplicities)) in 
let somePivot=optApp snd (List.find (%(x,y).x\<in>suitableVals) Multiplicities) in 
(somePivot, filter (%(x,y). x \<noteq> the somePivot & y \<noteq> the somePivot) P))"
definition "ReducedRelation3=ReducedRelation3'"



























lemma lm06: assumes "(Some ya, y) = ReducedRelation3 P" shows 
"ya \<in> set (map fst P) & ya \<notin> (snd`((set P)-{(ya,ya)})) & y=filter (%(x,y). x \<noteq> ya & y \<noteq> ya) P &
length y < length P"
proof -
let ?minRel="List.filter (%(x,y). x \<notin> (snd`((set P)-{(x,x)}))) P"
let ?Minimals="remdups (map fst ?minRel)"
let ?Multiplicities="map (%z. (size (nextForPartialOrder' P [z]),z)) ?Minimals"
let ?suitableVals="Maxs'(set (map fst ?Multiplicities))"
let ?somePivot="optApp' snd (List.find (%(x,y).x\<in>?suitableVals) ?Multiplicities)"
have "(Some ya, y)=(?somePivot, filter (%(x,y). x \<noteq> the ?somePivot & y \<noteq> the ?somePivot) P)"
using assms unfolding ReducedRelation3_def nextForPartialOrder_def Maxs_def optApp_def by meson 
then moreover have "?somePivot=Some ya" by simp moreover have "Some ya \<noteq> None" by simp ultimately 
moreover have "?somePivot=Some (snd (the (List.find (%(x,y). x\<in>?suitableVals) ?Multiplicities)))"
by presburger ultimately moreover obtain xx yy where 
0: "(xx,yy) \<in> set ?Multiplicities & xx\<in>?suitableVals & ?somePivot=Some yy"
by (smt case_prod_unfold mm05r prod.collapse) ultimately moreover have "yy=ya" by auto
ultimately moreover have "ya \<in> set ?Minimals"  using assms find_Some_iff nth_mem by (smt length_map nth_map option.collapse option.inject snd_conv)
then have "ya \<in> set (map fst P) & ya\<in>fst`(set ?minRel)" by force
then  have "ya \<in> set (map fst P) & ya \<notin> (snd`((set P)-{(ya,ya)}))" by force
ultimately moreover have "y=filter (%(x,y). x \<noteq> ya & y \<noteq> ya) P" using assms ReducedRelation3_def 
by (smt case_prod_unfold filter_cong fst_conv option.sel snd_conv) ultimately show ?thesis 
using Pair_inject case_prodE imageE length_filter_less list.set_map prod.collapse by smt
qed

function conflictsFor2 where "conflictsFor2 Or=(let (M,or)=ReducedRelation3 Or in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor2 or])" 
apply simp apply simp by blast

termination conflictsFor2 apply (relation "measure size") apply auto[1] apply auto using lm06 by fast

lemma mm07c: assumes "(M,or)=ReducedRelation3 Or" "M\<noteq>None" shows 
"set (conflictsFor2 Or)=\<Union>{set (generateConflicts (set Or) (the M) c)| c. c\<in>set(conflictsFor2 or)}"
proof-
have "conflictsFor2 Or=(let (M,or)=ReducedRelation3 Or in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor2 or])" by simp
moreover have "...=concat [remdups (generateConflicts (set Or) (the M) c). c <- conflictsFor2 or]"
using assms old.prod.case by (smt map_eq_conv) 
moreover have "...=concat (map (%x. remdups (generateConflicts (set Or) (the M) x)) (conflictsFor2 or))"
by blast ultimately have "set (conflictsFor2 Or) = 
  (set o concat) (map (generateConflicts (set Or) (the M)) (conflictsFor2 or))" by simp moreover have 
"... = Union ((set o (generateConflicts (set Or) (the M)))`(set (conflictsFor2 or)))" by auto
moreover have "{set (generateConflicts (set Or) (the M) c)| c. c\<in>set (conflictsFor2 or)}=
set`((generateConflicts (set Or) (the M))`(set (conflictsFor2 or)))"by blast
moreover have "...=(set o (generateConflicts (set Or) (the M)))`(set (conflictsFor2 or))" 
by fastforce ultimately show ?thesis by presburger
qed

lemma mm23: assumes "finite A" "A\<noteq>{}" shows "Maxs' A \<noteq>{}" using assms by simp
lemma mm22: "Maxs' A \<subseteq> A" by blast

lemma "List.find f l=None = (f`(set l)\<subseteq>{False})" 
by (simp add: image_subset_iff mm05o)

lemma mm05p: assumes  "(M,p)=ReducedRelation3 P" shows 
"(M=None) = ((Domain (set P))-Range (strict (set P))={})"
proof -
let ?minRel="List.filter (%(x,y). x \<notin> (snd`((set P)-{(x,x)}))) P"
let ?minRel3="List.filter (%(x,y). x \<notin> (Range (strict (set P)))) P"
have "\<forall> x. x\<notin>(Range ((set P)-{(x,x)}))= (x\<notin>(Range (strict' (set P))))" by blast then have 
2: "?minRel=?minRel3" unfolding strict_def using Range_snd by (metis(no_types,lifting)) have 
1: "set ?minRel3={} = ((Domain (set P))-Range (strict (set P))={})" by auto
let ?Minimals="remdups (map fst ?minRel)" 
let ?Multiplicities="map (%z. (size (nextForPartialOrder' P [z]),z)) ?Minimals"
let ?suitableVals="Maxs'(set (map fst ?Multiplicities))"
let ?somePivot="optApp' snd (List.find (%(x,y).x\<in>?suitableVals) ?Multiplicities)" 
obtain suitableVals Multiplicities where 
0: "suitableVals=?suitableVals & Multiplicities=?Multiplicities" by blast have 
"(M,p)=(?somePivot, filter (%(x,y). x \<noteq> the ?somePivot & y \<noteq> the ?somePivot) P)" using assms 
unfolding ReducedRelation3_def nextForPartialOrder_def Maxs_def optApp_def by meson then 
have "(M=None) = (List.find (%(x,y).x \<in> ?suitableVals) ?Multiplicities = None)" by simp
moreover have "(List.find (%(x,y).x\<in> suitableVals) Multiplicities = None) =
(suitableVals \<inter> (fst`(set Multiplicities))={})" unfolding mm05o by force
ultimately have "(M=None)=(suitableVals \<inter> (fst`(set Multiplicities))={})" using 0 by meson
moreover have "suitableVals \<subseteq> set (map fst Multiplicities)" using 0  by blast
ultimately moreover have "suitableVals \<subseteq> fst`(set Multiplicities)" by force
then moreover have "suitableVals \<inter> (fst`(set Multiplicities)) = suitableVals" by fast
ultimately have "(M=None)=(suitableVals={})" using 0 by metis
moreover have "finite (set (map fst Multiplicities))" by simp then moreover
have "Maxs'(set (map fst Multiplicities))={} \<longrightarrow> set (map fst Multiplicities)={}" 
using mm23 by blast moreover have "suitableVals=Maxs'(set (map fst Multiplicities))" using 0 by force
ultimately have "(M=None)=(set (map fst Multiplicities)={})" by fastforce then have 
"(M=None)=(set Multiplicities={})" by simp then have
"(M=None)=(set ?Minimals={})" using 0 by fast then have
"(M=None)=(set ?minRel={})" by fastforce then show ?thesis using 1 2 by presburger
qed

(*overlaps with lm07d*)
lemma mm05q: assumes "finite (strict (set P))" "trans (set P)" "antisym (set P)"
"P\<noteq>[]" "(M,p)=ReducedRelation3 P" shows "M\<noteq>None & the M \<in> Domain (set P) - Range (strict (set P))"
proof-
let ?P="set P" let ?PP="strict ?P" let ?m="the M" have "Domain ?P\<noteq>{}" using assms(4) by (simp add: Domain_empty_iff) 
then have "?PP={} \<longrightarrow> Domain ?P - Range ?PP \<noteq>{}" by simp then have 
1: "?PP={} \<longrightarrow> M\<noteq>None" using assms mm05p by blast have "wf ?PP" using assms mm05k by blast 
then have "?PP\<noteq>{} \<longrightarrow> Domain ?PP - Range ?PP \<noteq>{}" using mm05n by auto moreover have 
"Domain ?PP - Range ?PP \<noteq> {} \<longrightarrow> M\<noteq>None" using assms mm05p unfolding strict_def by blast 
ultimately moreover have "M\<noteq>None" using 1 by fast ultimately moreover have "the M\<in>set(map fst P)" 
using assms(5) lm06 option.collapse by (metis) 
ultimately have
2: "M\<noteq>None & the M \<in> Domain (set P)" by auto
{ assume "?m \<in> Range ?PP"
  then obtain x where "(x,?m)\<in>?PP" by blast moreover have "?m \<notin> snd`(?P-{(?m,?m)})" 
  using ReducedRelation3_def 2 mm05r assms(5) prod.sel(1) Conflicts2.lm06 option.collapse
by (smt)
  then moreover have "?m \<notin> Range (?P-{(?m,?m)})" by force
  moreover have "(?P-{(?m,?m)}) \<supseteq> ?PP" unfolding strict_def by blast
  ultimately have False by blast
  }
then show ?thesis using 2 by blast
qed

lemma mm05t: assumes "(M,or)=ReducedRelation3 []" shows "or=[] & M=None" using assms 
filter.simps(1) snd_conv find.simps(1) list.map_disc_iff 
lm06 ReducedRelation3_def remdups.simps(1) find_None_iff not_None_eq 
by (metis(no_types,lifting))

lemma mm24: assumes "fst (ReducedRelation3 Or)=None" shows "conflictsFor2 Or = [{}]"
proof -
obtain M or where "(M,or)=ReducedRelation3' Or" by metis then have 
0: "(M,or)=ReducedRelation3 Or" unfolding ReducedRelation3_def by blast
then have "M=fst (ReducedRelation3 Or)" by (metis fst_conv) then have 
2: "M=None" using  assms by blast 
moreover have "conflictsFor2 Or=(let (M,or)=ReducedRelation3 Or in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor2 or])" by simp then have 
"conflictsFor2 Or=( if M=None then [{}] 
  else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor2 or])" 
by (smt "0" calculation old.prod.case)
thus ?thesis using 2 by presburger
qed

lemma mm05x: "conflictsFor2 []=[{}]" using mm05t mm24 prod.collapse by metis

lemma mm05s: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)"  
"\<forall>c\<in>set(conflictsFor2 or). Range c \<subseteq> Field (set Or)" 
"(M,or)=ReducedRelation3 Or" "Or\<noteq>[]" shows 
"set (conflictsFor2 Or)=\<Union>{generateConflictsSet (set Or) (the M) c| c. c\<in>set(conflictsFor2 or)}"
proof -
let ?m="the M" let ?O="set Or" let ?OO="strict ?O" 
let ?g3="generateConflictsSet ?O ?m" let ?g5="generateConflicts ?O ?m"
let ?r3="{?g3  c| c. c\<in>set(conflictsFor2 or)}" let ?r5="{set (?g5 c)| c. c\<in>set(conflictsFor2 or)}" 
have "finite ?OO" unfolding strict_def by simp then moreover have 
0: "M\<noteq>None & ?m \<in> Domain ?O" using assms mm05q by blast moreover have 
"finite (set Or)" by blast ultimately have 
1: "?r5=?r3" using assms(1,2,3,4) mm07d by (metis (no_types, lifting)) have
2: "M\<noteq>None" using 0 by blast have "set (conflictsFor2 Or)=\<Union> ?r5" using assms(5) 2 by (rule mm07c)
then show ?thesis using 1 by presburger
qed

lemma mm06e: assumes "(M,or)=ReducedRelation3 Or" "trans (set Or)" shows "trans (set or)"
using assms(1,2) trans_def mem_Collect_eq old.prod.case set_filter snd_conv 
lm06 mm05t ReducedRelation3_def 
by smt

lemma mm06f: assumes "(M,or)=ReducedRelation3 Or" "antisym (set Or)" shows "antisym (set or)" 
using assms antisym_def mem_Collect_eq set_filter snd_conv 
lm06 mm05t ReducedRelation3_def by smt

lemma mm06g: assumes "(M,or)=ReducedRelation3 Or" "reflex (set Or)" shows "reflex (set or)" 
using assms reflex_def mem_Collect_eq set_filter snd_conv les.lm16 mm01f
lm06 mm05t ReducedRelation3_def by smt

lemma mm05y: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)"  
"\<forall>c\<in>set(conflictsFor2 or). Range c \<subseteq> Field (set Or)" 
"(M,or)=ReducedRelation3 Or" shows 
"set (conflictsFor2 Or)=\<Union>{generateConflictsSet (set Or) (the M) c| c. c\<in>set(conflictsFor2 or)}"
(is "?L=?R") proof - have 
0: "set (conflictsFor2 [])={{}}" by (metis empty_set list.simps(15) mm05x)
{
  assume "Or=[]" then moreover have "or=[]" using assms(5) mm05t by (metis ReducedRelation3_def filter.simps(1) snd_conv) 
then moreover have 
  "set (conflictsFor2 or)={{}}" using 0 by blast moreover have 
  "generateConflictsSet {} (the M) {}={{}}" unfolding generateConflictsSet_def by auto moreover have 
  "\<Union>{{{}}} = {{}}" by simp ultimately have "?R={{}}" by auto
} then have "Or=[] \<longrightarrow> ?R={{}}" by blast then have 
1: "Or=[] \<longrightarrow> ?thesis" using 0 by force
{assume 6: "Or\<noteq>[]" have ?thesis using assms(1,2,3,4,5) 6 by (rule mm05s)} 
then have "Or\<noteq>[] \<longrightarrow> ?thesis" by blast thus ?thesis using 1 by force
qed

lemma lm07: assumes "(ReducedRelation3 P) = (Some m,p)" shows 
"set p = (set P)-({m} \<times> UNIV)-(UNIV \<times> {m})"
proof -
have "p = filter (%(x,y). x \<noteq> m & y \<noteq> m) P" using assms lm06 by force thus ?thesis by auto
qed

lemma mm08b: assumes "size Or=N+1" "p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor2 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" "\<forall> Or. size Or \<le> N \<longrightarrow> P Or" "p (set Or)" shows "q Or"
proof -
let ?p="\<lambda> or. trans or & reflex or & antisym or" let ?c="conflictsFor" let ?c2="conflictsFor2"
let ?q="\<lambda> Or. (set (?c2 Or) = ?c (set Or))" let ?P="\<lambda> Or. (?p (set Or) \<longrightarrow> ?q Or)"
let ?r="ReducedRelation3 Or" let ?or="snd ?r" let ?M="fst ?r" let ?m="the ?M" let ?O="set Or"
let ?OO="strict ?O" have "Or\<noteq>[]" using assms by force moreover have 
0: "(?M,?or)=?r" by simp moreover have 
3: "finite ?OO" unfolding strict_def by simp ultimately have 
1: "?M\<noteq>None & ?m \<in> Domain ?O & ?M=Some ?m" using assms mm05q by (smt DiffD1 option.exhaust_sel) then have
25: "?m \<in> Domain ?O" by blast have "?p ?O" using assms by fast then have "?p (set ?or)" using 
mm06e mm06f mm06g 0 by blast moreover have "size ?or < N+1" using assms 0 1 lm06 by force ultimately have 
2: "?q ?or" using assms by auto moreover have "\<forall>c \<in> ?c (set ?or). Range c \<subseteq> Field (set ?or)" 
unfolding conflictsFor_def using FieldI2 by fastforce ultimately have 
"\<forall>c \<in> set (?c2 ?or). Range c \<subseteq> Field (set ?or)" by presburger moreover have 
"Field (set ?or) \<subseteq> Field ?O" using Domain_fst Range_snd Un_mono filter_is_subset image_mono snd_eqD 
unfolding Field_def ReducedRelation3_def by (metis (no_types, lifting)) ultimately have 
4: "\<forall>c \<in> set (?c2 ?or). Range c \<subseteq> Field ?O" by fast have 
11: "trans ?O" using assms by meson have
12: "reflex ?O" using assms by blast have
13: "antisym ?O" using assms by blast have 
"set (?c2 Or)=\<Union>{generateConflictsSet ?O ?m c| c. c\<in>set(?c2 ?or)}" using 11 12 13 4 0 by (rule mm05y)
moreover have "...=\<Union>{generateConflictsSet ?O ?m c| c. c\<in>?c (set ?or)}" using 2 by presburger
moreover have "set ?or=?O-{?m}\<times>UNIV-UNIV\<times>{?m}" using 0 lm07 ReducedRelation3_def Sigma_cong 1
by smt
ultimately have
5: "set (?c2 Or) = \<Union>{generateConflictsSet ?O ?m c| c. c\<in>?c (?O-{?m}\<times>UNIV-UNIV\<times>{?m})}" by presburger
have "?m\<notin>Range(?OO)" using assms mm05q 3 using 0 `Or \<noteq> []` by blast then have 
21: "?m \<notin> Range (?O-{(?m,?m)})" unfolding strict_def by blast
have "\<Union> {generateConflictsSet ?O ?m c | c. c\<in>?c(?O-({?m}\<times>UNIV)-(UNIV\<times>{?m}))}=?c ?O"
using 21 11 12 13 25 3 by (rule mm06h) then show ?thesis using 5 assms(3) by presburger
qed

lemma mm08bb: assumes "p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor2 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" "Q=(\<lambda>n. \<forall> Or. size Or=n \<longrightarrow> P Or)"
shows "\<forall> N. ((\<forall> n\<le>N. Q n) \<longrightarrow> Q (Suc N))" using assms mm08b 
proof -
{
  fix N assume "\<forall> n\<le>N. Q n" then have 
  "\<forall> n\<le>N. (\<forall>Or. size Or = n \<longrightarrow> P Or)" unfolding assms(4) 
  using assms by blast then have  "\<forall> Or. size Or \<le> N \<longrightarrow> P Or" by blast
  then have "\<forall> Or. ((size Or=N+1 & p (set Or)) \<longrightarrow> q Or)" using assms mm08b by blast
  then have "Q (Suc N)" using assms by simp
  }
thus ?thesis by blast
qed

lemma mm08c: assumes 
"p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor2 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" 
"Q=(\<lambda>n. \<forall> Or. size Or=n \<longrightarrow> P Or)"
shows "\<forall> N. Q N"
proof -
have "Q 0" using assms mm05w mm05x lm02 injections_alg.simps(1) length_0_conv set_empty 
by (metis (no_types)) moreover have "\<forall> N. ((\<forall> n\<le>N. Q n) \<longrightarrow> Q (Suc N))" using assms by (rule mm08bb) 
ultimately have "\<forall> N. Q N" using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust) thus ?thesis using assms by blast
qed

theorem conflictCorrectness: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)" shows 
"set (conflictsFor2 Or)=conflictsFor (set Or)" 
proof -
let ?p="(\<lambda> (or::('a \<times> 'a) set). trans or & reflex or & antisym or)" 
let ?q="(\<lambda> (Or::('a \<times> 'a)list). set (conflictsFor2 Or) = conflictsFor (set Or))"
let ?P="(\<lambda> Or. ?p (set Or) \<longrightarrow> ?q Or)" let ?Q="(\<lambda>(n::nat). (\<forall> Or. (size Or=n \<longrightarrow> ?P Or)))"
obtain p where 1: "p=?p" by simp obtain q where 2: "q=?q" by presburger  obtain P where
3: "P=?P" by blast obtain Q where 4: "Q=?Q" by blast have "\<forall>N. Q N" using 1 2 3 4 mm08c by fast
thus ?thesis using assms 1 2 3 4 by blast
qed


























lemma mm10u: assumes "N>(0::nat)" shows 
"set (map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N)))
partitions (esOver2 {0..<N})"
proof -
have "{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}=
(%or. {or}\<times>(conflictsFor or))`
{or. trans or & reflex or & antisym or & or \<in> posOver {0..<N}}"
unfolding posOver_def isPo_def by blast moreover have "...=(%or. {or}\<times>(conflictsFor or))`
{or. trans or & reflex or & antisym or & or \<in> set (map (set o adj2PairList) (allReflPartialOrders N))}" 
using assms mm10k by blast moreover have "...=
(%or. {or}\<times>(conflictsFor or))`{set Or|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by fastforce moreover have "...=
{{set Or}\<times>(conflictsFor (set Or))|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by auto moreover have "...=
{{set Or}\<times>(set (conflictsFor2 Or))|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using conflictCorrectness by blast
moreover have "...=(%Or. {set Or}\<times>(set (conflictsFor2 Or)))`
{Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by blast
moreover have "{Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))} = 
{Or. trans (set Or) & reflex (set Or) & antisym (set Or) & set Or\<in>posOver {0..<N} &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using assms mm10k
Collect_cong image_comp image_eqI list.set_map by (metis (no_types, lifting) ) moreover have "...=
{Or. set Or\<in>posOver {0..<N} & Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" 
unfolding posOver_def isPo_def mm13 by fast moreover have "...=
{Or. Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using assms mm10k 
by (metis (no_types, lifting) Collect_cong image_comp image_eqI list.set_map)
ultimately have "{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}=
(%Or. {set Or}\<times>(set (conflictsFor2 Or)))`{Or. Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}"
by presburger moreover have "...= 
(%Or. {set Or}\<times>(set (conflictsFor2 Or)))`(set (map (adj2PairList) (allReflPartialOrders N)))" by blast 
moreover have "...=set (map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N)))" 
unfolding mm16 by presburger ultimately have  
0: "set (map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N)))=
{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}" by presburger moreover have 
"... partitions (esOver2 {0..<N})" by (rule mm10g) ultimately show ?thesis by presburger
qed

lemma mm10h2: assumes "N>0" "finite (esOver2 {0..<N})" shows "setsum f (esOver2 {0..<N}) = setsum (setsum f)
(set (map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N))))" 
using assms setsum_associativity mm10u by blast

lemma mm16g: assumes "(M,or)=ReducedRelation3 Or" "M\<noteq>None" "conflictsFor2 or\<noteq>[]"
shows "conflictsFor2 Or\<noteq>[]" 
proof- let ?c="conflictsFor2" have "set (?c or)\<noteq>{}" using assms(3) by fastforce then obtain c where 
0: "c\<in>set (?c or)" using all_not_in_conv by blast then have "set (generateConflicts (set Or) (the M) c) \<subseteq>
\<Union>{set (generateConflicts (set Or) (the M) c)| c. c\<in>set(conflictsFor2 or)}" by blast
moreover have "...=set (conflictsFor2 Or)" using assms mm07c by blast ultimately show ?thesis 
using mm16e bot.extremum_unique remdups_eq_nil_iff set_empty by (metis (no_types, lifting))
qed

lemma mm16h: assumes "P=(%n. \<forall>(or::(('a::linorder \<times> 'a) list)). size or=n \<longrightarrow> conflictsFor2 or\<noteq>[])" 
"\<forall>n\<le>N. P n" shows "P (Suc N)"
proof - let ?r="ReducedRelation3"
{
  fix Or::"(('a \<times> 'a) list)" let ?M="fst (?r Or)" let ?or="snd (?r Or)"
  let ?m="the ?M" have "?M=None \<longrightarrow> conflictsFor2 Or=[{}]" using ReducedRelation3_def conflictsFor2.simps fst_conv prod.case by (smt )
  assume "size Or=(Suc N)" moreover assume "?M\<noteq>None" then moreover have "?M=Some ?m" by force 
  ultimately moreover have "size ?or \<le>N" using lm06 by (metis Suc_leI leD leI prod.collapse)
  then moreover have "conflictsFor2 ?or\<noteq>[]" using assms by blast ultimately have 
  "conflictsFor2 Or\<noteq>[]" using mm16g by (metis option.distinct(1) prod.collapse)
} then have "\<forall> Or::(('a \<times> 'a) list). 
((size Or=Suc N & fst (?r Or)\<noteq>None) \<longrightarrow> conflictsFor2 Or \<noteq>[])" by blast moreover have 
"\<forall> Or::(('a \<times> 'a) list). fst (?r Or)=None \<longrightarrow> conflictsFor2 Or=[{}]" using 
 ReducedRelation3_def conflictsFor2.simps fst_conv prod.case by (smt)
ultimately show ?thesis using assms by fastforce
qed

lemma mm16i: assumes "P=(%n. \<forall>(or::(('a::linorder \<times> 'a) list)). size or=n \<longrightarrow> conflictsFor2 or\<noteq>[])"
shows "\<forall>N. P N"
proof -
have "P 0" unfolding assms using length_0_conv list.distinct(2) mm05x by (smt)
moreover have "\<forall> N. (\<forall>n\<le>N. P n)\<longrightarrow>P (Suc N)" using assms mm16h by auto
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

lemma mm16j: "conflictsFor2 Or\<noteq>[]" using mm16i by fast

lemma mm10i2: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = setsum card 
(set (map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N))))"
using assms mm10h2 card_eq_setsum by (smt setsum.cong)

lemma mm10i3: assumes "N>0" shows "distinct 
(map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N)))"
proof - let ?f="%x. {set x}" let ?g="%x. set (conflictsFor2 x)" let ?l="allReflPartialOrders N" 
let ?h="%x. (?f x)\<times>(?g x)" have 
0: "set ?l \<subseteq>{a| a. size a=N & isRectangular4 N a}" using assms mm10e by auto then have 
"inj_on adj2PairList (set(allReflPartialOrders N))" using assms mm14 using subset_inj_on by blast then have 
1: "distinct (map (adj2PairList) (allReflPartialOrders N))" using assms mm15b distinct_map by blast
have "inj_on adj2Pairs1 (set ?l)" using assms 0 lm04bb lm93b subset_inj_on by (metis(no_types))
then have "inj_on set ((adj2PairList)`(set ?l))" unfolding mm10m 
by (simp add: inj_on_imageI) then have "inj_on set (set (map (adj2PairList) ?l))" by fastforce
then have "inj_on ?f (set (map (adj2PairList) ?l))" using inj_onI inj_on_contraD singleton_inject 
by (metis (mono_tags, lifting)) 
then have "inj_on ?f (adj2PairList`(set ?l))" by fastforce
moreover have "{}\<notin>?g`(adj2PairList` (set ?l))" using mm16j by blast
ultimately have "inj_on ?h (adj2PairList` (set ?l))" using mm16b by blast
then have "inj_on ?h (set (map (adj2PairList) ?l))" by fastforce then show ?thesis using 1
distinct_map by blast
qed

lemma mm17b: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = listsum 
(map (card o set o conflictsFor2) (map (adj2PairList) (allReflPartialOrders N)))"
(is "?L=?R")
proof -
let ?l="map (%Or.{set Or}\<times>(set (conflictsFor2 Or))) (map (adj2PairList) (allReflPartialOrders N))"
let ?M="listsum 
(map (card o (%Or.{set Or}\<times>(set (conflictsFor2 Or)))) (map (adj2PairList) (allReflPartialOrders N)))"
have "?L=setsum card (set ?l)" using assms mm10i2 by blast moreover have
"distinct ?l" using assms mm10i3 by blast then moreover have 
"listsum (map card ?l)=setsum card (set ?l)" by (rule listsum_distinct_conv_setsum_set) ultimately have 
1: "?L=?M" by (metis (no_types, lifting) List.map.compositionality)
 have "card o (%Or. {set Or}\<times>(set (conflictsFor2 Or))) = 
card o (%Or. ((%x. {set x}) Or)\<times>((set o conflictsFor2) Or))" by auto
moreover have "... = card o (set o conflictsFor2)" by (rule mm11) ultimately have 
"card o (%Or. {set Or}\<times>(set (conflictsFor2 Or))) = card o (set o conflictsFor2)"
by (metis(no_types)) moreover have "...= card o set o conflictsFor2" by (simp add: fun.map_comp) ultimately have
0: "card o (%Or. {set Or}\<times>(set (conflictsFor2 Or))) = card o set o conflictsFor2" by (metis(no_types)) 
have "?M=?R" unfolding 0 by blast then show ?thesis using 1 by presburger
qed

theorem cardCorrectness: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = 
listsum (map (size o remdups o conflictsFor2 o adj2PairList) (allReflPartialOrders N))" (is "?L=?R")
proof -
let ?M="map (card o set o conflictsFor2) (map (adj2PairList) (allReflPartialOrders N))" have 
0: "card o set o conflictsFor2 = size o remdups o conflictsFor2" using mm17a fun.map_comp
by (metis(no_types)) have 
"?M=map (size o remdups o conflictsFor2) (map (adj2PairList) (allReflPartialOrders N))"
unfolding 0 by blast moreover have "?L=listsum ?M" using assms by (rule mm17b) ultimately have 
"?L=listsum (map (size o remdups o conflictsFor2) (map (adj2PairList) (allReflPartialOrders N)))"
by presburger moreover have "...=
listsum (map (size o remdups o conflictsFor2 o adj2PairList) (allReflPartialOrders N))" by auto
ultimately show ?thesis by presburger
qed

































section{*Distinctness of conflictsFor2 mm07bb ll41i quasiDistinctMap ll41c*}

lemma lm06b: assumes "(Some m,or)=ReducedRelation3 Or" shows "set or \<subseteq> set Or"
using assms(1) ReducedRelation3_def Pair_inject filter_is_subset by (metis(no_types,lifting))

lemma mm12a: "Union (Field`set(conflictsFor2 []))\<subseteq>Field (set [])" using assms
unfolding mm05x by simp

lemma mm12b: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor2 Or))\<subseteq>Field (set Or))))"
"\<forall>n\<le>N. P n" shows "P (Suc N)"
proof -
{
  fix Or::"('a \<times> 'a)list" 
  let ?r="ReducedRelation3 Or" let ?M="fst ?r" let ?or="snd ?r" let ?m="the ?M"
  assume 
  1: "size Or=(Suc N)" have "?M=None \<longrightarrow> conflictsFor2 Or=[{}]" using ReducedRelation3_def 
    conflictsFor2.simps prod.case prod.sel(1) by (smt)
  then have "?M=None \<longrightarrow>Union (Field`set(conflictsFor2 (Or)))={}" by simp
  then have 
  3: "?M=None \<longrightarrow>Union (Field`set(conflictsFor2 (Or)))\<subseteq>Field(set Or)" by blast
  {
    assume 
    4: "?M\<noteq>None" then have 
    2: "(Some ?m,?or)=?r " using 4 by simp then have "(?M,?or)=?r" by auto then have 
    5: "set(conflictsFor2 Or)= \<Union>{set (generateConflicts (set Or) (the ?M) c)| c. c\<in>set(conflictsFor2 ?or)}"
    using 4 by(rule mm07c) fix C assume "C\<in>set(conflictsFor2 Or)" then 
    moreover obtain c where 
    0: "C\<in>set(generateConflicts (set Or) ?m c) & c\<in>set (conflictsFor2 ?or)"
    using 5 by blast
    moreover have "(Some ?m, ?or) = ReducedRelation3 Or" using 4 by simp
    then moreover have "?m\<in>Domain(set Or)" using lm06 by force
    ultimately moreover have "length ?or<(Suc N)" using lm06 1 2 by metis 
    then moreover have "P (length ?or)" using assms by (simp add: less_Suc_eq_le) then moreover have 
    "Union (Field`set(conflictsFor2 ?or))\<subseteq>Field (set ?or)" using assms by fast
    then moreover have "Field c\<subseteq>Field (set ?or)" using 0 by blast moreover have 
    "C \<subseteq> c \<union> {?m}\<times>(Range (set Or)) \<union>(Range (set Or))\<times>{?m}" using 0 mm12 by metis
    ultimately moreover  have 
    3: "Field C \<subseteq> Field c \<union> Field(set Or) \<union> Field (set Or)"
    unfolding Field_def by blast
    ultimately  have "Field c \<subseteq> Field (set ?or) \<union> Field (set Or)" by blast
    moreover have "(set ?or) \<subseteq> (set Or)" using 2 by (rule lm06b)
    then moreover have "Field(set ?or)\<subseteq>Field (set Or)" unfolding Field_def by blast
    ultimately have "Field C \<subseteq> Field (set Or)" using 3 by blast
    }
  then have "?M\<noteq>None \<longrightarrow> (\<forall>C\<in>set(conflictsFor2 Or). Field C \<subseteq> Field (set Or) )" by fast
  then have "?M\<noteq>None \<longrightarrow> (Union (Field`set(conflictsFor2 Or)) \<subseteq> Field (set Or) )" by fast
  then have "(Union (Field`set(conflictsFor2 Or)) \<subseteq> Field (set Or) )" using 3 by meson
  }
then show ?thesis using assms by blast
qed

lemma mm12bb: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor2 Or))\<subseteq>Field (set Or))))"
shows "\<And>N.(\<forall>n\<le>N. P n) \<longrightarrow>P (Suc N)"
proof - {fix N assume 2: "(\<forall>n\<le>N. P n)" have "P (Suc N)" using assms(1) 2 by (rule mm12b)} 
then show "\<And>N.(\<forall>n\<le>N. P n) \<longrightarrow>P (Suc N)" by meson 
qed

lemma mm12c: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor2 Or))\<subseteq>Field (set Or))))" shows "\<forall>n. P n"
proof -
have "P 0" using assms mm12a by blast
moreover have "\<And> N. ((\<forall>n\<le>N. P n)\<longrightarrow> P (Suc N))" using assms(1) by (rule mm12bb) 
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

lemma mm12d: "Union (Field`set(conflictsFor2 Or))\<subseteq>Field (set Or)" using mm12c by fast

lemma mm18a: assumes "L=[remdups (generateConflicts Or m c). c <- l]" "distinct l"
"\<forall> c\<in>set l. m\<notin>Field c" shows 
"\<forall>i j. i<size L & j<size L & i\<noteq>j  \<longrightarrow> (set (L!i) \<inter> set (L!j)={})"
proof -
let ?g="generateConflicts Or m" let ?n="size l" let ?L="map (remdups o ?g) l" have 
0: "size L=size l" using assms by force have "L=?L" using assms by force
{
  fix i1 i2 let ?c1="l!i1" let ?c2="l!i2" assume
  1: "i1<size L" then moreover have "L!i1=remdups (?g (l!i1))" using assms by fastforce
  moreover assume 
  2: "i2<size L" then moreover have "L!i2=remdups (?g (l!i2))" using assms by fastforce
  moreover assume "i1\<noteq>i2" 
  ultimately moreover have "?c1\<noteq>?c2" using assms 1 2 by (simp add: nth_eq_iff_index_eq)
  ultimately moreover have "m \<notin> Field ?c1 \<union> Field ?c2" using assms "1" "2" by auto 
  ultimately have "set (L!i1) \<inter> set (L!i2)={}"
  using assms 1 2 mm18 by (smt disjoint_iff_not_equal inf.right_idem set_remdups)
}
thus ?thesis by presburger
qed

(*
lemma lm07e: assumes "ReducedRelation3 Or = (M,or)" "m=the M" shows 
"set or = (set Or)-({m} \<times> UNIV)-(UNIV \<times> {m})" using assms  ReducedRelation3_def
sorry
*)

lemma mm19b: assumes "P=(%n. \<forall>(or::('a::linorder\<times>'a) list). size or=n \<longrightarrow> distinct(conflictsFor2 or))" 
"\<forall>n\<le>N. P n" shows "P (Suc N)"
proof -
let ?f=conflictsFor2 have 
4: "\<forall> (Or::('a \<times> 'a) list). 
(fst (ReducedRelation3 Or)=None \<longrightarrow> distinct (?f Or))" using ReducedRelation3_def 
  assms conflictsFor2.simps fst_conv le0 list.size(3) mm05x prod.case  by (smt distinct_singleton)
{
  fix Or::"('a \<times> 'a) list" let ?r="ReducedRelation3 Or" let ?M="fst ?r" let ?or="snd ?r" let ?m="the ?M"
  let ?L="[remdups (generateConflicts (set Or) ?m c). c <- ?f ?or]" obtain L where
  1: "L=?L" by auto assume "size Or=Suc N" moreover assume 
  5: "?M\<noteq>None" ultimately moreover have 
  0: "?f Or=concat [remdups (generateConflicts (set Or) ?m c). c <- ?f ?or]" using
  ReducedRelation3_def conflictsFor2.simps map_eq_conv prod.case prod.sel(1) prod.sel(2) by (smt)
  ultimately have "size ?or\<le>N" using lm06 leD not_less_eq_eq option.collapse 
  prod.collapse by (metis(no_types,lifting))
  then have "P(size ?or)" using assms by blast then have 
  2: "distinct (?f ?or)" using assms by fast
  have "ReducedRelation3 Or=(?M, ?or)" by simp moreover have "?m=the ?M" by force
  ultimately have "ReducedRelation3 Or=(Some ?m, ?or)" using 5 by simp
  then have "set ?or = (set Or)-({?m} \<times> UNIV)-(UNIV \<times> {?m})" using lm07 by metis   
  then have "?m \<notin> Field (set ?or)" using mm17c by force moreover have 
  "\<forall>c\<in>set (?f ?or). Field c\<subseteq> Field (set ?or)" using mm12d by blast ultimately have 
  3: "\<forall>c\<in>set (?f ?or). ?m\<notin>Field c" by blast have 
  "\<forall>i j. i<size L & j<size L & i\<noteq>j  \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" using 1 2 3 by (rule mm18a)
  then have 
  "\<forall>i j. i<size L & j<size L & i\<noteq>j & L!i\<noteq>[] & L!j\<noteq>[] \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" by presburger
  moreover have "\<forall>l\<in>set L. distinct l" using 1 by fastforce
  ultimately have "distinct (concat L)" using ll41c by blast then have 
  "distinct (?f Or)" using 0 1 by fastforce
}
then have "\<forall> (Or::('a \<times> 'a) list). 
((size Or=Suc N & fst (ReducedRelation3 Or)\<noteq>None) \<longrightarrow> distinct (?f Or))" 
by blast then show ?thesis using assms 4 by meson
qed

lemma mm19a: assumes "P=(%n. \<forall>(or::('a::linorder\<times>'a) list). size or=n \<longrightarrow> distinct(conflictsFor2 or))"
shows "\<forall>N. P N"
proof -
have "P 0" using assms distinct_singleton mm05x by (smt length_0_conv)
moreover have "\<forall>N. ((\<forall>n\<le>N. P n)\<longrightarrow>P (Suc N))" using assms
mm19b by auto
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

theorem distinctness: "remdups o conflictsFor2 = conflictsFor2" using mm19a by fastforce

(*
theorem cardCorrectness2: assumes "N>0" shows "card (esOver {0..<N}) = 
listsum (map (size o conflictsFor2 o adj2PairList) (allReflPartialOrders N))"
proof -
have "finite (esOver2 {0..<N})" unfolding esOver2_def using mm21 by blast then 
have "card (esOver2 {0..<N}) = 
listsum (map (size o remdups o conflictsFor2 o adj2PairList) (allReflPartialOrders N))"
using assms cardCorrectness 
sorry
moreover have "...= listsum (map (size o (remdups o conflictsFor2) o adj2PairList) (allReflPartialOrders N))"
by (simp add: o_assoc) 
moreover have "...= listsum (map (size o conflictsFor2 o adj2PairList) (allReflPartialOrders N))"
unfolding distinctness by presburger
ultimately show ?thesis unfolding mm21b by presburger
qed
*)


















































section {* Evaluation *}

(*value "(size o concat) (map (conflictsFor2) (map adj2PairList (allReflPartialOrders 7)))"*)
(*value "map (%n. listsum (map (size o conflictsFor2 o adj2PairList) (allReflPartialOrders n))) [1..<7]"
"[1, 4, 41, 916, 41099, 3528258]"*)

(*value "listsum (map (card o set o conflictsFor2 o adj2PairList) (allReflPartialOrders 5))"*)
(*value "listsum (map (size o remdups o conflictsFor4 o adj2PairList) (allReflPartialOrders 2))"*)
(* value "size (concat (map (((%Or. (map (%x. (set Or, x)) (conflictsFor2 Or)))) o adj2PairList) (allReflPartialOrders 3)))"
value "enumerateEs 2"
*)
(*
abbreviation "mbc03=={(0::nat,0::nat),(0,1),(1,1)}"
abbreviation "mbc04=={(0::nat,0::nat),(0,1),(1,1),(1,2),(2,2),(0,2),(3,3)}"
abbreviation "mbc05=={(2,3),(3,2)}"
abbreviation "ExLinear == [(0::nat,1::nat),(0,0),(1,1),(2,2),(1,2),(0,2)]"
abbreviation "Ex01 == [(1::nat,1::nat),(1,2),(2,2),(3,3)]"
abbreviation "Ex02 == [(0::nat,1::nat),(0,0),(1,1),(2,2),(1,2),(0,2), (10,11),(10,10),(11,11),(12,12),(11,12),(10,12)]"
abbreviation "allConflictsCompatibleWith' causality == remdups ((conflictsFor2 causality))"
definition "allConflictsCompatibleWith = allConflictsCompatibleWith'"
*)

(*
abbreviation "cond1Comp' matr col == 
allSubLists2 (if (filterpositions2 id col = []) then [True. i<-[0..<size (matr!0)]] else
rowAnd3 (map (nth matr) (filterpositions2 id col)))"

abbreviation "cond2Comp' matr roww == 
((set o concat) (map (filterpositions2 id) (sublist matr (set (filterpositions2 id roww))))) 
\<subseteq> (set (filterpositions2 id roww))"

abbreviation "cond3Comp' matr col == 
(set o concat) (map (filterpositions2 id) (sublist (List.transpose matr) (set (filterpositions2 id col))))
\<subseteq> (set (filterpositions2 id col))"

abbreviation "auxFun' matr col == map (appendAsColumn (col@[True])) 
(map (\<lambda> l. matr@[l]) (filter (cond2Comp' matr) (cond1Comp' matr col)))"

fun enumeratePreorders where 
"enumeratePreorders 0 = [[[]]]" | 
"enumeratePreorders (Suc n) = concat 
(map (split auxFun') [ (matr, col). matr <- enumeratePreorders n, col <- 
filter (cond3Comp' matr) (allSubLists [True. n<-[0..<size (matr!0)]])])"
*)

(*value "(card o Union o set) (map (%Or. set (map (%x. (set Or, x)) (conflictsFor3 Or))) (enumeratePo 5))"*)
(*value "(card o Union o set) (map (%Or. set (map (%x. (set Or, x)) (conflictsFor3 Or))) (enumeratePo 5))"
StA: 3.52
3.43
3.40
3.42
*)

(*value "(%n. listsum (map (size o remdups o conflictsFor3) (enumeratePo n))) 5"*)
(*StA: 23
10
10
11
*)
(*
value "listsum (map (size o conflictsFor2) (enumeratePo 5))"
(* StA: 2 seconds *)
*)
(*
value "listsum (map (size o conflictsFor2 o adj2PairList) (allReflPartialOrders 3))"
*)
(*StAndrews: conflictsFor2 6=1:22
1:19
1:13

1:26
1:20
1:19
1:17
conflictsFor2 6 = 1:33
1:33
1:34
1:34
*)
(*conflictsFor2 6=(2:40')
2:38'
2:47
2:55
2:35
conflictsFor2 6=3:10'
3:27
3:22
*)

value "size (allStrictPartialOrders2 1)"

lemma "generateConflictsSet' Or m c = {c\<union>({m}\<times>Y)\<union>(Y\<times>{m})| Y. Y\<in>
image (Image(Or-{m}\<times>UNIV-UNIV\<times>{m}))
(Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> (order2strictCover Or)``{m}})))
}" by blast

end
