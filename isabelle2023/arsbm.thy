theory arsbm
imports Sefm Conflicts2

begin
lemma lm00: assumes "(l,n)\<in>(trancl G)" "(l,n) \<notin> G" shows "\<exists> m. (l,m)\<in>G & (m,n)\<in>trancl G"
using assms(1,2) by (meson converse_tranclE)
lemma lm01: "transred P \<subseteq> P" unfolding transred_def using Conflicts.lm30 DiffD1 mm03a 
Image_singleton_iff SigmaE mem_Collect_eq mem_simps(9) singletonD subsetCE subsetI by (smt)
lemma lm02: "trancl (transred P) \<subseteq> trancl P" using lm01 subsetI trancl_mono by (metis)
lemma lm03a: assumes "reflex' G" shows "trancl G \<subseteq> {(l,n)| l n. l \<in> Field G & n \<in> Field G &
(l=n \<or> (l,n) \<in> G \<or> (\<exists> m. ((l,m)\<in>G & (m,n) \<in> trancl G)))}" 
using assms lm00 Domain.DomainI Field_def Range.intros les2.lm31 mem_Collect_eq subrelI 
subsetCE sup_ge2 trancl_range by (smt)

lemma lm03b: assumes "reflex' G" shows "trancl G \<supseteq> {(l,n)| l n. l \<in> Field G & n \<in> Field G &
(l=n \<or> (l,n) \<in> G \<or> (\<exists> m. ((l,m)\<in>G & (m,n) \<in> trancl G)))}" 
using assms refl_on_def by fastforce

lemma lm03: assumes "reflex G" shows 
"trancl G = {(l,n)| l n. l \<in> Field G & n \<in> Field G & (l=n \<or> (l,n) \<in> G \<or> (\<exists> m. ((l,m)\<in>G & (m,n) \<in> trancl G)))}" 
using assms unfolding reflex_def using lm03a lm03b by force 

abbreviation "isMaximalConfSmt2' Ca Cf C == 
(\<forall>z \<in> events Ca - C. z \<in> Cf``C \<or> ((transred Ca)^-1`` {z})-C \<noteq> {})"

abbreviation "isMaximalConfSmt3' Ca Cf C == 
(\<forall>z \<in> events Ca - C. z \<in> Cf``C \<or> (Ca^-1`` {z})-C \<noteq> {})"

definition "immediatePredecessors=immediatePredecessors'"
lemma lm06: "(transred P)^-1 = transred (P^-1)" unfolding transred_def next1_def by blast

lemma lm05a: "next1 (P^-1) {y} = Union {{x}\<times>(next1 (P^-1) {x})|x. x\<in>Domain (P^-1)}``{y}"
unfolding next1_def by blast

lemma lm05: "immediatePredecessors P {y} = ((transred P)^-1)``{y}"
unfolding immediatePredecessors_def transred_def next1_def by blast

lemma lm05b: assumes "x\<in>(transred Ca)^-1``{y}" shows "x\<in>immediatePredecessors Ca {y}"
unfolding immediatePredecessors_def using assms unfolding transred_def using Sefm.lm06 by fastforce

lemma lm04: assumes "events cau=UNIV" "isMaximalConfSmt3' cau cf C" shows 
"IsMaximalConfSmt' (set2pred cau) (set2pred cf) (unset C)" using assms(1,2) unfolding Field_def by auto

value "((inter {1::nat,2,3})`{{0::nat,1},{2,3}} \<inter>{{0::nat,1},{2,3}})"

abbreviation "unordered' relation == (\<lambda> (x,y). {x,y}) ` relation"
definition "unordered = unordered'"
value "unordered' {(0::nat,1::nat), (2,3)}"

term ImageList'
definition "ImageList=ImageList'"
term rangeList
term "next1"
term "Predecessors"
abbreviation "propagatedConflict' po cfl == (remdups o concat) [List.product (x#(Successors po x)) (y#(Successors po y)). (x,y)<-cfl]"
(* NB: po is assumed to be a partial order, rather than a covering relation; cfl is assumed to be symmetric *)
definition "propagatedConflict=propagatedConflict'"

value "let po=[(1::nat,2::nat),(2,4),(2,3),(3,5),(3,10),(5,8),(5,9),(8,9),(9,12),(9,10),(10,11),(10,14),(11,14),(11,13),
(14,15),(14,16),(4,14),(4,6),(4,7)] in let cf= symCl [(3,4),(5,10),(8,9),(11,14)] in
propagatedConflict' (trancL po) cf" 
abbreviation "corestriction' R Y == ((Domain R) \<times> Y) \<inter> R" definition "corestriction=corestriction'"
abbreviation "firstMinimalClashingNode' po cfl == let Y=[x. (x,y) <- cfl, x=y] in
let R=filter (% (x,y). x\<in>set Y & y\<in>set Y & x\<noteq>y) po in List.find (%y. y\<notin>set (RangeList R)) Y"
(* cfl is assumed to have been already propagated, and po to be a partial order *)
definition "firstMinimalClashingNode=firstMinimalClashingNode'"
abbreviation "edgeToRemove' immediateOrder cfl == let b=the (firstMinimalClashingNode (trancL immediateOrder) cfl) in
the (List.find (%(x,y). y=b & (x,y)\<in>set cfl) immediateOrder)"
(* cfl is assumed to be propagated already *)
definition "edgeToRemove=edgeToRemove'"

(* abbreviation "clashingNodes' immediateOrder conflict == [y. y<- rangeList immediateOrder, 
let po=set (immediateOrder) in 
(card ((po^-1)``{y}) > 1) & 
((inter ((trancl (po^-1)) `` {y}))`(unordered conflict)) \<inter> (unordered conflict) \<noteq> {}]"*)

(*
abbreviation "clashingNodes' immediateOrder conflict == {y\<in>Range(immediateOrder). (card ((immediateOrder^-1)``{y}) > 1) & 
((inter ((trancl (immediateOrder^-1)) `` {y}))`(unordered conflict)) \<inter> (unordered conflict) \<noteq> {}}"
definition "clashingNodes=clashingNodes'"
*)

(*
function Unfolding where
"Unfolding (immediateOrder, reducedConflict) = 
(let po=trancL immediateOrder in let cfl=propagatedConflict po reducedConflict in 
if firstMinimalClashingNode po cfl=None then (immediateOrder, reducedConflict) else
let z=edgeToRemove immediateOrder cfl in let a=fst z in let b=snd z in let succs=ImageList immediateOrder [b] in
let copy=(fst b, (Suc o Max o set o (map snd)) (filter (%(x,y). x=fst b) (rangeList immediateOrder))) in
Unfolding ((a, copy)#(List.removeAll z immediateOrder)@(List.product [copy] succs), 
reducedConflict @ [(b, copy), (copy, b)]))" 
apply force by fast
termination Unfolding sorry

(*
function Unfolding where
"Unfolding (immediateOrder, reducedConflict) = 
(let C=clashingNodes immediateOrder reducedConflict in 
if C=[] then (immediateOrder, reducedConflict) else let c=hd C in 
let succs=ImageList immediateOrder [c] in
let predPair=the (List.find (% (x,y). y=c) immediateOrder) in 
let copy=(fst c, (Suc o Max o set o (map snd)) (filter (%(x,y). x=fst c) (rangeList immediateOrder))) in
Unfolding ((fst predPair, copy)#(List.removeAll predPair immediateOrder)@(List.product [copy] succs), 
reducedConflict \<union> {(c, copy), (copy, c)}))" 
apply force by fast
termination Unfolding sorry
value "List.product [1::nat] []"
*)
(* abbreviation "enumerator' relationList == (% (x,y). let f=(nth (FieldList relationList)) in ((x, f x), (y, f y)))"
definition "enumerator=enumerator'"*)
abbreviation "enumerator' z==((fst z, 0::nat), (snd z, 0::nat))" definition "enumerator=enumerator'"

value "let po=map enumerator 
[(1::nat,2::nat),(2,4),(2,3),(3,5),(3,10),(5,8),(5,9),(8,9),(9,12),(9,10),(10,11),(10,14),(11,14),(11,13),
(14,15),(14,16),(4,14),(4,6),(4,7)
]
in let cf=map enumerator (symCl [(3,4),(5,10),(8,9),(11,14)]) in 
let f = (%((u,v),x,y). (''start'',u,''underscore'',v,''space'',x,''underscore'', y,''end'')) in
(map f (fst (Unfolding (po, cf))), map f (snd (Unfolding (po, cf))))"
(*CKD*)
(* 
cat po.txt | sed "s/[^0-9]*underscore[^0-9]*/_/g"| sed "s/^.*start[^0-9]*/cx/g" | sed "s/[^0-9]*space[^0-9]*/ cx/g" | sed "s/[^0-9]*end[^0-9]*$/\n/g" | sed "/^ *$/ d" 
*)

value "let po=map enumerator [(1::nat, 2::nat), (2,5), (2,3), (2,4), (4,7), (4,8), (3,6), (3,9), (6,9), (9,10), (9,11)] 
in let cf=map enumerator (symCl [(6,9), (7::nat,8::nat)]) in
let f = (%((u,v),x,y). (''start'',u,''underscore'',v,''space'',x,''underscore'', y,''end'')) in
(map f (fst (Unfolding (po, cf))), map f (snd (Unfolding (po, cf))))"
(*Afib*)

find_consts " ('a \<times> 'b) list => bool"
abbreviation "symList' l == set (map flip l) \<subseteq> set l"
definition "symList=symList'"
lemma assumes "symList' l" shows "sym (set l)"  using assms sym_def 
 converse_iff flip_conv set_map subsetCE
by (metis)

lemma assumes "sym (set l)" shows "symList' l" using assms flip_conv set_map equalityE sym_conv_converse_eq
by (metis)

value "let po=map enumerator 
[(1::nat,2::nat),(2,4),(2,3),(3,5),(3,10),(5,8),(5,9),(8,9),(9,12),(9,10),(10,11),(10,14),(11,14),(11,13),
(14,15),(14,16),(4,14),(4,6),(4,7)
]
in let cf=map enumerator (symCl [(3,4),(5,10),(8,9),(11,14)]) in 
let f = (%((u,v),x,y). (''start'',u,''underscore'',v,''space'',x,''underscore'', y,''end'')) in 
let outputPo=trancL (fst (Unfolding (po, cf))) in 
(
f`(transred (set outputPo)), map f outputPo, 
map f (snd (Unfolding (po, cf)))
,map f (propagatedConflict outputPo (snd (Unfolding (po, cf))))
)"
(*CKD*)
(*conflict: | sed "s/[^0-9]*underscore[^0-9]*/_/g"| sed "s/^.*start[^0-9]*/cx/g" | sed "s/[^0-9]*space[^0-9]*/ cx/g" | sed "s/[^0-9]*end[^0-9]*$/\n/g" | sed "/^ *$/ d" | while read i j k; do echo "(and (= arg0 l$i) (= arg1 l$j)) (and (= arg0 l$j) (= arg1 l$i))"; done *)
(*transitive po: | sed "s/[^0-9]*underscore[^0-9]*/_/g"| sed "s/^.*start[^0-9]*/cx/g" | sed "s/[^0-9]*space[^0-9]*/ cx/g" | sed "s/[^0-9]*end[^0-9]*$/\n/g" | sed "/^ *$/ d"  | { 
echo "(define-fun transitive_coverRelations/ckd.txt ((arg0 nodeType) (arg1 nodeType)) Bool (ite (or " ; while read i j k; do echo "(and (= arg0 $i) (= arg1 $j))"; done; echo ") true false))"; } *)

value "let po=map enumerator [(1::nat, 2::nat), (2,5), (2,3), (2,4), (4,7), (4,8), (3,6), (3,9), (6,9), (9,10), (9,11)]
in let cf=map enumerator (symCl [(6,9), (7::nat,8::nat)]) in 
let f = (%((u,v),x,y). (''start'',u,''underscore'',v,''space'',x,''underscore'', y,''end'')) in 
let outputPo=trancL (fst (Unfolding (po, cf))) in 
(f`(transred (set outputPo)), map f outputPo, 
map f (snd (Unfolding (po, cf)))
, map f (propagatedConflict outputPo (snd (Unfolding (po, cf)))))"
*)
(*Afib*)
(*conflict: | sed "s/[^0-9]*underscore[^0-9]*/_/g"| sed "s/^.*start[^0-9]*/ax/g" | sed "s/[^0-9]*space[^0-9]*/ ax/g" | sed "s/[^0-9]*end[^0-9]*$/\n/g" | sed "/^ *$/ d" | while read i j k; do echo "(and (= arg0 l$i) (= arg1 l$j)) (and (= arg0 l$j) (= arg1 l$i))"; done *)
(*| sed "s/[^0-9]*underscore[^0-9]*/_/g"| sed "s/^.*start[^0-9]*/ax/g" | sed "s/[^0-9]*space[^0-9]*/ ax/g" | sed "s/[^0-9]*end[^0-9]*$/\n/g" | sed "/^ *$/ d"  | { echo "(define-fun transitive_coverRelations/afib.txt ((arg0 nodeType) (arg1 nodeType)) Bool (ite (or " ; while read i j k; do echo "(and (= arg0 $i) (= arg1 $j))"; done; echo ") true false))"; } *)






section{* Facs2017 extended: 20190722 *}

lemma lm04b: assumes "IsMaximalConfSmt' (set2pred cau) (set2pred cf) (unset C)"
shows "isMaximalConfSmt3' cau cf C" using assms by fast

lemma assumes "(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l & f\<noteq>s) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) < the (findFirstIndex (\<lambda>x. x=s) l))"
shows
"(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) <= the (findFirstIndex (\<lambda>x. x=s) l))"
using assms by fastforce

lemma lm41k:
"(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) <= the (findFirstIndex (\<lambda>x. x=s) l))
=
(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l & f\<noteq>s) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) < the (findFirstIndex (\<lambda>x. x=s) l))"
using assms lm41g lm41h lm41i less_or_eq_imp_le
by (metis)

theorem "(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) <= the (findFirstIndex (\<lambda>x. x=s) l)) \<longleftrightarrow>
(isOrderPreserving G l)" using lm41j lm41k 
by metis

theorem lm44d: "X\<in>maximals F \<longleftrightarrow> (X\<in>F & (\<forall> Y\<in>F. X\<subseteq>Y \<longrightarrow> X=Y))" using lm44c by metis

end
