(*
Author:
* Marco B. Caminati http://caminati.co.nr
Dually licenced under
* Creative Commons Attribution (CC-BY) 3.0
* ISC License (1-clause BSD License)
See LICENSE file for details
(Rationale for this dual licence: http://arxiv.org/abs/1107.3212)
*)

theory preorder

imports Main 
(* Transitive_Closure *)
(* "~/afp/Show/Show_Instances" *)
(* "~~/src/HOL/Multivariate_Analysis/Finite_Cartesian_Product" *)
(*"~/AuctionMbc2016/Sandbox/scratch"*)
"./Sandbox/ListUtils"
"./Vcg/MiscTools"
"./Vcg/Partitions"
(*"~/afp/Vickrey_Clarke_Groves/MiscTools"*) 
"./Sandbox/AuctionTools"
(*"~/AuctionMbc2016/Sandbox/Trust3"
"./Vcg/CombinatorialAuction"
 "~/afp/Rank_Nullity_Theorem/Dim_Formula"*)
(*"~~/src/HOL/Library/Code_Numeral"
"~~/src/HOL/Library/Code_Target_Nat" 
"~~/src/HOL/Library/Code_Target_Int" 
"~~/src/HOL/Library/Code_Target_Numeral"
"~~/src/HOL/Library/IArray"
"~~/src/HOL/String"
"~~/src/HOL/Library/Code_Char"
"~~/src/HOL/Library/Code_Binary_Nat" 
"~~/src/HOL/ex/Quicksort" *)
(*les3 *)
les2

begin

section {*Intro*}

lemma lm13: "set (filterpositions2 id list)={i| i. list!i & i<size list}" unfolding filterpositions2_def by force

lemma lm09: "map (nth list) (filterpositions2 Pred list) = filter Pred list"
unfolding filterpositions2_def 
by (simp add: Argmax.ll10 map_commutes map_nth)



section {* Definition of the algorithm *}

(* NB: An m x n matrix here is thought of as a list: [row1, row2, ..., rowM ], with each rowI having size n.
This implies that elements are denoted by indices starting from 0 *)

text{*Since a row is a list, a column is a list of one-element lists. *}
abbreviation "myTranspose' == map (%x. x#[])"
definition "myTranspose = myTranspose'"

abbreviation "appendAsColumn' col matr == pairWise (op @) matr (myTranspose' col)"
definition "appendAsColumn = appendAsColumn'"
(* definition "prependAsColumn col matr == pairWise (op #) col matr" *)

text{* X is a set of indices where the elements of l are changed by applying f. *}
abbreviation "update3' l f X == tolist (override_on (nth l) f X) (size l)"
definition "update3 = update3'"
(* abbreviation "update3 l f X == [if (n \<in> X) then (f n) else (l!n). n <- [0..<size l]]" *)
(* abbreviation "mySublist l == map (nth l)" 
Given a list of indices, returns the corresponding sublist extracted from l *)
abbreviation "update4' l f xs == [if i\<in>set xs then f i else l!i. i<-[0..<size l]]"
definition "update4 = update4'"
lemma lm46: "update3' l f (set xs) = update4' l f xs" by auto
lemma lm46b: "update3 l f (set xs) = update4 l f xs" unfolding update3_def update4_def by auto

abbreviation "sublist2' list A == [ list!i. i<-[0..<size list], i\<in>A]"

text{* Given a boolean list, @{term allSubLists} returns all the possible boolean lists obtained by leaving
the Falses fixed and assigning arbitrary values to the other entries *}
abbreviation "allSubLists' l == map (update3 [ False. n<-[0..<size l]] (%m. True)) 
(map set (sublists (filterpositions2 id l)))"
abbreviation "allSubLists2' l == map (update4 [ False. n<-[0..<size l]] (%m. True)) 
(sublists (filterpositions2 id l))"
definition "allSubLists2 = allSubLists2'"
definition "allSubLists = allSubLists'"
(* abbreviation "allNonSubLists l == 
map (update3 [ False. n<-[0..<size l]] (%m. True)) 
(map set (sublists (filterpositions2 Not l)))" *)
definition "myInter A == (if (A={}) then {} else (Inter A))"

text{*Iterates a binary operation along the elements of a list, starting from the right *}
fun listerate where
(*"listerate Op [] = []"|*)
"listerate Op [x] = x"|
"listerate Op [x, y] = Op x y" |
"listerate Op (x#xs) = Op x (listerate Op xs)"
find_consts
  "('a => 'a => 'a) => ('a list) => 'a"
(*
found 6 constant(s):
  Groups_List.monoid_mult.listprod :: "'a \<Rightarrow> ('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
  myDigraph.Operation2Map :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
  preorder.listerate :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
  preorder.listerate2 :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
  preorder.listerate2' :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
  preorder.listerate3' :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a"*)


text{* Non-commutative list intersection (ordering and repetitions are given by the first list)*}
definition "listIntersection l1 l2 == [x<-l1. x \<in> set l2]"

text{* Application of @{term listIntersection} iteratively along a list, starting from the right *}
definition "ListIntersection L == 
if (size L = 0) then ([]) else if (size L=1) then (L!0) else (listerate listIntersection L)"





(* Given an n x n adjacency matrix, and an n-entries adjacency column, 
returns all the possible rows which 
can be appended to the matrix while still yielding a preorder adjacency matrix *)
abbreviation "listerate2' Op list == foldr Op (tl list) (hd list)"
definition "listerate2=listerate2'"
abbreviation "rowAnd' matr == listerate2 (pairWise conj) matr"
definition "rowAnd = rowAnd'"
abbreviation "rowAnd3' matr == [\<forall>i<size matr. matr!i!j. j<-[0..<size (matr!0)]]"
definition "rowAnd3 = rowAnd3'"

text{* this corresponds to cond1' *}
abbreviation "possibleRows2' matr col == 
(*ListIntersection
((allSubLists [True. n <- [0..<size col]]) # 
(map allSubLists (map (nth matr) (filterpositions2 id col))))
*)
allSubLists (
if (filterpositions2 id col = []) then [True. i<-[0..<size col]] else
(* listerate (pairWise conj) (map (nth matr) (filterpositions2 id col)) *)
rowAnd (map (nth matr) (filterpositions2 id col))
)
(*
allSubLists [set (filterpositions2 id col)\<subseteq> 
set (filterpositions2 id (transpose matr!i)). i<-[0..<size matr]]
*)
"
definition "possibleRows2 = possibleRows2'"

abbreviation "possibleRows3' matr col == 
allSubLists2 (if (filterpositions2 id col = []) then [True. i<-[0..<size (matr!0)]] else
rowAnd3 (map (nth matr) (filterpositions2 id col)))"
definition "possibleRows3 = possibleRows3'"

abbreviation "isRowTransitiveAccordingToMatrix' matr roww == 
((set o concat) (map (filterpositions2 id) (sublist matr (set (filterpositions2 id roww))))) 
\<subseteq> (set (filterpositions2 id roww))"
definition "isRowTransitiveAccordingToMatrix = isRowTransitiveAccordingToMatrix'"

text{* Given a transitive matrix and a transitive column, yields all the possible 
completions which are transitive. *}
abbreviation "children2' matr col == map (appendAsColumn (col@[True])) 
(map (\<lambda> l. matr@[l]) (filter (isRowTransitiveAccordingToMatrix(*cond2*) matr) (possibleRows3(*cond1*) matr col)))"
definition "children2 = children2'"

text{* Suppose we append the N+1-th column to a matrix. If the element N+1 is > m, then N+1 must be > also wrt
all the minorants of m. This condition expresses this requirement in terms of the facency matrix. 
Note that this does not depend on the original NxN matrix being transitive.*}
abbreviation "isColTransitiveAccordingToMatrix' matr col == 
(set o concat) (map (filterpositions2 id) (sublist (List.transpose matr) (set (filterpositions2 id col))))
\<subseteq> (set (filterpositions2 id col))"
definition "isColTransitiveAccordingToMatrix = isColTransitiveAccordingToMatrix'"

abbreviation "allColsTransitiveAccordingToMatrix' matr ==
filter (isColTransitiveAccordingToMatrix (*cond3*) matr) (allSubLists [True. n<-[0..<size (matr!0)]])"
definition "allColsTransitiveAccordingToMatrix = allColsTransitiveAccordingToMatrix'"

text{* Yields a list of square boolean matrices, each of dimension equal to the argument. *}
fun allPreorders2 where 
"allPreorders2 0 = [[[]]]" | 
"allPreorders2 (Suc n) = concat 
(map (case_prod children2) 
 (* (List.product (allPreorders2 n) ((allSubLists [True. n<-[0..<n]]))) *)
[ (matr, col). matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr])"
























section{* Adequacy proof *}


abbreviation "isRowTransitiveAccordingToMatrix2' matr roww == 
Union (set ` (filterpositions2 id)`
(set (sublist matr (set (filterpositions2 id roww)))))
\<subseteq> (set (filterpositions2 id roww))"
abbreviation "isRowTransitiveAccordingToMatrix3' matr roww == 
Union (set ` (filterpositions2 id)`
(set [matr!i. i<-[0..<size matr], i \<in> set (filterpositions2 id roww)]
)) \<subseteq> (set (filterpositions2 id roww))"

text{* Corresponds to cond2' *}
abbreviation "isRowTransitiveAccordingToMatrix4' matr roww == 
(\<forall>i \<in> set (filterpositions2 id roww). 
set (filterpositions2 id (matr!i)) \<subseteq> set (filterpositions2 id roww))"

text{* Corresponds to condCol' *}
abbreviation "isColTransitiveAccordingToMatrix4' matr col == 
(\<forall>i \<in> set (filterpositions2 id col). 
    set (filterpositions2 id ((transpose matr)!i)) \<subseteq> set (filterpositions2 id col))"
(*abbreviation "condCol' A N col == (\<forall> i j. (i<N & j<N & A j i & col i) \<longrightarrow> (col j))"*)
lemma lm21: assumes "(
\<forall>i \<in> set (filterpositions2 id roww). set (filterpositions2 id (matr!i)) \<subseteq> set (filterpositions2 id roww)
)" shows "isRowTransitiveAccordingToMatrix2' matr roww" 
using assms unfolding mem_Collect_eq set_sublist using Sup_least imageE by fast


















lemma lm99c: assumes "size l \<le> N" shows "set (filterpositions2 f l) \<subseteq> {0..<N}" using assms lm99b by force

lemma ll10a: fixes f m P shows "(map f [n . n <- l, P n]) = [ f n . n <- l, P n]" 
by (induct l) auto

lemma ll10b: "[f n. n<-l, P n] = map f (filter P l)" by (induct l) auto

lemma ll00: "map f (map g l)=map (f o g) l" by simp

lemma ll47: "concat (filter (%x. x\<noteq>[]) L) = concat L" apply (induct L) apply simp by auto

lemma ll11: assumes "inj_on g (set l)" shows 
"map g (filter P l) = filter (P o (the_inv_into (set l) g)) (map g l)"
using assms comp_apply filter_cong filter_map the_inv_into_f_f
by (metis (mono_tags, lifting))

lemma ll01: assumes "inj f" shows "filter (P o f) l = map (the_inv f) (filter P (map f l))" 
using assms ll11 comp_apply filter_cong filter_map the_inv_into_f_f
apply (induct l) apply force by fastforce

lemma ll02: assumes "n>0" shows "the_inv Suc n = n -1" using assms Suc_diff_1 inj_Suc the_inv_f_f by (metis)

lemma ll02b: "\<forall>n\<in>UNIV-{0::nat}. the_inv Suc n = (%x. x-1) n" using ll02 by fast

lemma ll03: assumes "\<forall> x\<in>set list. f x = g x" shows "map f list = map g list" using assms by simp

corollary ll04: assumes "0 \<notin> set list" shows "map (the_inv Suc) list = map (%x. x-1) list" using 
assms ll03 ll02b by auto

lemma ll05a: "(nth (x#l2)) o Suc = nth l2" by auto

lemma ll05: "(nth (l1@l2)) o (%y. y+size l1) = nth l2" apply (induct l1)
apply simp by simp

lemma ll06b: assumes "sublist l {j. Suc j\<in>A} = [l!i. i<-[0..<size l], i\<in>{j. Suc j \<in> A}]" shows 
"sublist (x # l) A = [(x#l)!i. i<-[0..<size(x#l)], i\<in>A]"
proof -
have 2: "0 \<notin> set (filter (%i. i\<in>A) ([1..<1+size l]))" by fastforce
have "[l!i. i<-[0..<size l], Suc i\<in>A] = map (nth l) (filter (%i. Suc i\<in>A) [0..<size l])"
 by (simp add: ll10b)
moreover have "... = map (nth l) (filter ((%i. i\<in>A) o Suc) [0..<size l])" 
by (metis comp_apply filter_cong) moreover have 
3: "filter ((%i. i\<in>A) o Suc) [0..<size l] = map (the_inv Suc) (filter (%i. i\<in>A) (map Suc [0..<size l]))"
using ll01 using inj_Suc by blast moreover have 
"... = map (the_inv Suc) (filter (%i. i\<in>A) ([1..<1+size l]))" 
by (simp add: map_Suc_upt) moreover have 
4: "... = map (%i. i-1) (filter (%i. i\<in>A) ([1..<1+size l]))" using 2 ll04 by blast
moreover have "... = [i-1. i<- [1..<1+size l], i\<in>A]" using ll10b by metis
ultimately have 
5: "[l!i. i<-[0..<size l], Suc i\<in>A] = map (nth l) [i-1. i<- [1..<1+size l], i\<in>A] &
[l!i. i<-[0..<size l], Suc i\<in>A] = map (nth l) (map (%i. i-1) (filter (%i. i\<in>A) ([1..<1+size l])))"
by presburger have "map (nth l) (map (%i. i-1) (filter (%i. i\<in>A) ([1..<1+size l]))) = 
map (nth (x#l) o Suc) (map (%i. i-1) (filter (%i. i\<in>A) ([1..<1+size l])))" by auto
moreover have "... = map (nth(x#l)) (map (Suc o (%i. i-1)) (filter (%i. i\<in>A) ([1..<1+size l])))" by fastforce
moreover have "... = map (nth(x#l)) (map id (filter (%i. i\<in>A) ([1..<1+size l])))" using ll02b 3 4
by (metis (no_types, lifting) One_nat_def 
  add_Suc_shift filter_map list.map_comp list.map_id map_Suc_upt plus_nat.add_0) moreover have
"... = map (nth(x#l)) (filter (%i. i\<in>A) [1..<1+size l])" by auto ultimately have 
"[l!i. i<-[0..<size l], Suc i\<in>A] = map (nth(x#l)) (filter (%i. i\<in>A) [1..<1+size l])" using 5 by metis
moreover have "sublist l {j. Suc j\<in>A} = [l!i. i<-[0..<size l], i\<in>{j. Suc j \<in> A}]" using assms(1) by blast
moreover have "... = [l!i. i<-[0..<size l], Suc i\<in>A]" by simp ultimately have
"sublist l {j. Suc j\<in>A} = map (nth(x#l)) (filter (%i. i\<in>A) [1..<1+size l])" by presburger then
have 
6: "sublist (x # l) A = (if 0:A then [x] else []) @ (map (nth(x#l)) (filter (%i. i\<in>A) [1..<1+size l]))"
using sublist_Cons by metis
have "[x]=map (nth([x]@l)) [0]" 
by simp
{ assume 
  1: "0\<in>A" then have 
  "sublist (x#l) A=[x] @ (map (nth([x]@l)) (filter (%i. i\<in>A) [1..<1+size l]))" using 6 by auto
  moreover have "... = map (nth([x]@l)) [0] @ (map (nth([x]@l)) (filter (%i. i\<in>A) [1..<1+size l]))" 
  by fastforce moreover have "...=map (nth([x]@l)) ([0]@(filter (%i. i\<in>A) [1..<1+size l]))" by force 
  moreover have "... = map (nth([x]@l)) (filter (%i. i\<in>A) ([0]@[1..<1+size l]))" using 1 by fastforce
  moreover have "... = map (nth([x]@l)) (filter (%i. i\<in>A) [0..<1+size l])" using One_nat_def le0 
  append.simps(1) list.map_disc_iff list.size(3) map_nth upt.simps(2) upt_add_eq_append by (metis)
  ultimately have "sublist (x#l) A=map (nth (x#l)) (filter (%i. i\<in>A) [0..<size (x#l)])" by fastforce
}
  then have 
  7: "0\<in>A \<longrightarrow> sublist (x#l) A = [(x#l)!i. i<-[0..<size(x#l)], i\<in>A]" using ll10b by metis
{
  assume "0\<notin>A" then moreover have "sublist (x#l) A=(map (nth([x]@l)) (filter (%i. i\<in>A) [1..<1+size l]))" 
  using 6 by auto ultimately moreover have "... = 
  (map (nth([x]@l)) (filter (%i. i\<in>A) ([0]@[1..<1+size l])))" by simp moreover have 
  "[0]@[1..<1+size l] = [0..<1+size l]" using One_nat_def append.simps(1) le0 
  list.map_disc_iff list.size(3) map_nth upt.simps(2) upt_add_eq_append by (metis(no_types))
  ultimately have "sublist (x#l) A = map (nth(x#l)) (filter (%i. i\<in>A) [0..<size(x#l)])" using 
  One_nat_def Suc_eq_plus1 append_Cons append_Nil length_append list.size(3) list.size(4) by (metis)
}
then show ?thesis using 7 ll10b by metis
qed

theorem ll06c: assumes "size l=n" shows "\<forall> A. sublist l A = [l!i. i<-[0..<size l], i\<in>A]"
apply (induct l) 
apply simp using ll06b 
by (simp add: ll06b) 

lemma lm23: "sublist l A = [l!i. i<-[0..<size l], i\<in>A]" using ll06c by blast

lemma lm20: "isRowTransitiveAccordingToMatrix2' = isRowTransitiveAccordingToMatrix'" by auto

lemma lm20b: "isRowTransitiveAccordingToMatrix3' = isRowTransitiveAccordingToMatrix2'" 
using lm23 by metis

lemma lm25: assumes "isRowTransitiveAccordingToMatrix3' matr roww"
"i \<in> set (filterpositions2 id roww)" "i<size matr" shows 
"set (filterpositions2 id (matr!i)) \<subseteq> set (filterpositions2 id roww)" 
using assms by fastforce

lemma lm26: assumes "isRowTransitiveAccordingToMatrix3' matr roww" "size roww\<le>size matr" shows
"\<forall>i \<in> set (filterpositions2 id roww). set (filterpositions2 id (matr!i)) \<subseteq> set (filterpositions2 id roww)"
using assms lm25 lm13 unfolding map_eq_conv
using atLeastLessThan_iff linorder_neqE_nat nat_less_le order_refl set_upt upt_eq_Nil_conv
mem_Collect_eq by auto

lemma lm50a: assumes "isColTransitiveAccordingToMatrix4' matr col" 
shows "isColTransitiveAccordingToMatrix' matr col" using assms 
using preorder.lm21 by auto

abbreviation "refl01' A (N::nat) == (\<forall> i. i<N \<longrightarrow> A i i)"
definition "refl01=refl01'"

abbreviation "trans01' A (N::nat) == (\<forall> i j k. (i<N & j<N & k<N & A i j & A j k) \<longrightarrow> A i k)"
definition "trans01 = trans01'"
abbreviation "transMatr' A == (\<forall> i j k. 
(i<size A & k<size(A!0) & j<min (size A) (size(A!0)) & A!i!j & A!j!k)\<longrightarrow>A!i!k)"

definition "fun2Row A i = (% j. A i j)"
definition "fun2Col A j == (% i. A i j)"
abbreviation "matr2Col' j A == (transpose A)!j"

text{* cond1 is ``more computable'' than cond2' *}
abbreviation "cond1' A (N::nat) row == (\<forall> i j. ((i<N & j<N & A i N & (\<not> A i j)) \<longrightarrow> (\<not> row j)))"
definition "cond1 = cond1'"
abbreviation "cond11' A (N::nat) row == (\<forall>j<N. ((\<exists> i<N. A i N & (\<not> A i j)) \<longrightarrow> \<not> row j))"
definition "cond11=cond11'"
lemma lm78: "cond1=cond11" unfolding cond11_def cond1_def by blast

abbreviation "cond2' A N row == (\<forall> i j. (i<N & j<N & row i & A i j) \<longrightarrow> row j)"
definition "cond2 = cond2'"

abbreviation "condCol' A N col == (\<forall> i j. (i<N & j<N & A i j & col j) \<longrightarrow> (col i))"
definition "condCol = condCol'"

abbreviation "addRow' A (N::nat) row == (\<lambda> i j. if (i=N) then row j else A i j)"
definition "addRow = addRow'"
abbreviation "addCol' A (N::nat) col == (\<lambda> i j. if (j=N) then col i else A i j)"
definition "addCol = addCol'"

abbreviation "enlarge' A N col row == (% i j. 
if (i=N & j=N) then (EX k. k<N & col k & row k) else (addRow' (addCol' A N col) N row) i j)"
definition "enlarge = enlarge'"

abbreviation "enlarge2' A N col row == (% i j. 
(if i=N & j=N then True else (addRow' (addCol' A N col) N row) i j))"
definition "enlarge2 = enlarge2'"

lemma lm55aa: assumes "refl01 (enlarge2' A N col row) (N+1) " 
shows "refl01 A N" using assms unfolding refl01_def by auto

lemma lm55ab: assumes "trans01 (enlarge2' A N col row) (N+1)" 
shows "trans01 A N" 
proof -
  have f1: "\<forall>x0 x1. enlarge2' A N col row x1 x0 = (x1 = N \<and> x0 = N \<or> addRow' (addCol' A N col) N row x1 x0)"
    by simp
  have "\<forall>x0 x1 x2. (x2 < N + 1 \<and> x1 < N + 1 \<and> x0 < N + 1 \<and> (x2 = N \<and> x1 = N \<or> addRow' (addCol' A N col) N row x2 x1) \<and> (x1 = N \<and> x0 = N \<or> addRow' (addCol' A N col) N row x1 x0) \<longrightarrow> x2 = N \<and> x0 = N \<or> addRow' (addCol' A N col) N row x2 x0) = ((\<not> x2 < N + 1 \<or> \<not> x1 < N + 1 \<or> \<not> x0 < N + 1 \<or> (x2 \<noteq> N \<or> x1 \<noteq> N) \<and> \<not> addRow' (addCol' A N col) N row x2 x1 \<or> (x1 \<noteq> N \<or> x0 \<noteq> N) \<and> \<not> addRow' (addCol' A N col) N row x1 x0) \<or> x2 = N \<and> x0 = N \<or> addRow' (addCol' A N col) N row x2 x0)"
    by meson
  hence f2: "\<forall>n na nb. \<not> n < N + 1 \<or> \<not> na < N + 1 \<or> \<not> nb < N + 1 \<or> (n \<noteq> N \<or> na \<noteq> N) \<and> \<not> addRow' (addCol' A N col) N row n na \<or> (na \<noteq> N \<or> nb \<noteq> N) \<and> \<not> addRow' (addCol' A N col) N row na nb \<or> n = N \<and> nb = N \<or> addRow' (addCol' A N col) N row n nb"
    using f1 assms unfolding trans01_def by presburger
  obtain nn :: nat and nna :: nat and nnb :: nat where
    "(\<exists>v0 v1 v2. (v0 < N \<and> v1 < N \<and> v2 < N \<and> A v0 v1 \<and> A v1 v2) \<and> \<not> A v0 v2) = ((nn < N \<and> nna < N \<and> nnb < N \<and> A nn nna \<and> A nna nnb) \<and> \<not> A nn nnb)"
    by (metis (no_types))
  moreover
  { assume "nna < N + 1"
    { assume "nn < N + 1 \<and> nna < N + 1 \<and> nnb < N + 1"
      moreover
      { assume "\<not> addRow' (addCol' A N col) N row nn nna"
        hence "nn = N \<or> (\<not> nn < N \<or> \<not> nna < N \<or> \<not> nnb < N \<or> \<not> A nn nna \<or> \<not> A nna nnb) \<or> A nn nnb"
          by (metis nat_neq_iff) }
      moreover
      { assume "\<not> addRow' (addCol' A N col) N row nna nnb"
        hence "(\<not> nn < N \<or> \<not> nna < N \<or> \<not> nnb < N \<or> \<not> A nn nna \<or> \<not> A nna nnb) \<or> A nn nnb"
          by (metis nat_neq_iff) }
      moreover
      { assume "addRow' (addCol' A N col) N row nn nnb"
        hence "nn = N \<or> (\<not> nn < N \<or> \<not> nna < N \<or> \<not> nnb < N \<or> \<not> A nn nna \<or> \<not> A nna nnb) \<or> A nn nnb"
          using nat_neq_iff by presburger }
      ultimately have "(\<not> nn < N \<or> \<not> nna < N \<or> \<not> nnb < N \<or> \<not> A nn nna \<or> \<not> A nna nnb) \<or> A nn nnb"
        using f2 by blast }
    hence "(\<not> nn < N \<or> \<not> nna < N \<or> \<not> nnb < N \<or> \<not> A nn nna \<or> \<not> A nna nnb) \<or> A nn nnb"
      using trans_less_add1 by blast }
  ultimately show ?thesis unfolding trans01_def
    by (meson trans_less_add1)
qed

lemma lm55a: assumes "refl01 (enlarge2' A N col row) (N+1) & trans01 (enlarge2' A N col row) (N+1)" 
shows "refl01 A N & trans01 A N" using assms lm55aa lm55ab by blast

lemma lm55b: assumes "trans01 (enlarge2' A N col row) (N+1)" 
shows "condCol A N col" using assms   
add.commute less_add_one nat_neq_iff trans_less_add2 
proof -
let ?A="enlarge2' A N col row"
{ 
  fix i j assume 0: "i<N & j<N & A i j & col j" 
  then have "i<N+1 & j<N+1 & N<N+1 & ?A i j & ?A j N" by auto
  then have "?A i N" using assms unfolding trans01_def by blast then
  have "col i" using 0 by simp
}thus ?thesis unfolding condCol_def by blast
qed

(* cmp lm32 *)
lemma lm55c: assumes "trans01 (enlarge2' A N col row) (N+1)" 
shows "cond1 (addCol A N col) N row" 
proof -
let ?A="addCol A N col" let ?AA="enlarge2' A N col row"
{
  fix i j assume 0: "i<N & j<N & ?A i N & \<not> ?A i j"
  then have "i<N+1 & j<N+1 & N<N+1 & ?AA i N & \<not> ?AA i j" unfolding addCol_def by fastforce
  then moreover have "\<not> ?AA N j" using assms unfolding trans01_def by blast
  ultimately have "\<not> row j" using assms by meson
}
then show ?thesis unfolding cond1_def by blast
qed

lemma lm55d: assumes "trans01 (enlarge2' A N col row) (N+1)" 
shows "cond2 (addCol A N col) N row"
proof-
let ?A="addCol A N col" let ?AA="enlarge2' A N col row"
{
  fix i j assume "i<N & j<N & row i & ?A i j" then moreover have
  "i<N+1 & j<N+1 & N<N+1 & ?AA N i & ?AA i j" unfolding addCol_def  by force then moreover have 
  "?AA N j" using assms unfolding trans01_def by blast
  ultimately have "row j" using assms by (metis (no_types) less_not_refl)
} thus ?thesis unfolding cond2_def by blast
qed

lemma lm55e: assumes "trans01 A N" 
"condCol A N col"
"cond1 (addCol A N col) N row"
"cond2 (addCol A N col) N row" shows "trans01 (enlarge2' A N col row) (N+1)"
using assms Suc_eq_plus1 le_neq_trans less_Suc_eq_le 
unfolding addCol_def enlarge_def trans01_def cond2_def cond1_def condCol_def
by metis

lemma lm55f: assumes "refl01 A N" shows "refl01 (enlarge2' A N col row) (N+1)"
using assms(1)
Suc_eq_plus1 le_neq_trans less_Suc_eq_le unfolding refl01_def by (metis(no_types))

lemma lm56a: assumes "refl01 (enlarge2' A N col row) (N+1)" "trans01 (enlarge2' A N col row) (N+1)" shows 
"(refl01 A N &
trans01 A N & 
condCol A N col & 
cond1 (addCol A N col) N row & 
cond2 (addCol A N col) N row)" (is "?t1 & ?t2 & ?t3 & ?t4 & ?t5")
using assms lm55a lm55b lm55c lm55d by blast

lemma lm56b: assumes 
"(refl01 A N &
trans01 A N & 
condCol A N col & 
cond1 (addCol A N col) N row & 
cond2 (addCol A N col) N row)"
shows 
"refl01 (enlarge2' A N col row) (N+1) & trans01 (enlarge2' A N col row) (N+1)" 
using assms lm55e lm55f refl01_def by blast 

lemma lm56: "(refl01 (enlarge2' A N col row) (N+1) & trans01 (enlarge2' A N col row) (N+1)) = 
(refl01 A N &
trans01 A N & 
condCol A N col & 
cond1 (addCol A N col) N row & 
cond2 (addCol A N col) N row)" 
using assms lm56a lm56b by blast

abbreviation "fun2Matr' M N A == [[ A i j. j<-[0..<N]]. i<-[0..<M]]"
abbreviation "matr2Fun' A == (\<lambda> i j. A!i!j)"
definition "matr2Fun = matr2Fun'"

abbreviation "hasCols' N A == ((set o (map size)) A = {N})"
abbreviation "isRectangular2' A == (hasCols' (size(A!0)) A)"
abbreviation "isRectangular3' A == (\<forall> i j. (i<size A & j<size A) \<longrightarrow> size(A!i) = size(A!j))"
abbreviation "isRectangular4' n A == (A \<noteq> [] & (\<forall> i. i<size A \<longrightarrow>size(A!i)=n))"
definition "isRectangular4 = isRectangular4'"
abbreviation "isSquare3' A == (A\<noteq>[] & isRectangular3' A & length A = length(A!0))"
abbreviation "allPreordersFun' == (map matr2Fun') o allPreorders2"

(*
lemma lm34b: assumes "A\<noteq>[]" "isRectangular3' A" shows "isRectangular2' A" using assms
length_map Set.set_insert length_greater_0_conv 
all_not_in_conv insert_iff nth_map in_set_conv_nth
unfolding comp_def
by smt

lemma lm34a: assumes "isRectangular2' A" shows "isRectangular3' A" using assms 
by (metis comp_def length_map nth_map nth_mem singletonD)

lemma lm34: assumes "A\<noteq>[]" shows "isRectangular2' A = isRectangular3' A" using assms lm34a lm34b by metis
*)

lemma transpose_rectangle2:
  assumes "isRectangular4' n xs"
  shows "transpose xs = [[xs!j!i. j<-[0..<length xs]]. i<-[0..<n]]" using assms transpose_rectangle 
by blast

corollary lm37: assumes "isRectangular4' n xs" "j<size xs" "i<n" shows
"(transpose xs)!i!j = xs!j!i" using assms transpose_rectangle2
by force

lemma transpose_rectangle3:
  assumes "isRectangular4' n xs"
  shows "transpose xs = fun2Matr' n (length xs) (%  i j. xs!j!i)" using assms transpose_rectangle 
by blast

lemma lm35: "size [f n. n<-[0..<N]]=N" by fastforce

lemma lm36: assumes "isRectangular4' n matr" shows "size (transpose matr)=n" using assms lm35 transpose_rectangle2 by metis

lemma lm38: assumes "isRectangular4' n matr" "i<n" shows "size ((transpose matr)!i)=size matr"
using assms transpose_rectangle2 by force

lemma lm39a: assumes 
"size col=size matr" 
"size col=n" 
"isRectangular4' n matr" 
"isRectangular4' (size matr) matr"
"condCol' (matr2Fun' matr) (size matr) (nth col)"
shows
"isColTransitiveAccordingToMatrix4' matr col" 
proof -
let ?A="matr2Fun' matr" let ?m="transpose matr" 
let ?f="set o (filterpositions2 id)" let ?N="size matr"
{
  fix i fix j assume 0: "i \<in> ?f col" moreover assume 1: "j \<in> ?f ((transpose matr)!i)"
  ultimately moreover have 2: "i<?N & i<size col & i<n" using assms(1,2) 
by (metis atLeast0LessThan comp_apply lessThan_iff lm99b subsetCE)
  ultimately moreover have 4: "j<?N" using assms(3) lm38 
by (metis atLeast0LessThan comp_apply lessThan_iff lm99b subsetCE)
ultimately moreover have "(transpose matr)!i!j" unfolding comp_apply lm13 mem_Collect_eq by blast
  ultimately moreover have "matr!j!i" using assms(4) lm37 by metis
  ultimately moreover have "?A j i" by simp
  ultimately moreover have "nth col i"
  using comp_def mem_Collect_eq lm13 by (metis (no_types, lifting))
  ultimately moreover have "nth col j" using assms(5) 
by meson
  ultimately have "j \<in> ?f col" using assms(1) lm13 by auto
}
then show ?thesis by auto
qed

lemma lm39b: assumes 
"size col=size matr" 
"size col=n" 
"isRectangular4' n matr" 
"isRectangular4' (size matr) matr"
"isColTransitiveAccordingToMatrix4' matr col" 
shows
"condCol' (matr2Fun' matr) (size matr) (nth col)"
proof -
let ?A="matr2Fun' matr" let ?N="size matr" let ?c="nth col" let ?f="set o (filterpositions2 id)"
{
  fix i j assume 0: "i<?N" assume 1:"j<?N" assume 2: "?A j i" assume 3: "?c i" 
  have "i \<in> ?f col" using assms(1) 0 3 by (simp add: preorder.lm13)
  then moreover have "?f ((transpose matr)!i) \<subseteq> ?f col" using assms(5) by simp
  ultimately moreover have "((transpose matr)!i)!j" using assms(4) 0 1 2 by (metis preorder.lm37)
  ultimately moreover have "j \<in> ?f col" using assms(2,3) 1 lm13 lm38  mem_Collect_eq 
  unfolding comp_apply subset_iff by (metis(no_types,lifting))
  ultimately have "?c j" by (simp add: lm13)
  }
thus ?thesis by blast
qed

lemma lm39: assumes 
"size col=size matr" 
"isRectangular4' (size matr) matr"
shows "isColTransitiveAccordingToMatrix4' matr col=condCol' (matr2Fun' matr) (size matr) (nth col)"
using assms lm39a lm39b by metis

lemma lm59: assumes "l\<in> set (allSubLists' [True. n<-[0..<N]])" shows "size l=N" using assms 
update3_def by (metis ex_map_conv length_map preorder.lm35)

(*
lemma (*lm41:*)
"l=update3 [ False. n<-[0..<(length l)]] (%m. True) (set (filterpositions2 id l))" 
unfolding update3_def using assms atLeast0LessThan lessThan_iff map_nth 
map_eq_conv set_upt lm35 unfolding override_on_def lm13 mem_Collect_eq  
by smt
*)

lemma lm42: "set (filterpositions2 id l) \<in>  (Pow {0..<(size l)})" using assms filterpositions2_def
sublists_powset lm13 by auto

lemma "A \<in> set (allPreorders2 (Suc N)) = (EX a col. a\<in>set(allPreorders2 N)
& col \<in> set (allColsTransitiveAccordingToMatrix a) & A\<in>set (children2 a col))" 
using assms by auto

lemma lm43: "set (filter P l) = {x| x. x\<in>set l & P x}" by auto

lemma lm44: "set (children2 matr col) = { appendAsColumn (col@[True]) (matr@[row])| row. 
row \<in> set (possibleRows3 matr col) & isRowTransitiveAccordingToMatrix matr row }"
unfolding children2_def by force

lemma lm45: "A \<in> set (allPreorders2 (Suc N)) = (EX a col row. a\<in>set(allPreorders2 N)
& col \<in> set (allColsTransitiveAccordingToMatrix a) 
& A=appendAsColumn (col@[True]) (a@[row])
& row \<in> set (possibleRows3 a col) & isRowTransitiveAccordingToMatrix a row)" 
using assms lm43 lm44 children2_def by auto

lemma lm45b: "A \<in> set (allPreorders2 (Suc N)) = (EX a col row. a\<in>set(allPreorders2 N)
& col \<in> set (allSubLists [True. n<-[0..<size (a!0)]]) & isColTransitiveAccordingToMatrix a col
& A=appendAsColumn (col@[True]) (a@[row])
& row \<in> set (possibleRows3 a col) & isRowTransitiveAccordingToMatrix a row)" 
unfolding lm45 allColsTransitiveAccordingToMatrix_def
using assms lm45 by simp

(* duplicate of lm59? *)
lemma lm60a: assumes "l' \<in> set (allSubLists' l)" shows "size l'=size l"
using assms update3_def ex_map_conv preorder.lm35 by (metis(no_types,lifting))

lemma lm60b: assumes "l' \<in> set (allSubLists2' l)" shows "size l'=size l"
using assms lm35 update4_def update3_def lm46 lm60 
proof -
  have "\<And>bs p ns. length (update4 (bs::bool list) p ns) = length bs"
    by (simp add: update4_def)
  hence "\<And>bs bsa p nss. length (bs::bool list) = length bsa \<or> bs \<notin> set (map (update4 bsa p) nss)"
    by auto
  thus ?thesis
    by (metis assms preorder.lm35)
qed


lemma lm61: assumes "i<N" shows "(update4 [False. n<-[0..<N]] (%m. True) l)!i \<longleftrightarrow> (i\<in>set l)"
using assms unfolding update4_def by simp

lemma lm62: "size (update4 L f l)=size L" using assms unfolding update4_def by simp

lemma lm61b: assumes "i<N" shows "(update4 [False. n<-[0..<N]] (%m. True) l)!i = [n\<in>set l . n<-[0..<N]]!i"
unfolding update4_def using assms by (simp)

lemma lm61c: "(update4 [False. n<-[0..<N]] (%m. True) l) = [n\<in>set l . n<-[0..<N]]" 
by (simp add: lm62 nth_equalityI preorder.lm61b)

lemma lm63: "set (allSubLists2' L) = 
(update4 [ False. n<-[0..<size L]] (%m. True))`set(sublists(filterpositions2 id L))" 
by simp

lemma lm64: "set (allSubLists2' L) = 
(%X. [n\<in>X . n<-[0..<size L]]) ` (set`set(sublists(filterpositions2 id L)))" 
using lm63 lm61c by auto

lemma lm64b: "set (allSubLists2' L) = 
(%X. [n\<in>X . n<-[0..<size L]]) ` (Pow {i| i. L!i & i<size L})" 
using lm64 sublists_powset lm13 by metis

lemma lm64c: "set (allSubLists2' L) = 
{[n\<in>X. n<-[0..<size L]]| X. X\<subseteq>{i| i. L!i & i<size L}}" 
using lm64b by auto

lemma lm65a: "{[n\<in>X. n<-[0..<size L]]| X. X\<subseteq>{i| i. L!i & i<size L}} \<subseteq> 
{l| l. size l=size L & (\<forall>i<size L. L!i=False \<longrightarrow> l!i=False)}" by auto

lemma lm65b: "{[n\<in>X. n<-[0..<size L]]| X. X\<subseteq>{i| i. L!i & i<size L}} \<supseteq> 
{l| l. (\<forall>i<size L. L!i=False \<longrightarrow> l!i=False) & size l=size L }" (is "?L \<supseteq> ?R")
proof -
{ 
  fix l assume "l\<in>?R" then have
  0: "size l = size L & (\<forall>i<size L. L!i=False \<longrightarrow> l!i=False)" by fast
  let ?XX="{i| i. l!i & i<size l}" let ?X="{i| i. l!i & i<size L}" have 
  "l = [n\<in>?XX. n<-[0..<size l]]"    
  using atLeastLessThan_iff map_nth map_eq_conv set_upt unfolding mem_Collect_eq    
  proof -
    have "tolist (op ! l) (length l) = tolist (\<lambda>n. \<exists>na. n = na \<and> l ! na \<and> na < length l) (length l)"
      by simp
    thus "l = tolist (\<lambda>n. \<exists>na. n = na \<and> l ! na \<and> na < length l) (length l)"
      by (simp add: map_nth)
  qed
  then have "l=[n\<in>?X. n<-[0..<size L]]" using 0 by auto
  moreover have "?XX \<subseteq> {i| i. L!i & i<size L}" using 0 by auto
  moreover have "?X = ?XX" using 0 by presburger
  ultimately have "l \<in> ?L" by blast 
  }
then show ?thesis by blast
qed

lemma lm64d: assumes "l \<in> set (allSubLists2' L)" "i<size L" "L!i = False" shows "size l= size L & \<not>l!i"
using assms lm64c by force

lemma lm64e: assumes "\<forall>i<size L. L!i=False \<longrightarrow> l!i=False" "size l=size L" shows "l\<in>set(allSubLists2' L)"
proof -
  have f1: "\<And>bs. {bsa. (\<forall>n<length bs. \<not> bs ! n \<longrightarrow> \<not> bsa ! n) \<and> length bsa = length bs} \<subseteq> set (allSubLists2' bs)"
    using lm64c lm65b by presburger
  have "l \<in> {bs. (\<forall>n<length L. \<not> L ! n \<longrightarrow> \<not> bs ! n) \<and> length bs = length L}"
    using assms(1,2) by blast
  thus ?thesis
    using f1 by blast
qed

lemma lm64f: "(l\<in>set(allSubLists2' L)) = (size l=size L & (\<forall>j<size L. (\<not>L!j) \<longrightarrow> \<not>l!j))"
using lm64e lm64d by (metis lm60b)

lemma lm49a: assumes 
"(size l=N)" shows "(l \<in> set (allSubLists2' [True. n<-[0..<N]]))" 
proof -
  have f1: "length l = length (tolist (\<lambda>n. True) N)"
    by (simp add: assms)
  obtain nn :: "bool list \<Rightarrow> bool list \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2. (v2 < length x0 \<and> \<not> x0 ! v2) \<and> x1 ! v2) = ((nn x0 x1 < length x0 \<and> \<not> x0 ! nn x0 x1) \<and> x1 ! nn x0 x1)"
    by moura
  hence f2: "\<forall>bs bsa. (bs \<notin> set (allSubLists2' bsa) \<or> length bs = length bsa \<and> (\<forall>n. \<not> n < length bsa \<or> bsa ! n \<or> \<not> bs ! n)) \<and> (bs \<in> set (allSubLists2' bsa) \<or> length bs \<noteq> length bsa \<or> nn bsa bs < length bsa \<and> \<not> bsa ! nn bsa bs \<and> bs ! nn bsa bs)"
    using lm64f by auto
  have f3: "\<forall>n ns p. \<not> n < length ns \<or> map p ns ! n = (p (ns ! n::nat)::bool)"
    by force
  { assume "tolist (\<lambda>n. True) N ! nn (tolist (\<lambda>n. True) N) l \<noteq> True"
    hence ?thesis
      using f3 f2 f1 by (metis (no_types) length_map) }
  thus ?thesis
    using f2 f1 by blast
qed

lemma lm49b: assumes  "(l \<in> set (allSubLists2' [True. n<-[0..<N]]))" shows 
"(size l=N)" using assms by (metis lm60b preorder.lm35)

lemma lm49c: "(size l=N) = (l \<in> set (allSubLists2' [True. n<-[0..<N]]))" 
using lm49a lm49b by blast

lemma lm01aa: assumes "l\<in>set (allSubLists2' L)" shows" (l\<in>set (allSubLists' L))"
proof -
  obtain nn :: "bool list list \<Rightarrow> bool list \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2<length x0. x0 ! v2 = x1) = (nn x0 x1 < length x0 \<and> x0 ! nn x0 x1 = x1)"
    by moura
  hence f1: "nn (allSubLists2' L) l < length (allSubLists2' L) \<and> allSubLists2' L ! nn (allSubLists2' L) l = l"
    by (metis (no_types) assms in_set_conv_nth)
  hence "nn (allSubLists2' L) l < length (sublists (filterpositions2 id L))"
    by simp
  hence "allSubLists' L ! nn (allSubLists2' L) l = l"
    using f1 by (simp add: lm46b)
  thus ?thesis
    using f1 by (metis (no_types) in_set_conv_nth length_map)
qed

lemma lm01ab: assumes "l\<in>set (allSubLists' L)" shows" (l\<in>set (allSubLists2' L))"
using assms lm46b by fastforce

lemma lm01a: "l\<in>set (allSubLists2' L) = (l\<in>set (allSubLists' L))"
using lm01aa lm01ab by blast

lemma lm01: "set o allSubLists2' = set o allSubLists'" using lm01a by auto 

lemma lm49: "(size l=N) = (l \<in> set (allSubLists [True. n<-[0..<N]]))" 
using lm49c lm01a unfolding allSubLists_def by blast

lemma lm45c: "A \<in> set (allPreorders2 (Suc N)) = (EX a col row. 
a\<in>set(allPreorders2 N) & size col = size (a!0) & isColTransitiveAccordingToMatrix a col & 
A=appendAsColumn (col@[True]) (a@[row])
& row \<in> set (possibleRows3 a col) & isRowTransitiveAccordingToMatrix a row)" 
unfolding lm45 allColsTransitiveAccordingToMatrix_def
using assms lm45 lm49 by simp

lemma lm45d: "A \<in> set (allPreorders2 (Suc N)) = (EX a col row. 
a\<in>set(allPreorders2 N) & 
isColTransitiveAccordingToMatrix a col &
row \<in> set (possibleRows3 a col) &
isRowTransitiveAccordingToMatrix a row &
size col = size (a!0) &  
A=appendAsColumn (col@[True]) (a@[row])
)" 
using lm45c by blast

lemma lm50b: assumes 
"size col \<le> size (transpose matr)"
"isColTransitiveAccordingToMatrix' matr col"
shows  
"isColTransitiveAccordingToMatrix4' matr col"
using assms mem_Collect_eq le_antisym le_trans nat_less_le 
lm13 lm20 lm20b lm25 by (metis (no_types,lifting))

lemma lm50: assumes
"size col \<le> size (transpose matr)" shows
"isColTransitiveAccordingToMatrix' matr col=isColTransitiveAccordingToMatrix4' matr col"
using assms lm50a lm50b by blast

lemma lm51a: assumes "size col=size matr" "size col \<le> size (transpose matr)"
"isRectangular4' (size matr) matr" shows "isColTransitiveAccordingToMatrix' matr col=condCol' (matr2Fun' matr) (size matr) (nth col)"
using assms lm39 lm50 lm36 by (metis(no_types,lifting))

lemma lm51: assumes "size col=size matr" "isRectangular4' (size matr) matr" shows 
"isColTransitiveAccordingToMatrix matr col=condCol' (matr2Fun' matr) (size matr) (nth col)"
proof -
have "size col = size (transpose matr)" using assms lm36 by metis
thus ?thesis using assms lm51a unfolding isColTransitiveAccordingToMatrix_def by auto
qed

lemma lm52: assumes "i<size matr" "i<size col" shows "(appendAsColumn col matr) ! i =matr!i @ [col!i]"
unfolding pairWise_def appendAsColumn_def
using assms by simp

lemma lm53: assumes "size a=N" "isRectangular4' N a" "size col=N" 
"size row=N" "i<N+1" "j<N+1" shows "(matr2Fun' (appendAsColumn (col@[True]) (a@[row]))) i j = 
    (enlarge2' (matr2Fun' a) N (nth col) (nth row)) i j"
proof -
let ?A="a@[row]" let ?f=appendAsColumn let ?Col="col@[True]"
have True by blast
moreover have "i < size ?A" using assms(1,5) by simp
moreover have "i < size ?Col" using assms(3,5) by fastforce
ultimately moreover have "(?f ?Col ?A)!i = (?A!i)@[?Col!i]" using lm52 by blast
ultimately show ?thesis using assms(1,2,3,4,6) by (simp add: nth_append)
qed

lemma lm54: assumes "\<forall> i j. (i<N & j<N) \<longrightarrow> f i j = g i j" "trans01' f N" shows "trans01 g N"
using assms unfolding trans01_def by meson

lemma lm58: assumes "M>0" shows "isRectangular4' N (fun2Matr' M N f)" using assms(1) by force

abbreviation "listerate3' Op list == foldr Op ((rev o tl) list) (hd list)"

lemma lm67: assumes "j<N" "isRectangular4' N matr" shows 
"size (rowAnd3' matr) = N & ((\<not> rowAnd3' matr!j) = (EX i<size matr. \<not>matr!i!j))" using assms by force

lemma lm68: assumes "isRectangular4' N matr" "L=rowAnd3' matr" "size L=N" shows
"(l\<in>set(allSubLists2' L)) = (size l=size L & (\<forall>j<size L. (EX i<size matr. \<not> matr!i!j) \<longrightarrow> \<not>l!j))"
using assms unfolding lm64f using length_map nth_map map_nth by auto

lemma lm68b: assumes "isRectangular4' N matr" "L=rowAnd3' matr" shows
"(l\<in>set(allSubLists2' L)) = (size l=N & (\<forall>j<N. (EX i<size matr. \<not> matr!i!j) \<longrightarrow> \<not>l!j))"
using assms lm68 lm67 by force

lemma ll12: "(l\<in>set (allSubLists2' [True. i<-[0..<N]])) = (size l=N)" 
proof -
  obtain nn :: "bool list \<Rightarrow> bool list \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2. (v2 < length x0 \<and> \<not> x0 ! v2) \<and> x1 ! v2) = ((nn x0 x1 < length x0 \<and> \<not> x0 ! nn x0 x1) \<and> x1 ! nn x0 x1)"
    by moura
  hence f1: "\<forall>bs bsa. (bs \<notin> set (allSubLists2' bsa) \<or> length bs = length bsa \<and> (\<forall>n. \<not> n < length bsa \<or> bsa ! n \<or> \<not> bs ! n)) \<and> (bs \<in> set (allSubLists2' bsa) \<or> length bs \<noteq> length bsa \<or> nn bsa bs < length bsa \<and> \<not> bsa ! nn bsa bs \<and> bs ! nn bsa bs)"
    using lm64f by auto
  have "\<not> nn (tolist (\<lambda>n. True) N) l < length (tolist (\<lambda>n. True) N) \<or> tolist (\<lambda>n. True) N ! nn (tolist (\<lambda>n. True) N) l \<or> \<not> l ! nn (tolist (\<lambda>n. True) N) l"
    by simp
  hence "length l = length (tolist (\<lambda>n. True) N) \<longrightarrow> l \<in> set (allSubLists2' (tolist (\<lambda>n. True) N))"
    using f1 by blast
  thus ?thesis
    using f1 by (metis (no_types) preorder.lm35)
qed


lemma lm69: "map (nth L) entryList=[L!i. i<-entryList]" using assms by simp

lemma lm69b: assumes "i<size entryList" shows "map (nth L) entryList!i=L!(entryList!i)"
using assms by fastforce

lemma lm69c: assumes "set entryList \<subseteq>{0..<size L}" shows "size(map (nth L) entryList)=size entryList"
using assms by simp

lemma lm68c: assumes "isRectangular4' N matr" "L=rowAnd3' matr" "matr=map (nth A) rowList" shows
"(l\<in>set(allSubLists2' L)) = (size l=N & (\<forall>j<N. (EX i<size rowList. \<not> (A!(rowList!i))!j) \<longrightarrow> \<not>l!j))"
using assms(1) length_greater_0_conv length_map map_nth nth_map unfolding assms(2,3) lm64f by auto
(*
lemma lm70a: assumes "A\<noteq>[]" shows "(isRectangular4' N A) = (size`(set A)={N})"
using assms comp_def empty_iff image_eqI insert_iff length_greater_0_conv nth_mem lm34b set_map
by (metis(no_types,lifting))

lemma lm70b: "(isRectangular4' N []) = (size`(set [])={N})" by auto

lemma lm70: "(isRectangular4 N A) = (size`(set A)={N})" using lm70a lm70b isRectangular4_def by metis
*)
lemma lm71: assumes "isRectangular4 N A" "set B \<subseteq> set A" "B\<noteq>[]" shows "isRectangular4 N B"
using assms unfolding isRectangular4_def using lm84e nth_mem subsetCE by (metis(no_types,lifting))

lemma lm72: assumes "isRectangular4 N A" "(map (nth A) rowList) \<noteq> []" "set rowList \<subseteq> {0..<size A}"
shows "isRectangular4 N (map (nth A) rowList)" using assms(1,2,3) lm71 by (metis (no_types, lifting) nn01 set_map)

corollary lm68d: assumes "isRectangular4 N A" "L=rowAnd3' matr" "matr=map (nth A) rowList"
"set rowList \<subseteq> {0..<size A}" "set rowList \<noteq> {}" shows
"(l\<in>set(allSubLists2' L)) = (size l=N & (\<forall>j<N. (EX i<size rowList. \<not> (A!(rowList!i))!j) \<longrightarrow> \<not>l!j))"
using assms unfolding lm68c using lm72 isRectangular4_def
proof -
have "isRectangular4' N matr" using assms lm72 unfolding isRectangular4_def by blast
thus ?thesis using assms lm68c by presburger
qed

corollary lm68e: assumes "isRectangular4 N A" "L=rowAnd3 (map (nth A) rowList)"
"set rowList \<subseteq> {0..<size A}" "set rowList \<noteq> {}" shows
"(l\<in>set(allSubLists2 L)) = (size l=N & (\<forall>j<N. (EX i<size rowList. \<not> (A!(rowList!i))!j) \<longrightarrow> \<not>l!j))"
using assms lm68d rowAnd3_def allSubLists2_def by presburger

lemma lm73: "(\<forall>j<N. (\<exists> i<size rowList. \<not> (A!(rowList!i))!j) \<longrightarrow> \<not>l!j) = 
(\<forall>j<N. (\<exists> k\<in>set rowList. \<not> (A!k)!j) \<longrightarrow> \<not>l!j)"
by (metis(no_types) in_set_conv_nth)

corollary lm68f: assumes "isRectangular4 N A" "L=rowAnd3 (map (nth A) rowList)"
"set rowList \<subseteq> {0..<size A}" "set rowList \<noteq> {}" shows
"(l\<in>set(allSubLists2 L)) = (size l=N & (\<forall>j<N. (\<exists> k. k\<in>set rowList & \<not> (A!k)!j) \<longrightarrow> \<not>l!j))"
using assms lm68e unfolding lm73 by blast

corollary lm68g: assumes "isRectangular4 N A" "L=rowAnd3 (map (nth A) rowList)"
"set rowList \<subseteq> {0..<size A}" "set rowList \<noteq> {}" "rowList=filterpositions2 id col" shows
"(l\<in>set(allSubLists2 L)) = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col) & \<not> (A!k)!j) \<longrightarrow> \<not>l!j))"
proof -
have "\<forall>k. (k \<in> set rowList = (col!k & k<size col))" using assms lm13 by force
thus ?thesis using assms(1,2,3,4) lm68f by presburger
qed

lemma (*lm74a:*) assumes "\<forall>k<size col. \<not>col!k" shows 
"size l=N = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col & \<not>(A!k!j))) \<longrightarrow> \<not>l!j))"
using assms by blast

lemma lm74b: assumes "\<forall>k<size col. \<not>col!k" shows 
"(l\<in>set (allSubLists2 [True. i<-[0..<N]])) = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col & \<not>(A!k!j))) \<longrightarrow> \<not>l!j))"
using assms ll12 allSubLists2_def by metis

lemma lm75: assumes "filterpositions2 id col=[]" "k<size col" shows "\<not>col!k"
using assms(1,2) atLeast0LessThan lessThan_iff less_nat_zero_code mem_Collect_eq lm13 set_upt upt.simps(1)
by (metis (no_types, lifting)) 

corollary lm68h: assumes "isRectangular4 N A" "set (filterpositions2 id col) \<subseteq> {0..<size A}" 
"filterpositions2 id col \<noteq> []" shows
"(l\<in>set(possibleRows3' A col)) = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col) & \<not> (A!k)!j) \<longrightarrow> \<not>l!j))"
using assms lm68g unfolding possibleRows3_def by auto

corollary lm68i: assumes "isRectangular4 N A" "size col \<le> size A" "filterpositions2 id col \<noteq> []" 
shows "(l\<in>set(possibleRows3' A col)) = 
  (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col) & \<not> (A!k)!j) \<longrightarrow> \<not>l!j))"
using assms lm68h lm99c by blast

lemma lm74c: assumes "filterpositions2 id col=[]" shows 
"(l\<in>set (allSubLists2 [True. i<-[0..<N]])) = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col & \<not>(A!k!j))) \<longrightarrow> \<not>l!j))"
using assms lm74b lm75 by presburger

lemma lm74d: assumes "isRectangular4 N A" "filterpositions2 id col=[]" shows 
"(l\<in>set (possibleRows3' A col)) = (size l=N & (\<forall>j<N. (\<exists> k. (col!k & k<size col & \<not>(A!k!j))) \<longrightarrow> \<not>l!j))"
using assms by (simp add: isRectangular4_def lm74c)

lemma lm76: assumes "isRectangular4 N A" "size col\<le>size A" shows
"(l\<in>set (possibleRows3 A col)) = (size l=N & (\<forall>j<N. (\<exists> i. col!i & i<size col & \<not> A!i!j) \<longrightarrow> \<not>l!j))"
using assms lm74d lm68i unfolding possibleRows3_def
by force

lemma lm77: "(addCol f N col) i N = col i" unfolding addCol_def by simp
lemma lm77b: assumes "j<N" shows "addCol f N col i j = f i j" using assms unfolding addCol_def by auto

lemma lm77c: assumes "size col=N" "AA=addCol (matr2Fun' A) N (nth col)" shows 
"(\<forall>j<N. (\<exists> i. col!i & i<size col & (\<not> A!i!j)) \<longrightarrow> \<not>row!j) = cond11 AA N (nth row)"
using assms unfolding addCol_def cond11_def by auto

lemma cond1Equiv: assumes "isRectangular4 N A" "size col\<le>size A" "size col=N" shows 
"(row \<in> set (possibleRows3 A col)) = 
  (size row=N & (cond1 (addCol (matr2Fun' A) N (nth col)) N (nth row)))" 
using assms lm76 lm77c isRectangular4_def unfolding lm78 by simp

lemma ll07b: assumes "size col=size A" shows "size (appendAsColumn' col A) = size A" using assms 
unfolding pairWise_def by simp

lemma ll07c: assumes "isRectangular4' N A" "size col=size A" shows 
"isRectangular4' (N+1) (appendAsColumn' col A)" using assms unfolding pairWise_def by simp

lemma ll07: assumes "size A=M" "size B=M" "isRectangular4' N A" "isRectangular4' N B"
"\<forall> i j. ((i<M & j<N) \<longrightarrow> A!i!j = B!i!j)" shows "A=B" using assms by (simp add: nth_equalityI)

lemma ll07d: assumes "isRectangular4' N A" "size row=N" shows "isRectangular4' N (A@[row])"
using assms(1,2) One_nat_def Suc_eq_plus1 diff_is_0_eq' less_Suc_eq_le 
list.size(3,4) nth_Cons_0 append_is_Nil_conv nth_append 
unfolding length_append by (metis)

lemma ll07e: assumes "i<size A" "size col=size A" "j<size(A!i)" shows "(appendAsColumn' col A)!i!j = A!i!j" 
using assms  pairWise_def by (simp add: pairWise_def nth_append)

lemma ll07f: assumes "isRectangular4' N a" "isRectangular4' (N+1) A" "size a=M" "size A=M+1" 
"row=[A!M!j. j<-[0..<N]]" "col=[A!i!N. i<-[0..<M]]" 
"\<forall> i j. i<M & j<N \<longrightarrow> a!i!j=A!i!j"
shows "A=appendAsColumn' (col@[A!M!N]) (a@[row])" using assms pairWise_def ll07 ll07b ll07c 
proof -
let ?aa="a@[row]" let ?coll="col@[A!M!N]" let ?AA="appendAsColumn' ?coll ?aa" have 
8: "size row=N & size col=M" using assms lm35 by blast
have "size ?aa=M+1" using assms by auto then have 
0: "size ?AA=M+1" using assms length_append list.size(3) list.size(4) ll07b 8 by (metis)
have "isRectangular4' N ?aa" using assms ll07d 8 by metis then have 
1: "isRectangular4' (N+1) ?AA" 
using ll07c length_0_conv list.size(4) ll07b length_append assms(3) 8 
proof -
  have f1: "\<forall>ass n as. (ass = [] \<or> (\<exists>na<length ass. length (ass ! na) \<noteq> n) \<or> length (as::'a list) \<noteq> length ass) \<or> appendAsColumn' as ass \<noteq> [] \<and> (\<forall>na. \<not> na < length (appendAsColumn' as ass) \<or> length (appendAsColumn' as ass ! na) = n + 1)"
    using `\<And>col N A. \<lbrakk>A \<noteq> [] \<and> (\<forall>i<length A. length (A ! i) = N); length col = length A\<rbrakk> \<Longrightarrow> appendAsColumn' col A \<noteq> [] \<and> (\<forall>i<length (appendAsColumn' col A). length (appendAsColumn' col A ! i) = N + 1)` by blast
  obtain nn :: "nat \<Rightarrow> 'a list list \<Rightarrow> nat" where
    "\<forall>x1 x2. (\<exists>v3<length x2. length (x2 ! v3) \<noteq> x1) = (nn x1 x2 < length x2 \<and> length (x2 ! nn x1 x2) \<noteq> x1)"
    by moura
  hence f2: "\<forall>ass n as. (ass = [] \<or> nn n ass < length ass \<and> length (ass ! nn n ass) \<noteq> n \<or> length as \<noteq> length ass) \<or> appendAsColumn' as ass \<noteq> [] \<and> (\<forall>na. \<not> na < length (appendAsColumn' as ass) \<or> length (appendAsColumn' as ass ! na) = n + 1)"
    using f1 by presburger
  have "length col = length a"
    by (metis "8" `length a = M`) (* > 1.0 s, timed out *)
  hence "length (col @ [matr2Fun' A M N]) = length (a @ [row])"
    by simp
  thus ?thesis
    using f2 by (meson `a @ [row] \<noteq> [] \<and> (\<forall>i<length (a @ [row]). length ((a @ [row]) ! i) = N)`)
qed
(*by (smt One_nat_def Suc_eq_plus1 appendAsColumn_def lm52)*)
{
  fix i fix j assume 
  2: "i<size a" then have "a!i=?aa!i" using nth_append by metis
  moreover assume "j<N" ultimately moreover have "a!i!j=?AA!i!j" using assms(1,3) 2 8 ll07e
  length_append list.size(3,4) trans_less_add1 by (metis)
  ultimately have "A!i!j=?AA!i!j" using assms 2 by metis
}
then have 
4: "\<forall>i j. i<M & j<N \<longrightarrow> A!i!j=?AA!i!j" using assms by blast
have 
3: "\<forall>j<N. A!M!j=?AA!M!j" 
proof -
  { fix nn :: nat
    have ff1: "\<And>as ass. (a @ as # ass) ! M = as"
      using assms(3) by auto
    { assume "nn < N"
      { assume "row ! nn = matr2Fun' A M nn \<and> nn < length ((a @ [row]) ! M)"
        hence "matr2Fun' (a @ [row]) M nn = matr2Fun' A M nn \<and> length (col @ [matr2Fun' A M N]) = length (a @ [row]) \<and> nn < length ((a @ [row]) ! M) \<and> M < length (a @ [row])"
          using ff1 by (simp add: assms(3,6))
        hence "\<not> nn < N \<or> matr2Fun' A M nn = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nn"
          by (metis ll07e) }
      hence "\<not> nn < N \<or> matr2Fun' A M nn = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nn"
        using ff1 by (simp add: assms(5)) }
    hence "\<not> nn < N \<or> matr2Fun' A M nn = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nn"
      by fastforce }
  thus ?thesis
    by meson
qed
have "\<forall>i<M. ?AA!i!N = ?coll!i" using assms(1,3) 8 lm52
proof -
  { fix nn :: nat
    have ff1: "\<And>as ass. length ((as::'a list) # ass) = Suc (length ass)"
      by (metis One_nat_def Suc_eq_plus1 list.size(4))
    have ff2: "\<And>ass n as asa. ass ! n @ [as ! n::'a] = appendAsColumn' (as @ asa) ass ! n \<or> \<not> n < length ass \<or> \<not> n < length as"
      by (metis appendAsColumn_def length_append lm52 nth_append trans_less_add1)
    { assume "nn < Suc M"
      hence "(col @ [matr2Fun' A M N]) ! nn = col ! nn \<and> nn < Suc M \<or> \<not> nn < M \<or> matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn N = (col @ [matr2Fun' A M N]) ! nn"
        by (metis "8" nth_append) (* > 1.0 s, timed out *)
      moreover
      { assume "(col @ [matr2Fun' A M N]) ! nn = col ! nn \<and> nn < Suc M"
        hence "((a @ [row]) ! nn @ [col ! nn]) ! N = (col @ [matr2Fun' A M N]) ! nn \<and> nn < Suc M"
          using `a @ [row] \<noteq> [] \<and> (\<forall>i<length (a @ [row]). length ((a @ [row]) ! i) = N)` `length (a @ [row]) = M + 1` by force
        hence "\<not> nn < M \<or> matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn N = (col @ [matr2Fun' A M N]) ! nn"
          using ff2 ff1 by (metis "8" One_nat_def Suc_eq_plus1 assms(3) length_append list.size(3)) (* > 1.0 s, timed out *) }
      ultimately have "\<not> nn < M \<or> matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn N = (col @ [matr2Fun' A M N]) ! nn"
        by blast }
    hence "\<not> nn < M \<or> matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn N = (col @ [matr2Fun' A M N]) ! nn"
      by linarith }
  thus ?thesis
    by blast
qed
(* length_append nth_append nth_append_length trans_less_add1
unfolding appendAsColumn_def by smt*)
moreover have "\<forall>i<M. col!i=A!i!N" using assms 0 1 nth_append by force ultimately have 
5: "\<forall>i<M. ?AA!i!N = A!i!N" using assms nth_append 8 by metis have 
6: "?AA!M!N=A!M!N" using less_add_one 8 length_append list.size(3,4) One_nat_def 
assms(3) lm52 nth_append_length unfolding appendAsColumn_def Suc_eq_plus1 by (metis)
have 
7: "\<forall>i j. i<M+1 & j<N+1 \<longrightarrow> A!i!j=?AA!i!j" using 3 4 5 6 not_add_less1 linorder_neqE_nat
 less_imp_Suc_add add_Suc add.left_neutral add.commute  Suc_inject unfolding One_nat_def (*by smt*) 
proof -
  { fix nn :: nat and nna :: nat
    { assume "nna \<noteq> N"
      moreover
      { assume "nna < N"
        hence "matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nna = matr2Fun' A M nna"
          by (metis (no_types) "3") }
      ultimately have "nna < Suc N \<longrightarrow> matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nna = matr2Fun' A M nna"
        using less_antisym by blast }
    moreover
    { assume "matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) M nna = matr2Fun' A M nna"
      moreover
      { assume "nn \<noteq> M"
        moreover
        { assume "nn < M"
          { assume "nna \<noteq> N \<and> nn < M"
            moreover
            { assume "nna < N \<and> nn < M"
              hence "(\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
                by (metis (full_types) "4") (* > 1.0 s, timed out *) }
            ultimately have "nna < Suc N \<longrightarrow> (\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
              by (meson linorder_neqE_nat not_less_eq) }
          hence "nna < Suc N \<longrightarrow> (\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
            using "5" 
using `nn < M` by fastforce }
        ultimately have "nna < Suc N \<longrightarrow> (\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
          by linarith }
      ultimately have "nna < Suc N \<longrightarrow> (\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
        by fastforce }
    ultimately have "(\<not> nn < M + Suc 0 \<or> \<not> nna < N + Suc 0) \<or> matr2Fun' A nn nna = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) nn nna"
      by (metis "6" One_nat_def Suc_eq_plus1) }
  thus "\<forall>n na. n < M + Suc 0 \<and> na < N + Suc 0 \<longrightarrow> matr2Fun' A n na = matr2Fun' (appendAsColumn' (col @ [matr2Fun' A M N]) (a @ [row])) n na"
    by meson
qed
have "A=?AA" using assms(4) 0 assms(2) 1 7 by (rule ll07) 
thus ?thesis by blast
qed

lemma (*lm79a:*) (* cmp lm39a lm39b *)
assumes "size col=N" "size row=N" "size a=N"
"isRectangular4' N a" 
"cond2' (matr2Fun' a) N (nth row)"
  (* = cond2' (addCol' (matr2Fun' a) N (nth col)) N (nth row)*) shows
"isRowTransitiveAccordingToMatrix4' a row" 
using assms(1,2,3,4,5) mem_Collect_eq subsetI unfolding lm13 by (smt)

lemma lm79b: assumes "size row=N" "size a=N" "isRectangular4' N a" shows
"isRowTransitiveAccordingToMatrix4' a row = cond2' (matr2Fun' a) N (nth row)"
using mem_Collect_eq assms(3) lessThan_iff subsetCE subsetI
unfolding lm13 comp_def comp_apply atLeast0LessThan assms(1,2) by auto

lemma lm79c: "cond2' f N g = cond2' (addCol' f N h) N g" by auto

lemma lm79e: assumes "isRowTransitiveAccordingToMatrix4' a row" shows "isRowTransitiveAccordingToMatrix' a row"
using assms lm20 by (metis (no_types, lifting) preorder.lm21)

lemma lm79f: assumes "size row\<le>N" "size a=N" "isRowTransitiveAccordingToMatrix' a row" shows 
"isRowTransitiveAccordingToMatrix4' a row"
using assms(1,2,3) mem_Collect_eq le_antisym le_trans nat_less_le 
lm13 lm20 lm20b lm25 by (metis (no_types, lifting))

lemma lm79g: assumes "size row \<le> N" "size a=N" shows
" isRowTransitiveAccordingToMatrix a row=  isRowTransitiveAccordingToMatrix4' a row"
using assms lm79f lm79e unfolding isRowTransitiveAccordingToMatrix_def by metis

lemma lm79d: assumes "size row=N" "size a=N" "isRectangular4' N a" shows
"isRowTransitiveAccordingToMatrix4' a row = 
 cond2 (addCol (matr2Fun' a) N (nth col)) N (nth row)"
using assms lm79b unfolding lm79c addCol_def cond2_def by auto

lemma lm79: assumes "size row=N" "size a=N" "isRectangular4' N a" shows
"isRowTransitiveAccordingToMatrix a row = 
 cond2 (addCol (matr2Fun' a) N (nth col)) N (nth row)" (is "?L=?R")
proof -
have "?L = isRowTransitiveAccordingToMatrix4' a row" using assms lm79g by blast
moreover have "...=?R" using assms lm79d by blast ultimately show ?thesis by meson
qed

lemma ll08: "isRectangular4' 0 [[]]" by fastforce

lemma lm81a: assumes "isRectangular4' Nl left" "isRectangular4' Nr right" "size left=size right" shows 
"isRectangular4' (Nl+Nr) (pairWise (op @) left right)" unfolding pairWise_def using assms by simp

lemma lm81b: assumes "isRectangular4' N upper" "isRectangular4' N lower" shows "isRectangular4' N (upper@lower)"
using assms by (simp add: nth_append)

lemma lm82: "allPreorders2 0=[[[]]]" by simp

lemma lm83a: assumes "l\<in>set (possibleRows3' [[]] [])" shows "(l=[])" 
using assms map_nth map_is_Nil_conv concat.simps(1) nth_Cons_0 lm60b
unfolding allSubLists2_def using filterpositions2_def (* by smt *) 
proof -
  assume a1: "l \<in> set (allSubLists2' (if filterpositions2 id [] = [] then tolist (\<lambda>i. True) (length ([[]::bool list] ! 0)) else rowAnd3 (map (op ! [[]]) (filterpositions2 id []))))"
  have "(if filterpositions2 id [] = [] then tolist (\<lambda>n. True) (length ([[]::bool list] ! 0)) else rowAnd3 (map (op ! [[]]) (filterpositions2 id []))) = []"
    by (simp add: filterpositions2_def)
  thus ?thesis
    using a1 lm60b by force
qed

lemma lm83b: assumes "l=[]" shows "l\<in>set (possibleRows3' [[]] [])" 
proof -
  have "[] \<in> set (allSubLists2' (tolist (\<lambda>n. True) 0))"
    using preorder.lm49a by force
  thus ?thesis
    by (simp add: allSubLists2_def assms filterpositions2_def)
qed

lemma lm83: "set (possibleRows3 [[]] [])={[]}" using lm83a lm83b unfolding possibleRows3_def by blast

lemma lm84: "appendAsColumn' [True] [[],[]] = [[True]]" unfolding pairWise_def by auto

lemma lm85a: assumes "A\<in>set (allPreorders2 (Suc 0))" shows "(A=[[True]])" 
using assms(1) unfolding lm45d using appendAsColumn_def lm82 lm83 lm84 append_Cons 
append_Nil empty_iff empty_set insert_iff length_greater_0_conv list.simps(15) nth_Cons_0
by (metis)

lemma lm85b: "[[True]] \<in> set (allPreorders2 (Suc 0))" 
proof -
let ?a="[[]]" let ?col="[]" let ?row="[]"
have "isColTransitiveAccordingToMatrix ?a ?col" unfolding isColTransitiveAccordingToMatrix_def by simp 
moreover have "isRowTransitiveAccordingToMatrix ?a ?row" unfolding isRowTransitiveAccordingToMatrix_def by auto
ultimately show ?thesis using lm83 lm84 appendAsColumn_def lm45d lm82
append_Cons append_Nil empty_set list.set_intros(1) list.simps(15) nth_Cons_0
by (metis)
qed

lemma lm85: "set (allPreorders2 1) = {[[True]]}" using lm85a lm85b unfolding One_nat_def by blast

lemma ll09: assumes "size l>0" shows "l=[hd l]@tl l" using assms by force
lemma ll13: assumes "size l\<le>1" shows "size (tl l)=0" using assms by auto
corollary ll14: assumes "size l=1" shows "l=[l!0]" using assms ll09 ll13 
impossible_Cons le0 list.collapse not_one_le_zero order_refl by (metis (no_types) hd_conv_nth)
lemma ll15: assumes "size a=1" "isRectangular4' 1 a" shows "a=[[a!0!0]]" using assms ll14 ll14 by force
lemma lm85c: assumes "size a=1" "isRectangular4' 1 a" "refl01 (matr2Fun' a) 1" shows "a=[[True]]" 
using assms ll15 unfolding refl01_def by force

lemma lm85d: "set (allPreorders2 1) \<subseteq> 
  {a| a. 1=size a & isRectangular4' 1 a & trans01 (matr2Fun' a) 1 & refl01 (matr2Fun' a) 1}"
unfolding lm82 using assms lm85 unfolding trans01_def refl01_def by fastforce

lemma lm85e: "{a| a. 1=size a & isRectangular4' 1 a & trans01 (matr2Fun' a) 1 & refl01 (matr2Fun' a) 1}
\<subseteq> {[[True]]}" using lm85c by auto

lemma lm85f: "set (allPreorders2 1) = 
  {a| a. 1=size a & isRectangular4 1 a & trans01 (matr2Fun' a) 1 & refl01 (matr2Fun' a) 1}"
using lm85 lm85d lm85e unfolding isRectangular4_def by blast

lemma lm80a: assumes "\<forall>a \<in> set(allPreorders2 N). (size a=N & isRectangular4' N a)" 
"A \<in> set(allPreorders2 (Suc N))" "N>0" shows "size A=N+1" 
using assms(2) unfolding lm45d using assms(1,3) ll07b One_nat_def Suc_eq_plus1 
appendAsColumn_def length_append list.size(3) list.size(4) by (metis(no_types))

lemma lm80b: assumes "\<forall>a \<in> set(allPreorders2 N). (size a=N & isRectangular4' N a)" 
"A \<in> set(allPreorders2 (Suc N))" 
"N>0" shows "isRectangular4' (N+1) A" 
proof -
obtain a col row where 
1: "a\<in>set(allPreorders2 N) & isColTransitiveAccordingToMatrix a col &
row \<in> set (possibleRows3 a col) & isRowTransitiveAccordingToMatrix a row &
size col = size (a!0) & A=appendAsColumn (col@[True]) (a@[row])" using assms(2) lm45d by blast
have "size row=N" using assms 1 by (metis cond1Equiv isRectangular4_def less_or_eq_imp_le)
then have "isRectangular4' N (a@[row])" using 1 assms ll07d by metis
moreover have "size (a@[row])=N+1" using 1 assms length_append by force
moreover have "size (col@[True])=N+1" using 1 assms length_append by auto
ultimately show ?thesis using assms ll07c 1 appendAsColumn_def by metis
qed

lemma lm80c: assumes "\<forall>a \<in> set(allPreorders2 N). (size a=N & isRectangular4' N a)" 
"A \<in> set(allPreorders2 (Suc N))" shows "size A=Suc N & isRectangular4' (Suc N) A"
using assms lm80a lm80b length_0_conv neq0_conv Suc_eq_plus1
unfolding lm45d by (metis(no_types))

lemma lm80d: assumes "\<forall>a \<in> set(allPreorders2 N). (size a=N & isRectangular4' N a)" shows  
"\<forall>A \<in> set(allPreorders2 (Suc N)). (size A=Suc N & isRectangular4' (Suc N) A)"
using assms lm80c by fast

lemma lm80e: "\<forall>a \<in> set(allPreorders2 (Suc N)). (size a=(Suc N) & isRectangular4' (Suc N) a)"
apply (induct N) unfolding lm85 
apply (metis One_nat_def Suc_eq_plus1 less_one list.distinct(1) list.size(3) list.size(4) lm85a nth_Cons_0)
using lm80d by presburger

lemma lm86: assumes "N>0" 
"set (allPreorders2 N) = {a| a. N=size a & isRectangular4 N a & 
  trans01 (matr2Fun' a) N & refl01 (matr2Fun' a) N}" 
shows 
"set (allPreorders2 (Suc N)) = {a| a. (Suc N)=size a & isRectangular4 (Suc N) a & 
  trans01 (matr2Fun' a) (Suc N) & refl01 (matr2Fun' a) (Suc N)}" 
proof - have
9: "N>0" using assms(1) by simp
let ?P=allPreorders2 let ?L="set o ?P" let ?r=isRectangular4' let ?t=trans01
let ?m=matr2Fun' let ?R="% N. ({a| a. N=size a & isRectangular4' N a & trans01 (matr2Fun' a) N
& refl01 (matr2Fun' a) N})"
let ?Q="%N. ?L N = ?R N" have
0: "?Q N" using assms(2) unfolding isRectangular4_def by force
{
  fix A assume 
  60: "A\<in>?L (N+1)" then obtain a col row where 
  1: "a\<in>set(allPreorders2 N) & isColTransitiveAccordingToMatrix a col & 
  row \<in> set (possibleRows3 a col) & isRowTransitiveAccordingToMatrix a row &
  size col = size (a!0) & A=appendAsColumn (col@[True]) (a@[row])" using lm45d by auto
  let ?A="matr2Fun' A" let ?a="matr2Fun' a" let ?col="nth col" let ?row="nth row" have 
  8: "size a=N & isRectangular4 N a" unfolding isRectangular4_def using 1 0 mem_Collect_eq
  unfolding comp_apply by force then have 
  20: "size col=N" using 1 unfolding isRectangular4_def by force have 
  41: "refl01 ?a N" using 0 1 unfolding refl01_def by simp have 
  42: "trans01 ?a N" using 1 0 by fastforce moreover have 
  40: "condCol ?a N ?col" using 0 1 lm51 
  unfolding isColTransitiveAccordingToMatrix_def condCol_def by fastforce have 
  43: "size row=N & cond1 (addCol ?a N ?col) N ?row" using 1 cond1Equiv isRectangular4_def 20 8 by force
  have 
  45: "isRectangular4' N a" using 8 unfolding isRectangular4_def by fast then have 
  "cond2 (addCol ?a N ?col) N ?row" using 1 8 lm79 43 by blast then have 
  44: "refl01 (enlarge2' ?a N ?col ?row) (N+1) & 
       trans01 (enlarge2' ?a N ?col ?row) (N+1)" using 40 41 42 43 lm56 by presburger
  (* apply inductiveStepThm to conclude trans01 (enlarge ?a). 
   This should imply trans01 (matr2Fun (appendAsColumn ...)) ???? *)
  then have 
  "\<forall> i j. (i<N+1 & j<N+1) \<longrightarrow>(matr2Fun' (appendAsColumn (col@[True]) (a@[row]))) i j = (enlarge2' ?a N (?col) (?row)) i j"
  using 8 20 43 assms lm53 unfolding isRectangular4_def by presburger then moreover have 
  4: "trans01 (matr2Fun' A) (N+1)" using 1 44 unfolding trans01_def by blast ultimately have 
  5: "refl01 (matr2Fun' A) (N+1)" using 0 1 by (simp add: refl01_def) have 
  "isRectangular4' N (a@[row])" using 8 43 ll07d unfolding isRectangular4_def by blast then have 
  3: "isRectangular4' (N+1) A"  using 1 8 by (metis Suc_eq_plus1 60 comp_apply lm80e)
  have "N+1 = size (a@[row])" using 8 length_append by simp moreover have 
  "... = size (appendAsColumn' (col@[True]) (a@[row]))" using ll07b 20 8 length_append by (metis list.size(3) list.size(4))
  ultimately have "N+1 = size A" using 1 unfolding appendAsColumn_def by presburger
  then have "A \<in> ?R (N+1)" using 3 4 5 by blast
} then have 
52: "?L (N+1) \<subseteq> ?R (N+1)" by blast
{
  fix A assume "A \<in> ?R (N+1)" then have
  10: "N+1=size A & isRectangular4' (N+1) A & trans01 (matr2Fun' A) (N+1) & refl01 (matr2Fun' A) (N+1)" by fast
  let ?A="matr2Fun' A" let ?a="fun2Matr' N N ?A" let ?aa="[[A!i!j. j<-[0..<N]]. i<-[0..<N]]"
  let ?row="% j. ?A N j" let ?Row="[?A N j. j<-[0..<N]]" let ?roww="nth ?Row" 
  let ?col="% i. ?A i N" let ?Col="[?A i N. i<-[0..<N]]" let ?coll="nth ?Col"
  let ?AA="enlarge2' ?A N ?col ?row" have 
  26: "?A N N = True" using 10 refl01_def by (meson less_add_one)
  then moreover have "?A=?AA" using 10 by auto
  then moreover have "trans01 ?AA (N+1) & refl01 ?AA (N+1)" using 10 by force
  then moreover have 
  11: "refl01 ?A N & trans01 ?A N & condCol ?A N ?col & 
    cond1 (addCol ?A N ?col) N ?row & cond2 (addCol ?A N ?col) N ?row" using 10 lm56 by meson
  moreover have 
  25: "\<forall> i j. i<N & j<N \<longrightarrow> (?A i j = matr2Fun' ?a i j)" by force then moreover have 
  23: "trans01 (matr2Fun' ?a) N" using 11 lm54 unfolding trans01_def by blast moreover have 
  24: "refl01 (matr2Fun' ?a) N" using 11 unfolding refl01_def by auto ultimately moreover have 
  21: "size ?a=N" using 10 by (meson preorder.lm35) ultimately have 
  22: "isRectangular4' N ?a" using 
  9 
  lm58 by auto
  have "isRectangular4' N ?a" using 22 by blast
  moreover have "isRectangular4' (N+1) A" using 10 by fast
  moreover have "size ?a=N" by simp
  moreover have "size A=N+1" using 10 by presburger
  moreover have "?Row=[A!N!j. j<-[0..<N]]" by blast
  moreover have "?Col=[A!i!N. i<-[0..<N]]" by blast
  moreover have "\<forall>i j. i<N & j<N \<longrightarrow> ?a!i!j = A!i!j" by simp 
  ultimately have  "A=appendAsColumn' (?Col@[A!N!N]) (?a@[?Row])" by (rule ll07f) then have 
  31: "A=appendAsColumn (?Col@[True]) (?a@[?Row])" using 26 unfolding appendAsColumn_def by simp
  moreover have "?a \<in> ?R N" using trans01_def refl01_def 21 22 23 24 by auto then moreover have 
  23: "?a \<in> ?L N" using 0 by presburger
  moreover have "condCol' (matr2Fun' ?a) N ?col" using 11 unfolding condCol_def using 25 by blast  
  ultimately moreover have "condCol' (matr2Fun' ?a) N (nth ?Col)" using 21 add.left_neutral
  by (metis (no_types, lifting) length_map length_upt nth_map_upt) ultimately moreover have 
  24: "size ?Col = size ?a" using 10 by (metis preorder.lm35) ultimately moreover have 
  "isRectangular4' N ?a" using 10 by blast ultimately moreover have 
  29: "isColTransitiveAccordingToMatrix ?a ?Col" 
  using 21 lm51 by presburger ultimately moreover have 
  51: "\<forall>j<N. ?roww j = ?row j" using 21 by (metis(no_types) add.left_neutral length_map nth_map nth_upt)
  have "cond1' (addCol' ?A N ?col) N ?row" using 11 unfolding addCol_def cond1_def by blast
  then have "cond1' (addCol' (matr2Fun' ?a) N ?col) N ?row" using 10 by force
  then have "cond1 (addCol (matr2Fun' ?a) N ?coll) N ?roww" 
  unfolding addCol_def cond1_def using 10 by force then have 
  50: "size ?Row=N & cond1 (addCol (matr2Fun' ?a) N ?coll) N ?roww" 
  using lm35 by blast moreover have 
  39: "size ?Col=N" using lm35 by blast then moreover have 
  "size ?Col\<le>size ?a" using 21 by simp ultimately have
  30: "?Row \<in> set (possibleRows3 ?a ?Col)" using cond1Equiv unfolding isRectangular4_def using 22 by meson
  have "cond2' (addCol' ?A N ?col) N ?row" using 11 unfolding addCol_def cond2_def by satx 
  then have "cond2' (addCol' (matr2Fun' ?a) N ?col) N ?row" using 10 by (metis "25")
  then have "cond2 (addCol (matr2Fun' ?a) N ?coll) N ?roww" 
  unfolding addCol_def cond2_def using 51 26 by metis then have 
  32: "isRowTransitiveAccordingToMatrix ?a ?Row" using lm79 lm35 21 22 11 by blast have 
  33: "size ?Col = size (?a!0)" using 10 using "22" by auto have 
  27: "A \<in> ?L(N+1)" using 23 29 30 32 33 31 lm45d by fastforce 
}
then have "?L(N+1)=?R(N+1)" using 52 by blast
then show ?thesis unfolding isRectangular4_def by force
qed

lemma lm86b: assumes  
"set (allPreorders2 (N+1)) = {a| a. (N+1)=size a & isRectangular4 (N+1) a & 
  trans01 (matr2Fun' a) (N+1) & refl01 (matr2Fun' a) (N+1)}" 
shows 
"set (allPreorders2 (Suc N+1)) = {a| a. (Suc N+1)=size a & isRectangular4 (Suc N+1) a & 
  trans01 (matr2Fun' a) (Suc N+1) & refl01 (matr2Fun' a) (Suc N+1)}" 
using assms  Suc_eq_plus1 lm86 zero_less_Suc by presburger

lemma lm86c: "set (allPreorders2 (N+1)) = {a| a. (N+1)=size a & isRectangular4 (N+1) a & 
  trans01 (matr2Fun' a) (N+1) & refl01 (matr2Fun' a) (N+1)}" apply (induct N) 
using lm85f apply force using lm86b by presburger

theorem correctnessThm: assumes "N>0" shows "set (allPreorders2 N) = 
    {a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun' a) N & refl01 (matr2Fun' a) N}"
using assms lm86c Suc_eq_plus1 less_imp_Suc_add by (metis (no_types, lifting) Collect_cong)

















subsection{* Distinctness *}

lemma ll16a: assumes "(isRectangular4' N1 matr1 & isRectangular4' N1 matr2)\<or>
(isRectangular4' N2 matr3 & isRectangular4' N2 matr4)" 
"size matr1=M" "size matr2=M" "size matr3=M" "size matr4=M" 
"pairWise (op @) matr1 matr3 = pairWise (op @) matr2 matr4" shows "matr1=matr2"
using assms(1,2,3,4,5,6) unfolding pairWise_def using nth_equalityI by fastforce

lemma ll16b: assumes "(isRectangular4' N1 matr1 & isRectangular4' N1 matr2)\<or>
(isRectangular4' N2 matr3 & isRectangular4' N2 matr4)" 
"size matr1=M" "size matr2=M" "size matr3=M" "size matr4=M" 
"pairWise (op @) matr1 matr3 = pairWise (op @) matr2 matr4" shows "matr1=matr2 & matr3=matr4"
proof -
have "matr1=matr2" using assms(1,2,3,4,5,6) by (rule ll16a)
then moreover have "\<forall>i<M. matr3!i=matr4!i" using assms(3,4,5,6) unfolding pairWise_def by auto
ultimately show ?thesis using assms(4,5,6) list_eq_iff_nth_eq by (metis)
qed

lemma ll18: assumes "distinct (map set l)" shows "distinct l" using assms 
distinct_map by (metis)

lemma ll17: assumes "distinct l" shows "inj_on set (set (sublists l))" using assms 
using distinct_map distinct_set_sublists by blast

lemma ll19: assumes "distinct l" shows "distinct (sublists l)" using assms distinct_set_sublists 
ll18 distinct_map by blast

lemma ll20: "filterpositions2 P l= map id (filter (P o (nth l)) [0..<size l])" (is "?L=?R") 
proof -
have "?L=[id n. n<-[0..<size l], (P o (nth l)) n]" unfolding filterpositions2_def 
by (metis comp_apply id_apply) then show ?thesis using ll10b by metis
qed

lemma ll21: "distinct (filterpositions2 f l)" unfolding ll20 by simp

lemma ll22: "inj_on ((op @) l) UNIV" by (meson injI same_append_eq)

abbreviation "update5' l f xs == map (%i. if i\<in>set xs then f i else l!i) [0..<size l]"
definition "update5 = update5'"

lemma ll24: assumes "i<size l" "j<size l" "i\<noteq>j" "l!i=l!j" shows "\<not> distinct l" using assms
using distinct_conv_nth by blast

lemma ll25: assumes "l\<in>set (sublists L)" shows "set l \<subseteq> set L" 
using assms sublists_powset by (metis PowD imageI)

lemma ll26: assumes "distinct l" "x \<notin> set l" shows "distinct (x#l)" using assms by (metis distinct.simps(2))
(*
lemma assumes "l\<in>set (sublists L)" "i<size l" shows "EX j<size L. l!i=L!j" sorry 
*)
lemma ll27: assumes "\<forall> xs\<in>set(sublists L). distinct xs" "l\<in>set(sublists(x#L))" "l\<notin>set(sublists L)" 
"x \<notin> set L" shows "distinct l"
(* using ll25 assms(1,2,3,5) ll26 sublists.simps(2) Un_iff imageE set_append set_map subsetCE by smt *)
proof -
let ?f=sublists let ?LL="?f L" have "l\<in>set (map (Cons x) ?LL @ ?LL)" using assms(2) 
sublists.simps(2) by (metis (no_types)) then have "l\<in>set (map (Cons x) ?LL)" 
using assms(3) by fastforce thus ?thesis using ll25 assms(1,4) by fastforce
qed

lemma ll27a: assumes "\<forall> xs\<in>set(sublists L). distinct xs" "l\<in>set(sublists(x#L))" "x\<notin>set L" shows "distinct l"
using assms ll27 by fast

lemma ll27b: assumes "l\<in>set(sublists [])" shows "distinct l" using assms by simp

lemma ll27c: "distinct L \<longrightarrow> (\<forall> xs\<in>set(sublists L). distinct xs)" 
apply (induct L) using sublists_def apply simp using ll27a by fastforce


lemma ll23: assumes "\<forall> y\<in>set L. f y \<notin> set L" "n<size L" "n\<in>set l1" "n\<notin>set l2" 
"set l1 \<subseteq> set [0..<size L]" "distinct l1 \<or> distinct l2" shows 
"(update5' L (f o (nth L))) l1 \<noteq> (update5' L (f o (nth L))) l2" 
using assms(1,2,3,4,5,6) unfolding comp_apply map_eq_conv using in_mono nth_mem by (metis(no_types))
lemma ll23b: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" "set ` X \<subseteq> Pow (set [0..<size L])" "\<not> set l1 \<subseteq> set l2"
"\<forall>l\<in>X. distinct l" "l1\<in>X" "l2\<in>X" shows "(update5' L (f o (nth L))) l1 \<noteq> (update5' L (f o (nth L))) l2"
proof -
obtain n where "n<size L & n\<in>set l1 & n\<notin>set l2" using assms by fastforce
thus ?thesis using assms ll23 by fast
qed

lemma ll23c: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" "set ` X \<subseteq> Pow (set [0..<size L])" 
"set l1 \<noteq> set l2" "\<forall>l\<in>X. distinct l" "l1\<in>X" "l2\<in>X" shows 
"(update5' L (f o (nth L))) l1 \<noteq> (update5' L (f o (nth L))) l2" (is "?L\<noteq>?R")
proof -
{ assume 4: "\<not>set l1 \<supseteq> set l2" have "?R\<noteq>?L" using assms(1,2,3) 4 assms(5,7,6) by (rule ll23b) }
then have 0: "\<not>set l1 \<supseteq> set l2 \<longrightarrow> ?thesis" by fastforce
{ assume 4: "\<not>set l1 \<subseteq> set l2" have "?L\<noteq>?R" using assms(1,2,3) 4 assms(5,6,7) by (rule ll23b) }
thus ?thesis using 0 assms(4) by blast qed

corollary ll23d: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" "set ` X \<subseteq> Pow (set [0..<size L])" 
"l1 \<noteq> l2" "\<forall>l\<in>X. distinct l" "l1\<in>X" "l2\<in>X" shows 
"(update5 L (f o (nth L))) l1 \<noteq> (update5 L (f o (nth L))) l2" 
using ll23c assms inj_on_def proof - have 4: "set l1 \<noteq> set l2" using assms inj_on_def by metis  
show ?thesis unfolding update5_def using assms(1,2,3) 4 assms(5,6,7) by (rule ll23c) qed

corollary ll23e: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" 
"set ` X \<subseteq> Pow (set [0..<size L])" "\<forall>l\<in>X. distinct l" shows 
"\<forall> l1 l2. (l1\<in>X & l2\<in>X & l1\<noteq>l2) \<longrightarrow> (update5 L (f o (nth L)) l1 \<noteq> update5 L (f o (nth L)) l2)" 
using assms ll23d by metis

corollary ll23f: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" 
"set ` X \<subseteq> Pow (set [0..<size L])" "\<forall>l\<in>X. distinct l" shows "inj_on (update5 L (f o (nth L))) X" 
unfolding inj_on_def using assms ll23e by (metis(no_types,lifting))

lemma ll28: "update4 = update5" unfolding update4_def update5_def by simp

corollary ll23g: assumes "\<forall> y\<in>set L. f y \<notin> set L" "inj_on set X" 
"set ` X \<subseteq> Pow (set [0..<size L])" "\<forall>l\<in>X. distinct l" shows "inj_on (update4 L (f o (nth L))) X" 
using assms ll23f unfolding ll28 by fast

lemma ll34: "\<forall>l\<in> set (sublists (filterpositions2 id l)). distinct l" using ll21 ll27c by blast

lemma ll33: "set ` (set (sublists (filterpositions2 id l))) \<subseteq> Pow (set [0..<size [False. n<-[0..<size l]]])"
by (metis Pow_iff lm35 preorder.lm42 set_upt sublists_powset subsetI subset_trans)

lemma ll32: "inj_on set (set (sublists (filterpositions2 id l)))" using lm35 lm13 by (simp add: ll17 ll21)

lemma ll31: "\<forall> y\<in>set[False. n<-[0..<size l]]. (%m. True) y \<notin> set[False. n<-[0..<size l]]" by force

lemma ll35: assumes "L=[False. n<-[0..<size l]]" "f=(%m. True)" "X=set (sublists (filterpositions2 id l))" 
shows "inj_on (update4 L (f o (nth L))) X" using assms ll31 ll32 ll33 ll34 ll23g by fast

lemma ll36: "distinct (allSubLists2 l)" using ll35 distinct_map ll19 ll21 
proof -
let ?L="[False. n<-[0..<size l]]" let ?f="%m. True" let ?x="(sublists (filterpositions2 id l))" let ?X="set ?x"
let ?g="update4 ?L (?f o (nth ?L))" 
have "distinct ?x" using ll19 ll21 by blast
moreover have "inj_on ?g ?X" using ll35 by blast
ultimately have "distinct (map ?g ?x)" using distinct_map by blast
moreover have "update4 ?L ?f = update4 ?L (?f o (nth ?L))" unfolding update4_def by fastforce
ultimately show ?thesis unfolding allSubLists2_def by presburger
qed

corollary ll36a: "distinct (possibleRows3 matr col)" unfolding possibleRows3_def using ll36 by simp

lemma ll36c: "inj_on (\<lambda> l. matr@[l]) X" unfolding inj_on_def by blast

corollary ll36b: "distinct 
((map (\<lambda> l. matr@[l]) (filter (isRowTransitiveAccordingToMatrix matr) (possibleRows3 matr col))))"
using ll36a ll36c distinct_filter distinct_map by metis

lemma ll36d: assumes "size matr1=M" "size matr2=M" "size col\<ge>M" "i<M" 
"(appendAsColumn col matr1)!i = (appendAsColumn col matr2)!i" shows
"matr1!i = matr2!i" using assms unfolding appendAsColumn_def pairWise_def by fastforce

lemma ll37: "size (pairWise (op @) matr1 matr2)=min (size matr1) (size matr2)" unfolding pairWise_def by simp

lemma ll36e: assumes "size matr1=M" "size matr2=M" "size col\<ge>M" 
"(appendAsColumn col matr1)=(appendAsColumn col matr2)" shows "matr1=matr2"
using assms ll36d by (metis list_eq_iff_nth_eq)

corollary ll36f: assumes "size matr1\<le>size col" "size matr2\<le>size col" "matr1\<noteq>matr2"
shows "(appendAsColumn col matr1 \<noteq> appendAsColumn col matr2)" using assms ll36e 
by (metis (no_types, lifting) appendAsColumn_def length_map lm35 min.absorb1 pairWise_def)

lemma ll36g: "inj_on (appendAsColumn col) {matr| matr. size matr\<le>size col}"
unfolding inj_on_def using ll36f by force

lemma ll38a: assumes "size matr\<le>size col" shows 
"set (map (\<lambda> l. matr@[l]) (filter (isRowTransitiveAccordingToMatrix matr) (possibleRows3 matr col)))
\<subseteq> {a| a. size a\<le>size(col@[True])}" using assms by fastforce

lemma ll38b: assumes "size matr\<le>size col" shows
"inj_on (appendAsColumn (col@[True])) 
(set (map (\<lambda> l. matr@[l]) (filter (isRowTransitiveAccordingToMatrix matr) (possibleRows3 matr col))))"
(is "inj_on ?f (set ?x)") proof -
let ?Y="{a| a. size a\<le>size(col@[True])}" have "inj_on ?f ?Y" using ll36g by blast
moreover have "set ?x \<subseteq> ?Y" using assms ll38a by presburger
ultimately show ?thesis using subset_inj_on by blast
qed

corollary ll38c: assumes "size matr\<le>size col" shows "distinct (children2 matr col)" 
unfolding children2_def using assms ll38b ll36b distinct_map by blast

lemma ll39: "update3' l f (set xs)= update4' l f xs" by simp

lemma lm40: assumes "col\<in>set (allColsTransitiveAccordingToMatrix' matr)" "isRectangular4' (size matr) matr" 
shows "size matr=size col"
using assms by (metis eq_iff filter_is_subset length_greater_0_conv lm49 subset_code(1))

lemma ll40: "map f [ (x, y). x <- l1, y <- l2 x] = [f (x, y). x<-l1, y<-l2 x]" 
apply (induct l1) apply simp by simp

lemma ll42: assumes "distinct l" shows "distinct (map (% r. matr@[r]) l)" using assms ll22 
by (simp add: distinct_map inj_on_def)

lemma ll43a: assumes "col\<noteq>[]" shows "isRectangular4' 1 (myTranspose' col)" using assms by simp

lemma ll43b: "size (myTranspose' col)=size col" by simp

lemma ll16c: assumes "(isRectangular4 N1 matr1 & isRectangular4 N1 matr2)\<or>
(isRectangular4 N2 matr3 & isRectangular4 N2 matr4)" 
"size matr1=M" "size matr2=M" "size matr3=M" "size matr4=M" 
"pairWise (op @) matr1 matr3 = pairWise (op @) matr2 matr4" shows "matr1=matr2 & matr3=matr4"
using assms ll16b unfolding isRectangular4_def pairWise_def by blast

lemma ll44: "inj myTranspose'" by (meson injI list.inj_map list.inject)

lemma lm89a: assumes "matr \<in> set (children2 matr1 col1)" "matr\<in>set (children2 matr2 col2)" 
"size matr1=M" "size matr2=M" "size col1=M" "size col2=M" shows "matr1=matr2 & col1=col2" 
proof -
obtain row1 where 
1: "matr=appendAsColumn' (col1@[True]) (matr1@[row1]) & 
row1\<in>set(filter (isRowTransitiveAccordingToMatrix matr1) (possibleRows3 matr1 col1))" 
using assms(1) unfolding children2_def appendAsColumn_def by auto
obtain row2 where 
2: "matr=appendAsColumn' (col2@[True]) (matr2@[row2]) & 
row2\<in>set(filter (isRowTransitiveAccordingToMatrix matr2) (possibleRows3 matr2 col2))" 
using assms(2) unfolding children2_def appendAsColumn_def by auto
have "isRectangular4 1 (myTranspose' (col1@[True]))" unfolding isRectangular4_def 
using ll43a by (metis Nil_is_append_conv list.distinct(1)) moreover have 
"isRectangular4 1 (myTranspose' (col2@[True]))" unfolding isRectangular4_def 
using ll43a by (metis Nil_is_append_conv list.distinct(1))
ultimately have "(isRectangular4 M (matr1@[row1]) & isRectangular4 M (matr2@[row2])) \<or>
(isRectangular4 1 (myTranspose' (col1@[True])) & isRectangular4 1 (myTranspose' (col2@[True])))"
by blast
moreover have "size (matr1@[row1])=M+1" using assms(3) by simp
moreover have "size (matr2@[row2])=M+1" using assms(4) by simp
moreover have "size (myTranspose' (col1@[True]))=M+1" using assms(5) ll43b by simp
moreover have "size (myTranspose' (col2@[True]))=M+1" using assms(6) ll43b by simp
moreover have "pairWise (op @) (matr1@[row1]) (myTranspose' (col1@[True])) = 
pairWise (op @) (matr2@[row2]) (myTranspose' (col2@[True]))"
using 1 2 unfolding pairWise_def by fast
ultimately have "matr1@[row1] = matr2@[row2] & (myTranspose' (col1@[True]) = myTranspose' (col2@[True]))" 
by (rule ll16c) then moreover have "col1=col2" using myTranspose_def ll44 unfolding inj_on_def by blast
ultimately show ?thesis by blast
qed

lemma lm89b: assumes "set (children2 matr1 col1) \<inter> set (children2 matr2 col2)\<noteq>{}" 
"size matr1=M" "size matr2=M" "size col1=M" "size col2=M" shows "matr1=matr2 & col1=col2"
using assms lm89a by blast

lemma ll45: assumes "\<forall> l1 l2. l1\<in>set L & l2\<in>set L & l1 \<noteq> l2 \<longrightarrow> set (f l1) \<inter> set (f l2)={}" 
"\<forall> l\<in>set L. f l\<noteq>[]" shows "inj_on f (set L)" using assms 
by (metis inf.idem inj_onI set_empty)
(*
lemma (* lm89d: *) "EX col. isColTransitiveAccordingToMatrix matr col"
unfolding isColTransitiveAccordingToMatrix_def
using mem_Collect_eq not_less0 lm13 
list.size(3) by (metis(no_types,lifting) lm50a)

lemma (* lm89e: *) assumes "size (matr!0)=N" "N>0" "size matr>0" shows "EX col. size col=N & isColTransitiveAccordingToMatrix matr col"
unfolding isColTransitiveAccordingToMatrix_def
using assms mem_Collect_eq not_less0 lm13 lm41
list.size(3) lm50a length_map lm35 nth_map by (smt)
*)
lemma lm89c: "set (allSubLists2 l) \<noteq> {}" using allSubLists2_def by (metis(no_types) empty_iff lm64f)

corollary lm89f: "set (possibleRows3 matr col) \<noteq> {}" unfolding possibleRows3_def using lm89c by simp

lemma ll41: assumes "\<forall>l\<in>set L. distinct l & set l\<noteq>{}" 
"\<forall>i j. i<size L & j<size L & i\<noteq>j \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" shows "distinct (concat L)" 
proof - have "distinct L" using assms distinct_conv_nth by (metis inf_idem nth_mem) 
thus ?thesis using assms distinct_concat by (metis lm84e) qed

lemma ll41a: assumes 
"\<forall>l\<in>set L. distinct l" 
"\<forall>i j. i<size L & j<size L & i\<noteq>j & L!i\<noteq>[] & L!j\<noteq>[] \<longrightarrow> (set (L!i) \<inter> set (L!j)={})"
"distinct l" 
"\<forall> l'\<in>set L. l\<noteq>[] & l'\<noteq>[] \<longrightarrow>set l \<inter> set l'={}"
"distinct (concat L)" shows "distinct (concat(l#L))"
using assms distinct_conv_nth inf_idem nth_mem distinct_concat lm84e distinct_def by auto

lemma ll41b: assumes 
"\<forall>l\<in>set (l#L). distinct l" 
"\<forall>i j. i<size (l#L) & j<size (l#L) & i\<noteq>j & (l#L)!i\<noteq>[] & (l#L)!j\<noteq>[] \<longrightarrow> (set ((l#L)!i) \<inter> set ((l#L)!j)={})"
"distinct (concat L)" shows "distinct (concat(l#L))" 
proof -
have "distinct l" using assms(1) by auto moreover have
"\<forall> l'\<in>set L. l\<noteq>[] & l'\<noteq>[] \<longrightarrow>set l \<inter> set l'={}" using assms(2) Suc_less_eq add.right_neutral 
add_Suc_right in_set_conv_nth list.size(4) nth_Cons_0 nth_Cons_Suc zero_less_Suc
by (metis (no_types, hide_lams)) ultimately show ?thesis using assms(3) ll41a by auto
qed

text{* Version of distinct_concat with slightly weaker assumptions. Useful for our purposes. *}
lemma ll41c: assumes "\<forall>l\<in>set L. distinct l" (is "?P1 L")
"\<forall>i j. i<size L & j<size L & i\<noteq>j & L!i\<noteq>[] & L!j\<noteq>[] \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" (is "?P2 L")
shows "distinct (concat L)" 
proof -
let ?Q="%M. ?P1 M & ?P2 M \<longrightarrow> (distinct (concat M))"
have "?Q []" by fastforce moreover have "\<forall> x xs. ?Q xs \<longrightarrow> ?Q (x#xs)" 
proof - {fix x fix xs assume 
0: "?Q xs" 
{ assume 1: "?P1 (x#xs)" then moreover have "?P1 xs" by fastforce assume 2: "?P2 (x#xs)" 
  then moreover have "?P2 xs" by fastforce ultimately have "distinct (concat xs)" using 0 by fastforce
  then have "distinct(concat (x#xs))" using 1 2 ll41b by blast }
then have "?Q (x#xs)" by blast } thus ?thesis by presburger
qed
ultimately have "?Q L" by (rule structInduct) thus ?thesis using assms by blast
qed

corollary ll41d: assumes "M=filter (%x. x\<noteq>[]) L" "distinct M" "\<forall>l\<in>set M. distinct l" 
"\<forall> l1 l2. l1 \<in> (set M) & l2\<in> (set M) & l1\<noteq>l2 \<longrightarrow> set l1 \<inter> set l2={}"
shows "distinct (concat L)" using assms distinct_concat ll47 
by metis

abbreviation "condensed' == filter (%x. x\<noteq>[])"
abbreviation "quasiDistinct' L == distinct (condensed' L)"
(*abbreviation "nonOverlapping' L == is_non_overlapping (set`set L-{{}})"
abbreviation "quasiDistinctAndNonOverlapping' L == \<forall> L1 L2. (L=L1@L2 \<longrightarrow> 
Union (set`set L1) \<inter> Union(set`set L2)={})"
lemma assumes "quasiDistinct' L" "nonOverlapping' L" shows 
"quasiDistinctAndNonOverlapping' L" using assms is_non_overlapping_def sorry*)
abbreviation "quasiInj' f X == inj_on f (X-(f-`{[]}))"
abbreviation "quasiInj2' f X==(\<forall> x y. x\<in>X & y\<in>X & f x\<noteq>[] & f y \<noteq> [] & x\<noteq>y \<longrightarrow> f x \<noteq> f y)"
lemma ll57: "quasiInj' = quasiInj2'" using assms unfolding inj_on_def by blast

corollary ll41e: assumes "distinct (filter (%x. x\<noteq>[]) L)" "\<forall>l\<in>set L-{[]}. distinct l" 
"\<forall> l1 l2. l1 \<in> (set L-{[]}) & l2\<in> (set L-{[]}) & l1\<noteq>l2 \<longrightarrow> set l1 \<inter> set l2={}"
shows "distinct (concat L)" using assms by (simp add: ll41d)

corollary ll41i: assumes "quasiDistinct' L" "\<forall>l\<in>set L. distinct l" 
"\<forall> l1 l2. l1\<in>set L & l2\<in>set L & l1\<noteq>l2 \<longrightarrow> set l1 \<inter> set l2={}" shows
"distinct (concat L)" using assms ll41e by (simp add: ll41e)

lemma ll54a: assumes "distinct l" shows "inj_on (nth l) {0..<size l}" 
unfolding inj_on_def using assms by (meson atLeastLessThan_iff ll24)

lemma ll54b: assumes "inj_on (nth l) {0..<size l}" shows "distinct l" 
unfolding inj_on_def using assms by (metis atLeast0LessThan distinct_conv_nth inj_on_def lessThan_iff)

lemma ll54: "distinct l = (inj_on (nth l) {0..<size l})" using ll54a ll54b by blast

lemma ll55a: assumes "distinct l" "l=l1@l2" shows "set l1 \<inter> set l2={}" using assms 
by (metis distinct_append)

lemma ll55b: assumes "\<forall> l1 l2. l=l1@l2 \<longrightarrow> set l1 \<inter> set l2={}" shows "distinct l" using assms 
by (metis IntI empty_iff list.set_intros(1) not_distinct_conv_prefix)

lemma ll55: "distinct l = (\<forall> l1 l2. l=l1@l2 \<longrightarrow> set l1 \<inter> set l2={})" using ll55a ll55b by metis

lemma ll56: assumes "i<size l" shows "i<size (take (Suc i) l) & (l!i) = (take (Suc i) l)!i & (l!i)=hd(drop i l)" using assms 
by (simp add: hd_drop_conv_nth)

(*
lemma assumes "quasiDistinct' l" shows 
"(\<forall>i j. i<size l & j<size l & i<j & l!i\<noteq>[] & l!j\<noteq>[] \<longrightarrow> l!i\<noteq>l!j)"
using assms distinct_conv_nth comp_def lm84e mem_Collect_eq ll56 nth_mem set_filter ll54 ll55
set_drop_subset_set_drop list.set(2) subset_iff append_take_drop_id filter_append
proof -
{
  fix i j assume 1: "j<size l" moreover assume 2: "i<j" moreover assume 3: "l!i\<noteq>[]" 
  moreover assume 4: "l!j\<noteq>[]" moreover assume  5: "l!i=l!j" ultimately have 
  6: "l!i \<in> set (condensed' (take (Suc i) l))" using ll56 
  Suc_eq_plus1 nth_mem mem_Collect_eq set_filter by (smt Suc_lessD less_trans_Suc) have 
  7: "l!j \<in> set (condensed' (drop j l))" using 1 2 3 5 ll56 Suc_eq_plus1 nth_mem mem_Collect_eq 
  set_filter by (metis (mono_tags, lifting) Cons_nth_drop_Suc insertCI list.set(2)) have 
  "set ((drop j l)) \<subseteq> set ((drop (Suc i) l))" using 2 set_drop_subset_set_drop
  by (simp add: set_drop_subset_set_drop) then have 
  8: "set (condensed' (drop j l)) \<subseteq> set (condensed' (drop (Suc i) l))" by (simp add: subset_iff)
  have "condensed' l = condensed' (take (Suc i) l) @ (condensed' (drop (Suc i) l))" 
  by (metis append_take_drop_id filter_append) moreover have 
  "l!j\<in>set (condensed' (drop (Suc i) l))" using 7 8 by blast ultimately have 
  "\<not>distinct (condensed' l)" using ll55 6 5 by auto
  }
thus ?thesis sorry
qed


lemma assumes
"(\<forall>i j. i<size l & j<size l & i\<noteq>j & l!i\<noteq>[] & l!j\<noteq>[] \<longrightarrow> l!i\<noteq>l!j)"
 shows  "quasiDistinct' l"
using assms distinct_conv_nth comp_def lm84e mem_Collect_eq nth_mem set_filter sorry

lemma (* quasiDistinctConvNth:*) "quasiDistinct' l =
(\<forall>i j. i<size l & j<size l & l!i\<noteq>[] & l!j\<noteq>[] \<longrightarrow> l!i\<noteq>l!j)"
using distinct_conv_nth sorry

lemma assumes "quasiDistinct' L" "L=L1@L2" "l\<in>set L1" "l\<in>set L2" shows "l=[]" using assms 
distinct_append
IntI empty_iff list.set_intros(1) not_distinct_conv_prefix sorry
*)

lemma quasiDistinctMap: assumes "distinct l" "quasiInj2' f (set l)" 
shows "quasiDistinct' (map f l)" using assms distinct_map 
proof - 
let ?c=condensed' let ?l="filter (%x. f x\<noteq>[]) l" have "inj_on f (set ?l)" using assms 
unfolding inj_on_def by auto then show ?thesis by (simp add: assms(1) distinct_map filter_map)
qed

lemma ll46: assumes "L=[f x y. x <- l1, y <-g x]" 
"i<size L" shows 
"EX xx yy. L!i=f xx yy & xx\<in>set l1 & yy\<in>set(g xx)" 
proof -
let ?LL="{f x y| x y. x\<in>set l1 & y\<in>set (g x)}"
have "set L=?LL" using assms(1) by auto
moreover have "L!i \<in> set L" using assms(2) by simp
ultimately show ?thesis by blast
qed

lemma ll49: "[f x y. x <- l, y <-g x] = concat [[f x y. y<-g x]. x<-l]" by simp

lemma lm87a: "allPreorders2 (Suc n) = concat (map (case_prod children2)
(concat (map (%x. List.product [x] (allColsTransitiveAccordingToMatrix x)) (allPreorders2 n))))" 
 by simp

lemma lm87b:
"allPreorders2 (Suc n)=concat
[children2 matr col. matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr]"
proof - have
"concat (map (case_prod children2) [ (matr, col). matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr]) 
= concat [ (case_prod children2) (matr, col). matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr]"
unfolding ll40 by simp thus ?thesis  by simp
qed

lemma "allPreorders2 (Suc n)=concat
(concat [[ children2 matr col. col<-allColsTransitiveAccordingToMatrix matr]. matr<-allPreorders2 n])" 
using lm87b by blast

lemma lm87c: "allPreorders2 (Suc n)=concat
(concat [map (children2 matr) (allColsTransitiveAccordingToMatrix matr). matr<-allPreorders2 n])" 
using lm87b by blast

lemma "allPreorders2 (Suc n)=
(concat o concat) [map (children2 matr) (allColsTransitiveAccordingToMatrix matr). matr<-allPreorders2 n]" 
unfolding lm87c by simp

lemma "allPreorders2 (Suc n) = (concat o concat o 
(map (%x. map (children2 x) (allColsTransitiveAccordingToMatrix x)))) 
(allPreorders2 n)" 
unfolding lm87c by simp

(*
lemma "(z\<in>set (concat [[f x y. y<-g x]. x<-l])) = 
(EX i<size l. z\<in>set [f (l!i) y. y<-g (l!i)])" sorry

lemma assumes "\<forall> I1 I2. I1 \<inter> I2={} \<longrightarrow> set (sublist L I1) \<inter> set (sublist L I2)={}" shows
"distinct L" using assms sorry
*)

lemma ll50: "List.product l1 l2 = [(x1, x2). x1<-l1, x2<-l2]" apply (induct l1) apply simp by fastforce

lemma ll41f: assumes "distinct l1" "distinct l2" shows 
"distinct [(x1, x2). x1<-l1, x2<-l2]"  using distinct_product assms 
by (metis preorder.ll50)

lemma ll52: assumes "\<forall>x1\<in>X1. f x1\<noteq>[]" shows "inj_on (% x1. [(x1, x2). x2<-f x1]) X1"
unfolding inj_on_def using assms last_map old.prod.inject by (metis ) 

lemma ll41g: assumes "distinct l1" "\<forall>x1\<in>set l1. distinct (f x1)" "\<forall>x1\<in>set l1. f x1\<noteq>[]" shows 
"distinct [(x1, x2). x1<-l1, x2<-f x1]" (is "distinct ?l") using assms ll41f distinct_concat
proof -
have "?l = concat [[(x1, x2). x2<-f x1]. x1<-l1]"(is "?l=concat ?L") by auto have 
"?L=map (% x1. [(x1, x2). x2<-f x1]) l1" (is "?L=map ?f l1") by auto moreover have 
"inj_on ?f (set l1)" using assms(3) ll52 by metis ultimately have 
0: "distinct ?L" using assms(1) distinct_map by fast have 
"\<forall>x1. inj_on (Pair x1) (set (f x1))" unfolding inj_on_def by blast moreover have 
"\<forall>x1. [(x1,x2). x2<-f x1] = map (Pair x1) (f x1)" by auto ultimately have 
"\<forall>x1\<in>set l1. distinct [(x1,x2). x2<-f x1]" using assms(1,2) ll10b distinct_map by blast
then have "\<forall>l\<in>set ?L. distinct l" by simp then moreover have 
"\<forall>l1 l2. l1\<in>set ?L & l2\<in>set ?L & l1\<noteq>l2 \<longrightarrow> set l1 \<inter> set l2={}" by fastforce
ultimately show ?thesis using 0 distinct_concat by fastforce
qed

lemma ll41h: assumes "distinct l1" "\<forall>x1\<in>set l1. distinct (f x1)" shows 
"distinct [(x1, x2). x1<-l1, x2<-f x1]" (is "distinct ?l") 
proof -
let ?l1="filter (%x. f x\<noteq>[]) l1" have "distinct ?l1" using assms using distinct_filter by blast
moreover have "\<forall>x1\<in>set ?l1. distinct (f x1)" using assms by simp moreover have 
"\<forall>x1\<in>set ?l1. f x1 \<noteq> []" by fastforce ultimately have 
0: "distinct [(x1, x2). x1<-?l1, x2<-f x1]" by (rule ll41g) have 
"filter (%x. x\<noteq>[]) (map (\<lambda>x1. map (Pair x1) (f x1)) l1) =
(map (\<lambda>x1. map (Pair x1) (f x1)) [x\<leftarrow>l1 . f x \<noteq> []])" 
apply (induct l1) apply fastforce by simp then have "concat (map (\<lambda>x1. map (Pair x1) (f x1)) l1) 
=concat(map(\<lambda>x1. map(Pair x1) (f x1))[x\<leftarrow>l1. f x \<noteq> []])" using ll47 by (metis (mono_tags,lifting))
thus ?thesis using 0 by presburger
qed

lemma lm89g: assumes "set ((case_prod children2) (matr1,col1)) \<inter> set ((case_prod children2) (matr2,col2))\<noteq>{}" 
"size matr1=M" "size matr2=M" "size col1=M" "size col2=M" shows "matr1=matr2 & col1=col2"
using assms lm89b by simp

corollary lm89h: "quasiInj2' (case_prod children2) {(matr, col)| matr col. size matr=M & size col=M}"
proof -
let ?f="case_prod children2"
{
  fix matr1 col1 matr2 col2 assume "?f (matr1, col1) = ?f (matr2,col2)" (is "?f ?z1=?f ?z2")
  then have "?f ?z1=[] \<or> ?f ?z2=[] \<or> set (?f ?z1) \<inter> set (?f ?z2)\<noteq>{}" by simp
  moreover assume "size matr1=M" "size col1=M" "size matr2=M" "size col2=M" 
  ultimately have "(?f ?z1\<noteq>[] & ?f ?z2\<noteq>[]) \<longrightarrow> (matr1=matr2 & col1=col2)" using lm89g by force
}
then show ?thesis by blast
qed

lemma lm90: "size (allSubLists l) = size (allSubLists2 l)" using allSubLists_def allSubLists2_def 
by (simp add: allSubLists2_def allSubLists_def)

lemma ll58: assumes "set l1 = set l2" "size l1=size l2" "distinct l1" shows "distinct l2" 
using assms card_distinct distinct_card by fastforce

corollary ll36h: "distinct (allSubLists l)" using lm90 ll58 ll36 lm01 
allSubLists2_def allSubLists_def comp_apply by (metis (lifting))

corollary ll36i: "distinct (allColsTransitiveAccordingToMatrix' matr)" using ll36h 
using distinct_filter by blast

lemma lm91a: assumes 
"L=[children2 matr col. matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr]"
"M=filter (%x. x\<noteq>[]) L"
"\<forall>matr\<in>set(allPreorders2 n). size matr=n & distinct (allColsTransitiveAccordingToMatrix matr)
& (\<forall>col\<in>set (allColsTransitiveAccordingToMatrix matr). size col=n)"
"distinct (allPreorders2 n)" shows
"distinct M & (\<forall>l\<in>set M. distinct l) & (\<forall> l1 l2. (l1 \<in> set M & l2 \<in> set M & l1 \<noteq> l2) \<longrightarrow> set l1 \<inter> set l2={})" (is "?t1 & ?t2")
proof -
let ?ap="allPreorders2 n" let ?ac="allColsTransitiveAccordingToMatrix" let ?c=children2
have "L = [(case_prod children2) (matr,col).matr<-?ap, col<-?ac matr]" 
using split_def by (simp add: assms(1)) moreover have 
"...= map (case_prod children2) [(matr, col). matr<-?ap, col<-?ac matr]" using ll40 by metis
ultimately have "L=map (case_prod children2) [(matr, col). matr<-?ap, col<-?ac matr]" by blast
moreover have "distinct [(matr, col). matr<-?ap, col<-?ac matr]" using assms ll41h by blast
moreover have "quasiInj2' (case_prod children2) {(matr, col)| matr col. matr\<in>set ?ap & col\<in>set(?ac matr)}"
using lm89h assms unfolding mem_Collect_eq by (metis(no_types,lifting))
moreover have "set [(matr, col). matr<-?ap, col<-?ac matr] = {(matr, col)| matr col. matr\<in>set ?ap & col\<in>set(?ac matr)}"
by auto ultimately have 
0: ?t1 using assms quasiDistinctMap by (simp add: quasiDistinctMap) moreover have 
3: "\<forall> l\<in>set M. distinct l" using ll38c assms by auto
{
  fix l1 fix l2 assume "l1\<in>set M" then obtain matr1 col1 where
  1: "l1=children2 matr1 col1 & matr1\<in>set ?ap & col1\<in>set(?ac matr1)" using assms by auto then have 
  11: "size matr1=n & size col1=n" using assms by fast assume "l2\<in>set M" then obtain matr2 col2 where
  2: "l2=children2 matr2 col2 & matr2\<in>set ?ap & col2\<in>set(?ac matr2)" using assms by auto then have 
  12: "size matr2=n & size col2=n" using assms by blast assume "l1\<noteq>l2" then have 
  "matr1\<noteq>matr2 \<or> col1\<noteq>col2" using 1 2 by fast then have 
  "set l1 \<inter> set l2={}" using 11 12 1 2 lm89b by blast
}
then show ?thesis using 0 3 assms by blast
qed

lemma lm91b: assumes "n>0" "distinct (allPreorders2 n)" (is "distinct (?f n)") shows "distinct (allPreorders2 (Suc n))" 
proof -
let ?L="[children2 matr col. matr <- allPreorders2 n, col <- allColsTransitiveAccordingToMatrix matr]"
let ?M="filter (%x. x\<noteq>[]) ?L"
have "?f (Suc n) = concat ?L" using lm87b by blast
have "\<forall>matr\<in>set (allPreorders2 n). size matr=n & isRectangular4' n matr" by (metis Suc_pred assms(1) lm80e)
then moreover have "\<forall>matr \<in> set (allPreorders2 n). \<forall> col\<in>set (allColsTransitiveAccordingToMatrix matr). 
size col=n" unfolding allColsTransitiveAccordingToMatrix_def using assms by (metis preorder.lm40)
moreover have "\<forall>matr\<in>set(?f n). distinct (allColsTransitiveAccordingToMatrix matr)" 
using ll36i allColsTransitiveAccordingToMatrix_def by metis
ultimately have
"distinct ?M & (\<forall>l\<in>set ?M. distinct l) & (\<forall> l1 l2. (l1 \<in> set ?M & l2 \<in> set ?M & l1 \<noteq> l2) \<longrightarrow> set l1 \<inter> set l2={})"
using assms lm91a by (metis (no_types, lifting))
then have "distinct (concat ?L)" using ll41d by blast
then show ?thesis using lm87b by simp
qed

lemma lm91c: assumes "distinct (allPreorders2 (n+1))" shows "distinct (allPreorders2 (Suc (n+1)))"
using assms lm91b by fastforce

lemma ll60: assumes "set l={x}" "distinct l" shows "l=[x]" using assms(1,2)
all_partitions_list.cases distinct.simps(2) insert_iff list.simps(15)  singletonD set_empty 
by (metis)

lemma ll61: "allSubLists [True. n<-[0..<0]]=[[]]" (is "allSubLists ?l =[[]]") using allSubLists_def ll36h lm59 lm64f 
proof - 
let ?f=allSubLists have "set (?f ?l)={[]}" using lm64f lm49 allSubLists_def by blast moreover have 
"distinct (?f ?l)" using ll36h by simp ultimately show "?f ?l=[[]]" using ll60 by fast
qed

lemma lm91d: "allSubLists [True. n<-[0..<0]]=[[]] & allSubLists2 [True. n<-[0..<0]]=[[]]"
proof -
let ?l="[True. n<-[0..<0]]"
have "filterpositions2 id [True. n<-[0..<0]] = []" by (simp add: filterpositions2_def)
then moreover have "(sublists (filterpositions2 id [True. n<-[0..<0]])) = [[]]" by auto
ultimately moreover have "allSubLists ?l=[[]]" using ll61 by blast
ultimately moreover have "allSubLists2 ?l=[[]]" unfolding allSubLists2_def  
using length_0_conv length_map  lm62 upt_eq_Nil_conv unfolding map_eq_Cons_conv by metis
ultimately show ?thesis by blast
qed

lemma lm91e: "size [ (matr, col). matr <- allPreorders2 0, col <- allColsTransitiveAccordingToMatrix matr] \<le> 1"
unfolding allColsTransitiveAccordingToMatrix_def
using lm91d by simp

lemma lm91g: "possibleRows3 [[]] []=[[]]" 
unfolding possibleRows3_def filterpositions2_def nth_Cons_0
using concat.simps(1) list.map_disc_iff list.size(3) upt_0 using lm91d by fastforce

lemma lm91h: "size (children2 [[]] [])\<le>1" unfolding children2_def lm91g by force 

lemma lm91f1: "allColsTransitiveAccordingToMatrix [[]] =
filter (isColTransitiveAccordingToMatrix [[]]) [[]]" 
unfolding allColsTransitiveAccordingToMatrix_def using lm91d by auto

lemma lm91f2: "isColTransitiveAccordingToMatrix' [[]] []" by force

lemma lm91f3: "allColsTransitiveAccordingToMatrix [[]] = [[]]" unfolding lm91f1 
using lm91f2 by (simp add: isColTransitiveAccordingToMatrix_def)

lemma lm91j: 
"[(matr, col). matr <- [[[]]], col <- allColsTransitiveAccordingToMatrix matr] = [([[]],[])]"  
using lm91d lm91f3 by simp

lemma lm91k: "size (allPreorders2 1)\<le>1"  
proof -
have "allPreorders2 (Suc 0) = concat (map (case_prod children2) [([[]],[])])" using 
lm91j by simp moreover have "... = concat [children2 [[]] []]" by force
ultimately show ?thesis using lm91h by auto
qed

lemma lm91l: "allPreorders2 1 = [[[True]]]" using lm91k lm85 set_ConsD One_nat_def 
all_not_in_conv le_0_eq le_Suc_eq length_0_conv ll14 lm85b set_empty2 by (metis)

lemma lm91m: "distinct (allPreorders2 (n+1))" apply (induct n) using lm91l 
apply force using lm91c by auto

lemma lm91n: "distinct (allPreorders2 n)" using lm91m One_nat_def distinct_conv_nth 
less_one list.size(3,4) Suc_eq_plus1 by (metis(no_types,lifting) allPreorders2.elims)

lemma lm91: "card o set o allPreorders2 = size o allPreorders2" 
using lm91n distinct_card by fastforce

theorem assumes "N>0" shows "size (allPreorders2 N) = 
card {a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun' a) N & refl01 (matr2Fun' a) N}" 
using assms lm91n correctnessThm distinct_card by (metis (no_types, lifting))























section {* Preorders3 *}
text{* Preorders3 slightly differs from Preorders2, and has comparable performances.
Note that the correctness theorem is only proved for Preorders2. *}

abbreviation "children3' matr col == map (appendAsColumn (col@[True])) 
(map (\<lambda> l. matr@[l]) (filter (isRowTransitiveAccordingToMatrix matr) (possibleRows3 matr col)))"
definition "children3 = children3'"

fun allPreorders3 where 
"allPreorders3 0 = [[[]]]" | 
"allPreorders3 (Suc n) = concat 
(map (case_prod children3) 
 (* (List.product (allPreorders2 n) ((allSubLists [True. n<-[0..<n]]))) *)
[ (matr, col). matr <- allPreorders3 n, col <- allColsTransitiveAccordingToMatrix matr])"

(*value "size (allPreorders3 7)"*)



section{*Translation of formalisms: from adjacency matrices to pairs. *}

abbreviation "adj2PairList' matr == [ (i,j) . i <- [0..<size matr], j<-filterpositions2 id (matr!i)]"
definition "adj2PairList=adj2PairList'"
abbreviation "adj2Pairs1' matr == set [ (i,j) . i <- [0..<size matr], j<-filterpositions2 id (matr!i)]"
abbreviation "adj2Pairs2' matr== {(i,j)| i j. matr!i!j & i<size matr & j<size matr}"
abbreviation "adj2Pairs3' matr== {(i,j)| i j. matr!i!j & i<size matr & j<size (matr!i)}"
definition "adj2Pairs1=adj2Pairs1'"
definition "adj2Pairs2=adj2Pairs2'"
definition "adj2Pairs3=adj2Pairs3'"

abbreviation "pairs2Adj' N R == [[(i,j) \<in> R. j <- [0..<N]]. i<- [0..<N]]"
definition "pairs2Adj=pairs2Adj'"
abbreviation "isSquare' matr == (\<forall> m \<in> {0..<size matr}. length (matr!m)=length matr)"
definition "isSquare = isSquare'"
abbreviation "sizes' == set o (map size)"
abbreviation "isRectangular' matr == ((card o sizes') matr = 1)"

abbreviation "isSquare2' matr == sizes' matr = {size matr}" 
definition "isSquare2 = isSquare2'"

lemma lm02a: assumes "isSquare2' matr" shows "isSquare' matr" using assms comp_def by auto

lemma lm03a: assumes "isSquare2' matr" "(x,y) \<in> adj2Pairs1' matr" shows "x<size matr"
using assms by force
lemma lm03b: assumes "isSquare2' matr" "(x,y) \<in> adj2Pairs1' matr" shows "y<size matr"
using assms lm02a  lm99b by fastforce
lemma lm03: assumes "isSquare' matr" shows "adj2Pairs2' matr \<subseteq> {0..<size matr} \<times> {0..<size matr}"
using assms lm03a lm03b atLeast0LessThan by blast
lemma lm04: assumes "isSquare' matr" shows "adj2Pairs1' matr = adj2Pairs2' matr" 
unfolding filterpositions2_def using assms by auto
lemma lm04b: "adj2Pairs1' = adj2Pairs3'" unfolding filterpositions2_def by fastforce
lemma lm04bb: "adj2Pairs1 = adj2Pairs3" using lm04b unfolding adj2Pairs1_def adj2Pairs3_def 
by blast
lemma lm03c: assumes "isSquare' matr" shows "adj2Pairs1' matr \<subseteq> {0..<size matr} \<times> {0..<size matr}"
using assms lm03 lm04 by (metis (no_types, lifting)) 

lemma lm02b: assumes "isSquare' matr" "matr \<noteq> []" shows "sizes' matr \<supseteq> {size matr}" 
using assms comp_def atLeast0LessThan empty_subsetI insert_subset length_greater_0_conv length_map lessThan_iff nth_map nth_mem
by (metis (no_types, hide_lams))
lemma lm14: assumes "isSquare2' matr" "i<size matr" shows "size (matr!i)=size matr" using assms 
by fastforce
lemma assumes "isSquare' matr" shows "sizes' matr \<subseteq> {size matr}"
using assms atLeast0LessThan comp_def insertCI length_map lessThan_iff lm84e nth_map subset_iff by smt

abbreviation "majorants' matr elem == (set (filterpositions2 id (matr!elem)))"
definition "majorants = majorants'"
abbreviation "majorants2' matr roww == [j. j<-[0..<size (matr!roww)], matr!roww!j]"
definition "majorants2 = majorants2'"
abbreviation "majorants3' matr roww == {j| j. j \<in> {0..<size (matr!roww)} & matr!roww!j}"
definition "majorants3 = majorants3'"
lemma lm15: "set (majorants2' matr roww) = majorants3' matr roww" by force
abbreviation "matr2Rel3' matr== {(i,j)| i j. i<size matr & j \<in> majorants' matr i}"
lemma lm18: "majorants m i = majorants3 m i" unfolding majorants_def majorants3_def lm13 by auto
lemma lm10: assumes "isSquare2' matr" "roww<size matr" shows 
"majorants matr roww = set (majorants2 matr roww)" 
unfolding majorants_def majorants2_def using assms by (simp add: filterpositions2_def)

lemma lm00: assumes "isSquare2' matr" shows "length matr > 0" using assms by fastforce

lemma lm05: assumes "isSquare2' matr" "elem \<in>{0..< size matr}" shows 
"majorants' matr elem \<subseteq> {0..< size matr}"
using assms lm99b lm02a by fastforce

lemma lm11: assumes "x<size matr" "y<size matr" "isSquare2' matr"
shows "matr!x!y = (y \<in> majorants' matr x)"
unfolding lm13 using assms atLeast0LessThan lessThan_iff lm02a unfolding mem_Collect_eq 
by (metis(no_types,lifting))

abbreviation "isTransitive' matr == isSquare2 matr & (\<forall> elem \<in> {0..<size matr}. 
Union ((majorants' matr)`(majorants' matr elem)) \<subseteq> majorants' matr elem)"
definition "isTransitive = isTransitive'"
abbreviation "isTransitive2' matr == isSquare2 matr & 
(\<forall> x y z. (({x,y,z} \<subseteq> {0..<size matr} & matr!x!y & matr!y!z) \<longrightarrow> matr!x!z))"
definition "isTransitive2 = isTransitive2'"
abbreviation "isTransitive3' matr == isSquare2 matr & 
(\<forall> x y z. ((x<size matr & y<size matr & z<size matr & matr!x!y & matr!y!z) \<longrightarrow> matr!x!z))"
definition "isTransitive3 = isTransitive3'"
lemma lm16e: assumes "isTransitive' matr" shows "isTransitive2' matr" using assms lm11 
unfolding isSquare2_def
by fastforce
(*
lemma lm16a: assumes "isTransitive3' matr" "x < size matr" "y \<in> majorants3 matr x"
shows "majorants3 matr y \<subseteq> majorants3 matr x" using assms isSquare2_def majorants3_def
Collect_mono atLeast0LessThan lessThan_iff lm02a mem_Collect_eq by (smt)

lemma lm17: "isTransitive2 = isTransitive3" unfolding isTransitive2_def isTransitive3_def by fastforce

lemma lm16b: assumes "isTransitive3' matr" "x < size matr" "y \<in> majorants matr x"
shows "majorants matr y \<subseteq> majorants matr x" using assms lm16a lm18 by blast

lemma lm16c: assumes "isTransitive3' matr" "x \<in> {0..<size matr}" 
shows "Union ((majorants matr)`(majorants matr x)) \<subseteq> majorants matr x"
using lm18
using assms lm16b  majorants_def majorants3_def lm05
 atLeastLessThan_iff
by (smt Sup_least imageE)

lemma lm16d: assumes "isTransitive3' matr" shows "isTransitive' matr" 
using assms lm16c lm17 majorants_def by metis

lemma (* lm16f: *) assumes "isTransitive3 matr" shows "isTransitive matr" using assms lm16d
unfolding isTransitive3_def isTransitive_def by blast
*)
lemma lm19a: "trans R = (\<forall> x. x \<in> Domain R \<longrightarrow> R `` (R``{x}) \<subseteq> R``{x})" using trans_def by fast

lemma lm19b: "trans R = (\<forall> x. R `` (R``{x}) \<subseteq> R``{x})" using trans_def by fast

lemma lm27: "trans = trans o converse" by fastforce

lemma lm19c: "trans R = (\<forall> y. (converse R)``((converse R)``{y}) \<subseteq> (converse R)``{y})" 
using trans_def lm19b by fast

lemma lm19d: "trans R = (\<forall> y. y \<in> Range R \<longrightarrow> (converse R)``((converse R)``{y}) \<subseteq> (converse R)``{y})" 
using trans_def lm19b by fast

lemma lm92a: assumes "size a=N" "isRectangular4' N a" "(trans01 (matr2Fun' a) N)" shows 
"trans (adj2Pairs3' a)" unfolding trans_def using assms unfolding mem_Collect_eq prod.inject 
trans01_def by metis

lemma lm92b: assumes "size a=N" "isRectangular4' N a" "trans (adj2Pairs3' a)" 
shows "(trans01 (matr2Fun' a) N)" 
proof -
let ?A="matr2Fun' a" let ?R="adj2Pairs3' a"
{
  fix i j k assume "i<N" "j<N" "k<N" "?A i j" "?A j k" then have "(i,j) \<in> ?R & (j,k) \<in> ?R"
  using assms(1,2) by force then have "(i,k) \<in> ?R" using assms(3) unfolding trans_def by meson 
  then have "?A i k" using assms by blast
}
thus ?thesis unfolding trans01_def by blast
qed

lemma lm92c: assumes "size a=N" "isRectangular4 N a" shows 
"trans (adj2Pairs3' a) = (trans01 (matr2Fun' a) N)" using assms unfolding isRectangular4_def 
using lm92a lm92b by blast

lemma lm92d: assumes "size a=N" "isRectangular4' N a" "refl01 (matr2Fun' a) N" 
shows "refl_on {0..<N} (adj2Pairs3' a)" unfolding refl_on_def using assms unfolding refl01_def 
by fastforce

lemma lm92e: assumes "size a=N" "isRectangular4' N a" "refl_on {0..<N} (adj2Pairs3' a)" 
shows "refl01 (matr2Fun' a) N" using assms unfolding refl01_def by (simp add: refl_on_def)

lemma lm92f: assumes "size a=N" "isRectangular4 N a" shows  
"(refl_on {0..<N} (adj2Pairs3' a))= refl01 (matr2Fun' a) N"
using assms lm92d lm92e unfolding isRectangular4_def by blast

lemma lm92g: assumes "size a=N" "isRectangular4 N a" shows 
"(refl_on {0..<N} (adj2Pairs3 a))= refl01 (matr2Fun' a) N & 
(trans (adj2Pairs3 a) = (trans01 (matr2Fun' a) N))" using assms lm92f lm92c lm04b
unfolding adj2Pairs3_def by simp

lemma lm92gg: assumes "size a=N" "isRectangular4 N a" shows 
"(refl_on {0..<N} (adj2Pairs1 a))= refl01 (matr2Fun' a) N & 
(trans (adj2Pairs1 a) = (trans01 (matr2Fun' a) N))" using assms lm92g
unfolding lm04bb by simp

abbreviation "allPreordersPairs' == (map adj2Pairs1) o allPreorders2"
definition "allPreordersPairs = allPreordersPairs'"
(*
lemma assumes "trans r" "refl_on {0..<N} r" "Field r = {0..<N}" 
"a=pairs2Adj' N r"
shows "r=adj2Pairs3' a" using assms sorry
*)
lemma lm92i: assumes "i<N" "j<N" shows "(pairs2Adj N r)!i!j = ((i,j)\<in>r)"
unfolding pairs2Adj_def using assms by force

lemma assumes "N>0" shows "(isRectangular4' N (pairs2Adj' N r))"
using assms pairs2Adj_def by simp
lemma lm92j: "size (pairs2Adj N r)=N & (N>0 --> isRectangular4' N (pairs2Adj N r))" 
unfolding pairs2Adj_def using assms by simp

lemma lm92k: assumes "Field r \<subseteq> {0..<N}" shows "(adj2Pairs3' (pairs2Adj N r)) \<supseteq> r"
using assms unfolding Field_def pairs2Adj_def by fastforce
lemma lm92l: "(adj2Pairs3' (pairs2Adj N r)) \<subseteq> r" unfolding pairs2Adj_def by auto
lemma lm92m: assumes "Field r \<subseteq>{0..<N}" shows "adj2Pairs3 (pairs2Adj N r)=r" using assms lm92k lm92l 
unfolding adj2Pairs3_def by blast

lemma ll59: "(R\<subseteq>A \<times> A)=(Field R \<subseteq> A)" (is "?L=?R") unfolding Field_def by fast

lemma lm92h: assumes "N>0" shows "set (allPreordersPairs N) \<subseteq>
{r| r. trans r & refl_on {0..<N} r}"
unfolding allPreordersPairs_def
using assms correctnessThm lm92gg by force

lemma lm92n: "{r| r. trans r & refl_on {0..<N} r} = {r| r. Field r \<subseteq> {0..<N} & trans r & refl_on {0..<N} r}"
unfolding refl_on_def ll59 by blast

lemma lm92o: assumes "N>0" shows "set (allPreordersPairs' N) \<supseteq>
{r| r. Field r \<subseteq> {0..<N} & trans r & refl_on {0..<N} r}" (is "?L \<supseteq> ?R") 
using assms correctnessThm lm92m lm92j mem_Collect_eq
proof -
let ?f="pairs2Adj' N" let ?g="adj2Pairs3'"
let ?X="{a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun' a) N & refl01 (matr2Fun' a) N}"
{
  fix r let ?a="?f r" assume "r\<in>?R" then moreover have 
  0: "Field r \<subseteq> {0..<N} & trans r & refl_on {0..<N} r & r=?g (?f r)" using lm92m 
  unfolding pairs2Adj_def adj2Pairs3_def by force ultimately have "size ?a=N" by simp 
  then moreover have "isRectangular4 N ?a" using assms unfolding isRectangular4_def by auto 
  moreover have "trans (?g ?a)" using 0 by presburger ultimately moreover have 
  "trans01 (matr2Fun' ?a) N" using assms 0 lm92c by fast moreover have 
  "refl_on {0..<N} (?g ?a)" using 0 by presburger ultimately moreover have 
  "refl01 (matr2Fun' ?a) N" using lm92f by blast ultimately moreover have "?a\<in>?X" by simp 
  ultimately moreover have "?g ?a = adj2Pairs1 ?a" using lm04b unfolding adj2Pairs1_def by metis 
  ultimately have "r\<in>adj2Pairs1`?X" using 0 by auto then have "r\<in>?L" using correctnessThm assms by auto
}
thus ?thesis by blast
qed

lemma lm92p: assumes "N>0" shows "set (allPreordersPairs N) \<supseteq> {r| r. trans r & refl_on {0..<N} r}"
unfolding allPreordersPairs_def
using assms lm92o unfolding lm92n by presburger  

lemma lm92r: "{r| r. trans r & refl_on {(0::nat)..<0} r} = {{}}" (is "?L=?R") 
proof -
have "\<forall>r. refl_on {} r \<longrightarrow> r={}" using refl_on_def by blast then have "?L\<subseteq> ?R" by auto
moreover have "trans {} & refl_on {} {}" unfolding trans_def refl_on_def by blast
ultimately show ?thesis by force
qed

lemma lm92q: "map (adj2Pairs3') (allPreorders2 0) = [{}]" unfolding adj2Pairs1_def lm82 by auto

corollary lm92qq: "allPreordersPairs 0 = [{}]" 
unfolding allPreordersPairs_def adj2Pairs1_def using lm92q lm04b by simp

lemma lm92s: "set (allPreordersPairs 0) = {r| r. trans r & refl_on {(0::nat)..<0} r}"
using lm92r lm92qq by force

theorem correctnessPairs: "set (allPreordersPairs N) = {r| r. trans r & refl_on {0..<N} r}"
using lm92p lm92h lm92s Collect_cong neq0_conv subset_antisym by (metis (no_types, lifting))

lemma lm93a: assumes "size a=M" "size b=M" "isRectangular4' N a" "isRectangular4' N b" 
"adj2Pairs3' a = adj2Pairs3' b" shows "a=b"
proof -
{
  fix i j assume "i<M" "j<N" then have "a!i!j = b!i!j" using assms(1,2,3,4,5) by auto
}
then show "a=b" using assms(1,2,3,4) by (simp add: nth_equalityI)
qed

lemma lm93b: "inj_on adj2Pairs3 {a| a. size a=M & isRectangular4 N a}"
unfolding inj_on_def adj2Pairs3_def isRectangular4_def using lm93a by force

lemma lm93c: assumes "N>0" shows "set (allPreorders2 N) \<subseteq> {a| a. size a=N & isRectangular4 N a}"
using assms correctnessThm by auto 

corollary lm93d: assumes "N>0" shows "inj_on adj2Pairs1 (set (allPreorders2 N))"
proof - 
let ?F=allPreorders2
have "inj_on adj2Pairs1 {a| a. size a=N & isRectangular4 N a}" (is "inj_on adj2Pairs1 ?X")
unfolding lm04bb using lm93b by auto
moreover have "set (?F N) \<subseteq> ?X" using assms lm93c by presburger
ultimately show "inj_on adj2Pairs1 (set (allPreorders2 N))" by (rule subset_inj_on)
qed

lemma lm93e: "inj_on adj2Pairs1 (set (allPreorders2 N))" using lm93d lm82 
proof - have "N=0 \<longrightarrow> ?thesis" using lm82 by force thus ?thesis using lm93d by blast qed

lemma lm93f: "distinct (allPreordersPairs N)" 
unfolding allPreordersPairs_def using lm91n lm93e by (simp add: distinct_map)

lemma "card o set o allPreordersPairs = size o allPreordersPairs" 
using lm93f distinct_card by fastforce

theorem "size (allPreordersPairs N) = card {r| r. trans r & refl_on {0..<N} r}"
using lm93f correctnessPairs by (metis distinct_card)




































section{* From preorders to partial orders *}

abbreviation "numberColumns matr NCols == map (zip [0..<NCols]) matr"
abbreviation "numberRows matr NRows == zip [0..<NRows] matr"
(* indicize takes a matrix and returns a flat list of triples (i,j,val), where val is the (i,j)-th entry of the original matrix *)
abbreviation "indicize' matr NRows NCols == 
[((i, j), (matr!i)!j).  j <- [0..<NCols], i <- [0..<NRows]]"
definition "indicize=indicize'"
abbreviation "indicesOf' matr val == 
map fst (filter (\<lambda> triple. snd triple = val) (indicize matr (size matr) (size matr)))"
definition "indicesOf=indicesOf'"
abbreviation "symmetricEntries' strictAdjMatr == 
(set (indicesOf strictAdjMatr True)) \<inter> (set (map flip (indicesOf strictAdjMatr True)))"
definition "symmetricEntries=symmetricEntries'"
abbreviation "setDiag' val matr == [list_update (matr!i) i val. i <- [0..<size matr]]"
definition "setDiag=setDiag'"
abbreviation "allStrictPreorders' dim ==map (setDiag False) (allPreorders2 dim)"
definition "allStrictPreorders=allStrictPreorders'"
abbreviation "allStrictPartialOrders2' dim == 
filter (\<lambda> adjMatr. symmetricEntries adjMatr = {}) (allStrictPreorders dim)"
definition "allStrictPartialOrders2=allStrictPartialOrders2'"

abbreviation "isSymmetric' matr == (matr = transpose matr)"
definition "entryWise Op matr1 matr2 == pairWise (pairWise Op) matr1 matr2"
definition "isAntisymmetric' matr == ((set o concat) (entryWise HOL.conj matr (transpose matr))={False})"

definition "allStrictPartialOrders dim == filter isAntisymmetric' (allStrictPreorders dim)"

definition "exportMe8 = size (allStrictPartialOrders 8)"
definition "exportMe2 = size (allStrictPartialOrders 2)"
definition "myLength=List.length"
(*
export_code myLength nat_of_integer exportMe2 allPreorders2 allStrictPartialOrders allStrictPartialOrders2 
in SML module_name a file "/dev/shm/a.ML"
export_code exportMe8 in Scala module_name a file "/dev/shm/a.scala"
*)
































section{* Naive attempt *}

(* definition "projector2 R = (Image R) o (%x. {x})" *)
definition "reflCl2 R == (Domain R \<times> Domain R) \<union> (Range R \<times> Range R) \<union> R"
definition "preOrder subrelation == 
(* reflCl
*)
(trancl subrelation)"
(* Given a preoder and a subset X of its field, compute the set of maximal elements of X.
x is a maximal element iff y \<in> X \<rightarrow> y \<le> x *)
abbreviation "preorder1 == preOrder {(1::nat,2)}"
definition "allPreorders4 Set == preOrder ` (Pow (Set \<times> Set))"
definition "allRelations list1 list2==(set o map set o sublists) (List.product list1 list2)"
definition "allReflexiveRelations list == (%x. x \<union> set [(list!i,list!i). i<-[0..<size list]])`
(allRelations list list)"
definition "rel2adjMatr P == [[(i,j)\<in>P .j<-[0..<1+Max(Field P)]].i<-[0..<1+Max(Field P)]]"
definition "allPreordersNaive list == trancl`(allReflexiveRelations list)"
(*
value "card (rel2adjMatr`(allPreordersNaive [0..<3]))"

This seems to be much faster
value "card((rel2adjMatr o trancl)`(allReflexiveRelations [0..<3]))"
than this
value "card((trancl)`(allReflexiveRelations [0..<3]))".

Strange.

*)
(*value "targetF {0::nat..<3} {0::nat}"*)



















section{* Set-theoretical version: slower and no longer supported *}

(* Given a list representing a subset of 5, and a matrix representing a preorder 
(line m contains ones for the elements which m is greater or equal than),
we yield the list of members of the subset being greater-or-equal 
than all the members of the subset
*)

definition "greatestElems matr Subset == ListIntersection (map (filterpositions2 id) 
(map (nth (List.transpose matr)) Subset))"

definition "possibleRows matr col == {[False. n <- [0..<size col]]} 
\<union> (myInter o set) (map (set o allSubLists) (map (nth matr) (filterpositions2 id col)))" 

definition "children matr col == 
(appendAsColumn (col@[True]) ) `((% l. matr@[l])`(possibleRows matr col))"

fun allPreorders where 
"allPreorders 0 = {[[]]}" |
(*"allPreorders (Suc 0) = {[[True]]}"|*) 
"allPreorders (Suc n) = Union ((case_prod children) ` ((allPreorders n) \<times> (set (allSubLists [True. n<-[0..<n]]))))
(* {children matr col| matr col. matr \<in> allPreorders n & col \<in> set (allSubLists [True. n<-[0..<n]])} *)"








































































section{* Scraps and work in progress *}

definition "isNotStraddling Set family == (\<exists> A\<in>family. Set \<subseteq> A)"

definition "partialPseudoSort smallPartition Sort == concat [List.product list [Sort,,list]. list <- smallPartition]"
(* value "partialPseudoSort [[1::nat, 2],[3]] {([1::nat,2],10::nat),([3], 11)}"
value "product_lists [[0::nat,1],[10::nat],[11]]"

definition "maxSet2  sbset preorder == Inter ((projector preorder)``sbset)"
definition "targetF Set sbset = (maxSet2 sbset)`(allPreorders4 Set)"
definition "allSortsFamilies smallPartitions == 
product_lists [injectionsAlg smallPartition [0..<size smallPartition]. smallPartition <- smallPartitions]"
definition "pseudoSort smallPartitions sortFamily == concat [partialPseudoSort (smallPartitions!n) 
(sortFamily!n) . n <- [0..<size smallPartitions]]"
definition "allPseudoSorts bigPartition == concat [
[set (pseudoSort smallPartitions sortFamily). sortFamily <- allSortsFamilies smallPartitions]. smallPartitions <- product_lists (map mbc05 bigPartition)]"
definition "maxSet Set bigPartition pseudSort == if (isNotStraddling Set (set (map set bigPartition))) then 
((pseudSort^-1)``{Max (pseudSort``Set)}) 
else {}"
definition "resultWrtBigPartition Set bigPartition == map (maxSet Set bigPartition) (allPseudoSorts bigPartition)"
definition "result List SubSet == map (resultWrtBigPartition SubSet) (mbc05 List)"
definition "evalMe2 = result [0::nat..<2]`(((Pow o set) [0::nat..<2])-{{}} )"
value evalMe2
(* value "pseudoSort (hd (product_lists (map mbc05 bigPartition))) 
(hd (allSortFamilies (hd (product_lists (map mbc05 bigPartition)))))"*)
*)


(*
lemma lm57: assumes "refl01' A N" shows "refl01 (enlarge2' A N col row) (N+1)"
unfolding refl01_def using assms by fastforce

lemma lm33: assumes "trans01' (enlarge' A N col row) (N+1)" shows "condCol' A N col"
using assms less_add_one nat_neq_iff trans_less_add1 [[smt_solver=z3,smt_timeout=5]] by smt

lemma lm33b: assumes "trans01 (enlarge A N col row) (N+1)" shows "condCol A N col"
using assms lm33 unfolding trans01_def enlarge_def condCol_def by blast

lemma lm32: assumes "trans01' (enlarge' A N col row) (N+1)" 
(* "condCol' A N col" *) 
shows "cond1' (addCol' A N col) N row" 
(* using assms(1) less_add_one trans_less_add1 nat_neq_iff by smt *) 
proof -
let ?A="addCol' A N col" let ?AA="enlarge' A N col row"
{
  fix i j assume "i<N & j<N & ?A i N & \<not> ?A i j"
  then have "i<N+1 & j<N+1 & N<N+1 & ?AA i N & \<not> ?AA i j" by fastforce
  then moreover have "\<not> ?AA N j" using assms by blast
  ultimately have "\<not> row j" using assms by meson
}
thus ?thesis by blast
qed

lemma lm32b: assumes "trans01 (enlarge A N col row) (N+1)" 
(* "condCol A N col" *)
shows "cond1 (addCol A N col) (N) row" using assms lm32 unfolding trans01_def enlarge_def condCol_def cond1_def
addCol_def by blast

lemma lm31: assumes "trans01' (enlarge' A N col row) (N+1)" 
(* "condCol' A N col" "cond1' (addCol' A N col) (N) row" *)
shows "cond2' (addCol' A N col) (N) row"
using assms(1) less_add_one nat_neq_iff trans_less_add1 
proof-
let ?A="addCol' A N col" let ?AA="enlarge' A N col row"
{
  fix i j assume "i<N & j<N & row i & ?A i j" then moreover have
  "i<N+1 & j<N+1 & N<N+1 & ?AA N i & ?AA i j" by force then moreover have "?AA N j" using assms by blast
  ultimately have "row j" using assms by (metis (no_types) less_not_refl)
} thus ?thesis by blast
qed

lemma lm31b: assumes "trans01 (enlarge A N col row) (N+1)" 
(* "condCol A N col" "cond1 (addCol A N col) (N) row" *)
shows "cond2 (addCol A N col) (N) row" using assms lm31 unfolding trans01_def enlarge_def condCol_def 
cond1_def addCol_def cond2_def by blast

lemma lm28: assumes "trans01' A (N+1)" shows "trans01' A (N::nat)" using assms trans_less_add1 by meson
lemma lm29c: "\<forall> i j. i<N & j<N \<longrightarrow> (enlarge' A N col row) i j = A i j" using assms by auto
lemma lm29: assumes "trans01' (enlarge' A N col row) (N+1)" shows "trans01' A N" 
proof -
let ?A="enlarge' A N col row"
{
  fix i j k assume 0: "i<N & j<N & k<N & A i j & A j k" then have 
  "i<N+1 & j<N+1 & k<N+1 & ?A i j & ?A j k" by simp then have "?A i k" using assms by blast
  then have "A i k" using 0 by auto
}thus ?thesis by blast
qed

lemma lm29b: assumes "trans01 (enlarge A N col row) (N+1)" shows "trans01 A N" using assms 
lm29 unfolding trans01_def enlarge_def by blast

lemma lm30: assumes 
"trans01 A N" 
"condCol A N col" 
"cond1 (addCol A N col) N row" 
"cond2 (addCol A N col) N row"
shows "trans01 (enlarge A N col row) (N+1)"
using assms Suc_eq_plus1 le_neq_trans less_Suc_eq_le 
unfolding addCol_def enlarge_def trans01_def cond2_def cond1_def condCol_def
by metis

(* cmp lm45d & lm56 *)
lemma (* inductiveStepThm: *) "trans01 (enlarge A N col row) (N+1)=
(trans01 A N & 
condCol A N col & 
cond1 (addCol A N col) N row & 
cond2 (addCol A N col) N row)"
using assms lm29b lm33b lm32b lm31b lm30 by blast

unused_thms
*)
end
