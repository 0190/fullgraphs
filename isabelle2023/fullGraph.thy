theory fullGraph

imports arsbm Product_Type
"./Vcg/CombinatorialAuction"
(* "~~/src/HOL/ex/Quicksort" *)
begin

section{*Preliminaries*}

notation "corestriction" ("_|^")

proposition "P^-1||Y=(P|^Y)^-1" unfolding restrict_def corestriction_def by fast

proposition assumes "X\<subseteq>Domain P" "reflex'(P||X)"shows"(X\<subseteq>Domain(Id\<inter>P))" using assms unfolding refl_on_def Id_def
restrict_def Outside_def
using assms by fast

proposition l45p: "P||(X\<union>Y)=P||X \<union> P||Y" by (simp add: Int_Un_distrib2 Sigma_Un_distrib1 restrict_def)

abbreviation "luniq' P==runiq (P^-1)" definition "luniq=luniq'"
proposition l45u: "isInjection P=(runiq P & luniq P)" unfolding luniq_def by simp

proposition l45q: assumes "luniq' P" "X \<subseteq> Domain P" "Y \<subseteq> Domain P" "X\<noteq>Y" shows "P``X \<noteq> P``Y"
  using assms DomainE converse.intros converse_converse equalityI ll71 rev_ImageI subset_eq by (smt)

corollary l45qq: assumes "luniq' P" "X \<inter> Domain P\<noteq>(Y \<inter> Domain P)" shows "P``X \<noteq> P``Y"
  using assms l45q by (metis Int_lower2 lll84)

corollary l45qqq: assumes "luniq' P" "{x}\<inter>Domain P \<noteq> {y}\<inter> Domain P" "x\<noteq>y" shows "P``{x} \<noteq> P``{y}"
  using assms l45qq by metis

corollary l45qqqq: assumes "luniq' P" "x\<in>Domain P" "y\<in>Domain P" "x\<noteq>y" shows "P``{x}\<noteq>P``{y}" 
  using assms l45qqq by fast

proposition l45r: assumes "runiq f" "\<forall>x\<in>Domain f. \<forall>y\<in>Domain f-{x}. f,,x\<noteq>f,,y" shows "luniq' f"
  using assms(1,2) insertE insert_Diff les2.lm36b by (metis )

proposition l45rr: assumes "runiq f" shows "(luniq f)=(\<forall>x\<in>Domain f. \<forall>y\<in>Domain f-{x}. f,,x\<noteq>f,,y)" (is "?l=?r")
  unfolding luniq_def using assms l45r l45qqqq DiffE insertI1 lm36a by (metis)

proposition l45s: assumes "X0\<noteq>Y0" "(X0\<union>Y0) \<inter> (X1\<union>Y1)={}" shows "X0\<union>X1 \<noteq> Y0\<union>Y1" using assms by blast

proposition l45t: "luniq f=inj_on snd f" 
  using lll33 MiscTools.lm38c converse_converse luniq_def by (metis ) 

proposition l45tt: "isInjection f=(inj_on fst f & inj_on snd f)" using l45t lll33 by (metis luniq_def)

proposition "luniq f=(\<forall>z0\<in>f. \<forall>z1\<in>f-{z0}. ( snd z0\<noteq>snd z1))" 
  unfolding l45t using DiffD1 DiffD2 inj_onI inj_on_contraD insertE insert_Diff singletonI by (metis (no_types, lifting) )

proposition "runiq f=(\<forall>z0\<in>f. \<forall>z1\<in>f-{z0}. (fst z0\<noteq>fst z1))" 
  using lll33 DiffD1 DiffD2 inj_onI inj_on_contraD insertE insert_Diff singletonI by (metis (no_types, lifting))

proposition "isInjection f=(\<forall>z0\<in>f. \<forall>z1\<in>f-{z0}. (fst z0\<noteq>fst z1 & snd z0\<noteq>snd z1))" 
  using l45tt inj_onI inj_on_contraD DiffD1 DiffD2 insertE insert_Diff singletonI by smt

abbreviation "is121' f==isInjection f" definition "is121=is121'"

abbreviation "fixPts' P==Domain(Id\<inter>P)" definition "fixPts=fixPts'"
proposition l45f: "fixPts P=Range(Id\<inter>P)" "fixPts P=Field(Id\<inter>P)" 
  unfolding fixPts_def  apply auto unfolding Field_def apply blast by fast
proposition l45g: "reflex P=(fixPts P=Field P)" and "(reflex P) = (Id_on (Field P) \<subseteq> P)"
  unfolding fixPts_def reflex_def refl_on_def Field_def apply fast using lm28e by auto

proposition l45h: "refl_on X P=(reflex P & Field P = X)" unfolding reflex_def refl_on_def Field_def by auto

abbreviation "pointUnion' ff A ==ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A))" definition "pointUnion=pointUnion'"
notation "pointUnion" ("_ +u")

abbreviation "bouthside' P X Y == P - ((X\<times>Range P) \<union> ((Domain P)\<times>Y))" definition "bouthside=bouthside'"
notation "bouthside" ("_\\")
abbreviation "singlebouthside' P x y == bouthside' P {x} {y}" definition "singlebouthside=singlebouthside'"
notation "singlebouthside" ("_---")

abbreviation "symmDiff' A B == (A-B) \<union> (B-A)" definition "symmDiff=symmDiff'"

abbreviation "isRepresentation' f D U == \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((((x, y)\<in>D)=(f,,x \<supseteq> f,,y)) & (((x,y)\<in>U) = ((f,,x \<inter> f,,y)={}))))"
definition "isRepresentation=isRepresentation'"

lemma l11: "f`(X\<inter>Y)=(f|||X)``Y" (is "?L=?R") unfolding graph_def by blast

lemma "(U h - (h,,x)) \<inter> (U f) \<subseteq> U f - (h,,x)"  by blast

lemma l00: assumes "P^-1``Y \<inter> Domain Q = {}" shows "P^-1``Y \<subseteq>(P +* Q)^-1``Y"
  unfolding Image_def paste_def Outside_def using assms by blast

lemma l09: assumes "Y \<inter> Range Q={}" shows "(P +* Q)^-1``Y \<subseteq> P^-1``Y" 
  using assms paste_sub_Un by fastforce 

lemma l05: assumes "\<forall> X\<in>XX. A X\<noteq>{} & A X \<inter> \<Union> YY={}" shows "YY \<inter> Range ((graph (\<lambda> X. X \<union> A X) XX))={}"
proof -
  let ?f="\<lambda> X. X \<union> A X" 
  { fix Y assume "Y\<in>YY\<inter>?f`XX" then moreover obtain X where "X\<in>XX & ?f X=Y" by blast then 
    moreover have "A X \<subseteq> Y" by fast ultimately have False using assms(1) by blast }
  thus ?thesis unfolding graph_def by blast
qed

lemma l06: "(graph f X)^-1``Y = X \<inter> f-`Y" unfolding graph_def by blast

lemma l08: assumes "runiq f" shows "(Domain f) \<inter> (f,,-`Y) = f^-1``Y" 
  using assms by (metis l06 lm024 toFunction_def)  

lemma assumes "X \<subseteq> Domain Q" shows "(P +* Q)``X=Q``X" 
  using assms MiscTools.ll50 inf.absorb_iff2 by (metis)

lemma "f^-1``YY\<subseteq>f^-1``((\<lambda>Y. Y \<inter> Union(Range f))`YY)" by auto

lemma l14: "(g o f)|||X = (f|||X) O (g|||(f`X))" 
  unfolding graph_def Graph_def by fastforce
lemma "Domain((eval_rel f)|||X) \<supseteq> X\<inter>Domain f" 
  using graph_def by (simp add: MiscTools.ll37)

lemma l16: assumes "X\<subseteq>Domain f" "runiq f" shows "(f,,)|||X = f||X" 
  using assms(1,2) MiscTools.lm10 toFunction_def by (metis )

lemma assumes "g=(\<lambda>x. x \<union> A x)" "X\<subseteq> Domain f" "x\<in>X" shows 
"f,,x \<subseteq> Union (Range (f +* ((g o (f,,))|||X)))" 
  using assms  paste_def graph_def MiscTools.ll33 MiscTools.ll37 Range_Un_eq Un_upper1 
  Union_Un_distrib Union_upper comp_eq_dest_lhs eval_runiq_in_Range le_supI2 subset_trans by (smt)

lemma l19: assumes "x\<in>Domain f - Domain g" "runiq f" "runiq g" shows "(f +* g),,x=f,,x" 
  using assms(1,2,3) DiffD1 DiffD2 Domain.DomainI UnCI UnE eval_runiq_rel outside_union_restrict 
  paste_Domain paste_def runiq_basic runiq_paste2 by smt

lemma l17a: assumes "X\<subseteq> Domain f" "x\<in>X" "runiq f" shows 
"Union(f``{x}) \<subseteq> Union (Range (f +* (((\<lambda>x. x\<union>A x) o (f,,))|||X)))" 
  using assms(1,2,3) MiscTools.ll33 MiscTools.ll37 Range_Un_eq Sup_upper UnCI Union_Un_distrib 
  contra_subsetD eval_runiq_in_Range lll82 o_apply paste_def subsetI by (smt)

lemma l17b: assumes "X\<subseteq> Domain f" "x\<in>Domain f - X" "runiq f" shows 
"Union(f``{x}) \<subseteq> Union (Range (f +* (((\<lambda>x. x \<union> A x) o (f,,))|||X)))"
  using assms DiffD1 DiffD2 Diff_disjoint Diff_insert_absorb Image_runiq_eq_eval MiscTools.ll37 
  MiscTools.lm19b Un_absorb2 Union_upper cSup_singleton eval_runiq_in_Range paste_Domain runiq_paste2
  by (smt )

lemma l13: "f^-1``(Pow Y)=f^-1``((\<lambda> X. X \<inter> Union (Range f))`Pow Y)" by blast
lemma "(\<lambda>X. X \<inter> XX)`Pow Y = Pow(Y\<inter>XX)" by auto
lemma l18: "f^-1``(Pow Y)=f^-1``(Pow (Y\<inter>Union(Range f)))" by blast (*Y=U h - h,,x*)
lemma assumes "U=Union o Range" shows "f^-1``(Pow(U h - (h,,x)))=f^-1``(Pow ((U h - (h,,x)) \<inter> (U f)))"
  using assms by auto

lemma l17: assumes "X\<subseteq> Domain f" "runiq f" shows 
"Union(Range f) \<subseteq> Union(Range(f +* (((\<lambda>x. x\<union>A x)o(f,,))|||X)))"
  using assms l17a l17b by fast

lemma l15: assumes "g=(\<lambda>x. x \<union> A x)" "X\<subseteq> Domain f" shows "f,,x \<subseteq> (f +* (g o (f,,))|||X),,x"
  using assms paste_def Domain_empty Domain_empty_iff Int_insert_right_if1 MiscTools.ll33 
  MiscTools.ll37 MiscTools.ll50 MiscTools.lm19b RelationProperties.ll41 Un_upper1 
  eval_rel.simps lll01 outside_reduces_domain Diff_iff Int_insert_left_if0 all_not_in_conv 
  comp_apply inf.absorb1 ll56a subsetCE subsetI by smt

lemma l15b: assumes "Y1 \<subseteq> Y2" "U=Union o Range" shows "f^-1``(Pow(X - Y2)) \<subseteq> f^-1``(Pow ((U f - Y1)))"
  using assms by auto

lemma l07: assumes "\<forall>Y\<in>C. f Y \<inter> A X={}" shows "C \<inter> ((\<lambda>x. f x \<union> A x)-`(Pow(X \<union> A X))) \<subseteq> C \<inter> f-`(Pow X)"
  using assms IntE IntI Pow_iff UnCI UnE empty_iff subsetCE subsetI subsetI vimageE vimageI by fastforce

lemma l17d: "Union (Range (f +* g)) \<subseteq> Union (Range f) \<union> (Union (Range g))" 
  unfolding paste_def Outside_def by blast

lemma l17e: assumes "runiq f" "C \<subseteq> Domain f"  shows 
"Union (Range ((\<lambda>x. (f,,x) \<union> A x)|||C))\<subseteq> (Union (f``C)) \<union> Union (A`C)" 
  using assms Domain.DomainI RangeE RelationProperties.l31 MiscTools.ll33 MiscTools.ll37 image_eqI lm025 Un_iff Union_iff subsetI by smt

lemma l17f: assumes "runiq f" "C\<subseteq>Domain f" shows 
"((Union o Range) (f+*((\<lambda>x. (f,,x) \<union> A x)|||C))) \<subseteq> (Union o Range) f \<union> (Union (A`C))" (is "(Union o Range) (f+*?G)\<subseteq> ?R") 
proof - 
  have "Union (Range (f+*?G)) \<subseteq> (Union (Range f)) \<union> Union (Range ?G)" unfolding paste_def 
  Outside_def by blast moreover have "Union(Range ?G) \<subseteq> Union(f``C) \<union> Union (A`C)" using assms 
  by (rule l17e) ultimately show ?thesis using assms l17e UnCI UnE Union_iff eval_runiq_in_Range 
  imageE lm025 subsetCE subsetI by (smt o_apply)
qed

lemma l17g: assumes "runiq f" "C\<subseteq>Domain f" shows 
"((Union o Range) (f+*((\<lambda>x. (f,,x) \<union> A x)|||C))) - Union(A`C) \<subseteq> ((Union o Range) f) - Union (A`C)"
proof -
  have" ((Union o Range) (f+*((\<lambda>x. (f,,x) \<union> A x)|||C))) \<subseteq> ((Union o Range) f \<union> (Union (A`C)))" 
  using assms by (rule l17f) then show ?thesis by blast
qed

lemma l20a: assumes "runiq f" "x\<in>Domain f" shows "(f+<((\<lambda>x. (f,,x) \<union> A x)|||C)),,x \<subseteq> f,,x \<union> A x"
  using assms DiffI MiscTools.ll33 MiscTools.ll37 UnCI eval_runiq_rel l19 paste_Domain paste_def 
  runiq_basic runiq_paste2 subsetI by smt

lemma l20b: assumes "runiq f" "x\<in>Domain f" shows "f,,x \<subseteq> (f+<((\<lambda>x'. f,,x' \<union> A x')|||C)),,x" 
  using assms paste_def DiffI MiscTools.ll33 MiscTools.ll37 UnCI eval_runiq_rel l19 paste_Domain 
  runiq_basic runiq_paste2 subsetI by smt

lemma l22: assumes "x\<in>C" "C\<subseteq>Domain f" "runiq f" shows "(f+*((\<lambda>x'. f,,x' \<union> A x')|||C)),,x = f,,x \<union> A x"
  using assms(1,2,3) subset_iff DiffE Domain.intros MiscTools.ll33 MiscTools.ll37 UnE eval_runiq_rel 
  RelationProperties.l31 outside_reduces_domain paste_Domain paste_def runiq_paste2 sup.orderE by smt


abbreviation "isDownClosed' D X == D^-1``X\<subseteq>X" definition "isDownClosed=isDownClosed'" 
abbreviation "shuffler' == (\<lambda>(a,b) (c,d). ((a,c),(b,d)))" definition "shuffler=shuffler'"
(*Nomenclature: order-morphism, monotonic, or isotone map?*)
abbreviation "pair2List' == \<lambda>(x,y).[x,y]" definition "pair2List=pair2List'"
abbreviation "isMonotone' P Q f==((%(x,y). (f x, f y))`P)\<subseteq>Q"
abbreviation "isMonotone2' P Q f==(map f)`(pair2List`P)\<subseteq>pair2List`Q" definition "isMonotone2=isMonotone2'"
abbreviation "isMonotone3' P Q f==\<forall> x y. (x,y)\<in>P \<longrightarrow> (f``{x})\<times>(f``{y})\<inter>Q\<noteq>{}" definition "isMonotone3=isMonotone3'"
abbreviation "isMonotone4' P Q f==\<forall> x\<in>Domain f. \<forall> y\<in>Domain f. (x,y)\<in>P \<longrightarrow> (f,,x,f,,y)\<in>Q"
definition "isMonotone4=isMonotone4'"
abbreviation "isMonotone5' P Q f==\<forall> x\<in>Domain f. \<forall> y\<in>Domain f. (x,y)\<in>P \<longrightarrow> (f``{x})\<times>(f``{y})\<inter>Q\<noteq>{}"
abbreviation "isMonotone6' P Q f==\<forall> x\<in>Domain f. \<forall> y\<in>Domain f. (x,y)\<in>P \<longrightarrow> (f``{x})\<times>(f``{y})\<subseteq>Q"
abbreviation "rel2pairset' r==(case_prod r)-`{True}" definition "rel2pairset=rel2pairset'"
abbreviation "pairset2rel' P== \<lambda> x y. (x,y)\<in>P" definition "pairset2rel=pairset2rel'"
abbreviation "IsOrderMorph1' P Q f == \<forall>x\<in>Domain f. \<forall>y\<in>Domain f. P x y \<longrightarrow> Q (f,,x) (f,,y)"
abbreviation "IsOrderMorph2' P Q f == \<forall>x\<in>Domain f. \<forall>y\<in>Domain f. P x y \<longrightarrow> True \<in> (case_prod Q)`(f``{x}\<times>f``{y})"
abbreviation "IsOrderMorph3' P Q f == \<forall>x y. P x y \<longrightarrow> True \<in> (case_prod Q)`(f``{x}\<times>f``{y})"

proposition l48f: assumes "runiq f" "isMonotone3' P Q f" shows "isMonotone4' P Q f" using assms(1,2)
Image_runiq_eq_eval Int_commute Int_empty_right Int_insert_right_if0 (*Sigma_cong*) Universes.lm74
by (metis(no_types))

proposition l48g: assumes "isMonotone3' P Q f" shows "Field P \<subseteq> Domain f" unfolding Field_def using assms by blast
proposition l48h: assumes "Field P \<subseteq> Domain f" "runiq f" "isMonotone4' P Q f" shows "isMonotone3' P Q f" 
using assms(1,2,3) Image_runiq_eq_eval IntE Int_commute Restr_Field contra_subsetD insert_disjoint(2) 
mem_Sigma_iff mk_disjoint_insert singletonI by (smt)

proposition l35: "(x,y) \<in> rel2pairset' r = (r x y)" by simp

(*lemma assumes "M=(N::nat)" "\<forall>j<N. \<forall> i<j. (P i & P j) \<longrightarrow> Q i j" shows "\<forall>j<N. \<forall>i<M. (P i & P j & i\<noteq>j) \<longrightarrow> Q i j" 
using assms max.commute max_min_same(1) max_min_same(4) min.idem min_max_distrib2 inf_commute max_def min_def
not_le *)

lemma l39a: assumes "\<forall> i j. Q i j = Q j i" shows "(\<forall>j<(N::nat). \<forall> i<j. Q i j) \<longrightarrow> (\<forall>i j.  (i<N & j<N & i\<noteq>j) \<longrightarrow> Q i j)" 
using assms max_min_same(4) max_def min_def not_le by (metis(no_types, lifting))
lemma l39b: assumes  "(\<forall>i j.  (i<N & j<N & i\<noteq>j) \<longrightarrow> Q i j)" shows "(\<forall>j<(N::nat). \<forall> i<j. Q i j)"
using assms by simp

lemma l29a: "(Union o Range) P=Union((eval_rel2 P)`UNIV)" and "(Union o Range) P =Union((eval_rel2 P)`(Domain P))" by auto

lemma l33: "P,,,`(UNIV-Domain P) \<subseteq> {{}}" by blast

lemma l33b: assumes "x\<notin>Domain P" shows "P,,,x={}" using assms by blast

lemma l33c: "Union {(f z),,,x|z. P z x}=Union {(f z),,,x|z. P z x & x\<in>Domain(f z)}" by blast

lemma l37a: assumes "card(Range A)=1" shows "Domain A \<noteq> {}" using assms 
by (metis Domain_empty_iff Image_within_domain' Range_empty empty_iff lm014 runiq_wrt_ex1)
lemma l37b: assumes "trivial (Range P)" shows "P=(Domain P)\<times>Range P" using assms(1) 
unfolding trivial_def by auto
lemma l37c: assumes "card(Range A)=1" shows "A=(Domain A) \<times> (Range A)" using assms
l37a l37b Sigma_cong lm007b by fastforce
lemma l37d: assumes "card(Range A)=1" shows "A=(Domain A) \<times> {the_elem(Range A)}" using assms
l37a l37b Sigma_cong lm007b l37c by (metis nn56)
lemma l37e: assumes "A=(Domain A) \<times> {the_elem(Range A)}" shows "trivial(Range A)" unfolding trivial_def 
using assms l37a l37b Sigma_cong lm007b l37c by blast
lemma l37f: assumes "card X=1" shows "the_elem X=Union X" using assms 
by (metis cSup_singleton nn56)
lemma l37g: assumes "card(Range A)=1" shows "A=(Domain A) \<times> {(Union o Range) A}" using assms
 l37d l37f using Sigma_cong comp_apply by fastforce
lemma l37h: assumes "trivial (Range f)" shows "runiq f & isMonotone4 P (rel2pairset' (op \<supseteq>)) f"
  unfolding isMonotone4_def using assms Range.intros runiq_basic trivial_imp_no_distinct equalityD2 eval_runiq_in_Range 
  l35 trivial_imp_no_distinct by (metis(no_types))

lemma l37i: assumes "P=Domain P \<times> {(Union o Range) P}" shows "\<forall>x\<in>Domain P. P,,,x=(Union o Range) P"
using assms by blast

abbreviation "butfirst' l == drop 1 l" definition "butfirst=butfirst'"

abbreviation "concurs' D U X==Field D - ((D^-1``X)\<union>(D``X)\<union>(U^-1``X))" definition "concurs=concurs'"

lemma l37j: shows "card(Range f)=1 \<longrightarrow> trivial(Range f)" and "trivial(Range f) \<longrightarrow> runiq f"
and "card(Range f)=1 \<longrightarrow> runiq f"
using Range.intros runiq_basic trivial_imp_no_distinct lm007b apply (metis)
using Range.intros runiq_basic trivial_imp_no_distinct lm007b apply (metis)
using Range.intros runiq_basic trivial_imp_no_distinct lm007b apply (metis)
done 

lemma l37k: assumes "X\<noteq>{}" "{}\<notin>X" shows "Union X \<noteq> {}" using assms by fastforce

lemma l37l: assumes "runiq f" shows "y\<in>Range f = (\<exists> x\<in>Domain f. f,,x=y)" using assms 
using Domain.DomainI RangeE eval_runiq_in_Range RelationProperties.l31 by fastforce
                                                
lemma l37m: assumes "runiq f" shows "y\<in>Range f = (\<exists> x\<in>Domain f. f,,,x=y)" using assms 
l37l by (metis lll82)

lemma l41v: assumes "isDownClosed' P X" "isDownClosed' P Y" shows "isDownClosed' P (X\<union>Y)" using assms by blast

abbreviation "idempotent' f == f o f = f" definition "idempotent=idempotent'"
abbreviation "Idempotent' P == idempotent (Image P)" definition "Idempotent=Idempotent'"

lemma l41a: assumes "(\<forall>X. P``(P``X)=P``X)" shows "Idempotent' P" proof- let ?f="Image P"
{assume "\<not> ?thesis" then obtain X where "?f X \<noteq> (?f o ?f) X" unfolding idempotent_def by fastforce
  then have False using assms by simp} thus ?thesis by auto qed

lemma l41b: "(Idempotent P)=(\<forall>X. P``(P``X)=P``X)" unfolding Idempotent_def using assms idempotent_def 
l41a comp_apply idempotent_def by (metis(no_types))

lemma l41c: assumes "trans P" "reflex P" shows "Idempotent P" unfolding l41b 
using assms unfolding trans_def reflex_def refl_on_def by blast

lemma l41d: assumes "trans P" shows "P``(P``X)\<subseteq>P``X" using assms unfolding trans_def by blast

lemma l41e: assumes "reflex P" shows "P``(P``X)\<supseteq>P``X" using assms unfolding reflex_def refl_on_def by blast
lemma l41f: assumes "Idempotent P" shows "trans P" unfolding trans_def using assms unfolding l41b by blast

lemma l41g: assumes "trans(P^-1)" shows "isDownClosed' P (P^-1``X)" using assms l41d by fast

lemma l41h: assumes "Idempotent (P^-1)" shows "isDownClosed' P (P^-1``X)" using assms unfolding l41b by simp 
lemma l41i: assumes "Idempotent f" "runiq f" "Range f \<subseteq> Domain f" "x\<in>Domain f" shows "f,,(f,,x)=f,,x" 
using assms by (metis Image_runiq_eq_eval eval_rel.simps l41b)
lemma l41j: "P``X=Union {P``{x}|x. x\<in>X}" by blast
lemma l41k: assumes "(\<forall>x. P``(P``{x})=P``{x})" shows "Idempotent P" unfolding l41b using assms 
Idempotent_def idempotent_def by blast
lemma l41l: "(\<forall>x. P``(P``{x})=P``{x})=Idempotent P" using l41b l41k by (metis)
lemma l41m: "(\<forall>x. P``(P``{x})=P``{x})=(\<forall>x\<in>Domain P. P``(P``{x})=P``{x})" by auto
lemma l41n: "Idempotent P=(\<forall>x\<in>Domain P. P``(P``{x})=P``{x})" unfolding l41l using l41m by (metis l41b l41k)
lemma l41o: assumes "Range f \<subseteq> Domain f" "runiq f" shows "Idempotent f=(\<forall>x\<in>Domain f. f,,(f,,x)=f,,x)" 
using assms l41n Image_runiq_eq_eval by (metis (no_types, lifting) eval_runiq_in_Range l41i subsetCE)

abbreviation "maximals3' P==minimals(P^-1)" definition "maximals3=maximals3'"
abbreviation "maximalsOfSet' P X == maximals3 (corestriction P X)" definition "maximalsOfSet=maximalsOfSet'"
notation "maximalsOfSet" ("_maximals")

lemma l42a: "maximals3' P=(Range P) - (Domain (strict' P))" by auto

lemma l42b: "(y\<in>maximals3' P)=((y\<in>Range P) & (\<forall>x. (y,x)\<in>P \<longrightarrow> x=y))" unfolding Field_def by blast

lemma l42c: assumes "(y\<in>P maximals Y)" shows "y\<in>Y\<inter>Range P" 
 unfolding maximalsOfSet_def corestriction_def using l42b
  IntD1 Range.cases assms corestriction_def maximals3_def maximalsOfSet_def mem_Sigma_iff
  IntE Range.simps
by (metis (no_types, lifting) Int_iff)

lemma l42d: assumes "(y\<in>P maximals Y)" shows "\<forall>y'\<in>Y. (y,y')\<in>P \<longrightarrow> y'=y" using assms(1) unfolding 
maximalsOfSet_def corestriction_def restrict_def Outside_def maximals3_def by blast 

lemma l42e: assumes "y\<in>Y\<inter>Range P" "\<forall>y'\<in>Y. (y,y')\<in>P \<longrightarrow> y'=y" shows "y\<in>P maximals Y" 
unfolding maximalsOfSet_def corestriction_def restrict_def Outside_def maximals3_def
using assms by blast

lemma l43a: "Range (corestriction P Y)=Y\<inter>Range P" unfolding corestriction_def by auto

lemma l42f: "(y\<in>P maximals Y)=(y\<in>Y\<inter>Range P & (\<forall>y'\<in>Y. (y,y')\<in>P \<longrightarrow> y'=y))" (is "?L=?R") using l42d l42c l42e by metis 

corollary l42g: "P maximals Y \<subseteq> Y" using l42f by force

abbreviation "funprod' f g == \<lambda>(x,y). (f x, g y)" definition "funprod=funprod'"
abbreviation "bicomposition' g f1 f2== curry (case_prod g o (funprod f1 f2))" definition "bicomposition=bicomposition'"

lemma l43b: "isMonotonicOver U D=(isMonotone4 (D) (rel2pairset (op \<subseteq>)) ((((Image U) o (\<lambda>x. {x})))|||UNIV))"
unfolding isMonotone4_def rel2pairset_def using assms MiscTools.ll33 MiscTools.ll37 comp_apply l35  UNIV_I by (smt)

lemma l43c: assumes "isMonotonicOver U D" shows "isMonotonicOver U (D||X)" using assms 
by (metis contra_subsetD restriction_is_subrel)

lemma l43e: "B\<inter>(D maximals A) \<subseteq> D maximals (A\<inter>B)" using l42f by (smt IntE IntI subsetI)
(*by (smt Int_iff subset_eq) by (smt IntD1 IntD2 IntI subsetI)*)

lemma l43d: assumes "isMonotonicOver U (D||A)" shows "D maximals (A\<inter>(U^-1)``Y) \<subseteq> (U^-1``Y) \<inter> (D maximals A)"
proof- let ?V="U^-1" let ?X="?V``Y" let ?M'="maximals3'" let ?m="\<lambda>Y. D maximals Y"{
  fix a0 assume 2: "a0\<in>?m (A\<inter>?X)" then moreover have 
  1: "Y\<inter>U``{a0} \<noteq> {}" unfolding l42f by blast moreover assume "a0 \<notin> ?m A" ultimately obtain a1 where 
  0: "a1\<in>A-{a0} & (a0,a1)\<in>D" unfolding l42f by blast then have "(a0,a1)\<in>D||A" unfolding 
  restrict_def Outside_def using 2 unfolding l42f by blast then have "Y\<inter>U``{a1} \<noteq> {}" 
  using assms(1) 1 by fast then have "a1\<in>A \<inter> U^-1``Y" using 0 by fast then have "a0=a1" using 2 
  unfolding l42f using 0 by (metis) then have False using 0 by blast} then show ?thesis using l42g by fast
qed

lemma l43f: assumes "isMonotonicOver U (D||A)" shows 
"D maximals (A\<inter>(U^-1)``Y) = (U^-1``Y) \<inter> (D maximals A)" (is "?L=?R") 
proof - (* note: Isar proof generation bug, space between D and maximals missing *)
  have "D maximals (A \<inter> U\<inverse> `` Y) \<subseteq> U\<inverse> `` Y \<inter> D maximals A" by (metis assms l43d)
  then show ?thesis by (metis Int_absorb1 Int_absorb2 l43e) qed

abbreviation "isConflictFreeStrong' Co X == ((X \<times> X) \<inter> (Co \<union> Co^-1) = {})" 
definition "isConflictFreeStrong=isConflictFreeStrong'"
abbreviation "minimalsOfSet' P X == minimals (P||X)" definition "minimalsOfSet=minimalsOfSet'"
notation "minimalsOfSet" ("_ minimals")

lemma l42h: "corestriction P Y = (P^-1||Y)^-1" unfolding corestriction_def restrict_def Outside_def by fast

lemma l42i: "P minimals X = (P^-1 maximals X)" 
unfolding minimalsOfSet_def maximalsOfSet_def maximals3_def l42h by simp

lemma l42j: "(x\<in>P minimals X)=(x\<in>X\<inter>Domain P & (\<forall>x'\<in>X. (x',x)\<in>P \<longrightarrow> x'=x))"  
unfolding l42i l42f maximalsOfSet_def maximals3_def corestriction_def by auto

lemma l42k: assumes "X\<subseteq>Domain D" "X\<noteq>{}" "wf(strict D)" shows "D minimals X\<noteq>{}" 
using assms(1,2,3) lm01g empty_iff l42j le_iff_inf by (metis)  

lemma l42l: "D minimals X = D minimals (X\<inter>Domain D)" 
using converse_converse l42h l42i maximalsOfSet_def restriction_within_domain by (metis)

lemma l42m: "D minimals X \<subseteq> X \<inter> Domain D" using l42j by force

lemma l42n: assumes "X\<inter>Domain D\<noteq>{}" "wf(strict D)" shows "D minimals X\<noteq>{}" using assms l42k 
empty_iff le_iff_inf Domain.DomainI Int_iff Int_lower2 eq_iff l42i l42j l43e subset_iff by smt  

lemma l42o: "D maximals X = D maximals (X\<inter>Range D)" using converse_converse l42h  
maximalsOfSet_def restriction_within_domain by (metis Range_converse)

lemma l42p: assumes "Y\<inter>(Range D)\<noteq>{}" "wf(strict (D^-1))" shows "Y\<inter>(Range D)\<inter>(D maximals Y)\<noteq>{}" 
using assms empty_iff le_iff_inf Int_iff eq_iff l42i l42j subset_iff l42n Domain_converse 
converse_converse by (metis(no_types))

lemma l41r: assumes "isDownClosed D X" shows "isDownClosed D (X-(Y\<inter>maximals3 D))" 
unfolding isDownClosed_def maximals3_def using assms unfolding isDownClosed_def by blast

lemma l42r: assumes "trans P" shows "trans (P---x x)" using assms unfolding singlebouthside_def trans_def by blast

lemma l41t: "P|^Y \<subseteq>P||(P^-1``Y)" unfolding corestriction_def restrict_def Outside_def by blast

lemma l41u: assumes "sym U" "isMonotonicOver U (D|^ Y)" "isConflictFree2 U Y" shows "isConflictFree2 U (D^-1``Y)" 
using assms unfolding isConflictFree2_def sym_def corestriction_def by blast

proposition l45a: assumes "sym P" shows "sym (P\\X X)" using assms unfolding sym_def bouthside_def by blast
proposition l45b:  "P---x y=P\\{x} {y}" unfolding singlebouthside_def bouthside_def by auto
proposition l45c: assumes "isMonotonicOver U D" shows "isMonotonicOver (U\\X X) (D\\X X)" 
unfolding bouthside_def using assms by auto
proposition l45d: assumes "isMonotonicOver U D" shows "isMonotonicOver U (D-C)" using assms by auto

lemma l43g: assumes "Range P\<subseteq>Domain P" "trans P" "wf (strict P)" shows "Range P=P``(les.minimals P)"
proof- let ?D=Domain let ?R=Range let ?m="les.minimals P" let ?X="?R P - P``?m" have 
"?R P \<subseteq> ?D P" using assms(1) by blast then have 
5: "?X\<subseteq>?D P" by auto
{ assume "?X \<noteq> {}" then have "P minimals ?X\<noteq>{}" using 5 l42k assms(3) by blast then obtain y where 
  3: "y\<in>P minimals ?X" by blast then have 
  4: "y\<in>?X" by (simp add: IntE l42j) have "y\<in>Range (strict' P)" using 3 unfolding restrict_def 
  Outside_def minimalsOfSet_def strict_def by auto then obtain z where "(z,y)\<in>P & z\<in>P``?m" using 
  l42j 3 DiffD1 DiffD2 DiffI IntE Range.RangeI RangeE mem_Collect_eq Domain.intros ImageI by smt
  then have "y\<in>P``?m" using assms(2) unfolding trans_def by blast then have False using 4 by blast
} then show ?thesis by blast
qed

lemma l43h: assumes "Range P \<subseteq> Domain P" "trans (P||(P``(les.minimals P)))" "wf (strict P)" 
"les.minimals P \<subseteq> P``(les.minimals P)" shows "Range P = P``(les.minimals P)"
proof- let ?D=Domain let ?R=Range let ?m="les.minimals P" let ?X="?R P - P``?m" have 
"?R P \<subseteq> ?D P" using assms(1) by blast then have 
5: "?X\<subseteq>?D P" by auto
{ assume "?X \<noteq> {}" then have "P minimals ?X\<noteq>{}" using 5 l42k assms(3) by blast then obtain y where 
  3: "y\<in>P minimals ?X" by blast then have 
  4: "y\<in>?X" by (simp add: IntE l42j) have "y\<in>Range (strict' P)" using 3 unfolding 
  minimalsOfSet_def restrict_def Outside_def strict_def by auto then obtain z where
  6: "(z,y)\<in>P & z\<in>P``?m" using l42j 3 DiffD1 DiffD2 DiffI IntE Range.RangeI RangeE mem_Collect_eq
  Domain.intros ImageI by (smt) have "z\<in>(P||(P``?m))``?m" using 6 assms(4) Int_absorb1 lll02 lll85 
  by (metis (no_types, lifting)) moreover have "(z,y)\<in>P||(P``?m)" unfolding restrict_def Outside_def  
  using 6 by blast ultimately have "y\<in>(P||P``?m)``?m" using assms(2) 6 unfolding trans_def by blast 
  then have False using 4 unfolding restrict_def Outside_def by blast
} then show ?thesis by blast
qed

lemma l43i: assumes "Y\<subseteq>Range P" "trans(P||Range P)" shows "trans(P||Y)" 
using assms(1,2) unfolding trans_def restrict_def Outside_def by blast

corollary l43hb: assumes "Range P \<subseteq> Domain P" "trans (P||(Range P))" "wf (strict P)" 
"les.minimals P \<subseteq> P``(les.minimals P)" shows "Range P \<subseteq> P``(les.minimals P)" 
using assms(1,2,3,4) l43h l43i Collect_cong ImageE Range.intros subsetI by (smt) 

lemma l43ga: assumes "Range P\<supseteq>Domain P" "trans P" "wf (strict (P^-1))" shows "Domain P=P^-1``(maximals3 P)"
unfolding maximals3_def using assms l43g[of "P^-1"] by auto

lemma l43ha: assumes "Range P\<supseteq>Domain P" "trans (P|^(P^-1``(maximals3' P)))" "wf (strict (P^-1))" 
"maximals3' P \<subseteq> P^-1``(maximals3' P)" shows "Domain P = P^-1``(maximals3' P)" using assms 
unfolding corestriction_def using l43h [of "P^-1"]  Domain_converse Range_converse assms(2) l42h 
trans_converse by (metis (mono_tags, lifting))

abbreviation "cograph' f Y == (f|||UNIV)|^Y" definition "cograph=cograph'" (* UNIV makes this uncomputable *)
abbreviation "cograph'' f Y == (f|||(f-`Y))|^Y" (*unused, also uncomputable*)
notation "cograph" ("_||^") 

proposition l37p: "(card o Range)-`{1} \<noteq> {}" (is "?L\<noteq>_")
proof- obtain x::"'a" and y::"'b" where True by simp have "(card o Range) ({x}\<times>{y})=1" 
by simp thus ?thesis by blast 
qed

proposition l37n: "runiq`((card o Range)-`{1}) = {True}" using l37j(3) l37p by fastforce

proposition l37o: assumes "Range`X \<subseteq> card-`{1}-{{{}}}" shows 
"{}\<notin>Union(Range`X)" and "X\<subseteq>runiq-`{True}-{{}}" (*can this be generalised? e.g. to Range -> f *)
apply simp using assms DiffE image_insert insert_subset mk_disjoint_insert singleton_insert_inj_eq 
nn56 insertCI vimage_singleton_eq apply (metis (no_types,lifting)) using assms l37j by auto 

abbreviation "biRestr' P X Y==(P \<inter> ((X\<times>Range P)) \<inter> (Domain P\<times>Y))" definition "biRestr=biRestr'"
abbreviation "biRestr'' P X Y==(P \<inter> ((X\<times>UNIV)) \<inter> (UNIV\<times>Y))"
notation "biRestr" ("_ |~ __"  3)


proposition l45e: "(\<forall>x\<in>Field P. (x,x)\<in>P)=reflex P" using assms unfolding reflex_def refl_on_def
by (simp add: ll59 order_refl)

proposition l45i: "P\\X Y = P-X\<times>UNIV-UNIV\<times>Y" unfolding bouthside_def by blast
proposition l45j: "fixPts (P\\X Y) = fixPts P-X-Y" unfolding l45i fixPts_def by blast
corollary l45k: "fixPts (P\\X X)=fixPts P - X" unfolding l45j by simp 
proposition l45l: "irrefl P = (fixPts P={})" unfolding fixPts_def irrefl_def by blast
proposition l45m: "(X\<inter>(fixPts P)={})=irrefl(P||X)" using assms unfolding 
restrict_def irrefl_def fixPts_def by blast
proposition l45n: "(X\<inter>(fixPts P)={})=irrefl(P|^X)" using assms unfolding 
corestriction_def irrefl_def fixPts_def by blast


text{*takes a list of sets and returns a list of disjoint constant, singleton-natural-valued functions over those sets*}
abbreviation "constList' m setlist==[(setlist!i)\<times>{{m+i}}. i<-[0..<size setlist]]" definition "constList=constList'" 

proposition l37z: "Union((Union o Range)`(set(constList m l))) \<subseteq> {m..<m+size l}" unfolding constList_def by fastforce

lemma l37q: "(X\<subseteq>card-`{1})=(\<forall>x\<in>X. \<exists> u. x={u})"using One_nat_def subsetI nn56 rev_subsetD 
vimage_singleton_eq subsetI the_elem_eq by (metis (no_types, lifting))

proposition l37r: assumes "{}\<notin>set(setlist)" shows "Range`(set (constList m setlist)) \<subseteq> card-`{1}-{{{}}}" (is "?L\<subseteq>_")
proof- have "\<forall>i<size setlist. setlist!i\<noteq>{}" using assms nth_mem by fastforce 
then have "\<forall>x\<in>?L. \<exists> m. x={{m}}" unfolding constList_def using assms by fastforce thus ?thesis using l37q by fastforce
qed

proposition l37s: "let L=constList m l in size L=size l &(\<forall>i<size l. L!i=l!i\<times>{{m+i}} & (l!i\<noteq>{}\<longrightarrow>Range (L!i)={{m+i}}))" unfolding constList_def
by fastforce
proposition l37t: "constList m l=map (\<lambda>i. l!i\<times>{{m+i}}) [0..<size l]" unfolding constList_def by blast

proposition l37u: "map Domain (constList m l)=map (nth l) [0..<size l] & map (nth l) [0..<size l]=l" 
unfolding l37t using map_nth by auto
find_consts " nat => 'a => 'a list"
corollary l37uu: "map Domain (constList m l)=l" using l37u by metis

lemma l37v: assumes "(trivial (Domain P) \<or> trivial(Range P))" shows "P=Domain P\<times>Range P"
using assms l37b l37c lm007b Domain_converse Domain_empty_iff Range_converse Sigma_cong Sigma_cong Times_empty converse_Times converse_inject 
by (smt)

proposition l37w: assumes "P\<in>set(constList m l)" "P\<noteq>{}" shows "P=Domain P \<times> Range P" using assms(1) unfolding l37t by auto

proposition l37x: assumes "P\<in>set(constList m l)" shows "trivial(Range P)" using assms l37s 
 Sigma_empty1 in_set_conv_nth trivial_singleton  Range_empty_iff lm007b by (metis)

proposition l37y: assumes "P\<in>set(constList m l)" shows "trivial((Union o Range) P)" and "finite ((Union o Range)P)"
apply auto
proof -
  have "\<forall>Ps P. \<exists>n. ((P::('a \<times> nat set) set) \<notin> set Ps \<or> n < length Ps) \<and> (P \<notin> set Ps \<or> Ps ! n = P)"
    by (meson in_set_conv_nth)
  then show 0: "trivial (\<Union>Range P)"
    by (metis Range_empty_iff Sigma_empty1 Sup_empty assms ccpo_Sup_singleton l37s lm007b trivial_singleton)
show "finite (\<Union>Range P)" using 0 by (simp add: SetUtils.lm54)
qed

proposition l37xx: "map (trivial o Range) (constList m l) = replicate (size l) True" using l37x
comp_apply l37s list.map_cong0 map_replicate_const by smt
(* proof - by sledgehammer
  have "\<forall>P n As. P \<notin> set (constList n (As::'a set list)) \<or> trivial (Range P)"
    using l37x by blast
  then have "map (trivial \<circ> Range) (constList m l) = map (\<lambda>P. True) (constList m l)"
    by auto
  then show ?thesis
    by (metis (no_types) l37s map_replicate_const)
qed*) 

proposition l37yy: "map (trivial o Union o Range) (constList m l) = replicate (size l) True" using l37y
comp_apply l37s list.map_cong0 map_replicate_const by smt

proposition l38: assumes "finite (N::nat set)" shows "N\<subseteq>{0..<1+Max N}" using assms 
by (simp add: Max_ge One_nat_def add.left_neutral atLeast0LessThan le_simps(2) lessThan_iff plus_nat.simps(2) subsetI)

proposition l37aa: assumes "f\<in>set (constList m l)" shows "runiq f" using assms l37j(2) l37x by blast 

proposition l45o: "fixPts' P\<subseteq>Domain P \<inter> Range P" by fast

abbreviation "list2Pair' l==(hd l, hd(tl l))" abbreviation "list2Pair2' l==(l!0, l!1)"
definition "list2Pair=list2Pair'"

proposition l47a: "list2Pair' (pair2List' x)=x" 
by (metis (mono_tags, lifting) case_prod_conv list.sel(1) list.sel(3) old.prod.exhaust)

proposition l47b: "let l=pair2List' x in list2Pair' l=list2Pair' l" 
using One_nat_def hd_conv_nth list.discI nth_Cons_Suc case_prod_conv list.sel(3) old.prod.exhaust
by (metis(no_types,lifting))

proposition l47c: "list2Pair' o pair2List'=id" using l47a by fastforce


abbreviation "finPow' X==Pow X \<inter> (card-`(UNIV-{0}))" (*not including {}!*) definition "finPow=finPow'"

proposition l51a: "pointUnion' f g,,,x \<subseteq> (f \<union> g),,,x" unfolding paste_def graph_def Outside_def by blast

proposition l51b: "pointUnion' f g,,,x \<supseteq> (f \<union> g),,,x"
using paste_def Diff_disjoint Diff_insert_absorb Domain.intros Image_singleton_iff 
MiscTools.ll33 MiscTools.ll37 MiscTools.lm19b RelationProperties.l31 Un_iff eval_runiq_rel 
 mem_simps(9) subsetI by smt
 
lemma l51: "eval_rel2 (pointUnion f g)=eval_rel2 (f \<union> g)" unfolding pointUnion_def using l51a l51b by fast

proposition l42s:  assumes "wf(strict P)" "P\<noteq>{}" shows "les.minimals P \<noteq> {}" using assms l42k [of "Domain P" P]  
by (metis(no_types) Diff_empty Diff_mono Diff_subset Domain_empty_iff Domain_mono Range_empty mm05n strict_def subset_empty)

proposition l42t: assumes "wf(strict(P^-1))" "P\<noteq>{}" shows "maximals3 P\<noteq>{}" using assms l42s 
by (metis Domain_converse Range_converse converse_empty converse_inject lm04c maximals3_def strict_def)

proposition "les.minimals P \<subseteq> Domain P" "maximals3 P \<subseteq> Range P" unfolding maximals3_def apply blast apply blast done

proposition assumes "x\<in>Field P" shows "(x\<in>les.minimals P) = (\<forall>y. (y,x)\<in>P \<longrightarrow> y=x)" 
using assms unfolding Field_def by blast

proposition l42u: assumes "x\<in>Field P" shows "(x\<in>maximals3 P) = (\<forall>y. (x,y)\<in>P \<longrightarrow> y=x)" 
unfolding maximals3_def using assms unfolding Field_def by blast

proposition l42uu: assumes "x\<in>Field P" shows "(x\<in>maximals3 P) = (P``{x}\<subseteq>{x})" 
unfolding maximals3_def using assms unfolding Field_def by blast

abbreviation "isQuasiLes' causality conflict == propagation conflict causality & 
sym conflict & irrefl conflict & trans causality & reflex causality"

proposition l45v: assumes "Id_on (Field P) \<subseteq> P" shows "Domain P=Range P" using assms unfolding Field_def by fastforce

proposition l45vv: assumes "reflex P" shows "Domain P=Range P" and "Field P=Domain P" using assms apply (simp add: l45v lm28f)
unfolding Field_def by (metis Un_absorb assms l45v lm28f)

lemma l25: assumes "m+1\<le>size l" shows "foldl F f (take (m+1) l)=F (foldl F f (take m l)) (l!m)"
using assms 
proof -
  have "m < length l"
    using assms by linarith
  then show ?thesis
    by (simp add: take_Suc_conv_app_nth)
qed
(* by (smt One_nat_def Suc_eq_plus1 add_Suc add_lessD1 foldl_Cons foldl_Nil foldl_append foldl_conv_fold hd_drop_conv_nth lessI ordered_cancel_comm_monoid_diff_class.le_iff_add take_Suc_conv_app_nth)*) 


lemma l27: assumes "runiq f" "X\<subseteq>Domain f" shows "f``X = (f,,,)`X" using assms(1,2) image_cong lll82 
lm025 subsetCE by (metis (no_types, lifting))

lemma l27a: assumes "(N::(nat set))\<noteq>{}" shows "Inf N\<in>N" using assms Inf_nat_def LeastI2_wellorder 
equals0D set_eq_iff by (metis)

lemma l27b: assumes "N\<noteq>{}" "(n::nat)<Inf N" shows "n\<notin>N" using assms(2) using bdd_above_bot cInf_lower leD by auto

lemma l27c: assumes "size l=Suc n" shows "l=(hd l)#(drop 1 l)" using assms by (metis Cons_nth_drop_Suc One_nat_def drop_0 hd_conv_nth length_greater_0_conv zero_less_Suc)

lemma l27d: assumes "size l>0" shows "drop (size l-1) l=[last l]" using assms Cons_nth_drop_Suc Suc_pred 
cancel_comm_monoid_add_class.diff_cancel diff_Suc_1 diff_less last_conv_nth length_drop 
length_greater_0_conv list.size(3) zero_less_one by (metis (no_types, lifting))

lemma l27e: assumes  "P f" "\<forall>x y. P x & P y \<longrightarrow> P (F x y)" shows 
"\<forall>l. size l=n \<longrightarrow> (((\<forall>x\<in>set l. P x)) \<longrightarrow>P (foldl F f l))" 
proof (induction n) let ?Q="\<lambda>n. (\<forall>l. size l=n \<longrightarrow> (((\<forall>x\<in>set l. P x)) \<longrightarrow>P (foldl F f l)))"
show "?Q 0" using assms by (simp add: foldl_Nil length_0_conv) fix n assume
0: "?Q n"
{
  fix l::"'a list" let ?g="last l" let ?l="take n l" 
  assume "size l=Suc n" then moreover have 
  1: "size ?l=n & l=?l@[?g] & ?g\<in>set l" using Zero_neq_Suc last_in_set length_0_conv rev_drop 
  append_take_drop_id diff_Suc_1 l27d length_drop length_rev length_take zero_less_Suc by metis 
  then moreover have "foldl F f l=F (foldl F f ?l) ?g" by (metis foldl_Cons foldl_Nil foldl_append)
  moreover assume "\<forall>x\<in>set l. P x" then moreover have "P ?g" using assms 0 1 by blast ultimately 
  moreover have "P (foldl F f ?l)" using 0 1  set_take_subset subsetCE by metis ultimately have 
  "P (foldl F f l)" using assms by presburger  
}
then show " ?Q (Suc n)" using assms by blast
qed

corollary l27ee: assumes "G=disj \<or> G=conj" "P f" "\<forall>x y. (G (P x) (P y)) \<longrightarrow> P (F x y)" "\<forall>x\<in>set l. P x" 
shows "P (foldl F f l)" using assms l27e by metis

lemma l27f: assumes "x \<in> X" shows "(f|||X),,,x = f x" using assms unfolding graph_def by blast 

lemma l29: assumes "\<exists> x. P x" shows "\<exists> l. size l=n & (\<forall>x\<in>set l. P x)" 
using assms in_set_replicate length_replicate by (metis)

lemma l30: "(P +< ((\<lambda>xx. P,,,xx \<union> Q,,,xx))|||(Domain Q)),,,x=P,,,x \<union> Q,,,x" 
unfolding graph_def paste_def Outside_def by blast

lemma l29b: "{(l!i),,,x|i. i<size l} = (\<lambda>f. f,,,x)`(set l)" (is "?L=?R") proof- have "?R\<subseteq>?L"
using image_subsetI in_set_conv_nth mem_Collect_eq by smt then show ?thesis by fastforce qed




















































































section{*Conflict: original proof*}

lemma l21a: assumes "\<forall>z1\<in>{x,y}\<inter>C. \<forall>z2\<in>{x,y}-C. A z1 \<inter> (f,,z2)={}" (*You'll replace {x,y} with Domain f*)
"runiq f" "x\<in>Domain f" "y\<in>Domain f" "h=f+<((\<lambda>x'. f,,x' \<union> A x')|||C)" 
"trivial (C\<inter>{x,y})" (*you'll replace this with U``C\<inter>C={}}; i.e., C is conflict-free*) 
"(x,y)\<in>U \<longrightarrow> f,,x \<inter> f,,y={}" "(x,y)\<in>U" "x\<noteq>y" shows "h,,x \<inter> (h,,y)={}"
proof - 
let ?g="\<lambda>x'. f,,x'\<union>A x'" let ?G="?g|||C" let ?h="f+<?G"let ?X="?h,,x-f,,x" let ?Y="?h,,y-f,,y" have 
5: "?X\<noteq>{} \<longrightarrow> x\<in>C & ?Y\<noteq>{} \<longrightarrow> y\<in>C" using assms(2,4) paste_def Outside_def DiffI Diff_cancel l19
MiscTools.ll37 by (metis (no_types, lifting)) have "?h,,x = f,,x \<union> ?X & ?h,,y = f,,y \<union> ?Y" using 
assms(2,3,4) l20b by force then have 
"?h,,x \<inter> ?h,,y = (f,,x \<inter> f,,y) \<union> (f,,x \<inter> ?Y) \<union> (?X\<inter>f,,y) \<union> (?X \<inter> ?Y)" by blast moreover have 
"...= (f,,x \<inter> ?Y) \<union> (?X\<inter>f,,y) \<union> (?X \<inter> ?Y)" using assms(7,8) by force ultimately have 
"?h,,x \<inter> ?h,,y = (f,,x \<inter> ?Y) \<union> (?X\<inter>f,,y) \<union> (?X\<inter>?Y)" by presburger moreover have 
"?Y\<noteq>{} \<longrightarrow> ?X={}" using assms(2,3,4,6,9) DiffI Diff_cancel Int_insert_right MiscTools.ll37 
SetUtils.lm01 insertI1 insert_commute l19 by smt ultimately have
4: "?h,,x \<inter> ?h,,y = (f,,x \<inter> ?Y) \<union> (?X\<inter>f,,y)" by blast have 
1: "?Y \<subseteq> A y" using assms(2,4) l20a by fastforce have 
0: "?X \<subseteq> A x" using assms(2,3) l20a by fastforce have "(?X\<noteq>{})\<longrightarrow>(x\<in>C)" using assms(2,3) DiffI 
Diff_cancel MiscTools.ll37 l19 by (metis (no_types, lifting)) moreover have 
"x\<in>C \<longrightarrow> y\<notin>C" using assms(6,9) Int_insert_right SetUtils.lm01 insertI1 insert_commute by metis 
ultimately have "?X\<noteq>{} \<longrightarrow> x\<in>C & y\<notin>C & (A x)\<inter>f,,y={}" using assms(1) UnE Un_Diff_cancel2 
sup.absorb_iff1 DiffI insertCI by blast then have 
2: "?X\<inter>f,,y={}" using 0 Int_ac(3) Un_empty inf.absorb_iff2 inf_assoc sup_inf_absorb
by (metis(no_types,lifting)) have "(?Y\<noteq>{})\<longrightarrow>(y\<in>C)" using assms(2,4) DiffI Diff_cancel 
MiscTools.ll37 l19 by (metis (no_types, lifting)) moreover have "y\<in>C \<longrightarrow> x\<notin>C" using assms(6,9)
Int_insert_right SetUtils.lm01 insertI1 insert_commute by (metis(no_types)) ultimately have 
"?Y\<noteq>{} \<longrightarrow> x\<notin>C & y\<in>C & (A y)\<inter>f,,x={}" using assms(1) UnE Un_Diff_cancel2 
sup.absorb_iff1 DiffI insertI1 insertI2 by blast then have 
"?Y\<inter>f,,x={}" using 1 Int_ac(3) Un_empty inf.absorb_iff2 inf_assoc sup_inf_absorb 
by (metis(no_types)) then show ?thesis using 2 4 assms(5) by blast
qed

corollary l21b: assumes "runiq f" "x\<in>Domain f" "y\<in>Domain f" "h=f+<((\<lambda>x'. f,,x' \<union> A x')|||C)" 
"f,,x \<inter> f,,y={} \<longrightarrow> (x,y)\<in>U" shows "h,,x\<inter>h,,y={} \<longrightarrow> (x,y)\<in>U" using assms(1,2,3,4,5) l20b by force

lemma l21c: assumes "runiq f" "h=f+<((\<lambda>x'. f,,x' \<union> A x')|||C)" "x\<in>Domain f" "y\<in>Domain f"
"(x,y)\<in>U = (f,,x \<inter> f,,y={})" "\<forall>z1\<in>{x,y}\<inter>C. \<forall>z2\<in>{x,y}-C. A z1 \<inter> (f,,z2)={}" (*You'll replace {x,y} with Domain f*)
"(x,y)\<in>U \<longrightarrow> (x\<noteq>y & trivial (C\<inter>{x,y}))" shows "(x,y)\<in>U=(h,,x \<inter> (h,,y)={})" (is "?L=?R") 
proof - have 7: "(x,y)\<in>U \<longrightarrow> f,,x\<inter>f,,y={}" using assms by auto
have 0: "f,,x \<inter> f,,y={}\<longrightarrow>(x,y)\<in>U" using assms(5) by blast moreover have 
10: "?R\<longrightarrow>?L" using assms(1,3,4,2) 0 by (rule l21b)
{ assume 8: ?L 
  then moreover have 9: "x\<noteq>y" using assms by blast ultimately moreover have 
  6: "trivial (C\<inter>{x,y})" using assms by fast   
  have ?R using assms(6,1,3,4,2) 6 7 8 9 by (rule l21a)
} then show ?thesis using 10 by blast 
qed

lemma l21: assumes "runiq f" "h=f+<((\<lambda>x'. f,,x' \<union> A x')|||C)" "C\<subseteq>Domain f"
"\<forall>x\<in>Domain f.\<forall>y\<in>Domain f.((x,y)\<in>U = (f,,x \<inter> f,,y={})) & ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(C\<inter>{x,y})))" 
"\<forall>x\<in>(Domain f)\<inter>C. \<forall>y\<in>(Domain f)-C. A x \<inter> (f,,y)={}" shows 
"\<forall>x\<in>Domain h. \<forall> y\<in>Domain h. (x,y)\<in>U=(h,,x \<inter> (h,,y)={})"
proof-let ?g="\<lambda>x'. f,,x' \<union> A x'" let ?G="?g|||C" let ?h="f+<?G" let ?D=Domain have 
2: "h=?h" using assms by blast moreover have 
1: "?D ?h=?D f" using assms by (simp add: MiscTools.ll37 Un_absorb2 paste_Domain)
{
  fix x y assume "x\<in>?D h" moreover assume "y\<in>?D h" ultimately moreover have 
  6: "\<forall>z1\<in>{x,y}\<inter>C. \<forall>z2\<in>{x,y}-C. A z1 \<inter> (f,,z2)={}" using assms 1 by fast ultimately moreover have 
  5: "(x,y)\<in>U = (f,,x \<inter> f,,y={})" using assms 1 by auto ultimately moreover have
  7: "(x,y)\<in>U \<longrightarrow> (x\<noteq>y & trivial (C\<inter>{x,y}))" using assms 1 by auto ultimately moreover have 
  3: "x\<in>?D f" using 1 2 by metis ultimately moreover have 4: "y\<in>?D f" using 1 2 by metis 
  have "(x,y)\<in>U=(h,,x \<inter> (h,,y)={})" using assms(1) 2 3 4 5 6 7 by (rule l21c)
} thus ?thesis by simp
qed





















section{* partial order: original proof *}

lemma l23a: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" 
 "(x,y)\<in>D \<longrightarrow> (f,,x \<supseteq> f,,y & ((x\<in>C & y\<in>C) \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))" "(x,y)\<in>D" shows "h,,x \<supseteq> (h,,y)"
proof - let ?g="\<lambda>x'. f,,x' \<union> A x'" let ?G="?g|||C" let ?h="f+<?G" have 
0: "(f,,x) \<supseteq> (f,,y)" using assms(6,7) by linarith have
1: "f,,x \<subseteq> ?h,,x" using assms(1,3) l20b by fast have 
2: "?h,,y \<subseteq> f,,y \<union> A y" using assms(1,4) l20a by fast 
{
  assume "\<not> ?thesis" then have "f,,y \<noteq> ?h,,y" using 0 1 using assms(5) order_trans by blast then have 
  3: "y\<in>C" using assms(1,4) by (metis (no_types, lifting) DiffI MiscTools.ll37 l19) then have 
  4: "x\<in>C" using assms(6,7) by blast then have "?h,,x = f,,x \<union> A x" using assms(2,1) by (rule l22) 
  moreover have "A y \<subseteq> A x" using assms (6,7) 3 4 by blast ultimately have "?h,,y \<subseteq> ?h,,x" using 0 2
  inf_sup_aci(5) inf_sup_aci(7) subset_Un_eq by blast
}
thus ?thesis using assms(5) by fast
qed

lemma l23aa: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" 
"(x,y)\<in>D \<longrightarrow> (f,,x \<supseteq> f,,y & ((x\<in>C & y\<in>C) \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))" shows "(x,y)\<in>D \<longrightarrow>h,,x \<supseteq> (h,,y)"
using assms l23a 
proof-
{
  assume 0: "(x,y)\<in>D" have "h,,x \<supseteq> h,,y" using assms 0 by (rule l23a) 
} thus ?thesis by blast
qed

lemma l23b: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" 
"x\<in>C \<longrightarrow> f,,y \<inter> (A x - (f,,x)) = {}" "h,,x \<supseteq> (h,,y)" shows "f,,x \<supseteq> f,,y"
proof - let ?g="\<lambda>x'. f,,x' \<union> A x'" let ?G="?g|||C" let ?h="f+<?G" have 
0: "h=?h" using assms(5) by blast then have 
1: "?h,,x \<supseteq> ?h,,y" using assms(7) by blast moreover have 
2: "f,,y \<subseteq> ?h,,y" using l20b assms(1,4) by fast ultimately have 
4: "f,,y\<subseteq>?h,,x" by blast
{ assume 
  3: "\<not> (f,,x\<supseteq>f,,y)" then moreover have "f,,x\<noteq>?h,,x" using 1 2 by blast then moreover have 
  5: "x\<in>C" by (metis (lifting) assms(1,3) DiffI MiscTools.ll37 l19) ultimately moreover
  have "?h,,x=f,,x\<union>A x" using assms(1,2,3) eval_runiq_rel outside_reduces_domain paste_Domain l19  
  paste_def runiq_paste2 sup.orderE Domain.intros MiscTools.ll33 MiscTools.ll37 UnE RelationProperties.l31 by smt
  then have "f,,y \<subseteq> f,,x \<union> A x" using 4 by blast then 
  have "f,,y \<inter> (A x - f,,x) \<noteq> {}" using 3 by blast then have False using 5 assms(6) by fast 
}
thus ?thesis by blast
qed

lemma l23bb: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" 
"x\<in>C \<longrightarrow> f,,y \<inter> (A x - (f,,x)) = {}"  shows "h,,x \<supseteq> (h,,y) \<longrightarrow> f,,x \<supseteq> f,,y"
proof-{assume 0: "h,,x \<supseteq> h,,y" have "f,,x\<supseteq>f,,y" using assms 0 by (rule l23b)} thus ?thesis by blast qed

lemma "((x,y)\<in>D \<longrightarrow> (((x\<in>C & y\<in>C) \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))) = 
(((x,y)\<in>D & y\<in>C) \<longrightarrow> (x\<in>C & A x \<supseteq> A y))" by fast

lemma l23c: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)"
"(x,y)\<in>D \<longrightarrow> (((x\<in>C & y\<in>C) \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))" "x\<in>C \<longrightarrow> f,,y \<inter> (A x - (f,,x)) = {}" 
"(x,y)\<in>D = (f,,x \<supseteq> f,,y)" 
shows "(x,y)\<in>D = (h,,x \<supseteq> h,,y)" (is "?L = ?R")
proof- have 
0: "(x,y)\<in>D \<longrightarrow> (f,,x \<supseteq> f,,y & (x\<in>C & y\<in>C \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))" using assms(6,8) by force
have "?L\<longrightarrow>?R" using assms(1,2,3,4,5) 0 by (rule l23aa) moreover have 
"((h,,x) \<supseteq> (h,,y)) \<longrightarrow> f,,x \<supseteq> f,,y" using assms(1,2,3,4,5,7) by (rule l23bb)
ultimately show ?thesis using assms(8) by meson
qed

lemma l23cc: assumes "runiq f" "C\<subseteq>Domain f" "x\<in>Domain f" "y\<in>Domain f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)"
"x\<in>C\<longrightarrow>f,,y\<inter>(A x-(f,,x))={}" "(((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y))" "(x,y)\<in>D = (f,,x \<supseteq> f,,y)" 
shows "(x,y)\<in>D = (h,,x \<supseteq> h,,y)" 
proof - have 
6: "(x,y)\<in>D \<longrightarrow> (((x\<in>C & y\<in>C) \<longrightarrow> A x \<supseteq> A y) & (y\<in>C \<longrightarrow> x\<in>C))" using assms(7) by blast
show ?thesis using assms(1,2,3,4,5) 6 assms(6,8) by (rule l23c)
qed

lemma l23: assumes "runiq f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" "C\<subseteq>Domain f" "\<forall>x\<in>Domain f. \<forall>y\<in>Domain f.
(x\<in>C\<longrightarrow>f,,y\<inter>(A x-(f,,x))={}) & (((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y)) & ((x,y)\<in>D = (f,,x \<supseteq> f,,y))" 
shows "\<forall>x\<in>Domain h. \<forall>y\<in>Domain h. (x,y)\<in>D = (h,,x \<supseteq> h,,y)" 
proof-let ?g="\<lambda>x'. f,,x' \<union> A x'" let ?G="?g|||C" let ?h="f+<?G" let ?D=Domain have 
5: "h=?h" using assms by blast moreover have 
1: "?D ?h=?D f" using assms by (simp add: MiscTools.ll37 Un_absorb2 paste_Domain)
{
  fix x y assume "x\<in>?D h" moreover assume "y\<in>?D h" ultimately moreover  have 
  3: "x\<in>?D f" using 1 5 by metis ultimately moreover have 
  4: "y\<in>?D f" using 1 5 by metis ultimately moreover have 
  6: "x\<in>C\<longrightarrow>f,,y\<inter>(A x-(f,,x))={}" using assms by meson ultimately moreover have 
  7: "(((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y))" using assms by meson ultimately moreover have 
  8: "(x,y)\<in>D = (f,,x \<supseteq> f,,y)" using assms by meson have "(x,y)\<in>D = (h,,x \<supseteq> h,,y)" 
  using assms(1,3) 3 4 5 6 7 8 by (rule l23cc)
} thus ?thesis by blast
qed

lemma l26: assumes "runiq f" "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" "C\<subseteq>Domain f"
"\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(C\<inter>{x,y}))) & 
(x\<in>C\<longrightarrow>((y\<notin>C \<longrightarrow> f,,y\<inter>A x={}) & (y\<in>C \<longrightarrow> f,,y\<inter>(A x-(f,,x))={}))) & (((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y))"
"\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U = (f,,x \<inter> f,,y={})) & ((x,y)\<in>D=(f,,x \<supseteq> f,,y))"
shows "\<forall>x\<in>Domain h. \<forall>y\<in>Domain h. ((x,y)\<in>U = (h,,x \<inter> h,,y={})) & ((x,y)\<in>D=(h,,x \<supseteq> h,,y))"
proof- have 
4: "\<forall>x\<in>Domain f.\<forall>y\<in>Domain f.((x,y)\<in>U = (f,,x \<inter> f,,y={})) & ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(C\<inter>{x,y})))"
using assms by meson have 5: "\<forall>x\<in>(Domain f)\<inter>C. \<forall>y\<in>(Domain f)-C. A x \<inter> (f,,y)={}" using assms by blast have 
0: "\<forall>x\<in>Domain h. \<forall> y\<in>Domain h. (x,y)\<in>U=(h,,x \<inter> (h,,y)={})" using assms(1,2,3) 4 5 by (rule l21)
have "\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. (x\<in>C\<longrightarrow>f,,y\<inter>(A x-(f,,x))={})" using assms by blast then have 14: 
"\<forall>x\<in>Domain f. \<forall>y\<in>Domain f.(x\<in>C\<longrightarrow>f,,y\<inter>(A x-(f,,x))={}) & (((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y)) & 
 ((x,y)\<in>D = (f,,x \<supseteq> f,,y))" using assms by meson have "\<forall>x\<in>Domain h. \<forall>y\<in>Domain h. 
 (x,y)\<in>D = (h,,x \<supseteq> h,,y)" using assms(1-3) 14 by (rule l23) thus ?thesis using 0 by auto
qed

theorem preservedRepresentation: assumes "runiq f" "h=f+<((\<lambda>x'. f,,,x' \<union> A x')|||C)" "C\<subseteq>Domain f" (is "_\<subseteq>?D f")
"\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(C\<inter>{x,y})))" "(Union (Range f))\<inter>Union(A`C)={}"
"\<forall>x\<in>Domain f. \<forall>y\<in>C. (x,y)\<in>D\<longrightarrow>(x\<in>C & A x \<supseteq> A y)" "isRepresentation' f D U" shows "isRepresentation' h D U"
proof- 
have "\<forall>x\<in>C. f,,x=f,,,x" using assms (1-3) by (meson lll82 subsetCE) then have 
"((\<lambda>x'. f,,x' \<union> A x')|||C)=((\<lambda>x'. f,,,x' \<union> A x')|||C)" unfolding graph_def by blast then have
2: "h=f+*((\<lambda>x'. f,,x' \<union> A x')|||C)" using assms(2) by presburger
have "\<forall>x\<in>?D f. \<forall>y\<in>?D f. x\<in>C \<longrightarrow>(y\<in>C \<longrightarrow> f,,,y\<inter>(A x-(f,,,x))={})" using assms(1,5) by blast then have 
"\<forall>x\<in>?D f. \<forall>y\<in>?D f. x\<in>C \<longrightarrow>(y\<in>C \<longrightarrow> f,,y\<inter>(A x-(f,,x))={})" by (metis assms(1) lll82) moreover have 
"\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. x\<in>C\<longrightarrow>(y\<notin>C \<longrightarrow> f,,y\<inter>A x={})" using assms Int_commute Sup_inf_eq_bot_iff 
eval_runiq_in_Range image_eqI by (metis (no_types, lifting)) moreover have 
"\<forall>x\<in>?D f. \<forall>y\<in>?D f. (((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y))" using assms by meson ultimately have 
4: "\<forall>x\<in>?D f. \<forall>y\<in>?D f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(C\<inter>{x,y}))) & 
(x\<in>C\<longrightarrow>((y\<notin>C \<longrightarrow> f,,y\<inter>A x={}) & (y\<in>C \<longrightarrow> f,,y\<inter>(A x-(f,,x))={}))) & (((x,y)\<in>D & y\<in>C)\<longrightarrow>(x\<in>C & A x \<supseteq> A y))"
using assms by metis moreover have 
5: "\<forall>x\<in>?D f. \<forall>y\<in>?D f. ((x,y)\<in>U = (f,,x \<inter> f,,y={})) & ((x,y)\<in>D=(f,,x \<supseteq> f,,y))" using assms 
by meson have "\<forall>x\<in>?D h. \<forall>y\<in>?D h. ((x,y)\<in>U = (h,,x \<inter> h,,y={})) & ((x,y)\<in>D=(h,,x \<supseteq> h,,y))" 
using assms(1) 2 assms(3) 4 5 by (rule  l26) thus ?thesis by auto
qed



































section{*Representation extension*}

lemma l24: assumes "runiq f" "x\<in>Domain f" "(s,s)\<in>D" "D``{s}\<subseteq>{s}" (* Can you weaken this to 
D``{s}\<subseteq>D^-1``{s} for preorders? Probably yes. *) "\<not>(f,,x \<subseteq> A)" "A \<subseteq> f,,x = (x\<in>D^-1``{s})" 
"((f,,x\<inter>A)={})=(x\<in>U^-1``{s})" "s\<notin>Domain f" "((x,s)\<in>U) = ((s,x)\<in>U)" "F=f+<(s,A)" shows 
"F,,s\<supseteq>F,,x=((s,x)\<in>D)" "(F,,x \<supseteq> F,,s)=((x,s)\<in>D)" "(F,,x \<inter> F,,s={})=((x,s)\<in>U)" "(F,,s \<inter> F,,x={})=((s,x)\<in>U)"
proof- let ?d="D--s" let ?u="U--s" let ?F="f+<(s,A)" have
1: "runiq ?F" by (simp add: assms(1) runiq_paste2 runiq_singleton_rel) then have
2: "?F,,s=A" using RelationProperties.l31 paste_def by fastforce have 
10: "(?F,,s\<supseteq>?F,,x)=((s,x)\<in>D)" proof- have 
  "(s,x)\<in>D \<longrightarrow> A=?F,,x" using assms(4) 2 SetUtils.lm00 singletonD subset_eq by blast moreover have 
  "A\<supseteq>?F,,x \<longrightarrow> (s,x)\<in>D" using  assms(1,2,3,4,5) DiffI Domain_empty Domain_insert Image_singleton_iff 
   insert_absorb l19 prod.collapse runiq_singleton_rel subset_singletonD by metis ultimately 
  show ?thesis using 2 by blast qed then show "F,,s\<supseteq>F,,x=((s,x)\<in>D)" using assms(10) by fast  
have "(?F,,x \<supseteq> ?F,,s)=((x,s)\<in>D)" using assms(1,3,2,6) 2 DiffI Domain_empty Domain_insert Image_singleton_iff  
converse_iff l19 prod.collapse runiq_singleton_rel singletonD subset_iff by smt then show 
"(F,,x \<supseteq> F,,s)=((x,s)\<in>D)" using assms(10) by fast have 
7: "(?F,,x \<inter> ?F,,s={}) \<longrightarrow> (x,s)\<in>U" using assms(1,2,7) 2 inf.idem inf_bot_right l19 prod.collapse 
runiq_singleton_rel singletonD DiffI Domain_empty Domain_insert Image_singleton_iff converse.cases 
by (metis(no_types, lifting)) moreover have 
8: "(x,s)\<in>U \<longrightarrow> ?F,,x \<inter> ?F,,s={} " using assms(1,2,7,8) 2 
DiffI Domain_empty Domain_insert Image_singleton_iff converse.intros l19 prod.collapse 
runiq_singleton_rel singletonD by (metis) ultimately show
"(F,,x \<inter> F,,s={})=((x,s)\<in>U)" using assms(10) by fast
have "(?F,,s \<inter> ?F,,x={}) \<longrightarrow> ((s,x)\<in>U)" using assms(9) 7 by blast moreover 
have "((s,x)\<in>U) \<longrightarrow> (?F,,s \<inter> ?F,,x={})" using 8 assms(9) Int_commute by blast
ultimately show "(F,,s \<inter> F,,x={})=((s,x)\<in>U)" using assms(10) by fast
qed

lemma extension: assumes "runiq f" "(s,s)\<in>D" "D``{s}\<subseteq>{s}" "s\<notin>Domain f" 
"\<forall>x\<in>Domain f. \<not>(f,,x \<subseteq> RA)" 
"\<forall>x\<in>Domain f. RA \<subseteq> f,,x = (x\<in>D^-1``{s})" "\<forall>x\<in>Domain f. ((f,,x)\<inter>RA={})=(x\<in>U^-1``{s})" 
"\<forall>x\<in>Domain f. ((x,s)\<in>U) = ((s,x)\<in>U)" "isRepresentation' f (D---s s) (U---s s)"
"F=f+<(s,RA)" "RA\<noteq>{}" "(s,s)\<notin>U" shows "isRepresentation' F D U" 
proof- let ?d="D---s s" let ?u="U---s s" let ?F="f+<(s,RA)" have 
0: "F=?F & Domain ?F=Domain f \<union> {s}" using assms(10) by (simp add: paste_Domain) then moreover have 
1: "Domain f=Domain ?F-{s}" by (simp add: assms(4)) ultimately moreover have 
3: "s\<in>Domain ?F" using UnI2 insertI1 assms(1) fst_conv lm60 paste_def runiq_paste2 
runiq_singleton_rel snd_conv by metis have  
33: "?F,,s=RA" using MiscTools.lm60 Un_insert_right assms(1) fst_conv insert_iff paste_def runiq_paste2 runiq_singleton_rel snd_conv 
by (metis )
ultimately moreover have 
2: "\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. (((x, y)\<in>D)=((x,y)\<in>?d) & (((x, y)\<in>U)=((x,y)\<in>?u)))" 
unfolding singlebouthside_def using DiffD2 DiffI Outside_def mem_Sigma_iff by blast  have 
15: "\<forall>x\<in>Domain ?F. \<forall>y\<in>Domain ?F. (((x, y)\<in>D)=(?F,,x \<supseteq> ?F,,y))" proof-
{
  assume "\<not>?thesis" then moreover obtain x y where 
  11: "x\<in>Domain ?F & y\<in>Domain ?F & (x,y)\<in>D \<noteq> (?F,,x \<supseteq> ?F,,y)" by blast then 
  moreover have "x\<notin>Domain f \<or> y\<notin>Domain f"using assms(1,9) 1 2 Diff_idemp Domain_empty Domain_insert 
  l19 prod.collapse runiq_singleton_rel by (metis (no_types, lifting)) ultimately moreover have 
  "(x\<notin>Domain f & x=s) \<or> (y\<notin>Domain f & y=s)" using 0 1 2 Diff_iff singletonD by auto
  ultimately moreover have "y=s \<longrightarrow> x=s" using assms(1,6) 0 1 Diff_idemp Domain_empty Domain_insert 
  Image_singleton_iff UnE Un_insert_right 
  converse_iff eval_runiq_rel insert_iff l19 paste_def prod.collapse prod.inject runiq_basic 
  runiq_paste2 singletonD by smt ultimately moreover have "y=s \<longrightarrow> ?thesis" using assms(2) by auto
  ultimately moreover have 
  10: "x\<notin>Domain f & x=s & y\<in>Domain f & y\<noteq>s" by (metis "0" UnE singletonD) then have 
  2: "y\<in>Domain f" by simp have 
  5: "\<not>(f,,y \<subseteq> RA)" using "10" assms(5) by blast have 
  6: "RA \<subseteq> f,,y = (y\<in>D^-1``{s})" using "10" assms(6) by auto have 
  7: "((f,,y\<inter>RA)={})=(y\<in>U^-1``{s})" using "10" assms(7) by auto have 
  9: "((y,s)\<in>U) = ((s,y)\<in>U)" by (simp add: "10" assms(8))
  have "F,,s \<supseteq> F,,y=((s, y)\<in>D)" using assms(1) 2 assms(2,3) 5 6 7 assms(4) 9 assms(10) 
by (rule l24(1)) then have "F,,x\<supseteq>F,,y=((x,y)\<in>D)" using 10 by presburger then have False using 11 assms(10) by meson
} thus ?thesis by fast
qed have 
16: "\<forall>x\<in>Domain ?F. \<forall>y\<in>Domain ?F. (((x,y)\<in>U) = ((?F,,x \<inter> ?F,,y)={}))" proof-
{
  assume "\<not>?thesis" then moreover obtain x y where 
  21: "x\<in>Domain ?F & y\<in>Domain ?F & (x,y)\<in>U \<noteq> (?F,,x \<inter> ?F,,y={})" by blast then 
  moreover have "x\<notin>Domain f \<or> y\<notin>Domain f" using assms(1,9) 1 2 Diff_idemp Domain_empty Domain_insert l19 
  prod.collapse runiq_singleton_rel by (metis (no_types, lifting)) ultimately moreover have 
  "(x\<notin>Domain f & x=s) \<or> (y\<notin>Domain f & y=s)" using 0 1 2 Diff_iff singletonD by auto
  ultimately moreover  have "y=s \<longrightarrow> x=s" 
  print_state using assms(1-8) 21 Un_iff l24(3) singletonD 0 by (metis(no_types))
  ultimately moreover have "y=s" using assms(1-8) 0  Int_commute UnE l24(3) singletonD (*l19*) 
  by(metis(no_types, lifting))
  ultimately have False using assms(11,12) 3 33 inf.idem by (metis) 
} thus ?thesis by fast
qed
show ?thesis using 15 16 0 by meson
qed























section{*Iterative representation preservation*}

corollary l28: assumes "runiq f" "F=(\<lambda>f A. f +* ((\<lambda>x. f,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F")
"\<forall>A\<in>set l. Domain A \<subseteq> Domain f & runiq A" shows "Domain (foldl F f l) \<subseteq> Domain f & runiq (foldl F f l)" 
using l27ee[of conj "\<lambda>g. Domain g \<subseteq> Domain f & runiq g"] MiscTools.ll37 Un_subset_iff order_refl paste_Domain runiq_paste2
proof -
  have f1: "\<forall>P fa Ps. (Domain (P::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq P) \<and> (\<forall>P Pa. (Domain P \<subseteq> Domain f \<and> runiq P) \<and> Domain (Pa::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq Pa \<longrightarrow> Domain (fa P Pa) \<subseteq> Domain f \<and> runiq (fa P Pa)) \<and> (\<forall>P. unset (set Ps) P \<longrightarrow> Domain P \<subseteq> Domain f \<and> runiq P) \<longrightarrow> Domain (foldl fa P Ps) \<subseteq> Domain f \<and> runiq (foldl fa P Ps)"
    by (simp add: \<open>\<And>l fa F. \<lbrakk>op \<and> = op \<or> \<or> op \<and> = op \<and>; Domain fa \<subseteq> Domain f \<and> runiq fa; \<forall>x y. (Domain x \<subseteq> Domain f \<and> runiq x) \<and> Domain y \<subseteq> Domain f \<and> runiq y \<longrightarrow> Domain (F x y) \<subseteq> Domain f \<and> runiq (F x y); \<forall>x\<in>set l. Domain x \<subseteq> Domain f \<and> runiq x\<rbrakk> \<Longrightarrow> Domain (foldl F fa l) \<subseteq> Domain f \<and> runiq (foldl F fa l)\<close>)
  have f2: "\<forall>x0 x1 x3. ((Domain (x1::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq x1) \<and> Domain (x0::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq x0 \<longrightarrow> Domain (x3 x1 x0::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq (x3 x1 x0)) = (((\<not> Domain x1 \<subseteq> Domain f \<or> \<not> runiq x1) \<or> \<not> Domain x0 \<subseteq> Domain f \<or> \<not> runiq x0) \<or> Domain (x3 x1 x0) \<subseteq> Domain f \<and> runiq (x3 x1 x0))"
    by fastforce
  have f3: "\<forall>x0 x1 x2. ((Domain (x2::('a \<times> 'b set) set) \<subseteq> Domain f \<and> runiq x2) \<and> (\<forall>v3 v4. ((\<not> Domain v3 \<subseteq> Domain f \<or> \<not> runiq v3) \<or> \<not> Domain (v4::('a \<times> 'b set) set) \<subseteq> Domain f \<or> \<not> runiq v4) \<or> Domain (x1 v3 v4) \<subseteq> Domain f \<and> runiq (x1 v3 v4)) \<and> (\<forall>v3. v3 \<notin> set x0 \<or> Domain v3 \<subseteq> Domain f \<and> runiq v3) \<longrightarrow> Domain (foldl x1 x2 x0) \<subseteq> Domain f \<and> runiq (foldl x1 x2 x0)) = (((\<not> Domain x2 \<subseteq> Domain f \<or> \<not> runiq x2) \<or> (\<exists>v3 v4. ((Domain v3 \<subseteq> Domain f \<and> runiq v3) \<and> Domain v4 \<subseteq> Domain f \<and> runiq v4) \<and> (\<not> Domain (x1 v3 v4) \<subseteq> Domain f \<or> \<not> runiq (x1 v3 v4))) \<or> (\<exists>v3. unset (set x0) v3 \<and> (\<not> Domain v3 \<subseteq> Domain f \<or> \<not> runiq v3))) \<or> Domain (foldl x1 x2 x0) \<subseteq> Domain f \<and> runiq (foldl x1 x2 x0))"
    by meson
  obtain PP :: "('a \<times> 'b set) set list \<Rightarrow> ('a \<times> 'b set) set" where
    f4: "\<forall>x0. (\<exists>v3. unset (set x0) v3 \<and> (\<not> Domain v3 \<subseteq> Domain f \<or> \<not> runiq v3)) = (unset (set x0) (PP x0) \<and> (\<not> Domain (PP x0) \<subseteq> Domain f \<or> \<not> runiq (PP x0)))"
    by moura
  obtain PPa :: "(('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set) \<Rightarrow> ('a \<times> 'b set) set" and PPb :: "(('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set) \<Rightarrow> ('a \<times> 'b set) set" where
    "\<forall>x1. (\<exists>v3 v4. (Domain v3 \<subseteq> Domain f \<and> runiq v3 \<and> Domain v4 \<subseteq> Domain f \<and> runiq v4) \<and> (\<not> Domain (x1 v3 v4) \<subseteq> Domain f \<or> \<not> runiq (x1 v3 v4))) = ((Domain (PPa x1) \<subseteq> Domain f \<and> runiq (PPa x1) \<and> Domain (PPb x1) \<subseteq> Domain f \<and> runiq (PPb x1)) \<and> (\<not> Domain (x1 (PPa x1) (PPb x1)) \<subseteq> Domain f \<or> \<not> runiq (x1 (PPa x1) (PPb x1))))"
    by moura (* > 1.0 s, timed out *)
  then have f5: "\<forall>P fa Ps. (\<not> Domain P \<subseteq> Domain f \<or> \<not> runiq P \<or> (Domain (PPa fa) \<subseteq> Domain f \<and> runiq (PPa fa) \<and> Domain (PPb fa) \<subseteq> Domain f \<and> runiq (PPb fa)) \<and> (\<not> Domain (fa (PPa fa) (PPb fa)) \<subseteq> Domain f \<or> \<not> runiq (fa (PPa fa) (PPb fa))) \<or> unset (set Ps) (PP Ps) \<and> (\<not> Domain (PP Ps) \<subseteq> Domain f \<or> \<not> runiq (PP Ps))) \<or> Domain (foldl fa P Ps) \<subseteq> Domain f \<and> runiq (foldl fa P Ps)"
    using f4 f3 f2 f1 by presburger
  have "\<forall>P Pa. (\<not> runiq (P::('a \<times> 'b set) set) \<or> \<not> runiq Pa) \<or> runiq (Pa +< P)"
    by (meson \<open>\<And>Q P. \<lbrakk>runiq Q; runiq P\<rbrakk> \<Longrightarrow> runiq (P +< Q)\<close>)
  then have f6: "runiq (F (PPa F) (PPb F)) \<or> \<not> runiq (PPa F)"
    using \<open>\<And>f X. runiq (f ||| X) \<and> Domain (f ||| X) = X\<close> assms(2) by blast
  have "Domain (PPa F) \<union> Domain (PPb F) = Domain (F (PPa F) (PPb F))"
    by (simp add: \<open>\<And>Q P. Domain (P +< Q) = Domain P \<union> Domain Q\<close> \<open>\<And>f X. runiq (f ||| X) \<and> Domain (f ||| X) = X\<close> assms(2))
  then show ?thesis
    using f6 f5 by (metis \<open>\<And>C B A. (A \<union> B \<subseteq> C) = (A \<subseteq> C \<and> B \<subseteq> C)\<close> \<open>\<And>x. x \<le> x\<close> assms(1) assms(3))
qed

corollary (*l32:*) assumes "runiq f" "\<forall>x\<in>set l. runiq x" shows "runiq (foldl paste f l)" using assms l27e runiq_paste2 by (metis)

theorem l30a: assumes "F=(\<lambda>f A. f +< ((\<lambda>x. f,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F") shows 
"(foldl F f l),,,x = (f,,,x) \<union> Union {(l!i),,,x|i. i<size l}" 
proof-
let ?X="\<lambda> l. {(l!i),,,x|i. i<size l}" let ?L="\<lambda>l. foldl F f l" let ?P="\<lambda>l. (\<forall>A\<in>set l. True )"
let ?Q="\<lambda>l. (?L l),,,x = f,,,x \<union> Union (?X l)" let ?E="{n. \<exists> l. \<not>((size l=n & ?P l)\<longrightarrow> ?Q l)}" 
let ?n="Inf ?E" let ?m="?n-1" have 
0: "?L []=f & 0\<notin>?E" by auto
{
  assume "?E\<noteq>{}" then moreover  have "?n\<in>?E" by (rule l27a) ultimately have 
  "?E\<noteq>{} & ?n\<in>?E & ?n>0" using 0 by auto then have 
  1: "?E\<noteq>{} & ?n\<in>?E & ?n>0 & ?m<?n" by linarith then have "?m\<notin>?E" using l27b by meson then have 
  3: "\<forall>l. (size l=?m & ?P l)\<longrightarrow>?Q l" by blast have "\<exists> u. True" unfolding 
  runiq_def trivial_def by blast then have "\<exists>ln. size ln=?n & ?P ln" by (rule l29) 
  then obtain ln::"('a \<times> 'b set) set list" where 
  2: "size ln=?n & ?P ln & \<not>(?Q ln)" by (smt 1 mem_Collect_eq) let ?lm="take (size ln-1) ln" 
  have "size ?lm=?m & size ln=Suc ?m" using 1 2 by auto then have
  4: "size ?lm=?m & ?P ?lm & size ln=Suc ?m" using set_mp set_take_subset by 
  (metis (no_types, lifting)) then have
  5: "?Q ?lm" using 3 by meson have "?m+1\<le>size ln" using 2 1 3 by linarith 
  have "foldl ?F f (?lm@[last ln])=?F (foldl ?F f ?lm) (last ln)" by auto moreover have 
  6: "ln=?lm@[last ln]" using 1 2 3 by (metis append_take_drop_id l27d) moreover ultimately have 
  "foldl F f ln=F (foldl F f ?lm) (last ln)" unfolding assms(1) by fastforce moreover have "...=
  foldl F f ?lm +< (\<lambda>xx. (foldl F f ?lm),,,xx \<union> (last ln),,,xx)|||(Domain (last ln))" using assms 
  by blast moreover have "(...),,,x = (foldl F f ?lm),,,x \<union> (last ln),,,x" unfolding l30 by blast 
  ultimately have "(foldl F f ln),,,x = (foldl F f ?lm),,,x \<union> (last ln),,,x" by presburger then 
  have "(foldl F f ln),,,x = f,,,x \<union> Union (?X ?lm) \<union> (last ln),,,x" using 5 by presburger 
  moreover have "Union {(?lm!i),,,x|i. i<size ?lm} \<union> (last ln),,,x = 
  Union ({(?lm!i),,,x|i. i<size ?lm} \<union> {(last ln),,,x})" by auto ultimately have   
  7: "(foldl F f ln),,,x = f,,,x \<union>(Union ({(?lm!i),,,x|i. i<size ?lm} \<union> {(last ln),,,x}))" 
  by force have "{(?lm!i),,,x|i. i<size ?lm}={((?lm@[last ln])!i),,,x|i. i<size ?lm}"
  by (metis (hide_lams) nth_append) moreover have "(last ln)=(?lm@[last ln])!((size ?lm))" by 
  (metis nth_append_length) then moreover have 
  "{(last ln),,,x}={(?lm@[last ln])!i,,,x|i. i=size ?lm}" by blast ultimately 
  have "{(?lm!i),,,x|i. i<size ?lm} \<union> {(last ln),,,x} = {((?lm@[last ln])!i),,,x|i. i<size ?lm} \<union>
  {(?lm@[last ln])!i,,,x|i. i=size ?lm}" by presburger moreover have "...=
  {((?lm@[last ln])!i),,,x|i. i<size ?lm \<or> i=size ?lm}" by blast ultimately have
  "{?lm!i,,,x|i. i<size ?lm}\<union>{(last ln),,,x} = {(?lm@[last ln])!i,,,x|i. i<=size ?lm}" by auto
  moreover have "...={ln!i,,,x|i. i<Suc(size ?lm)}" using 6 by fastforce moreover have 
  "Suc(size ?lm)=size ln" using 4 by linarith ultimately have
  "{(?lm!i),,,x|i. i<size ?lm} \<union> {(last ln),,,x} = {ln!i,,,x|i. i<size ln}" by presburger then 
  have "Union ({(?lm!i),,,x|i. i<size ?lm} \<union> {(last ln),,,x}) = Union {ln!i,,,x|i. i<size ln}" 
  by presburger then have "?Q ln" using 7 by presburger then have False using 2 by blast
}
then have "?E={}" by fast thus ?thesis by force
qed

corollary l30b: assumes "F=(\<lambda>f A. f +< ((\<lambda>x. f,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F") shows 
"(foldl F f l),,,x = (f,,,x) \<union> Union ((\<lambda>A. A,,,x)`(set l))" (is "?L=?r1 \<union> ?r2")  
proof -
have "?L=?r1 \<union> Union {(l!i),,,x|i. i<size l}" using assms by (rule l30a) moreover have "?r2=
Union {(l!i),,,x|i. i<size l}" unfolding l29b by blast ultimately show ?thesis by presburger
qed

corollary l30bb: "(foldl pointUnion f l),,,x = (f,,,x) \<union> Union ((\<lambda>A. A,,,x)`(set l))"
proof - have "pointUnion=(\<lambda>f A. f +< ((\<lambda>x. f,,,x \<union> A,,,x)|||(Domain A)))" 
unfolding pointUnion_def by simp thus ?thesis by (rule l30b) qed

lemma l31: assumes "isInjection f" "\<forall> A\<in>set l. Domain A \<subseteq> Domain f & runiq A" 
"let U=Union o Range in (Union (U`(set l))) \<inter> (U f)={}" shows "isInjection (foldl pointUnion f l)"
proof- let ?g="foldl pointUnion f l" let ?D=Domain let ?R=Range let ?U="Union o ?R" have  
0: "?D ?g \<subseteq> ?D f" using assms by (simp add: l28 pointUnion_def) have 
6: "runiq ?g" using assms(1,2) l28 pointUnion_def by blast have
1: "(Union (?U`(set l))) \<inter> (?U f)={}" using assms(3) by meson
{
  fix x y obtain X Y where 
  2: "X=Union ((\<lambda>A. A,,,x)`(set l)) & Y=Union ((\<lambda>A. A,,,y)`(set l))" by blast have
  3: "?g,,,x = f,,,x \<union> X" unfolding l30bb using 2 by blast moreover have 
  4: "?g,,,y=f,,,y \<union> Y" (is "_=_ \<union> ?Y") unfolding l30bb using 2 by blast have "f,,,x\<union>f,,,y\<subseteq>?U f" 
  by auto moreover have "X\<union>Y\<subseteq>Union (?U`(set l))" using 2 by auto ultimately have 5: 
  "(f,,,x \<union> f,,,y) \<inter> (X \<union> Y)={}" using 1 by fast assume "x\<in>?D ?g" moreover assume "y\<in>?D ?g-{x}"
  ultimately have "f,,,x\<noteq>f,,,y" using 0 DiffD1 DiffD2 assms(1) insertCI lll82 lm36a subsetCE by (metis)
  then have "f,,,x\<union>X\<noteq>f,,,y\<union>Y" using 5 l45s by metis then have "?g,,,x\<noteq>?g,,,y" unfolding 3 4 2 by blast
}
then have "luniq ?g" using 6 l45rr DiffD1 lll82 by metis then show ?thesis by (simp add:"6"luniq_def)
qed

corollary l30c: assumes "F=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F")  
shows "(Union o Range) (foldl F f l) = (Union o Range) f \<union> Union ((Union o Range)`(set l))" 
using l29a(1) assms(1) l30b by fastforce 

theorem l36: assumes "runiq f" 
"F=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F")
"Q3=(\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial((Domain A)\<inter>{x,y})))))"
"Q4=(\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(x\<in>(Domain A) & A,,,x \<supseteq> A,,,y)))"
"\<forall>i j. (i<size l & j<size l & i\<noteq>j) \<longrightarrow> ((Union o Range) (l!i))\<inter>(Union o Range) (l!j)={}"
"isRepresentation f D U" (is "?i f D U") 
"\<forall>A\<in>set l. let C=Domain A in runiq A & C \<subseteq> Domain f & Q3 A & Q4 A & (Union o Range) f \<inter> (Union o Range) A={}"
shows "isRepresentation (foldl F f l) D U & Domain (foldl F f l)=Domain f"
proof- let ?D=Domain let ?R=Range let ?U="Union o ?R" let ?M="foldl F f" obtain P3 where 
5: "P3=(\<lambda>A::(('a \<times> 'b set) set). (\<forall>x\<in>?D f. \<forall>y\<in>?D f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial((Domain A)\<inter>{x,y})))))" by blast obtain P4 where 
6: "P4=(\<lambda>A::(('a \<times> 'b set) set). (\<forall>x\<in>?D f. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(x\<in>(Domain A) & A,,,x \<supseteq> A,,,y)))" by blast obtain P6 where
20: "P6=(\<lambda>l::(('a \<times> 'b set) set list). \<forall>i j. i<size l & j<size l & i\<noteq>j \<longrightarrow> ?U (l!i)\<inter>?U (l!j)={})" by blast have 
21: "\<forall>l. P6 l \<longrightarrow> P6 (butlast l)" by (simp add: "20" butlast_conv_take) 
let ?P="\<lambda>l::(('a \<times> 'b set) set list). 
  ((\<forall>A\<in>set l. let C=?D A in runiq A & C \<subseteq> ?D f & P3 A & P4 A & ?U f \<inter> ?U A={})& P6 l)" have
22: "\<forall>l. ?P l \<longrightarrow> ?P (butlast l)" using 21 butlast_conv_take rev_subsetD set_take_subset 
by (metis (no_types, lifting)) let ?ii="(\<lambda>ff. ?i ff D U & ?D ff = ?D f)" obtain P where
3: "\<forall>l. P=?P" by blast let ?E="{n. \<exists> l. size l=n & P l & \<not>(?ii (?M l))}" have 
0: "?M []=f & 0\<notin>?E" using assms(6) by auto let ?n="Inf ?E" let ?m="?n-1"
{
  assume "?E\<noteq>{}" then moreover have "?n\<in>?E" by (rule l27a) then moreover have "?n>0" using 0 by fastforce 
  ultimately have "?E\<noteq>{} & ?n\<in>?E & ?m<?n" by linarith then have 
  1: "?E\<noteq>{} & ?n\<in>?E & ?m<?n & ?m\<notin>?E" using l27b by meson then obtain ln where 
  2: "size ln=?n & P ln & \<not>?ii (?M ln)" by blast 
  let ?lm="butlast ln" let ?A="last ln" let ?C="Domain ?A" let ?AA="?A,,," let ?f="?M ?lm" have 
  7: "ln=?lm@[?A]" using 1 2 gr_implies_not0 length_0_conv snoc_eq_iff_butlast by 
  (metis) moreover have "?M (?lm@[?A]) = F ?f ?A" by simp ultimately have   
  12: "?M ln = (?f+< ((\<lambda>x'. (?f,,,x') \<union> ?AA x'))|||?C)" using assms(2,3) by (metis(no_types)) have 
  18: "P6 (?lm@[?A])" using 2 3 7 by force
  have "?U ?f = ?U f \<union> Union (?U`(set ?lm))" using assms(2) by (rule l30c) moreover have 
  "?U f\<inter>?U ?A={}" using 1 2 3 by (metis (mono_tags, lifting) gr_implies_not0 last_in_set length_0_conv) 
  moreover have "Union (?U`(set ?lm)) \<inter> ?U ?A={}" using 18 unfolding 20 using 2 1 7 nat_less_le 
  imageE Sup_inf_eq_bot_iff butlast_conv_take in_set_conv_nth length_butlast length_take 
  min_less_iff_conj nth_append_length nth_take by (smt)
  ultimately have "?U ?f \<inter> ?U ?A={}" by fast moreover have "?U ?A=Union(?AA`?C)" by auto ultimately have 
  15: "Union(Range ?f)\<inter>Union(?AA`?C)={}" by auto moreover have "P ?lm " using 2 3 22 by fast then have 
  4: "P ?lm  & ?ii (?M ?lm)" using "1" "2" by auto then have 
  8: "?D f = ?D ?f & (\<forall>A\<in>set ln. P3 A & P4 A)" using 2 3 by auto moreover have 
  9: "?A\<in>set ln" using 7 in_set_conv_decomp by fastforce ultimately moreover have 
  14: "\<forall>x\<in>?D ?f. \<forall>y\<in>?D ?f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial(?C\<inter>{x,y})))" unfolding 5 by auto ultimately have 
  16: "\<forall>x\<in>Domain ?f. \<forall>y\<in>?C. (x,y)\<in>D\<longrightarrow>(x\<in>?C & ?A,,,x \<supseteq> ?A,,,y)" unfolding 6 by blast have
  17: "isRepresentation' ?f D U" using 4 unfolding isRepresentation_def by blast have 
  13: "?C \<subseteq> ?D ?f" using "2" "3" 8 9 by auto have 
  11: "runiq ?f" using l28 2 using "3" "4" assms(1,2) foldl_cong by fastforce
  have "isRepresentation' (?M ln) D U" using 11 12 13 14 15 16 17 by (rule preservedRepresentation)
  moreover have "?D (?M ln)=?D ?f" by (simp add: "12" "13" MiscTools.ll37 paste_Domain subset_antisym) 
  ultimately have "?ii (?M ln)" using 8 unfolding isRepresentation_def by force then have False using 2 by blast
}
then have "?E={}" by fast moreover have "P l" using assms(3,4,5,7) 3 5 6 20 by blast 
ultimately show ?thesis by simp
qed

theorem iterativeRepresentationPreservation: assumes "runiq f" "irrefl (U||(Domain f))"
"F=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F")
"let u=Union o Range in \<forall>j<size l. u f\<inter>u (l!j)={} & (\<forall>i<j. u(l!i)\<inter>u(l!j)={})"
"let d=Domain in \<forall>A\<in>set l. d A \<subseteq> d f & runiq A & isDownClosed D (d A) & isConflictFree2 U (d A) & 
  isMonotone4 D (rel2pairset' (op \<supseteq>)) A"
"isRepresentation f D U" (is "?i f D U") 
shows "isRepresentation (foldl F f l) D U & Domain (foldl F f l)=Domain f"
proof - let?D=Domain let ?R=Range let ?u="Union o ?R"
let ?Q3="\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y & trivial((Domain A)\<inter>{x,y}))))"
let ?Q4="\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(x\<in>(Domain A) & A,,,x \<supseteq> A,,,y))"
let ?Q3a="(\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(x\<noteq>y))))"
let ?Q3b="(\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>Domain f. ((x,y)\<in>U\<longrightarrow>(trivial((Domain A)\<inter>{x,y})))))"
let ?Q4a="\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(x\<in>(Domain A)))"
let ?Q4b="\<lambda>A. (\<forall>x\<in>Domain f. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(A,,,x \<supseteq> A,,,y))"
let ?Q4bb="\<lambda>A. (\<forall>x\<in>Domain A. \<forall>y\<in>(Domain A). (x,y)\<in>D\<longrightarrow>(A,,x \<supseteq> A,,y))"
{
  assume "\<forall>A\<in>set l. isDownClosed' D (?D A) & runiq A & isMonotone4' D (rel2pairset' (op \<supseteq>)) A"
  then moreover have "\<forall>A\<in>set l. ?Q4a A" by blast
  ultimately have "\<forall>A\<in>set l. ?Q4 A" using l35 lll82 by metis 
} then have 
i0: "(\<forall>A\<in>set l. isDownClosed' D (?D A) & runiq A & isMonotone4' D (rel2pairset' (op \<supseteq>)) A) \<longrightarrow> 
  (\<forall>A\<in>set l. ?Q4 A)" by blast
{
   assume "\<forall>A\<in>set l. isConflictFree2' U (?D A)" then have
  "\<forall>A\<in>set l. ?Q3b A" using restrict_def irrefl_def trivial_def IntI Int_iff Int_insert_right_if0 
  Int_insert_right_if1 SigmaI contra_subsetD empty_iff insert_absorb2 insert_iff singletonI 
  singleton_insert_inj_eq' subsetI subset_eq the_elem_eq
by (smt Int_empty_right) moreover
  assume "irrefl (U||(?D f))"
  ultimately  have "\<forall>A\<in>set l. ?Q3 A"  unfolding restrict_def irrefl_def by blast
  } then have 
i1: "(irrefl (U||(?D f)) & (\<forall>A\<in>set l. isConflictFree2' U (?D A)))\<longrightarrow>(\<forall>A\<in>set l. ?Q3 A)" by linarith
obtain Q3 Q4 where 
10: "(Q3::(('a \<times> ('b set)) set \<Rightarrow> bool))=?Q3 & (Q4::(('a \<times> ('b set)) set \<Rightarrow> bool))=?Q4" by blast have 
ii1: "(irrefl (U||(?D f)) & (\<forall>A\<in>set l. isConflictFree2' U (?D A)))\<longrightarrow>(\<forall>A\<in>set l. Q3 A)" using i1 10 by fast have
ii0: "(\<forall>A\<in>set l. isDownClosed' D (?D A) & runiq A & isMonotone4' D (rel2pairset' (op \<supseteq>)) A) \<longrightarrow> 
  (\<forall>A\<in>set l. Q4 A)" using i0 10 by fast have 
0: "\<forall>A\<in>set l. ?D A \<subseteq> ?D f & runiq A & isDownClosed' D (?D A) & isConflictFree2' U (?D A) &
 isMonotone4' D (rel2pairset' (op \<supseteq>)) A" using assms(5) 
 unfolding isDownClosed_def isConflictFree2_def isMonotone4_def by presburger have
1: "irrefl (U||(?D f))" using assms(2) by blast have 
11: "\<forall>A\<in>set l. ?u f \<inter> ?u A={}" using assms(4) in_set_conv_nth by (metis (no_types, lifting))
then have 
h7: "\<forall>A\<in>set l. let C=Domain A in runiq A & C \<subseteq> Domain f & Q3 A & Q4 A & (Union o Range) f \<inter> (Union o Range) A={}"
using ii0 ii1 0 1 by meson have 
h5: "\<forall>i j. (i<size l & j<size l & i\<noteq>j) \<longrightarrow> (?u (l!i))\<inter>?u (l!j)={}" using assms(4)
max.commute max_min_same(1) max_min_same(4) min.idem min_max_distrib2 inf_commute max_def min_def
not_le by (metis (no_types, lifting)) have 
h6: "isRepresentation f D U" using assms(6) by blast have
h3: "Q3=?Q3" using 10 by linarith have
h4: "Q4=?Q4" using 10 by linarith
show "isRepresentation (foldl F f l) D U & Domain (foldl F f l)=Domain f" using assms(1,3) h3 h4 h5 h6 h7 by (rule l36)
qed
































section{*Putting all together*}

lemma assumes "isDownwardClosed P X" shows "P^-1``X\<subseteq>X" 
using Int_absorb2 Sefm.lm03a assms by blast

lemma assumes "isConfiguration2' D U X" shows "isConfiguration' D U X" using assms 
by (smt DiffI ImageE IntI Int_absorb2 Sefm.lm03a emptyE isConflictFree2_def isDownwardClosed_def mem_Sigma_iff subsetI)

sledgehammer_params [provers= z3 cvc4 e spass vampire(*, isar_proofs=smart, dont_try0*)]

(*ICICT paper, Lemma V-B.2.*)
theorem extension2: assumes "runiq f" "(s,s)\<in>D" "D``{s}\<subseteq>{s}" "s\<notin>Domain f" assumes 
hypOverlap: "\<forall>x\<in>Domain f. \<not>(f,,,x \<subseteq> RA)" assumes
hypCausality: "\<forall>x\<in>Domain f. RA \<subseteq> f,,,x = (x\<in>D^-1``{s}-{s})" assumes 
hypConflict: "\<forall>x\<in>Domain f. ((f,,,x)\<inter>RA={})=(x\<in>U^-1``{s})" 
"\<forall>x\<in>Domain f. ((x,s)\<in>U) = ((s,x)\<in>U)" "isRepresentation f (D---s s) (U---s s)"
"F=f+<(s,RA)" "RA\<noteq>{}" "(s,s)\<notin>U" shows "isRepresentation' F D U"
proof- let ?D=Domain have 
0: "\<forall>x\<in>?D f. f,,,x=f,,x" using assms(1) using lll82 by fastforce have 
h6: "\<forall>x\<in>?D f. RA \<subseteq> f,,x = (x\<in>D^-1``{s})" using assms(4,6) 0 by auto have
h9: "isRepresentation' f (D---s s) (U---s s)" using assms(9) unfolding isRepresentation_def by blast have
h7: "\<forall>x\<in>?D f. ((f,,x)\<inter>RA={})=(x\<in>U^-1``{s})" using assms(1,7) lll82 by metis have
h5: "\<forall>x\<in>?D f. \<not>(f,,x \<subseteq> RA)" using assms(1,5) lll82 by (metis)
show ?thesis using assms(1-4) h5 h6 h7 assms(8) h9 assms(10-12) by (rule extension) 
qed




















lemma l41ss: assumes "sym U" "isConflictFree2 U (X\<union>Y)" "isMonotonicOver U (D|^(X \<union> Y))"
shows "isConflictFree2 U (D^-1``X \<union> D^-1``Y)"
proof- let ?cf=isConflictFree2' let ?E="D^-1"  let ?X="?E``X" let ?Y="?E``Y" let ?Z="?X \<union> ?Y" have
"?cf U X" using assms(2) unfolding isConflictFree2_def by blast moreover have 
0: "isMonotonicOver U (D|^X)" using assms(3) unfolding corestriction_def by blast ultimately have 
1: "?cf U ?X" using assms(1) unfolding sym_def using 0 unfolding corestriction_def restrict_def Outside_def by blast have
"?cf U Y" using assms(2) unfolding isConflictFree2_def by blast moreover have 
2: "isMonotonicOver U (D|^Y)" using assms(3) unfolding restrict_def Outside_def corestriction_def by simp ultimately have
3: "?cf U ?Y" using assms(1) unfolding sym_def using 2 unfolding restrict_def Outside_def corestriction_def by blast 
{
  assume "\<not>?thesis" then obtain x0 y0 where 
  4: "x0\<in>?X & y0\<in>?Y & (x0,y0)\<in>U" unfolding isConflictFree2_def using assms(1) 1 3 DiffD2 SigmaE UnE 
  disjoint_iff_not_equal lm32h mem_Sigma_iff symE by (smt) then obtain x1 y1 where 
  7: "(x0,x1)\<in>D & (y0,y1)\<in>D & x1\<in>X & y1\<in>Y" by blast then have
  6: "x0\<in>?Z & y0\<in>?Z & (x0,x1)\<in>D|^(X\<union>Y) & (y0,y1)\<in>D|^(X\<union>Y)" unfolding corestriction_def using 4 by blast have
  "(x1,y1)\<in>U" using assms(1) unfolding sym_def using 6 assms(3) 4 by blast then have False using assms(2)
  unfolding isConflictFree2_def using 7 by blast  
} thus ?thesis by presburger
qed











































theorem l34: (* to be used with extension theorem*) assumes 
"F=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F") 
"\<forall>i<size l. U^-1``{s} \<inter> (Domain (l!i))={} & (Domain (l!i))\<supseteq>(D^-1``{s}-{s})"
"Domain (foldl F f ll) - (D^-1``{s} \<union> U^-1``{s}) (*\<union> D^-1``{s}*) \<subseteq> (Union o set) (map Domain l)" 
"ll=l@[LA]" 
"Domain LA=D^-1``{s}-{s}" (*5*) 
"U^-1``{s}\<inter>(D^-1``{s}-{s})={}" (*6*)
"s\<notin>Domain (foldl F f ll)" (*"Domain (foldl F f ll)\<subseteq>Field D" *) 
"\<forall>i<size ll. (ll!i)=(Domain(ll!i)) \<times> {r!i}"
"size r=size ll" (*9*)
"{}\<notin>set (butlast r)"
"\<forall>i<size l. Domain(ll!i)\<noteq>{}" (*11: redundant with previous one? NO*)
"(Union o set) r\<inter>(Union o Range) f={}" "\<not>(last r\<subseteq>(Union ((set (butlast r))\<union>Range f)))"
shows 
"let g=foldl F f ll in let RA=(Union o set) r in (*RA=(Union o set) (map (Union o Range) ll) &*)
(\<forall>x\<in>Domain g. RA \<subseteq> g,,,x = (x\<in>D^-1``{s}-{s}) & ((g,,,x)\<inter>RA={})=(x\<in>U^-1``{s}))"
proof- let ?D=Domain let ?U="Union o Range" let ?ll="l@[LA]" let ?R=Range let ?f="foldl F f ll"
let ?f'="foldl F f ?ll"
let ?RA="(Union o set) (map ?U ?ll)" let ?RA'="(Union o set) r" let ?ra="?U LA" have 
"?ra \<noteq> {} \<longrightarrow> ?ra \<subseteq> ?RA'" using assms(4,5,8,9) by auto then have 
c1: "?ra \<subseteq> ?RA'" by blast have
1: "\<forall>xx. (*?f,,,xx=f,,,xx \<union> Union {(?ll!i),,,xx|i. i<size ?ll} &*) 
(?f,,,xx=f,,,xx \<union> Union {(ll!i),,,xx|i. i<size ll & xx\<in>?D(ll!i)}) &
?f,,,xx=f,,,xx \<union> Union((\<lambda>f. f,,,xx)`(set ?ll))" 
proof-{fix xx 
  have "?f,,,xx=f,,,xx \<union> Union {(ll!i),,,xx|i. i<size ll}" using assms(1) by (rule l30a)
  moreover have "?f,,,xx=f,,,xx \<union> Union((\<lambda>f. f,,,xx)`(set ll))" using assms(1) by (rule l30b)
  moreover have "Union {(ll!i),,,xx|i. i<size ll} = Union{(ll!i),,,xx|i. i<size ll & xx\<in>?D (ll!i)}" 
  by (rule l33c [of "nth ll" _ "\<lambda> i x. i<size ll"]) ultimately have 
  "(?f,,,xx=f,,,xx \<union> Union {(ll!i),,,xx|i. i<size ll}) &
  (?f,,,xx=f,,,xx \<union> Union {(ll!i),,,xx|i. i<size ll & xx\<in>?D(ll!i)}) &
  (?f,,,xx=f,,,xx \<union> Union((\<lambda>f. f,,,xx)`(set ll)))" by force
  } then show ?thesis using assms(4) by meson
qed have 
14: "\<forall>i<size l. ?ll!i=l!i" by (metis butlast_snoc nth_butlast) have
15: "size ?ll=size l + 1" by simp have 
c8: "?RA'\<inter>?U f={}" using assms(12) by blast have
55: "size ll=size r & size ll>0 & size ll=1+size l & LA=last ll" by (simp add: assms(4,9)) have
56: "\<forall>i<size ll. D^-1``{s}-{s} \<subseteq> ?D(ll!i)" using 14 15 assms(2,4,5) One_nat_def add.right_neutral 
add_Suc_right le_simps(2) nat_less_le nth_append_length set_eq_subset by (metis(no_types)) have
58: "?ra \<subseteq> ?RA & r\<noteq>[] & last r \<subseteq> ?RA'" using 55 UnionI assms(4) comp_apply last_in_set last_snoc 
length_greater_0_conv length_map list.simps(9) map_append subsetI by (smt)
have "ll=map (nth ll) [0..<size ll]" by (simp add: map_nth) moreover have "map ?U ... =
map (?U o (nth ll)) [0..<size ll]" proof - show ?thesis by simp qed ultimately have 
c4: "map ?U ll=map (?U o (nth ll)) [0..<size ll]" by presburger have 
37: "set ?ll=set l \<union> {LA} & size (butlast r)=size l & r!(size ll-1)=last r" using 55 58
add.right_neutral add_Suc_right append_butlast_last_id assms(4) length_append list.set(1) 
list.simps(15) list.size(3) list.size(4) nat.inject set_append by (simp add: last_conv_nth) have 
68: "\<forall>i<size r. \<forall>x\<in>Domain(ll!i). ll!i,,,x=r!i" using assms(8) unfolding assms(9) by blast have 
72: "\<forall>i<size l. \<forall>x\<in>?D(l!i). l!i,,,x=r!i" using 15 assms(4,9) 68 14 One_nat_def 
add.right_neutral add_Suc_right le_simps(2) nat_less_le by (metis (no_types, lifting)) have 
73: "\<forall>i<size l. r!i\<noteq>{}" using assms(4,9,10) 15 One_nat_def add.right_neutral nth_mem butlast_snoc 
length_butlast nth_butlast add_Suc_right le_simps(2) nat_less_le by (metis) 
have "\<forall>i<size r. \<forall>x\<in>UNIV. (r!i) \<inter> (f,,,x)={}" using c8 by fastforce 
then have "\<forall>i<size r. \<forall>x\<in>UNIV. ll!i,,,x \<inter> f,,,x={}" using 68 by fast then have
"\<forall>i<size ll. \<forall>x\<in>?D(ll!i). (ll!i,,,x)\<inter>(f,,,x)={}" unfolding assms(9) by fast moreover have 
"size l<size ll" using assms(4)by simp ultimately have"\<forall>i<size l. \<forall>x\<in>?D(l!i).(l!i,,,x)\<inter>(f,,,x)={}" 
using assms(4) 14 15 One_nat_def add.right_neutral add_Suc_right le_simps(2) nat_less_le by metis then have 
78: "\<forall>i<size l. \<forall>x\<in>?D (l!i). (l!i,,,x) - (f,,,x)\<noteq>{}"using assms(4,9) 14 15 68 73 One_nat_def Int_absorb2 
add.right_neutral add_Suc_right le_simps(2) nat_less_le Diff_eq_empty_iff by (metis (no_types, lifting)) have 
69: "size l = size (butlast r)" by (simp add: assms(4,9)) have 
79: "\<forall>i<size l. \<forall>x\<in>Domain (l!i). l!i,,,x=r!i" using assms(4,9) 14 15 68 One_nat_def nat_less_le 
add.right_neutral add_Suc_right le_simps(2) by (metis(no_types,lifting)) have 
80: "ll!(size ll-1)=?D(ll!(size ll-1))\<times>{r!(size ll-1)}" using assms(8) 55 
by (meson Sigma_cong diff_less zero_less_one) moreover have 
81: "ll!(size ll-1)=last ll" by (simp add: assms(4))
have "\<forall>i<size ll. ?U((Domain(ll!i)) \<times> {r!i}) \<subseteq> r!i" by auto then
have "\<forall>i<size ?ll. ?U(?ll!i) \<subseteq> ?RA'" using assms(4,8) 37 55 58 Union_iff nth_mem o_apply set_rev_mp subsetI 
by (smt) then have "\<forall>i<size (map ?U ?ll). (map ?U ?ll)!i \<subseteq> ?RA'" by (metis length_map nth_map)
moreover have "\<forall>x\<in>set(map ?U ?ll). \<exists> i<size (map ?U ?ll). x=(map ?U ?ll)!i" by (metis in_set_conv_nth) 
ultimately have "\<forall>x\<in>set (map ?U ?ll). x\<subseteq>?RA'" by fast then have "?RA \<subseteq> ?RA'" 
by (smt Union_iff comp_apply subset_iff) then have
8: "?RA\<inter>?U f={}" using c8 by blast then have 
7: "?ra \<inter> ?U f={}" using Sup_inf_eq_bot_iff Un_insert_right comp_def insert_iff list.simps(15) 
list.simps(9) set_append set_map by auto have 
11: "?U f \<inter> ?ra ={}" using 7 by blast 
{fix x obtain X where
  63: "X=(f,,,x \<union> ((Union o set) (map ?U (filter (\<lambda>A. x\<in>?D A) l))))" by blast have 
  16: "?ra \<inter>f,,,x={}" using 11 by auto have
  4: "(?f,,,x=f,,,x \<union> Union((\<lambda>f. f,,,x)`(set l \<union> {LA})))" using 1 37 by metis have
  12:"?f,,,x=f,,,x \<union> Union {(?ll!i),,,x|i. i<size ?ll & x\<in>?D(?ll!i)}" by (metis 1 Collect_cong assms(4))
  have "{(?ll!i),,,x|i. i<size ?ll & x\<in>?D (?ll!i)} = 
    {(?ll!i),,,x|i. (i<size ?ll-1 \<or> i=size ?ll-1) & x\<in>?D (?ll!i)}" by auto moreover have 
  "...={(?ll!i),,,x|i.(i<size ?ll-1) & x\<in>?D(?ll!i)}\<union>{(?ll!i),,,x|i. i=size ?ll-1 & x\<in>?D(?ll!i)}" 
  by blast moreover have "{(?ll!i),,,x|i. (i<size ?ll-1) & x\<in>?D (?ll!i)} = 
    {(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)}" using 14 by auto moreover have 
  "{(?ll!i),,,x|i. i=size ?ll-1 & x\<in>?D(?ll!i)}={LA,,,x|i. i=size l &x\<in>?D LA}"by auto ultimately have 
  17: "{(?ll!i),,,x|i. i<size ?ll & x\<in>?D (?ll!i)}=
    {(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)} \<union> {LA,,,x|i. i=size l & x\<in>?D LA}" by simp have 
  21: "?f,,,x=f,,,x \<union> Union {(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)} \<union> 
    Union {LA,,,x|i. i=size l & x\<in>?D LA}" unfolding 12 using 17 by auto have 
  18: "(Union o set)(map ?U l) \<supseteq> Union {(l!i),,,x|i. i<size l & x\<in>?D (l!i)}" by fastforce have 
  67: "(?f,,,x=f,,,x \<union> Union {(ll!i),,,x|i. i<size r & x\<in>?D(ll!i)})" using 1 unfolding assms(9) by meson
  have "{(ll!i),,,x|i. i<size r & x\<in>?D(ll!i)}={r!i|i. i<size r & x\<in>?D(ll!i)}" using 68 by fast then have
  a53: "?f,,,x=f,,,x \<union> Union {r!i|i. (i<size r) & x\<in>?D (ll!i)}" using "12" assms(4,9) by auto
  have "\<forall>i<size l. l!i=?D (l!i)\<times>{r!i} & ?D(l!i)\<noteq>{}" using assms(8,11) 37 58 55
  "14" "15" Nat.add_0_right One_nat_def add_Suc_right assms(4) le_simps(2) nat_less_le 
by (metis (no_types, lifting)) then have "\<forall>i<size l. {r!i}=?R(l!i)" by auto then have
 "(\<forall>i<size l. (butlast r)!i=?U(l!i))" using "37" cSup_singleton comp_apply nth_butlast
 by (metis(no_types)) then have
62: "(butlast r)=map ?U l" by (simp add: "69" nth_equalityI)  
  have "{(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)} = {r!i|i. i<size (butlast r) & x\<in>?D(l!i)}" using 79 
  unfolding 69 by blast then moreover have "...={(butlast r)!i|i. i<size l & x\<in>?D(l!i)}" using 
  nth_butlast unfolding 69 by fastforce ultimately moreover have "...=
    {?U(l!i)|i. i<size l & x\<in>?D(l!i)}" unfolding 62 by (metis (no_types, hide_lams) nth_map) 
  moreover have "... \<subseteq> set (map ?U (filter (\<lambda>A. x\<in>?D A) l))" by fastforce moreover have 
  "... \<subseteq> {?U(l!i)|i. i<size l & x\<in>?D(l!i)}" using imageE in_set_conv_nth mem_Collect_eq set_filter 
  set_map subsetI by smt ultimately have 
  "set (map ?U (filter (\<lambda>A. x\<in>?D A) l))={(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)}" by force then have 
  70: "Union {(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)}=(Union o set) (map ?U (filter (\<lambda>A. x\<in>?D A) l))" 
  by auto have "\<forall>x\<in>Domain LA. (Union o Range) LA \<subseteq> LA,,,x" using assms(4) 80 by auto then have 
  "x\<in>?D LA \<longrightarrow> LA,,,x=?ra" using Image_singleton_iff Range.intros Sup_upper Union_least comp_def 
  set_eq_subset by (metis) then have "Union{?ra|i. x\<in>?D LA}=Union{LA,,,x|i. i=size l & x\<in>?D LA}" 
  by blast then have "...=Union{?ra|i. x\<in>D^-1``{s}-{s}}" using assms(5) by auto then have
  b53: "?f,,,x=f,,,x\<union>((Union o set) (map ?U (filter (\<lambda>A. x\<in>?D A) l)))\<union>Union {?ra|i. x\<in>D^-1``{s}-{s}}" 
  unfolding 21 unfolding 70 by presburger have
  57: "?f,,,x \<subseteq> f,,,x \<union> ?RA'" using UnCI a53 by auto assume 
  b35: "x\<in>?D ?f" then have 
  35: "x\<in>?D ?f - {s}" using assms(7) by blast have "LA=?D LA \<times> {last r}" using assms(4) 55 58 37 80 
  by force then have "LA\<noteq>{} \<longrightarrow> ?U LA=last r" by auto then have 
  c6: "(?U LA \<subseteq> (?U f \<union> (Union o set) (butlast r))) \<longrightarrow> LA={}" using assms(13) by auto
  {
    assume "x\<in>D^-1``{s}-{s}" then have 
    63: "\<forall>i<size ll. x\<in>?D (ll!i)" using 56 by blast have 
    66: "{r!i|i. (i<size r)& x\<in>?D (ll!i)}\<supseteq>{r!i|i. (i<size r)}" using 55 63 by fastforce have 
    64: "set r = {r!i|i. (i<size r)}" by (simp add: set_conv_nth) have 
    65: "set r \<subseteq> {r!i|i. (i<size r) & x\<in>?D (ll!i)}" using 64 66 by presburger
    then have "?RA' \<subseteq> ?f,,,x" using a53 Union_iff o_apply UnCI subset_iff by (smt)
  } then have 42: "x\<in>D^-1``{s}-{s} \<longrightarrow> ?RA' \<subseteq> ?f,,,x" by blast
  { assume 
    25: "x\<in>U^-1``{s}" then have 
    36: "\<forall>i<size l. x\<notin>?D(l!i)" using assms(2) by fast have "x\<notin>D^-1``{s}-{s}" using 25 assms(6) by fast then have 
    33: "x\<notin>?D LA" using assms(5) by blast have 
    34: "\<forall>i<size ?ll. x\<notin>?D(?ll!i)" using 33 36 14 15 One_nat_def add.right_neutral add_Suc_right 
    less_Suc_eq_le nat_less_le nth_append_length by metis have 
    a35:"?f,,,x \<subseteq> f,,,x" using 34 12 by auto have 
    a36: "?U f \<inter> ?RA'={}" using assms(12) by blast
    have "?f,,,x \<inter> ?RA'={}" using a35 a36 by fastforce
  } then have 44: "x\<in>U^-1``{s} \<longrightarrow> ?f,,,x\<inter>?RA'={}" by blast
  { assume "?ra ={}" moreover assume 
    c2: "?RA' \<subseteq> ?f,,,x" moreover assume "x\<notin>D^-1``{s}-{s}" ultimately 
    moreover have "?D LA={}" using c6 Domain_empty_iff empty_iff subsetI by (metis (no_types, lifting))
    ultimately moreover have 
    c5: "x\<notin>?D(ll!(size r -1))" by (simp add: "81" assms(4,9) empty_iff last_snoc) ultimately moreover have
    "{r!i|i. i<size r & x\<in>?D (ll!i)} \<subseteq> {r!i|i. (i<size r-1) & x\<in>?D (ll!i)}" using assms(4,9) 15 62 
    One_nat_def add.right_neutral add_Suc_right  le_simps(2) length_butlast length_map 
    mem_Collect_eq nat_less_le subsetI by (smt) then have 
    "f,,,x \<union> Union {r!i|i. i<size r & x\<in>?D (ll!i)} \<subseteq> f,,,x \<union> Union {r!i|i. (i<size r-1) & x\<in>?D (ll!i)}" 
    using Un_iff Union_Un_distrib subsetI subset_Un_eq by (smt) then have "?f,,,x=f,,,x \<union> 
      Union{r!i|i. (i<size r-1) & x\<in>?D(ll!i)}" unfolding a53 using assms(4,9) c5 15 62 le_simps(2)  
    One_nat_def add.right_neutral add_Suc_right length_butlast length_map nat_less_le by (metis (no_types, hide_lams))
    moreover have "{r!i|i. (i<size r-1) & x\<in>?D (ll!i)} \<subseteq> set (butlast r)" using in_set_conv_nth 
    nth_butlast by fastforce ultimately have "?f,,,x \<subseteq> f,,,x \<union> (Union o set)(butlast r)" 
by (metis (no_types, lifting) "18" "21" "62" Collect_empty_eq Sup_empty Un_mono assms(5) subset_refl sup_bot.right_neutral)
moreover have "... \<subseteq> ?U f \<union> (Union o set)(butlast r)" by auto moreover have 
"\<not>last r \<subseteq> ..." using assms(13) by auto ultimately have "\<not>(last r \<subseteq> ?f,,,x)" by blast then have "False" using c2 58 by fast  
    } then have c3: "?RA' \<subseteq> ?f,,,x & ?ra={} \<longrightarrow> x\<in>D^-1``{s}-{s}" by fast
  {
    assume "?RA' \<subseteq> ?f,,,x" then moreover have 
    "?ra \<subseteq> ?f,,,x" using c1 by fast then
    have "?ra \<subseteq> X \<union> (Union {?ra|i. x\<in>D^-1``{s}-{s}})" using b53 63 by (metis(no_types)) 
    moreover assume "?ra \<noteq> {}" ultimately
    moreover have "\<not>(?ra \<subseteq> X)" unfolding 63 using 62 c6 16 18 70 IntI Range_empty Sup_empty UnCI UnE 
    empty_iff o_apply subsetCE subsetI by (smt) ultimately have 
    "?ra \<inter> Union {?ra|i. x\<in>D^-1``{s}} \<noteq>{}" by fast then have "x\<in>D^-1``{s}" by auto
  } then have 41: "?RA' \<subseteq> ?f,,,x \<longrightarrow> x\<in>D^-1``{s}-{s}" using 35 c3 by fast
  {
    assume 30: "?f,,,x\<inter>?RA'={}" then have 22: "?f,,,x \<subseteq> f,,,x" using 57 by blast have
    23: "\<forall>i<size l. x\<notin>?D (l!i)" 
    proof-
    {
      fix i assume "i<size l" moreover assume "x\<in>?D (l!i)" 
      ultimately moreover have "(l!i,,,x) - (f,,,x)\<noteq>{}" using 78 by meson ultimately moreover 
      have "Union {(l!i),,,x|i. (i<size l) & x\<in>?D (l!i)} -f,,,x\<noteq>{}" by blast ultimately have 
      "f,,,x \<subset> ?f,,,x" unfolding 21 by blast then have False using 22 by blast 
    } thus ?thesis by blast
    qed have "x\<notin>(Union o set) (map Domain l)" using 23 Union_iff comp_def in_set_conv_nth nth_map 
    length_map by (smt) then have "x\<notin>Domain ?f - (D^-1``{s} \<union> U^-1``{s})" using assms(3) unfolding 
    concurs_def subsetCE by blast then moreover
    have "x\<notin>D^-1``{s}-{s}" using assms(13) 30 42 58 Int_absorb1 empty_iff subsetI subset_empty
 by blast
    ultimately have "x\<in>U^-1``{s}" using b35 35 by blast
  } then have 43: "?f,,,x\<inter>?RA'={} \<longrightarrow> x\<in>U^-1``{s}" by blast
  have "((?RA' \<subseteq> ?f,,,x) = (x\<in>D^-1``{s}-{s})) & ((?f,,,x)\<inter>?RA'={})=(x\<in>U^-1``{s})" using  41 42 43 44 by fast
  } 
  then have "\<forall>x\<in>?D ?f. ((?RA' \<subseteq> ?f,,,x) = (x\<in>D^-1``{s}-{s})) & ((?f,,,x)\<inter>?RA'={})=(x\<in>U^-1``{s})" by simp
  then show ?thesis by meson
qed

theorem l44a: assumes "isRepresentation f (D---s s) (U---s s)" "runiq f & r\<noteq>{} & {}\<notin>Range f & 
D``{s}={s} & irrefl ((U---s s)||(Domain f)) & (U^-1``{s}\<inter>(D^-1``{s})={} & s\<notin>U``{s}\<union>Domain f)"
"\<forall>x\<in>Domain f. ((x,s)\<in>U) = ((s,x)\<in>U)" "let d=Domain in let dd=d`(set l) in (*CC=Union dd*)
U^-1``{s} \<inter> (Union dd)={} & D^-1``{s}-{s} \<subseteq> Inter dd & d f-(D^-1``{s}\<union>U^-1``{s})\<subseteq>Union dd"
"let d=Domain in \<forall>A\<in>set (l@[(D^-1``{s}-{s})\<times>{r}]). 
d A \<subseteq> d f & isDownClosed (D---s s) (d A) & isConflictFree2 (U---s s) (d A)"
"Range`(set l) \<subseteq> card-`{1}-{{{}}}"
"let ur=Union o Range in let rr=(map ur l)@[r] in \<forall>j<size rr. ur f\<inter>rr!j={} & (\<forall>i<j. rr!i\<inter>(rr!j)={})"
shows "let d=Domain in let R=Range in let RA=(Union o set)((map (Union o Range) l)@[r]) in 
let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{r}]) in let h=g+<(s,RA) in d g=d f & 
d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & isRepresentation h D U
& (luniq f \<longrightarrow> (isInjection g & isInjection h))"
proof- let ?R=Range let ?D=Domain let ?U="Union o ?R" let ?d="D---s s" let ?b=butlast 
let ?u="U---s s" let ?da="D^-1``{s}-{s}" let ?ll="l@[?da\<times>{r}]" let ?LA="?da\<times>{r}"
let ?rr="(map ?U l)@[r]" let ?RA="(Union o set) ?rr" let ?DD="?D`(set l)" obtain ll where
1: "ll=l@[?LA]" by blast let ?l="?b ll" let ?f'="foldl pointUnion f ll" let ?g="?f'" let ?h="?g+<(s,?RA)" 
have "(s,s)\<notin>U \<longrightarrow> ((U^-1``{s}\<inter>(D^-1``{s}-{s})={})=(U^-1``{s}\<inter>(D^-1``{s})={}))" by blast
have "((s,s)\<in>D & D``{s}\<subseteq>{s}) = (D``{s}={s})" by blast have "(s,s)\<notin>U = (s\<notin>U``{s})" by blast have
4: "size ll=1+size l & size ll=size ?rr & ?b ?rr=map ?U l & last ll=?LA & r=last ?rr" using 1 by simp then have 
19: "?rr!(size l)=r" using length_map nth_append_length by (metis(no_types)) have
20: "\<forall>x. (f,,,x)\<subseteq>?U f" by auto have
3: "{} \<notin> set l & {} \<notin> Union(?R`(set l))" using assms(6) l37o by blast then have 
5: "\<forall>i<size l. ?ll!i\<noteq>{}" by (metis nth_append nth_mem) have
6: "(Union o set) ?rr = r \<union> Union(?U`set l)" by simp 
have "trivial (?R ?LA) & runiq ?LA" using runiq_basic Range.cases SetUtils.lm01 
SigmaD2 insert_absorb insert_iff insert_not_empty by smt moreover have 
11: "\<forall>A\<in>set ll. A\<in>set l \<or> A=?LA" unfolding 1 by force moreover have 
10: "\<forall>A\<in>set l. card (?R A)=1 & trivial (?R A) & runiq A" using assms(6) l37o(2)  Diff_empty 
Diff_eq_empty_iff Diff_iff image_eqI insert_iff l37j(1) l37j(3) vimage_eq by (metis) ultimately have 
7: "\<forall>A\<in>set ll. trivial(?R A) & runiq A" by fast then have
8: "let d=Domain in \<forall>A\<in>set ll. d A \<subseteq> d f & runiq A & trivial (?R A) & isDownClosed ?d (d A) & isConflictFree2 ?u (d A)"
using assms(5) "1" by auto
then moreover have "\<forall>A\<in>set ll. runiq A & isMonotone4 ?d (rel2pairset' (op \<supseteq>)) A" using l37h 
lm007b by fastforce ultimately have
g5: "let d=Domain in \<forall>A\<in>set ll. d A \<subseteq> d f & runiq A & isDownClosed ?d (d A) & isConflictFree2 ?u (d A) & 
  isMonotone4 ?d (rel2pairset' (op \<supseteq>)) A" by meson have
9: "\<forall>A\<in>set ll. trivial(?R A)" using 7 by blast have 
h5: "?D ?LA=?da" by blast have
h9: "size ?rr=size ll" by (simp add: "1") have 
h11: "{}\<notin>set (butlast ?rr)" using 4 3 Range_empty_iff Union_iff comp_apply imageE 
image_eqI l37k set_map by (smt) have
h12: "\<forall>i<size l. Domain(ll!i)\<noteq>{}" using 5 by (simp add: Domain_empty_iff 1) have
13: "\<forall>i<size l. l!i=ll!i & ?rr!i=?U(l!i)" using 1 by (metis butlast_snoc length_map nth_butlast nth_map)
moreover have 
14: "\<forall>i<size ll. (i<size l) \<or> (i=size l & ll!i=?LA)" using 1 4 
by (simp add: One_nat_def add.left_neutral last_snoc le_simps(2) nat_less_le nth_append_length plus_nat.simps(2))
moreover have "\<forall>i<size l. l!i=?D(l!i)\<times>{?U(l!i)}" using 7 8 9 10 
by (metis Sigma_cong l37g nth_mem) moreover have "?LA=?D ?LA \<times> {?rr!(size l)}" using 4 
by (metis Sigma_cong h5 length_map nth_append_length) ultimately have
h8: "\<forall>i<size ll. (ll!i)=Domain (ll!i) \<times> {?rr!i}" by metis have
g1: "runiq f" using assms(2) by blast have
g2: "irrefl ((U---s s)||(Domain f))" using assms(2) by blast obtain F::"('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set \<Rightarrow> ('a \<times> 'b set) set" where
g3: "F=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" by simp let ?f="foldl F f ll" have
12: "\<forall>j<size ?rr. ?U f\<inter>(?rr!j)={} & (\<forall>i<j. ?rr!i\<inter>(?rr!j)={})" using assms(7) by meson moreover have
18: "\<forall>i<size ?rr. (i<size l & ?rr!i=?U(l!i) & ?rr!i=(?b ?rr)!i) \<or> (i=size l & ?rr!i = r)" using 4 13 14 length_map 
nth_append_length subsetI by (simp add: nth_append) moreover have "?U(last ll) \<subseteq> r & ?U(ll!(size l))\<subseteq> r" 
using "1" by auto ultimately moreover have
15: "\<forall>i<size ?rr.(i<size l & ?rr!i=?U(ll!i))\<or>(i=size l & ?U(ll!i)\<subseteq>r)" using 13 by force ultimately moreover have 
16: "\<forall>i<size ?rr. ?U(ll!i) \<subseteq> ?rr!i" by (metis (no_types, lifting) "13" subset_eq)
ultimately moreover have "\<forall>j<size ?rr. ?U f\<inter>?U (ll!j)={}" using 12 by fast moreover have
"\<forall>j<size ?rr. (\<forall>i<j. ?U(ll!i)\<inter>?U(ll!j)={})" using 16 12 15 inf.orderE inf.strict_boundedE 
inf.strict_order_iff inf_bot_right inf_left_commute by (smt) ultimately have
g4: "let u=Union o Range in \<forall>j<size ll. u f\<inter>u (ll!j)={} & (\<forall>i<j. u(ll!i)\<inter>u(ll!j)={})"
using 4 by simp have
g6: "isRepresentation f ?d ?u" using assms(1) by simp have 
2: "isRepresentation (foldl F f ll) ?d ?u & Domain (foldl F f ll)=Domain f" using g1 g2 g3 g4 g5 g6
by (rule iterativeRepresentationPreservation [of f ?u F ll ?d]) then have
i9: "isRepresentation ?f' ?d ?u" unfolding g3 pointUnion_def by blast have 
26: "U^-1``{s} \<inter> (Union ?DD)={} & ?da \<subseteq> Inter ?DD & ?D f-(D^-1``{s}\<union>U^-1``{s})\<subseteq>Union(?DD)" 
using assms(4) by meson then have
h2: "\<forall>i<size l. U^-1``{s} \<inter> (?D (l!i))={} & (?D (l!i))\<supseteq>(?da)" by force
have "(Union o set) (map ?D l)=Union(?D`(set l))" by force have
h3: "Domain ?f - (D^-1``{s} \<union> U^-1``{s}) \<subseteq> (Union o set) (map Domain l)" using 2 26 by auto have 
h6: (*redundant from 26? NO*) "U^-1``{s}\<inter>?da={}" using assms(2) by blast have 
24: "s\<notin>?D f" using assms(2) by fast then have
h7: "s\<notin>Domain ?f" using 2 by blast have
h4: "ll=l@[?LA]" using "1" by blast have 
17: "Union(set ?rr) = (r \<union> Union(?U`set l))" by simp have 
33: "\<forall>A\<in>set ll. ?U A\<inter>?U f={}" using g4 in_set_conv_nth inf_commute 
(*by (metis in_set_conv_nth inf.commute) what's the difference bwt inf.commute and inf_commute?*)
by (metis(no_types)) then have "Union(?U`set ll) \<inter> (?U f)={}" by fast have
"\<forall>Y\<in>set ?rr. Y\<inter>(?U f)={}" using 12 in_set_conv_nth inf_commute by metis then
have "Union(set ?rr) \<inter> (?U f)={}" by fast then have (*aa1*) "(r \<union> Union(?U`set l)) \<inter> (?U f)={}" unfolding 17 by blast then have
h13: "(Union o set) ?rr\<inter>(Union o Range) f={}" by simp have 
"Union ((?U`(set l))\<union>Range f)=Union ((?U`(set l))\<union>{?U f})" by auto have "\<forall>i<size l. ?rr!i\<inter>r={}" 
using 12 18 4 19 One_nat_def add.left_neutral lessI plus_nat.simps(2) by (metis(no_types,hide_lams)) 
then have "\<forall>Y\<in>?U`(set l). Y\<inter>r={}" using "13" imageE in_set_conv_nth by (metis (no_types, lifting)) 
then have "r\<inter>Union (?U`(set l))={}" by blast moreover have "r \<inter> ?U f={}" using 12 19 4 Int_commute 
One_nat_def le_add2 le_simps(2) plus_nat.simps(2) by (metis) ultimately have 
"r\<inter>Union ((?U`(set l))\<union>{?U f})={}" by blast moreover have "r\<noteq>{}" using assms(2) by blast 
ultimately have (*aa2*) "\<not>(r\<subseteq>Union ((?U`(set l))\<union>{?U f}))" by fast then have
h14: "\<not>(last ?rr\<subseteq>(Union ((set (butlast ?rr))\<union>Range f)))" unfolding 4 by auto have 
22: "let g=foldl F f ll in let RA=(Union o set) ?rr in 
(\<forall>x\<in>Domain g. RA \<subseteq> g,,,x = (x\<in>D^-1``{s}-{s}) & ((g,,,x)\<inter>RA={})=(x\<in>U^-1``{s}))"
using g3 h2 h3 h4 h5 h6 h7 h8 h9 h11 h12 h13 h14 by (rule l34) have
23: "{}\<notin>Range f" using assms(2) by fast have 
g55: "\<forall>A\<in>set ll. Domain A \<subseteq> Domain f & runiq A" using g5 by auto then have 
i1: "runiq ?f" using l28 g3 g1 by blast
have "(D``{s}={s})=((s,s)\<in>D & D``{s}\<subseteq>{s})" by blast have
i2: "(s,s)\<in>D" using assms(2) by blast have
i3: "D``{s}\<subseteq>{s}" using assms(2) by simp have
i4: "s\<notin>Domain ?f" using 2 24 by blast have
i5: "\<forall>x\<in>Domain ?f. \<not>(?f,,,x \<subseteq> ?RA)" 
proof- {fix x have "?f,,,x = f,,,x  \<union> Union ((\<lambda>f. f,,,x)`(set ll))" using g3 by (rule l30b) 
   moreover assume "x\<in>?D f" then moreover have 
  "f,,,x\<noteq>{}" using 23 Range.simps eval_runiq_rel lll82 by (metis g1)
  moreover have "f,,,x \<inter> ?RA={}" using 20 h13 by blast
  ultimately moreover have "\<not>f,,,x\<subseteq>?RA" by blast ultimately have "\<not>(?f,,,x \<subseteq> ?RA)" by auto}
  thus ?thesis using 2 by blast
qed have
i6: "\<forall>x\<in>Domain ?f. ?RA \<subseteq> ?f,,,x = (x\<in>D^-1``{s}-{s})" using 22 by meson have
i7: "\<forall>x\<in>Domain ?f. ((?f,,,x)\<inter>?RA={})=(x\<in>U^-1``{s})" using 22 by meson 
have "\<forall>x\<in>Domain f. ((x,s)\<in>U) = ((s,x)\<in>U)" using assms(3) by blast then have 
i8: "\<forall>x\<in>Domain ?f. ((x,s)\<in>U) = ((s,x)\<in>U)" using "2" by blast have 
i9: "isRepresentation ?f (D---s s) (U---s s)" using 2 by blast obtain g where
i10: "g=?f+<(s,?RA)" by blast have
i11: "?RA\<noteq>{}" using "4" "6" h14 by blast have
i12: "(s,s)\<notin>U" using assms(2) by blast
have "isRepresentation' g D U" using i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12
by (rule extension2  [of ?f s D ?RA U g]) moreover have
21: "?f=?f'" unfolding g3 pointUnion_def by simp ultimately have 
27: "isRepresentation ?h D U & isRepresentation ?g ?d ?u" using 2 21 unfolding i10 
isRepresentation_def by simp have "?D ?g=?D f" using "2" "21" by auto moreover have 
"?D ?h=?D ?g \<union> {s}" by (simp add: paste_Domain) ultimately have 
28: "?D ?g=?D f & ?D ?h=?D f \<union> {s}" by presburger have 
34: "let U=Union o Range in (Union (U`(set ll))) \<inter> (U f)={}" using 33 by auto have 
i21: "runiq ?g" using "21" i1 by auto then have 
36: "runiq ?h" by (simp add: assms runiq_paste2 runiq_singleton_rel) have "\<forall>x\<in>?D ?g. \<not>(?g,,,x\<subseteq>?RA)" 
using i5 by (simp add: "21") then have 35: "?RA \<notin> ?R ?g" using i21 by (metis l37m subsetI)
{ assume "luniq f" then have 31: "isInjection f" using g1 l45u by blast have 
  30 :"isInjection ?g" using 31 g55 34 by (rule l31) have "isInjection ?g & luniq ?h" 
  unfolding luniq_def using 30 35 by (simp add: runiq_converse_paste_singleton) } then have 
37: "luniq f \<longrightarrow> (isInjection ?g & isInjection ?h)" using 36 by (simp add: luniq_def) have 
"{}\<notin>?R ?g " using l37m "21" empty_subsetI i1 i5 by (metis) moreover have "?R ?h\<subseteq>?R ?g \<union>{?RA}" 
unfolding paste_def Outside_def by force moreover have "?RA\<noteq>{}" using i11 by blast 
ultimately have "{}\<notin>?R ?h & {}\<notin>?R ?g" by blast thus ?thesis using 27 28  37 h4 by meson
qed

lemma l40a: assumes "conc=(Range D)-(p\<union>u)" "D``u\<subseteq>u" "XX=((\<lambda> m. D^-1``{m} -p)`conc)" "Range D \<supseteq> Domain D" 
(*"reflex (D^-1||conc)"*) "conc \<subseteq> fixPts D"
(*"p\<inter>u={}" "M\<inter>u={}" "p\<subseteq>Field D" "M\<noteq>{}" "M=maximals3' (D||((Field D)-(p \<union> u)))"*)  
shows "Union XX=conc"
proof- let ?f="\<lambda> m. D^-1``{m} -p" let ?D=Domain let ?R=Range let ?F=Field have 
2: "?F D=?F(D^-1)" unfolding Field_def by auto  
{ fix x assume "x\<in>Union XX" then obtain m where "m\<in>conc & x\<in>(D^-1``{m})-p" using assms(3) by fast 
  then moreover have "x\<notin>(p \<union> u)" using assms(1,2) by blast ultimately have "x\<in>conc" 
  unfolding assms(1) using assms(4) by blast
} then have 
0: "Union XX \<subseteq> conc" by blast have "Union XX\<supseteq>conc" 
proof-
{fix x assume "x\<in>conc" then moreover have 
  "x\<in>(D^-1)``{x}" using assms(5) unfolding fixPts_def by blast ultimately moreover have "x\<in>?f x" 
  unfolding assms(1) by blast ultimately have "x\<in>Union XX" unfolding assms(3) by blast
} thus ?thesis by blast
qed thus ?thesis using 0 by blast
qed

lemma l40b: assumes "conc=(Range D)-(p\<union>u)" "D``u\<subseteq>u" "XX=((\<lambda> m. D^-1``{m}\<union>p)`conc)" 
"Range D \<supseteq> Domain D" "conc \<subseteq> fixPts D" shows 
"(Union XX) \<union> p = conc \<union> p" and "(p\<inter>u={}) \<longrightarrow> u\<inter>(Union XX \<union> p)={}" 
proof- let ?f="\<lambda>m. D^-1``{m}-p" let ?g="\<lambda>m. D^-1``{m} \<union> p" have "Union (?f`conc)=conc" using 
assms l40a by metis moreover have "(Union (?g`conc)) \<union> p = Union (?f`conc) \<union> p" by blast 
ultimately show "(Union XX) \<union> p = conc \<union> p" using assms(3) by presburger then 
show "(p\<inter>u={}) \<longrightarrow> u\<inter>(Union XX \<union> p)={}" unfolding assms(3,1) by auto
qed

lemma l40c: assumes "set conc=(Range D)-(p\<union>u)" "ll=(map (\<lambda> m. D^-1``{m}\<union>p) (conc))@[p]" 
"CC=(Union o set)(butlast ll)" "D``u\<subseteq>u" "Range D \<supseteq> Domain D" "set conc \<subseteq> fixPts D" shows 
"CC \<union> p = (set conc) \<union> p" and "(p\<inter>u={}) \<longrightarrow> (CC\<union>p) \<inter> u={}" using assms l40b 
proof- let ?Conc="set conc" let ?f="\<lambda>x. D^-1``{x} \<union> p" have "Union (?f`?Conc)\<union>p=?Conc \<union> p" 
using assms l40b by metis moreover have "Union (?f`?Conc)=CC" unfolding assms(3,2) by force 
ultimately moreover show "CC \<union> p = (set conc) \<union> p" using assms l40b by force 
ultimately show "(p\<inter>u={}) \<longrightarrow> (CC\<union>p) \<inter> u={}" using assms by auto
qed

lemma l40d: assumes "set conc=(Range D)-(p\<union>u)" "ll=(map (\<lambda> m. D^-1``{m}\<union>p) (conc))@[p]" 
"CC=(Union o set)(butlast ll)" "D``u\<subseteq>u" "Range D \<supseteq> Domain D" "set conc \<subseteq> fixPts D" shows 
"(set conc) \<subseteq> CC" and "(p\<inter>u={}) \<longrightarrow> (CC\<union>p) \<inter> u={}"
using assms(1-6) l40c(1) l40c(2) apply blast using assms(1-6) l40c(1) l40c(2) by metis

(* TODO: l41p,q,s,ss,u; ss implies s and u?*)

lemma l44b: assumes "set conc=(Range d)-(p\<union>us)" "ll=(map (\<lambda> m. d^-1``{m}\<union>p) (conc))@[p]" 
"CC=(Union o set)(butlast ll)" "d``us\<subseteq>us" "Range d \<supseteq> Domain d" "{s}\<union>set conc \<subseteq> fixPts D" (*6*) 
"p=d^-1``(D^-1``{s})" "trans(d^-1)" "d=(D---s s)" "irrefl (U||({s} \<union> set conc))" "sym U" (*11*)
"isMonotonicOver U (D|^((D^-1``{s}) \<union> set conc))" "us \<supseteq> U^-1``{s}"
shows 
"(set conc) \<subseteq> CC" and "(p\<inter>us={}) \<longrightarrow> (CC\<union>p) \<inter> us={}" and "p\<subseteq>(Inter o set) ll" and 
"\<forall>C\<in>set ll. C\<subseteq>Domain D-{s} & isDownClosed (D---s s) C & isConflictFree2 (U---s s) C"
(*using assms(1-6) l40d(1)[of _ "d"] sorry print_state 
using assms(1-6) l40d(2) apply auto[1] print_state*)
proof- let ?D=Domain let ?R=Range let ?d="D---s s" let ?u="U---s s" let ?g="\<lambda> m. ?d^-1``{m}\<union>p"
let ?P="D^-1``{s}" let ?p="?d^-1``?P" let ?l="map ?g conc" let ?cf'="isConflictFree2'" 
let ?cf=isConflictFree2 let ?dc'=isDownClosed' let ?dc=isDownClosed let ?mo'=isMonotonicOver have
"fixPts' D \<subseteq> {s} \<union> fixPts' d" unfolding fixPts_def assms(1,9) singlebouthside_def 
by fast then have "fixPts D = {s} \<union> fixPts d" using assms(6,9) Un_Diff_cancel l45b l45k le_supE sup.commute sup.orderE 
by (metis (no_types, lifting)) moreover have "s\<notin>set conc" unfolding assms(1,9) singlebouthside_def by blast
ultimately have 
31: "set conc \<subseteq> fixPts d" using assms(6) by fast then 
show "(set conc) \<subseteq> CC" and "(p\<inter>us={}) \<longrightarrow> (CC\<union>p) \<inter> us={}" using assms(1-5) l40d apply fast 
using assms(1-5) 31 l40d(2) by auto have 
1: "p=?p & d=?d & butlast ll=?l" unfolding assms(2,7,9) by simp have
5: "isDownClosed ?d p" using 1 l41g assms(8) by (metis isDownClosed_def)  have 
8: "size ll=1+size conc" using assms(2) One_nat_def add.commute add.right_neutral length_append 
length_map list.size(3) list.size(4) by (metis (no_types, lifting)) have
11: "p\<in>set ll & p \<subseteq> ?D D-{s}" using assms(2,7,9) unfolding singlebouthside_def by force have
3: "?p=?d^-1``(?P-{s})" unfolding singlebouthside_def by blast have 
22: "(s,s)\<notin>U" using assms unfolding restrict_def irrefl_def by fast have
20: "sym U & sym ?u"using assms(11) unfolding sym_def singlebouthside_def by blast moreover have 
24: "?cf U {s}" unfolding isConflictFree2_def using 22 by simp have 
"?mo' ?u ((D|^(?P \<union> set conc))---s s)" unfolding l45b using assms(12) l45c by metis
moreover have "(D|^(?P \<union> set conc))---s s=?d|^((?P\<union>set conc)-{s})" unfolding singlebouthside_def corestriction_def 
by blast ultimately have 
26: "?mo' ?u (?d|^(?P \<union> set conc))" unfolding singlebouthside_def corestriction_def by blast
have "s\<in>?P" using assms(6) unfolding fixPts_def by blast then have 
25: "?mo' U (D|^({s} \<union> set conc))" using assms(12) unfolding corestriction_def by blast then have 
27:"?mo' U (D|^{s})" unfolding corestriction_def by fast have 
"?cf U ?P" using 27 24 20 l41u by blast then have "?cf ?u ?P" unfolding isConflictFree2_def singlebouthside_def by blast
moreover have "?mo' ?u (?d|^?P)" using 26 by (simp add: l42h restrict_def) ultimately have 
2: "?cf ?u p" using 1 l41u 20 by blast have
29: "irrefl (U||(set conc))" using assms(10) unfolding restrict_def irrefl_def by fast have 
30: "set conc \<subseteq> fixPts D" using assms(6) by simp 
{ assume "\<exists>C\<in>set ll. \<not>(C\<subseteq>?D D-{s} & isDownClosed (D---s s) C & isConflictFree2 (U---s s) C)" 
  then obtain C where
  12: "C\<in>set ll & \<not>(C\<subseteq>?D D-{s} & isDownClosed ?d C & isConflictFree2 ?u C)" by blast then have
  "C\<noteq>p" using 2 5 11 by fast then have "C\<in>set ?l" using assms(2,9) 12 Un_insert_right append_Nil2 
  by (simp add: insert_iff list.simps(15) set_append) then obtain m where 
  13: "m\<in>set conc & C=?d^-1``{m} \<union> ?p" using 1 by auto then have 
  4: "C\<subseteq>(?D D)-{s}" unfolding singlebouthside_def by force have
  31: "m\<in>Field (D|^(set conc))" unfolding corestriction_def Field_def using 13 assms(1,9) 
  unfolding singlebouthside_def by auto have 
  10: "m\<in>set conc - {s}" using 13 unfolding assms(1,9) singlebouthside_def by blast have 
  "?dc' d (d^-1``{m})" using assms(8) l41g by metis then have 
  6: "?dc' d C" using 13 l41v assms(8,9) l41g by (metis) have 
  23: "m\<notin>us" using 13 assms(1) DiffD2 rev_subsetD sup_ge2 by blast moreover have 
  "?mo' U (D|^{s})" using 27 by blast moreover have "?cf U {s}" using 
  22 unfolding isConflictFree2_def by simp ultimately have "?cf U ?P" using l41u 20 by blast 
  have "(s,m) \<notin> U" using assms(11,13) 23 subsetCE Image_singleton_iff sym_conv_converse_eq by (metis) 
  moreover have "(m,m)\<notin>U" using 29 13 unfolding restrict_def Outside_def irrefl_def by blast 
  ultimately have "?cf U ({m}\<union>{s})" using 20 22 unfolding sym_def isConflictFree2_def by fast 
  moreover have "?mo' U (D|^( {s} \<union> set conc))" using 25 by blast ultimately 
  moreover have "?mo' U (D|^( {m}\<union>{s} ))" using 10 unfolding corestriction_def by auto
  ultimately have "?cf U (D^-1``{m} \<union>?P)" using l41ss 20 Un_insert_right insert_is_Un by blast
  then have "?cf' U (D^-1``{m} \<union>?P)" unfolding isConflictFree2_def by blast 
  then have "?cf' ?u (D^-1``{m} \<union> ?P)" unfolding singlebouthside_def by blast moreover
  have "m\<in>D^-1``{m}" using 13 30 unfolding fixPts_def by blast
  ultimately have "?cf ?u ({m}\<union>?P)" unfolding isConflictFree2_def by blast
  moreover have "?mo' ?u (?d|^({m} \<union> ?P))" using 13 26 unfolding corestriction_def by auto
  ultimately have "?cf ?u (?d^-1``{m} \<union> ?d^-1``?P)" using l41ss 20 by blast then have "?cf ?u C" 
  using 13 3 by simp then have False using 12 4 6 1 by (simp add: isDownClosed_def)
} then show "\<forall>C\<in>set ll. C\<subseteq>?D D-{s} & isDownClosed (D---s s) C & isConflictFree2 (U---s s) C" 
using 1 by meson 
show "p\<subseteq>(Inter o set) ll" unfolding assms(2) by auto
qed

theorem l46: assumes "isRepresentation f (D---s s) (U---s s)" 
"runiq f & D``{s}={s} & sym U & (let dm=Domain in let R=Range in {}\<notin>R f & finite ((Union o R) f) 
& (Domain D)-{s} \<subseteq> dm f &(let d=D---s s in dm f \<subseteq> Range d & trans d))"
"let d=D---s s in let sparents=d^-1``(D^-1``{s}) in let sconfl=U^-1``{s} in 
let sconcurs=Range d-(sparents \<union> sconfl) in finite sconcurs & sconcurs\<subseteq>fixPts D & 
sparents=D^-1``{s}-{s} & irrefl (U||(sconcurs \<union> Domain f)) & sconfl \<inter> D^-1``{s}={} & 
d``sconfl\<subseteq>sconfl & isMonotonicOver U (D|^(D^-1``{s} \<union> (Range d - sconfl)))"
shows "\<exists> l. let N=Max ((Union o Range) f)+1+size l in let d=Domain in let R=Range in 
let RA=(Union o set)((map (Union o R) l)@[{N}]) in let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{{N}}]) in 
let h=g+<(s,RA) in d g=d f & d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & 
isRepresentation h D U & runiq g & runiq h & (Union o R) h \<subseteq> {0..<1+N} & (luniq f \<longrightarrow> (isInjection g & isInjection h))"
proof- let ?D=Domain let ?R=Range let ?d="D---s s" let ?u="U---s s" let ?p="?d^-1``(D^-1``{s})"
let ?p'="D^-1``{s}-{s}"let ?U="Union o Range"let ?b=butlast let ?us="U^-1``{s}"let ?M="Max (?U f)" have 
j2: "finite ((?R ?d)-(?p\<union>?us)) & finite (?U f)" using assms by meson have
ii2a: "runiq f" using assms(2) by blast have 
ii2b: "{}\<notin>Range f" using assms(2) by presburger have 
hh6: "((?R ?d)-(?p\<union>?us)) \<subseteq> fixPts D" using assms(3) by meson have 
ii2c: "D``{s}={s}" using assms(2) by linarith have        
hh8: "trans(?d^-1)" using assms(2) trans_converse by auto have
hh11: "sym U" using assms(2) by blast have
hh4: "?d``?us\<subseteq>?us" using assms(3) by meson have
hh12: "isMonotonicOver U (D|^(D^-1``{s} \<union> (?R ?d - ?us)))" using assms(3) by meson 
have (* weaker than reflex+trans? *) 
k9: "?p = D^-1``{s}-{s}" using assms(3) by meson have 
j1: "?D f \<subseteq> ?R ?d" using assms(2) by meson have
k7: "?D D-{s} \<subseteq> ?D f" using assms(2) by presburger have
j3: "irrefl((U||(?D f)) \<union> U||((?R ?d)-(?p\<union>?us)))" using l45p assms(3) by (metis Un_commute) have 
ii2e: "?us\<inter>(D^-1``{s})={}" using assms by meson have 52: "s\<notin>U``{s}" using ii2c ii2e by blast have 
"(U||{s})\<union>((U---s s)||(Domain f))={s}\<times>U``{s} \<union>(U||(?D f)-(U^-1``{s})\<times>{s})" unfolding restrict_def 
singlebouthside_def by blast moreover have "U||({s} \<union> ((?R ?d)-(?p\<union>?us))) \<union> (U---s s||(?D f))=
U||{s} \<union> U||((?R ?d)-(?p\<union>?us)) \<union> (U---s s||(?D f))" unfolding restrict_def by blast ultimately have 
"U||({s} \<union> ((?R ?d)-(?p\<union>?us))) \<union> (U---s s||(?D f))=
{s}\<times>U``{s} \<union> (U||(?D f) - (U^-1``{s})\<times>{s}) \<union> U||((?R ?d)-(?p\<union>?us))" by auto moreover have
"(irrefl (U||({s} \<union> ((?R ?d)-(?p\<union>?us)))) & irrefl ((U---s s)||(Domain f))) = 
irrefl (U||({s} \<union> ((?R ?d)-(?p\<union>?us))) \<union> (U---s s||(?D f)))" unfolding irrefl_def by blast
ultimately moreover have "... = irrefl ({s}\<times>U``{s} \<union> U||(?D f) \<union> U||((?R ?d)-(?p\<union>?us)))" using 52 
unfolding irrefl_def by auto moreover have "...=irrefl(U||(?D f) \<union> U||((?R ?d)-(?p\<union>?us)))" 
using 52 unfolding irrefl_def by blast ultimately have 
53: "(irrefl (U||({s} \<union> ((?R ?d)-(?p\<union>?us)))) & irrefl ((U---s s)||(Domain f)))=
irrefl(U||(?D f) \<union> U||((?R ?d)-(?p\<union>?us)))" by blast have "U``{s}=?us" using hh11 unfolding sym_def by fast then
have "U=?u \<union> ?us\<times>{s} \<union> {s}\<times>?us" unfolding singlebouthside_def using hh11 by blast 
have "D^-1``{s}=?p \<union> {s}" using k9 ii2c by blast moreover
have "?p \<union> ((?R ?d)-(?p\<union>?us))= ?p \<union> (?R ?d - ?us)" by fast moreover have
"... = D^-1``{s}-{s} \<union> (?R ?d - ?us)" unfolding k9 by blast ultimately have
51: "D^-1``{s} \<union> ((?R ?d)-(?p\<union>?us))= D^-1``{s} \<union> (?R ?d - ?us)" by blast obtain conc where 
3: "distinct conc & set conc=(?R ?d)-(?p\<union>?us)" using finite_distinct_list j2 by metis
let ?ds="map (\<lambda> m. ?d^-1``{m}\<union>?p) (conc)" let ?l="constList (1+?M) ?ds" let ?dd="?D`(set ?l)" let ?CC="Union ?dd" 
let ?r="map ?U ?l" let ?N="?M+size conc+1" let ?ra="{?N}" let ?rr="?r@[?ra]" let ?llb="?ds@[?p]"
obtain l r where
"l=?l & r=?ra" by blast  then moreover have 
a0: "l=?l" by blast ultimately have
a1: "r=?ra" by linarith let ?N'="Max ((Union o Range) f)+1+size ?l" let ?ra'="{?N'}" have 
1: "{}\<notin>set ?ds" using 3 by auto then have 
i6: "Range`(set l) \<subseteq> card-`{1}-{{{}}}" unfolding a0 using l37r by metis have
0: "size ?ds=size conc & size ?l=size ?ds & size ?rr=size?l+1 & size ?r=size ?l & ?r=?b ?rr & ?r=take (size ?r) ?rr" 
unfolding constList_def by simp then have "\<forall>i<size ?ds. ?R(?l!i)={{1+?M+i}}" using l37s 1 by (metis nth_mem) 
then have "\<forall>i<size ?r. ?r!i={1+?M+i}" using 0 by force moreover have "\<forall>i<size ?r. ?rr!i=?r!i" using 
0 by (metis nth_butlast) ultimately have "\<forall>i<size ?r.?rr!i={1+?M+i}" using 0 by presburger then have 
2: "\<forall>i<size ?rr. ?rr!i={1+?M+i}" using 0  One_nat_def add.left_neutral add.right_neutral add_Suc_right 
le_simps(2) nat_less_le nth_append_length plus_nat.simps(2) by (metis (no_types, lifting)) then have 
"\<forall>j<size ?rr. (\<forall>i<j. ?rr!i\<inter>(?rr!j)={})" by simp moreover have "finite (?U f)" using j2 by blast 
then have 
54: "?U f \<subseteq> {0..<1+?M}" using le_simps(2) by force ultimately have 
"\<forall>j<size ?rr. ?U f\<inter>?rr!j={} & (\<forall>i<j. ?rr!i\<inter>(?rr!j)={})" using 2 by fastforce then have
i7: "let ur=Union o Range in let rr=(map ur l)@[r] in \<forall>j<size rr. ur f\<inter>rr!j={} & (\<forall>i<j. rr!i\<inter>(rr!j)={})" unfolding a0 a1 by presburger  have
h1: "set conc=(Range ?d)-(?p\<union>?us)" using 3 by fast have 
4: "?dd=set ?ds" by (metis l37u list.set_map)  have
h2: "?llb=(map (\<lambda> m. ?d^-1``{m}\<union>?p) (conc))@[?p]" by blast have
h3: "?CC=(Union o set)(butlast ?llb)" using 4 by auto have
h4: "?d``?us\<subseteq>?us" using hh4 by auto have "?D ?d \<subseteq> ?D D-{s}" unfolding singlebouthside_def by blast then have 
h5: "?R ?d \<supseteq> ?D ?d" using j1 k7 by simp have "s\<in>fixPts D" using ii2c unfolding fixPts_def by auto then have 
h6: "{s}\<union>set conc \<subseteq> fixPts D" using 3 using hh6 by simp have
h7: "?p=?d^-1``(D^-1``{s})" by simp have
h8: "trans(?d^-1)" using hh8 by simp have
h9: "?d=(D---s s)" by simp have 
h10: "irrefl (U||({s} \<union> set conc))" using j3 53 3 by presburger have
h11: "sym U" using hh11 by simp have
h12: "isMonotonicOver U (D|^((D^-1``{s}) \<union> set conc))" using hh12 3 51 by presburger  have
h13:  "?us \<supseteq> U^-1``{s}" by simp
have
5: "(set conc) \<subseteq> ?CC & 
  ((?p\<inter>?us={}) \<longrightarrow> (?CC\<union>?p) \<inter> ?us={}) & 
  ?p\<subseteq>(Inter o set) ?llb & 
  (\<forall>C\<in>set ?llb. C\<subseteq>Domain D-{s} & isDownClosed ?d C & isConflictFree2 ?u C)"
using h1 h2 h3 h4 h5 h6 h7 h8 h9 h10 h11 h12 h13 l44b by metis have "?p\<inter>?us={}"using ii2e k9 by auto moreover ultimately have
i4a: "U^-1``{s} \<inter> (Union ?dd)={}" using 5 by blast  
have (* replace with ?p=D^-1``{s}-{s} or, alternatively, with reflex+trans *) 
9: "?p = D^-1``{s}-{s} " using k9 by blast moreover have "?p\<subseteq>Inter ?dd" using 4 5 by simp ultimately have 
i4b: "D^-1``{s}-{s}\<subseteq>Inter ?dd" by presburger have "(?R ?d)-(?p\<union>?us)\<subseteq>?CC" using 5 3 by simp 
moreover have "?p \<subseteq> D^-1``{s}" using 9 by auto ultimately have "(?R ?d)-(D^-1``{s}\<union>?us)\<subseteq>?CC" by auto
moreover have "?D f \<subseteq> ?R ?d" using j1 by simp
then moreover have "?D f-(D^-1``{s}\<union>?us) \<subseteq> (?R ?d)-(D^-1``{s}\<union>?us)" by blast ultimately have 
i4c: "?D f - (D^-1``{s} \<union> U^-1``{s}) \<subseteq> ?CC" by simp have
i4: "let d=Domain in let dd=d`(set l) in (*CC=Union dd*)
U^-1``{s} \<inter> (Union dd)={} & D^-1``{s}-{s} \<subseteq> Inter dd & d f-(D^-1``{s}\<union>U^-1``{s})\<subseteq>Union dd" unfolding a0
using i4a i4b i4c by presburger
have "map ?D (?l@[?p\<times>{?ra}])=?llb" using l37uu by auto then have 
6: "?D`(set(?l@[?p\<times>{?ra}]))=set ?llb" by (metis set_map) have
7: "?D D-{s} \<subseteq> ?D f" using k7 by blast have
i5: "let d=Domain in \<forall>A\<in>set (l@[(D^-1``{s}-{s})\<times>{r}]). 
d A \<subseteq> d f & isDownClosed (D---s s) (d A) & isConflictFree2 (U---s s) (d A)"
proof-
{
  fix A 
  assume "A\<in>set (l@[(D^-1``{s}-{s})\<times>{r}])" then have "A\<in>set (l@[?p\<times>{r}])" using 9 by presburger then have 
  "?D A \<in> set ?llb" unfolding a0 a1 using 6 set_map by blast
  then have "?D A\<subseteq>?D D-{s} & isDownClosed ?d (?D A) & isConflictFree2 ?u (?D A)" using 5 by meson
  then have "?D A\<subseteq>?D f & isDownClosed ?d (?D A) & isConflictFree2 ?u (?D A)" using 7 by blast
} thus ?thesis by meson
qed have
i1: "isRepresentation f ?d ?u" using assms(1) by blast have "s\<notin>?D f" using j1 unfolding 
singlebouthside_def by blast moreover have "s\<notin>U``{s}" using ii2c ii2e by auto ultimately have
i2: "runiq f & r\<noteq>{} & {}\<notin>Range f & D``{s}={s} & irrefl ((U---s s)||(Domain f)) & 
(U^-1``{s}\<inter>(D^-1``{s})={} & s\<notin>U``{s}\<union>Domain f)"  unfolding a1 using j3 53 ii2a ii2b ii2c ii2e by blast have
i3: "\<forall>x\<in>Domain f. ((x,s)\<in>U) = ((s,x)\<in>U)" using hh11 unfolding sym_def by blast have
40: "let d=Domain in let R=Range in let RA=(Union o set)((map (Union o Range) l)@[r]) in 
let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{r}]) in let h=g+<(s,RA) in d g=d f & 
d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & isRepresentation h D U
& (luniq f \<longrightarrow> (isInjection g & isInjection h))"
using i1 i2 i3 i4 i5 i6 i7 by (rule l44a) 
let ?ll="?l@[?p'\<times>{?ra}]" let ?ll'="?l@[?p'\<times>{?ra'}]" let ?g="foldl pointUnion f ?ll"  
let ?RA="(Union o set)((map ?U ?l)@[?ra])" let ?RA'="(Union o set)((map ?U ?l)@[?ra'])" 
let ?g'="foldl pointUnion f ?ll'" let ?h'="?g+<(s,?RA')" let ?h="?g+<(s,?RA)" have 
59: "?N=?N' & ?ra=?ra' & ?RA=?RA' & ?g=?g' & ?h=?h'" using 0 a0 a1 by simp have 
41: "?D ?g=?D f & ?D ?h=?D f\<union>{s} & {}\<notin>?R ?g & {}\<notin>?R ?h & isRepresentation ?g ?d ?u & isRepresentation ?h D U"
using 40 unfolding a0 a1 by metis have 
43: "pointUnion=(\<lambda>ff A. ff +< ((\<lambda>x. ff,,,x \<union> A,,,x)|||(Domain A)))" unfolding pointUnion_def by blast then have 
42: "?U ?g = ?U f \<union> Union (?U`(set ?ll))" by (rule l30c)
have "s\<notin>?D ?g"using i2 41 UnCI by metis then have 
44: "?h = ?g \<union> {(s,?RA)}" unfolding paste_def Outside_def by auto
then have "?R ?h=?R ?g \<union> {?RA}" by force then have "?U ?h = ?U f \<union> Union (?U`(set ?l)) \<union> ?ra" 
using 42 by force moreover have "Union (?U`(set ?l)) \<subseteq> {1+?M..<?N}" using l37z by fastforce
moreover have "?U f\<subseteq>{0..<1+?M}" using  l38 54 by blast ultimately moreover have 
"?U f \<union> (Union (?U`(set ?l))) \<subseteq> {0..<?N}" by force then moreover have 
"?U f \<union> Union (?U`(set ?l)) \<union> ?ra \<subseteq> {0..<1+?N}" by fastforce ultimately have 
57: "?U ?h \<subseteq> {0..<1+?N}" by presburger have "?ll=l@[(D^-1``{s}-{s})\<times>{r}]" unfolding a0 a1 by blast 
then have "\<forall>A\<in>set ?ll. Domain A \<subseteq> Domain f" using i5 by simp moreover have 
"\<forall>A\<in>set ?ll. A\<in>set ?l \<or> runiq A" using lll59b by fastforce moreover have 
"\<forall>A\<in>set ?l. runiq A" using l37aa by blast ultimately have "\<forall>A\<in>set ?ll. Domain A \<subseteq> Domain f & runiq A" 
by fast then have 
55: "runiq ?g" using ii2a pointUnion_def l28 by blast moreover have "runiq {(s,?RA)}"
using runiq_singleton_rel by fast ultimately have 
56: "runiq ?h" using runiq_paste2 by auto have 
"let N=Max ((Union o Range) f)+1+size l in let d=Domain in let R=Range in 
let RA=(Union o set)((map (Union o R) l)@[{N}]) in let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{{N}}]) in 
let h=g+<(s,RA) in d g=d f & d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & isRepresentation h D U
& runiq g & runiq h & (Union o R) h \<subseteq> {0..<1+N} & (luniq f \<longrightarrow> (isInjection g & isInjection h))" 
using 55 56 57 59 40 unfolding a0 a1 by (metis) then show ?thesis by blast
qed






























section{* Representation implies ES *}

proposition l47g: assumes "isMonotone' P Q f" shows 
"fixPts (P\<union>(Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q))) = f-`(fixPts Q)" unfolding fixPts_def using assms by blast

lemma l48a: assumes "trans Q" "isMonotone' P Q f" shows "trans (P\<union>(Union((%(y0,y1). (f-`{y0})\<times>(f-`{y1}))`Q)))"
proof- let ?p="Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q)" let ?P="P\<union>?p"
{
  fix x y z assume "(x,y)\<in>?P" then have "(f x, f y)\<in>Q" using assms(2) by fast moreover assume
  "(y,z)\<in>?P" then moreover have "(f y, f z)\<in>Q" using assms(2) by fast ultimately have
  "(f x, f z)\<in>Q" using assms(1) unfolding trans_def by meson then have "(x,z)\<in>?P" by blast
} thus ?thesis unfolding trans_def by blast
qed

lemma l48b: assumes "isMonotone' P Q f" "Field Q\<subseteq>fixPts Q" "f`(Field P)\<subseteq>Field Q" shows 
"let P'=(P\<union>(Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q))) in Field P' \<subseteq> fixPts P'" 
proof- let ?p="(Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q))" let ?P'="P\<union>?p"
have "f-`(Field Q) \<supseteq> Field P" using assms(3) image_subset_iff_subset_vimage by metis moreover have 
"f-`(Field Q) \<supseteq> Field ?p" unfolding Field_def by fast ultimately have "Field ?P' \<subseteq> f-`(Field Q)" 
unfolding Field_def by blast moreover have "fixPts ?P'=f-`(fixPts Q)" using assms(1) by (rule l47g) 
moreover have "f-`(fixPts Q) \<supseteq> f-`(Field Q)" using assms(2) by fast ultimately show ?thesis by force
qed

proposition l48c: assumes "Field P \<subseteq> Domain f" shows "isMonotone4' P Q f=isMonotone' P Q f,,"
using assms(1) unfolding Field_def using assms by fastforce

abbreviation "isPreorder' P==reflex P & trans P" definition "isPreorder=isPreorder'"

proposition assumes "isMonotone' P Q f" shows "P\<subseteq>(Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q))"
using assms by fast

proposition l47mm: assumes  "trans Q" "reflex Q" "antisym Q"
"(Union((%(y0,y1). (f-`{y0})\<times>(f-`{y1}))`Q)) \<subseteq> D" shows
"(\<forall>x0\<in>Field Q. (\<forall>x1\<in>Field Q. (((f x0, f x1)\<in>Q \<longrightarrow> ((x0, x1)\<in>D)))))"
using assms(4) 
by fast

proposition l47m: assumes "runiq f" 
"let Q=rel2pairset' (op \<supseteq>) in (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D" shows
"(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((f,,x0 \<supseteq> f,,x1) \<longrightarrow> ((x0, x1)\<in>D)))))"
proof- let ?g="%(x,y). (f^-1``{x})\<times>(f^-1``{y})" let ?Q="rel2pairset' (op \<supseteq>)" let ?p="Union(?g`?Q)" 
let ?d=Domain { fix x0 x1 let ?y0="f,,x0" let ?y1="f,,x1" assume "x0\<in>?d f" moreover assume "x1\<in>?d f"  
  ultimately have "(x0, x1)\<in>?g (?y0, ?y1)" using assms(1) 
  by (meson Image_singleton_iff SigmaI converse.intros eval_runiq_rel mem_case_prodI)
  moreover assume "?y0 \<supseteq> ?y1" ultimately have "(x0,x1)\<in>Union(?g`?Q)" by auto
  then have "(x0,x1)\<in>D" using assms(2) by force
} thus ?thesis by blast qed









proposition l47p: assumes "runiq f" "(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((f,,x0, f,,x1)\<in>Q \<longrightarrow> ((x0, x1)\<in>D)))))"
shows "(Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D" (is "?P \<subseteq> _")
using assms l08 vimage_singleton_eq Int_iff SigmaE Union_iff imageE mem_case_prodE subsetI
by (smt )
(*proof- let ?d=Domain
{
  fix x0 x1 assume "(x0, x1)\<in>?P" then obtain y0 y1 where 
  "(y0, y1)\<in>Q & (x0,x1)\<in>f^-1``{y0} \<times> f^-1``{y1}" by blast then moreover have "x0\<in>?d f & x1\<in>?d f" by blast
  ultimately moreover have "y0=f,,x0 & y1=f,,x1" using assms(1) IntD2 SigmaD1 SigmaD2 l08 vimage_singleton_eq 
by (metis ) ultimately have "(x0,x1)\<in>D" using assms(2) by blast
  } thus ?thesis by fast
qed*)

proposition l47q: assumes "runiq f" "(Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D" (is "?P \<subseteq> _")
shows "(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((f,,x0, f,,x1)\<in>Q \<longrightarrow> ((x0, x1)\<in>D)))))" using assms RelationProperties.l31 
proof- let ?d=Domain { fix x0 x1 let ?y0="f,,x0" let ?y1="f,,x1"
  assume "x0\<in>?d f & x1\<in>?d f & (f,,x0, f,,x1)\<in>Q" then moreover have 
  "(x0,x1)\<in>f^-1``{?y0} \<times> f^-1``{?y1}" using RelationProperties.l31 assms(1) by fastforce 
  ultimately have "(x0, x1)\<in>D" using assms(2) by blast
} thus ?thesis by blast qed

proposition l47r: assumes "runiq f" shows "(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((f,,x0, f,,x1)\<in>Q \<longrightarrow> ((x0, x1)\<in>D)))))=
((Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D)" using assms l47p l47q by metis 


proposition l47s: assumes "runiq f" "Field D \<subseteq> Domain f" 
"(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. ((((x0, x1)\<in>D)\<longrightarrow>(f,,x0, f,,x1)\<in>Q  ))))"
shows "D \<subseteq> (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q))" (is "_\<subseteq>?P") 
proof- let ?d=Domain
{
  fix x0 x1 let ?y0="f,,x0" let ?y1="f,,x1" assume "(x0,x1)\<in>D" then moreover have 
  "x0\<in>?d f & x1\<in>?d f" using assms(2) unfolding Field_def by blast ultimately moreover have "(?y0, ?y1) \<in>Q" 
  using assms(3) by blast ultimately have "(?y0, ?y1)\<in>Q & (x0, x1)\<in>f^-1``{?y0}\<times>f^-1``{?y1}" 
  using assms(1) eval_runiq_rel by fastforce then have "(x0,x1)\<in>?P" by blast
} thus ?thesis by fast
qed

proposition l47t: assumes "runiq f" 
"D \<subseteq> (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) " (is "_\<subseteq>?P")
shows "(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. ((((x0, x1)\<in>D)\<longrightarrow>(f,,x0, f,,x1)\<in>Q))))"
using assms(1,2) Field_def eval_runiq_rel Image_singleton_iff RelationProperties.l31 SigmaD1 SigmaD2 
UnionE converse_iff imageE mem_case_prodE subsetCE by (smt)

proposition l47u: assumes "runiq f" "Field D \<subseteq> Domain f" shows
"(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. ((((x0, x1)\<in>D)\<longrightarrow>(f,,x0, f,,x1)\<in>Q)))) = 
(D \<subseteq> (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)))"
using assms l47s l47t by metis

proposition l47n: assumes "runiq f" "(\<forall>x0\<in>Domain f.(\<forall>x1\<in>Domain f.(((f,,x0\<supseteq>f,,x1)\<longrightarrow>((x0,x1)\<in>D)))))" 
shows "let Q=rel2pairset' (op \<supseteq>) in (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D" 
proof- let ?g="%(x,y). (f^-1``{x})\<times>(f^-1``{y})" let ?d=Domain
let ?Q="rel2pairset' ((op \<supseteq>)::('b set => 'b set => bool))" let ?p="Union(?g`?Q)" 
{ fix y0::"'b set" fix y1 assume "(y0, y1)\<in>?Q" 
  then have "(f^-1``{y0}) \<times> (f^-1``{y1})\<subseteq>D" using assms(1,2) IntE SigmaE l08 l35 subsetI 
  vimage_singleton_eq by smt } thus ?thesis by auto
qed













proposition l48e: "(\<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. (x, y)\<in>D\<longrightarrow>(f,,x, f,,y)\<in>Q)) = 
                   (isMonotone4 D  Q f)" unfolding isMonotone4_def by force                   
corollary l48ee: "(\<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. ((((x, y)\<in>D)\<longrightarrow>(f,,x \<supseteq> f,,y))))) = 
                   (isMonotone4 D (rel2pairset' (op \<supseteq>)) f)" unfolding isMonotone4_def by force

proposition l48d: "(P\<subseteq>(Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)))=isMonotone3 P Q f" 
unfolding isMonotone3_def Field_def by fast

lemma l47o: assumes "runiq f" shows "(\<forall>x0\<in>Domain f.(\<forall>x1\<in>Domain f.(((f,,x0\<supseteq>f,,x1)\<longrightarrow>((x0,x1)\<in>D)))))=
                (let Q=rel2pairset' (op \<supseteq>) in (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> D)" 
using assms l47m l47n by metis

corollary l48i: assumes "runiq f" shows "isMonotone3 P Q f=(Field P \<subseteq> Domain f & isMonotone4 P Q f)"
unfolding isMonotone3_def isMonotone4_def
using assms l48f l48g l48h by metis

lemma l48j: assumes "runiq f" "Field D \<subseteq> Domain f" shows 
"(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((x0, x1)\<in>D)=(f,,x0 \<supseteq> f,,x1))))=
((let Q=rel2pairset' (op \<supseteq>) in (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q))) = D)" 
(is "?L=(?r=D)") proof- let ?Q="rel2pairset' (op \<supseteq>)" 
let ?l1="\<forall>x0\<in>Domain f. \<forall>x1\<in>Domain f. (x0, x1)\<in>D\<longrightarrow>f,,x0 \<supseteq> f,,x1" 
let ?l2="\<forall>x0\<in>Domain f. \<forall>x1\<in>Domain f. f,,x0 \<supseteq> f,,x1 \<longrightarrow> (x0, x1)\<in>D" have 
0: "isMonotone3 D ?Q f=isMonotone4 D ?Q f" using assms(1,2) l48i by blast have "?r\<subseteq>D \<longrightarrow> ?l2" 
using assms(1) l47o by metis moreover have "?r\<supseteq>D \<longrightarrow> isMonotone3 D ?Q f" using l48d by metis 
moreover have "...=?l1" unfolding 0 l48ee by simp  ultimately have 
1: "(?r=D) \<longrightarrow> ?L" by blast have "?L\<longrightarrow>isMonotone3 D ?Q f" using assms l48ee 0 by fast 
then have "?L\<longrightarrow>?r \<supseteq> D" using l48d by metis moreover have "?L\<longrightarrow>?r \<subseteq> D" using assms(1) l47o 
by metis ultimately show ?thesis using 1 by blast
qed

proposition l48k: "fixPts (Union((%(y_0,y_1). (f^-1``{y_0})\<times>(f^-1``{y_1}))`Q)) \<supseteq> f^-1``(fixPts Q)" 
unfolding fixPts_def by blast

proposition l48kk: assumes "fixPts' Q \<supseteq> Field Q" shows 
"Field (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q)) \<subseteq> f^-1``(fixPts Q)" using assms(1)
unfolding Field_def fixPts_def by fast

lemma l48l: assumes "trans Q" "runiq f" shows 
(*"trans (Union((%(y0,y1). (f^-1``{x})\<times>(f^-1``{y}))`Q))" also works *)
"trans (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q))" (is "trans ?p")
proof- let ?d=Domain
{
  fix x0 x1 x2 assume "(x0,x1)\<in>?p" then moreover have 
  1: "(f,,x0, f,,x1)\<in>Q" using assms(2) 
  RelationProperties.l31 by fastforce ultimately have "x0\<in>?d f & x1\<in>?d f" by fast moreover
  assume
  "(x1,x2)\<in>?p" then moreover have "(f,,x1, f,,x2)\<in>Q" using assms(2) RelationProperties.l31 by fastforce
  ultimately moreover have "(f,,x0, f,,x2)\<in>Q" using assms(1) 1 unfolding trans_def by meson
  ultimately  have "x0\<in>?d f & x2\<in>?d f & (f,,x0, f,,x2)\<in>Q" by fast 
  then moreover have "(x0,x2)\<in>(f^-1``{f,,x0})\<times>(f^-1``{f,,x2})" using assms(2) 
  using Image_singleton_iff converse.intros eval_runiq_rel mem_Sigma_iff by fastforce
  ultimately have "(x0,x2)\<in>?p" by blast
} thus ?thesis unfolding trans_def by blast
qed

lemma l49a: assumes "runiq f" "Field D \<subseteq> Domain f" 
"\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((x0, x1)\<in>D)=(f,,x0\<supseteq>f,,x1)))" shows "Field D \<subseteq> fixPts D & trans D"
proof- let ?Q="rel2pairset' (op \<supseteq>)" let ?D="Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`?Q)"
let ?DD="let Q=rel2pairset' (op \<supseteq>) in (Union((%(y0,y1). (f^-1``{y0})\<times>(f^-1``{y1}))`Q))" have 
"(\<forall>x0\<in>Domain f. (\<forall>x1\<in>Domain f. (((x0, x1)\<in>D)=(f,,x0 \<supseteq> f,,x1)))) = (?DD=D)" using assms(1,2) l48j 
by blast moreover have "?D=?DD" by presburger ultimately have "?D=D" using assms(3) by force 
moreover have "Field ?Q \<subseteq> fixPts' ?Q & trans ?Q" unfolding trans_def by auto then have 
"Field ?D\<subseteq>fixPts ?D & trans ?D"using assms l48k l48kk l48l by fast ultimately show ?thesis by fast 
qed

proposition l49b: assumes "(x0, x1)\<in>D" "(x0, y)\<in>U" "Field D \<subseteq> Domain f" "Range U \<subseteq> Domain f" 
"\<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. ((((x, y)\<in>D)\<longrightarrow>(f,,x \<supseteq> f,,y)) & (((x,y)\<in>U) = ((f,,x \<inter> f,,y)={}))))"
shows "(x1,y)\<in>U" proof- have "y\<in>Domain f & f,,x0 \<inter> f,,y={} & x1\<in>Domain f & f,,x0 \<supseteq> f,,x1" 
using assms(1-5) Domain.DomainI Field_def Range.intros UnCI contra_subsetD by (metis) then 
moreover have "f,,x1 \<inter> f,,y={}" by blast ultimately show ?thesis using assms(5) by meson qed

proposition l49c: "isMonotonicOver U D=(\<forall> x0 x1 y. (x0, x1)\<in>D & (x0, y)\<in>U \<longrightarrow> (x1, y)\<in>U)"
using assms by blast

proposition l49bb: assumes "Field D \<union> Range U \<subseteq> Domain f" 
"\<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. ((((x, y)\<in>D)\<longrightarrow>(f,,x \<supseteq> f,,y)) & (((x,y)\<in>U) = ((f,,x \<inter> f,,y)={}))))"
shows "isMonotonicOver U D"
proof- { fix x0 x1 y assume "(x0, x1)\<in>D" moreover assume"(x0, y)\<in>U" moreover have 
"Field D \<subseteq> Domain f" using assms(1) by auto moreover have "Range U \<subseteq> Domain f" using assms(1) 
by auto ultimately have "(x1,y)\<in>U" using assms(2) by (rule l49b)} then show ?thesis 
unfolding l49c by blast qed

theorem main1: assumes "runiq f" "Field D \<union> Field U \<subseteq> Domain f" "isRepresentation' f D U" shows 
"isPreorder D & isMonotonicOver U D & sym U & (luniq f \<longrightarrow> antisym D) & ({}\<notin>(Range f) \<longrightarrow> irrefl U)"
proof- let ?D=Domain let ?R=Range let ?F=Field have 
2: "?F D \<union> ?R U\<subseteq>?D f" using assms(2) unfolding Field_def by simp then have "isMonotonicOver U D" 
using assms(3) l49bb by (metis(no_types)) moreover have "Field D \<subseteq> fixPts D & trans D" 
using assms(1,2,3) l49a by fast ultimately have 1: "isPreorder' D & isMonotonicOver U D" 
using assms(2,3) by (meson Un_subset_iff l45e order_refl subsetCE) have 3: "luniq f \<longrightarrow> antisym D" 
using assms(1,2,3) Field_def Range.RangeI antisym antisym_def le_sup_iff lm36a luniq_def rev_subsetD
by (metis (no_types, lifting)) have 4: "{}\<notin>(Range f) \<longrightarrow> irrefl U" using assms(3) Int_absorb 
Range.intros Un_subset_iff assms(1) 2 eval_runiq_in_Range irreflI subsetCE by metis have "sym U" 
using assms(2,3) Domain.DomainI Field_def Int_commute Range.intros Un_subset_iff subsetCE symI 
by (metis) then show ?thesis using 1 3 4 by (simp add: isPreorder_def)
qed





















section{*Induction*}

proposition ll50a: assumes "f={}" "D={}" shows "isRepresentation f D U & Domain f=Field D & isInjection f & runiq f & {}\<notin>Range f"
unfolding assms(1,2) using isRepresentation_def Domain_empty Field_empty converse_empty empty_iff runiq_emptyrel 
by (metis Range_empty)

proposition (*l41w:*) assumes "(s,s)\<notin> U" "sym U" "isMonotonicOver U (D|^{s})" "reflex (D|^{s})" 
shows "U^-1``{s} \<inter> D^-1``{s}={}" using assms
proof- have "isMonotonicOver U (D|^{s})" using assms unfolding corestriction_def 
by blast moreover have "isConflictFree2 U {s}" using assms unfolding isConflictFree2_def  
by blast ultimately have 
0: "isConflictFree2 U (D^-1``{s})" using assms l41u by blast
{
assume "s\<in>Range D" then have "s\<in>D^-1``{s}" using assms unfolding corestriction_def using l45vv
Image_singleton_iff refl_onD refl_on_converse reflex_def
using Domain.DomainI Int_iff RangeE converse.intros mem_Sigma_iff refl_onD2 singletonI by fastforce then have
 ?thesis using 0 assms unfolding isConflictFree2_def sym_def by fast
} thus ?thesis by blast
qed

lemma l50: assumes "E={n|n. \<exists> D U. (finite D & card D=n & isLes D U & (\<forall>f::('a \<times> nat set)set. 
(Domain f=Field D & isInjection f & {}\<notin>Range f & finite ((Union o Range) f)) \<longrightarrow> \<not>isRepresentation f D U))}"
shows "E={}"
proof- 
  let ?i=isRepresentation let ?D=Domain let ?R=Range let ?F=Field 
  let ?freqs="\<lambda>f. isInjection f & {}\<notin>Range f & finite ((Union o Range) f)" let ?n="Inf E" 
  let ?E="{n|n. \<exists> D U. (finite D & card D=n & isLes D U & 
            (\<forall>f::('a \<times> nat set)set. (?D f=?F D & ?freqs f) \<longrightarrow> \<not>isRepresentation f D U))}"
  let ?n'="Inf ?E" have 
  0: "E=?E & 0\<notin>?E & ?n'=?n" unfolding assms using ll50a card_0_eq mem_Collect_eq by fastforce  
  { 
    assume "E\<noteq>{}" then have 
    1: "E\<noteq>{} & ?n\<in>E & ?n\<in>?E" using 0 Inf_nat_def LeastI2_wellorder ex_in_conv by metis then 
    obtain m where 
    7: "?n=m+1" using 0 add.commute add.left_neutral less_imp_add_positive less_one 
    linorder_neqE_nat by (metis) then have 
    2: "?n=m+1 & m\<notin>E" by (simp add: 1 l27b) obtain D U where  
    3: "finite D & card D=?n & isLes D U & (\<forall>f::('a\<times>nat set)set. (?D f=?F D & ?freqs f)\<longrightarrow>
                                            \<not>isRepresentation f D U)" using 1 by auto then have 
    6: "wf(strict (D^-1)) &  D^-1\<noteq>{}"using 0 1 antisym_converse card_0_eq finite_Diff mm05k (*only step needing antisymmetry?*)  
    finite_converse strict_def trans_converse converse_empty converse_inject by (metis(lifting)) then 
    obtain s where 
    4: "s\<in>maximals3 D & s\<in>Range D" using l42t unfolding maximals3_def by blast
    let ?d="D---s s" let ?u="U---s s" let ?j="card ?d" let ?sparents="?d^-1``(D^-1``{s})" 
    let ?sconfl="U^-1``{s}" let ?sconcurs="?R ?d-(?sparents \<union> ?sconfl)"
    obtain d u where 
    5: "d=?d & u=?u" by auto have "?d \<subset> D" using 4 unfolding singlebouthside_def by blast then have 
    8: "finite ?d & card ?d<?n" using 3 7 psubset_card_mono infinite_super psubset_eq by metis then have 
    9: "?j \<notin> E" by (metis equals0D l27b) have 
    10: "antisym ?d & isMonotonicOver ?u ?d" using 3 unfolding singlebouthside_def antisym_def by blast  
    have "?F D-{s} = fixPts D-{s}" using 3 unfolding l45g by blast moreover have 
    14: "...=fixPts ?d" unfolding l45b l45k by blast moreover have "?F D-{s}\<supseteq>?F ?d" unfolding 
    Field_def singlebouthside_def by blast ultimately have "?F ?d \<subseteq> fixPts ?d" by presburger then have 
    11: "reflex ?d" by (metis DomainE IntE fixPts_def l45e pair_in_Id_conv subset_eq) have 
    17: "trans ?d & irrefl ?u & sym ?u" using 3 unfolding trans_def sym_def irrefl_def 
    singlebouthside_def by blast then have "isLes ?d ?u" using 10 11 by fast then have
    "\<not>(\<forall>f::('a\<times>nat set)set. (?D f=?F ?d & ?freqs f)\<longrightarrow>\<not>isRepresentation f ?d ?u)" using 9 8 0 by blast
    then obtain f where 
    12: "?D (f::('a \<times> nat set)set)=?F ?d & ?freqs f & isRepresentation f ?d ?u" by auto
    have "D``{s}\<subseteq>{s}" using 4 l42uu unfolding Field_def by fast moreover have "{s}\<subseteq>D``{s}" using 3 4 
    unfolding reflex_def refl_on_def Field_def by blast ultimately have 
    13: "D``{s}={s} & luniq f" using 12 by (simp add: l45u psubsetE psubsetI)
    have "?D ?d = ?R ?d" using "11" l45vv by blast then have
    h3a: "finite ?sconcurs & ?sconcurs\<subseteq>fixPts D" using 11 8 14 Diff_subset finite_Diff les2.lm31 l45g 
    finite_Field reflex_def subset_trans by (smt) have
    h3e: "?d``?sconfl\<subseteq>?sconfl" using "3" \<open>D--- s s \<subset> D\<close> by blast have "?sparents \<supseteq> ?D ?d \<inter> D^-1``{s}" 
    using 11 Domain_converse Field_def ImageI Int_lower1 Int_lower2 converse_converse l45e l45vv(1) 
    les2.lm31 refl_on_converse reflex_def subsetCE subsetI by (smt) then have "D^-1``{s}-{s} \<subseteq> ?sparents" 
  by (smt "3" DiffD2 DiffI Diff_subset Image_singleton_iff IntI Range.intros Range_converse \<open>reflex (D--- s s)\<close> l45b l45g l45k les2.lm31 reflex_def subset_iff)
    moreover have "trans (D^-1)" by (simp add: "3") then
    moreover have "D^-1``(D^-1``{s}) \<subseteq> D^-1``{s}" by (simp add: l41d) then moreover have
    "?sparents \<subseteq>  D^-1``{s}-{s}" unfolding singlebouthside_def by blast ultimately have
    h3b: "?sparents = D^-1``{s}-{s}" by blast have
    h1: "isRepresentation f (D---s s) (U---s s)" using 12 by blast have 
    15: "?D D-{s} \<subseteq> ?D f" using "11" "12" "3" 14 l45g les2.lm31 reflex_def subsetI by (metis) have
    16: "?D f \<subseteq> ?R d" by (simp add: "11" "12" "5" l45vv(1) l45vv(2)) have
    h2: "runiq f & D``{s}={s} & sym U & (let dm=Domain in let R=Range in {}\<notin>R f & 
    finite ((Union o R) f) & (Domain D)-{s} \<subseteq> dm f &(let d=D---s s in dm f \<subseteq> Range d & trans d))" 
    using 3 12 13 15 16 "17" "5" by auto have "?sparents=D^-1``{s}-{s}" using h3b by blast have 
    h3c: "irrefl (U||(?sconcurs \<union> Domain f))" by (metis "3" Int_iff irrefl_def restrict_def) have 
    h3d: "?sconfl \<inter> D^-1``{s}={}" using "3" Image_singleton_iff Int_lower1 Int_lower2 all_not_in_conv 
    converse_iff irrefl_def subset_iff by (smt) have 
    h3f: "isMonotonicOver U (D|^(D^-1``{s} \<union> (Range d - ?sconfl)))" 
    by (simp add: "3" corestriction_def) have
    h3: "let d=D---s s in let sparents=d^-1``(D^-1``{s}) in let sconfl=U^-1``{s} in 
    let sconcurs=Range d-(sparents \<union> sconfl) in finite sconcurs & sconcurs\<subseteq>fixPts D & 
    sparents=D^-1``{s}-{s} & irrefl (U||(sconcurs \<union> Domain f)) & sconfl \<inter> D^-1``{s}={} & 
    d``sconfl\<subseteq>sconfl & isMonotonicOver U (D|^(D^-1``{s} \<union> (Range d - sconfl)))" using h3a h3b h3c h3d h3e h3f 
    by (simp add: "5") have "\<exists> l. let N=Max ((Union o Range) f)+1+size l in let d=Domain in let R=Range in 
    let RA=(Union o set)((map (Union o R) l)@[{N}]) in let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{{N}}]) in 
    let h=g+<(s,RA) in d g=d f & d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & 
    isRepresentation h D U & runiq g & runiq h & (Union o R) h \<subseteq> {0..<1+N} & (luniq f \<longrightarrow> (isInjection g & isInjection h))"
  using h1 h2 h3 by (rule l46) then obtain l where 
    20: "let N=Max ((Union o Range) f)+1+size l in let d=Domain in let R=Range in 
    let RA=(Union o set)((map (Union o R) l)@[{N}]) in let g=foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{{N}}]) in 
    let h=g+<(s,RA) in d g=d f & d h=d f\<union>{s} & {}\<notin>R g & {}\<notin>R h & isRepresentation g (D---s s) (U---s s) & 
    isRepresentation h D U & runiq g & runiq h & (Union o R) h \<subseteq> {0..<1+N} & (luniq f \<longrightarrow> (isInjection g & isInjection h))" by fast
    let ?N="Max ((Union o Range) f)+1+size l"  
    let ?RA="(Union o set)((map (Union o ?R) l)@[{?N}])" 
    let ?g="foldl pointUnion f (l@[(D^-1``{s}-{s})\<times>{{?N}}])" 
    let ?h="?g+<(s,?RA)" have
    21: "?D ?g=?D f & ?D ?h=?D f\<union>{s} & {}\<notin>?R ?g & {}\<notin>?R ?h & isRepresentation ?g ?d ?u & 
    isRepresentation ?h D U & runiq ?g & runiq ?h & (Union o ?R) ?h \<subseteq> {0..<1+?N} & 
    (luniq f\<longrightarrow>(isInjection ?g & isInjection ?h))" using 20 by metis then moreover have 
    "finite ((Union o ?R) ?h)" using finite_subset by blast ultimately have "?freqs ?h" using 13 
    by fast moreover have "?F D=?D f\<union>{s}" using 11 12 14 3 4 Diff_cancel Diff_partition l45vv(1,2)  
    l45g Diff_subset insert_subset sup_commute by (metis) ultimately have "False" using 3 21 by metis
  }
  then show ?thesis by blast
qed

theorem main2: assumes "finite D" "isLes D U" obtains f::"('a \<times> nat set)set" where  
"Domain f=Field D & isInjection f & {}\<notin>Range f & finite ((Union o Range) f) & isRepresentation f D U"
proof- let ?freqs="\<lambda>f. isInjection f & {}\<notin>Range f & finite ((Union o Range) f)"
let ?E="{n|n. \<exists> D U. (finite D & card D=n & isLes D U & 
          (\<forall>f::('a \<times> nat set)set. (Domain f=Field D & ?freqs f) \<longrightarrow> \<not>isRepresentation f D U))}"
have "card D \<notin> ?E" using assms l50 by (metis empty_iff) then have 
"\<not>(\<forall>f::('a \<times> nat set)set. (Domain f=Field D & ?freqs f) \<longrightarrow> \<not>isRepresentation f D U)" using assms by blast 
then obtain f::"('a \<times> nat set)set" where "Domain f=Field D & isInjection f & {}\<notin>Range f & finite ((Union o Range) f) & isRepresentation f D U"
by blast then show ?thesis using that by blast qed

abbreviation "isRepresent==isRepresentation" 

theorem representation: assumes "finite D" "Field U \<subseteq> Field D" shows "(isLes D U) =
(\<exists> f. isInjection f & Domain f = Field D & ({}::nat set)\<notin>Range f & finite ((Union o Range) f) & 
isRepresent f D U)" (is "?L=?R")
proof- let ?D=Domain let ?F=Field { assume "?R" then obtain f where
  0: "isInjection f & Domain f = Field D & {}\<notin>Range f & finite (((Union o Range) f)::nat set) & 
  isRepresentation' f D U" unfolding isRepresentation_def by blast 
  then moreover have "?F D \<union> ?F U\<subseteq>?D f" using assms by simp ultimately moreover have 
  "isPreorder D & isMonotonicOver U D & sym U & (luniq f \<longrightarrow> antisym D)&({}\<notin>(Range f) \<longrightarrow> irrefl U)"
  using main1 by blast ultimately have "?L" using 0 by (simp add: isPreorder_def luniq_def)
} moreover have "?L\<longrightarrow>?R" using assms(1) main2 by metis ultimately show ?thesis by satx
qed

(* WRONG nitpick counterex 
lemma assumes "runiq f" "{}\<notin>Range f" "Field U \<subseteq> Field D" "Field D=Domain f" "U\<inter>D={}" (*"sym U"*) 
"isRepresentation2' f D (concurrency D U)" shows "isRepresentation' f D U" nitpick
using assms 
Diff_cancel Diff_eq_empty_iff Diff_iff Domain.DomainI Field_converse Field_def 
Range_converse UnCI converse.cases converse_iff insert_iff mem_Sigma_iff 

Nitpicking formula... 
Nitpick found a counterexample for card 'a = 2 and card 'b = 2:

  Free variables:
    D = {(a\<^sub>1, a\<^sub>1), (a\<^sub>2, a\<^sub>1), (a\<^sub>2, a\<^sub>2)}
    U = {(a\<^sub>1, a\<^sub>2)}
    f = {(a\<^sub>1, {b\<^sub>1}), (a\<^sub>2, {b\<^sub>1, b\<^sub>2})}
  Skolem constants:
    ??.Ball.x = a\<^sub>1
    ??.Ball.x = a\<^sub>2

value "let D = {(1::nat, 1), (2,1), (2,2::nat)} in let 
    U = {(1::nat,2::nat)} in let
    f = {(1::nat, {1::int}), (2, {1, 2})} in let x= 1 in let y=2 in 
    (isRepresentation2' f D U)
    "
*)




(* Todo: weaken l24 and consequences; remove Seg, nodes; apostrophise isLes, les.minimals, 
isInjection, isMonotonicOver, propagation, concurrency and eval_rel2 ?*)















































section{*representation equivalent definitions, unused*}

lemma l01a: assumes "Domain f \<supseteq> X" "runiq f" "P``{x} = f^-1``(Pow(f,,x))" shows "(\<forall> y\<in>X. (((x, y)\<in>P) \<longleftrightarrow> (f,,x \<supseteq> f,,y)))" 
  using assms eval_runiq_rel RelationProperties.l31 Image_iff Image_singleton_iff Pow_iff converse_iff in_mono by smt

lemma l01b: assumes "Domain f \<supseteq> Field P" "runiq f" 
"\<forall> y\<in>Field P. (((x, y)\<in>P) \<longleftrightarrow> (f,,x \<supseteq> f,,y))" shows "P``{x} \<subseteq> f^-1``(Pow(f,,x))"
  using assms(1,2,3) Field_def Image_iff Image_singleton_iff Pow_iff Range.RangeI UnCI 
  contra_subsetD converse.intros eval_runiq_rel subsetI by smt

lemma l01c: assumes "Domain f \<subseteq> X" "runiq f" "\<forall> y\<in>X. (((x, y)\<in>P) \<longleftrightarrow> (f,,x \<supseteq> f,,y))" 
shows "P``{x} \<supseteq> f^-1``(Pow(f,,x))" 
  using assms Domain.DomainI Image_iff PowD converse.cases insertI1 RelationProperties.l31 subsetI by fastforce

lemma l01: assumes "Domain f = Field P" "runiq f" shows
"P``{x} = f^-1``(Pow(f,,x)) = (\<forall> y\<in>Field P. (((x, y)\<in>P) \<longleftrightarrow> (f,,x \<supseteq> f,,y)))" (is "?L = ?R")
  proof -
  have "?L \<longrightarrow> ?R" using l01a assms by (smt insert_subset mk_disjoint_insert subset_insertI)
  moreover have "?R \<longrightarrow> ?L" using assms l01b l01c by (metis equalityD2 subset_antisym)
  ultimately show ?thesis by meson
  qed

lemma l02a: assumes "Domain f \<subseteq> X" "runiq f" "\<forall> y\<in>X. (((x,y)\<in>P) = ((f,,x \<inter> f,,y)={}))" 
shows "P``{x} \<supseteq> f^-1``(Pow((Union (Range f))-(f,,x)))" 
  using assms(1,2,3) RelationProperties.l31 Image_iff Image_singleton_iff converse_iff 
  Diff_disjoint Domain.DomainI Int_commute Sup_inf_eq_bot_iff Union_Pow_eq subset_iff by (smt)

lemma l02b: assumes "Domain f \<supseteq> Field P" "runiq f" "\<forall> y\<in>Domain f. f,,y \<subseteq> Y"
"\<forall> y\<in>Field P. (((x,y)\<in>P) = ((f,,x \<inter> f,,y)={}))" shows "P``{x} \<subseteq> f^-1``(Pow(Y-(f,,x)))" 
  using assms RelationProperties.l31 DiffI Field_def ImageI Image_singleton_iff IntI Range.intros UnCI 
  converse.intros empty_iff eval_runiq_rel mem_Collect_eq subsetCE subsetI subset_iff Pow_def by (smt)

lemma l02c: assumes "Domain f \<supseteq> Field P" "runiq f" "\<forall> y\<in>Field P. (((x,y)\<in>P) = ((f,,x \<inter> f,,y)={}))" 
shows "P``{x} \<subseteq> f^-1``(Pow((Union (Range f))-(f,,x)))" 
  using assms l02b Union_upper eval_runiq_in_Range by metis

lemma l02d: assumes "Domain f \<supseteq> Field P" "runiq f" "\<forall> y\<in>Domain f. (((x,y)\<in>P) = ((f,,x \<inter> f,,y)={}))" 
shows "P``{x} = f^-1``(Pow((Union (Range f))-(f,,x)))" (is "?L=?R")
  proof -
  have "?L \<subseteq> ?R" using assms l02c by fast moreover have "?L \<supseteq> ?R" using assms l02a 
  by (metis subsetI) ultimately show ?thesis by blast
  qed

lemma l03a: assumes "Domain f \<supseteq> Field P" "runiq f" "\<forall> y\<in>Domain f. f,,y \<subseteq> Y" "y\<in> Domain f" 
"P``{x} = f^-1``(Pow(Y-(f,,x)))" shows "((f,,x \<inter> f,,y)={}) \<longrightarrow>(x,y)\<in>P" 
  using assms Diff_Diff_Int Diff_disjoint Diff_eq_empty_iff Domain.cases Int_Diff 
  Int_absorb Pow_iff SetUtils.lm00 converse.intros RelationProperties.l31 rev_ImageI by fastforce

lemma l03b: assumes "Domain f \<supseteq> Field P" "runiq f" "\<forall> y\<in>Domain f. f,,y \<subseteq> Y"
"P``{x} = f^-1``(Pow(Y-(f,,x)))" 
"y\<in>Domain f" shows "(x,y)\<in>P \<longrightarrow>(f,,x) \<inter> (f,,y)={}" 
  using assms ImageE converse.cases converseD Diff_disjoint Int_commute 
  SetUtils.lm00 Sup_inf_eq_bot_iff Union_Pow_eq RelationProperties.l31 by (smt)

lemma l03c: assumes "Domain f \<supseteq> Field P" "runiq f" "P``{x} = f^-1``(Pow((Union (Range f))-(f,,x)))" 
shows "\<forall>y\<in>Domain f. (((x,y)\<in>P) = ((f,,x) \<inter> (f,,y)={}))" 
  using assms l03a l03b Union_upper eval_runiq_in_Range by (metis (no_types, lifting))

lemma l03: assumes "Domain f \<supseteq> Field P" "runiq f" shows 
"(\<forall> y\<in>Domain f. (((x,y)\<in>P) = ((f,,x \<inter> f,,y)={})))=(P``{x} = f^-1``(Pow((Union (Range f))-(f,,x))))"
using assms l02d l03c by metis

lemma l10: assumes "Y\<in>(\<lambda>X. X \<union> A X)`XX" (is "_\<in> ?gg`_") "(Union (A`XX)) \<inter> Union XX={}" shows 
"{Y-(Union (A`XX))} = (?gg|||XX)^-1``{Y}" (is "?L=?R")
proof - let ?g="\<lambda>X. X \<union> A X" let ?G="?g|||XX" have 
7: "\<forall> X\<in>XX. \<forall> X'\<in>XX. X' - A X=X'" using assms(2) by blast have 
6: "?G=?gg|||XX" by fast have
4: "\<forall>X\<in>XX. ?G,,X=?g X" by (metis MiscTools.ll33) have
2: "Domain ?G=XX" unfolding graph_def by blast obtain X where 
0: "X\<in>XX & Y=?g X" using assms(1,2) by blast then moreover have "Y=X \<union> A X" by force then moreover have 
1: "X=Y-A X" by (simp add: 7 0 Diff_cancel Un_Diff Un_empty_right) ultimately
moreover have "X \<subseteq> Y-Union(A`XX)" using assms(2) by blast ultimately moreover have 
3: "X=Y-Union(A`XX)" by auto ultimately moreover have "Y=?G,,X" by (metis MiscTools.ll33)
ultimately moreover have "X\<in>(?gg|||XX)^-1``{Y}" using assms(1) IntI 
l06 singletonI vimageI2 by metis ultimately moreover have 
5: "?L \<subseteq> ?R" by fastforce  
{
  fix Z assume "Z\<in>?G^-1``{Y}" then moreover have "Z\<in>XX" using IntD1 l06 by (metis)
  ultimately moreover have "Y=?G,,Z" using Image_singleton_iff MiscTools.ll33 converse_iff 
  graph_def mem_Collect_eq prod.simps(1) by (smt) ultimately moreover have "Y=?g Z" using 4 by meson
  ultimately have "Z=Y-Union (A`XX)" using assms(2) singletonD sup_bot.comm_neutral by blast
}
then have "{Y-Union(A`XX)}\<supseteq>?G^-1``{Y}" by force then moreover have "...=?R" 
using 6 5 subset_antisym by auto then show ?thesis using 5 by blast
qed

lemma l10b: assumes "g=(\<lambda>X. X \<union> A X)" "Y\<in>g`XX" "(Union (A`XX)) \<inter> Union XX={}" shows 
"{Y-(Union (A`XX))} = (g|||XX)^-1``{Y}" using assms(2,3) unfolding assms(1) by (rule l10)



section{*Full Graphs*}

abbreviation "Overlap' X Y == (X \<inter> Y \<notin> {X, Y, {}})" definition "Overlap=Overlap'"
notation "Overlap" ("_ overlaps ")

proposition l52: "X overlaps Y = (let Z=X \<inter> Y in Z \<noteq> {} & Z \<subset> X & Z \<subset> Y)" unfolding Overlap_def 
by (smt Int_commute inf.strict_order_iff inf_right_idem insert_iff trivial_imp_no_distinct trivial_singleton)

proposition "sym (rel2pairset' Overlap')" unfolding sym_def by auto 

proposition "irrefl (rel2pairset' Overlap')" unfolding irrefl_def by auto

abbreviation "isFgRepr' f D T == \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((((x, y)\<in>D)=(f,,x \<supseteq> f,,y)) & (((x,y)\<in>T) = ((f,,x) overlaps (f,,y)))))" definition "isFgRepr=isFgRepr'"

abbreviation "unRel' D==(Field D \<times> Field D) - (D \<union> D^-1)" definition "unRel=unRel'"

abbreviation "fgReprsFor' D T == {f|f. Domain f = Field D & isFgRepr f D T}" definition "fgReprsFor=fgReprsFor'"
abbreviation "fgUndirectedsFor' D==fgReprsFor D-`(
(UNIV :: ('a \<times> 'b set) set set set)
-{{}})" 
definition "fgUndirectedsFor=fgUndirectedsFor'"
abbreviation "reprsFor' D U == {f|f. Domain f = Field D & isRepresentation f D U}" definition "reprsFor=reprsFor'"
abbreviation "undirectedsFor' D==reprsFor D -` (UNIV-{{}})" definition "undirectedsFor=undirectedsFor'"

abbreviation "fgUndirectedsFor2' D == {T|T. \<exists> f. Domain f=Field D & isFgRepr f D T}"

lemma l52b: assumes "runiq f" "{}\<notin>Range f" "Domain f \<subseteq> Field D" "isFgRepr f D T" 
shows "isRepresentation' f D (unRel' D - T)"
using assms(1,2,3,4) unfolding isFgRepr_def using Diff_iff Int_commute SigmaI Un_iff converse_iff  le_iff_inf subsetCE eval_runiq_in_Range
 inf.right_idem inf.strict_order_iff l52 by smt
(*proof- let ?d=Domain
{ fix x y let ?X="f,,x" let ?Y="f,,y" assume "x\<in>?d f" moreover assume "y\<in>?d f"
  ultimately moreover have 
  1: "(x,y)\<in>D = (?X \<supseteq> ?Y)" using assms(5) by simp ultimately have 
  0: "x\<in>?d f & y\<in>?d f & ?X\<noteq>{} & ?Y\<noteq>{}" 
  using assms(1,3) eval_runiq_in_Range by (metis) then
  have "(?X \<inter> ?Y={}) = (\<not>((?X \<supseteq> ?Y)\<or>(?Y \<supseteq> ?X)\<or>(?X overlaps ?Y)))" using l52 Overlap_def using assms 
  by (smt Int_commute Int_lower1 inf.strict_order_iff le_iff_inf) moreover have 
  "...=((x,y) \<notin> D \<union> D^-1 \<union> T)" using assms(5) 0 by auto
  ultimately have "(x,y)\<in>D = (?X \<supseteq> ?Y) & ((?X \<inter> ?Y={})=
  ((x,y) \<in> (unRel' D) - T)
  )" using 0 1 Diff_iff SigmaI Un_iff assms(4) subsetCE by auto
}
thus ?thesis by simp
qed
*)

lemma l52c: assumes "Domain f \<subseteq> Field D" "isRepresentation f D U" 
shows "isFgRepr f D (unRel' D - U)" unfolding isFgRepr_def using assms(1,2) unfolding isRepresentation_def using
Diff_iff Int_commute Overlap_def SigmaI Un_iff  converse_iff insertCI le_iff_inf subsetCE eval_runiq_in_Range
 inf.right_idem inf.strict_order_iff l52 
by smt
(*proof- let ?d=Domain
{ fix x y let ?X="f,,x" let ?Y="f,,y" assume "x\<in>?d f" moreover assume "y\<in>?d f"
  ultimately moreover have 
  1: "(x,y)\<in>D = (?X \<supseteq> ?Y)" using assms(5) by simp ultimately have 
  0: "x\<in>?d f & y\<in>?d f & ?X\<noteq>{} & ?Y\<noteq>{}" 
  using assms(1,3) eval_runiq_in_Range by (metis) 
}
thus ?thesis
using assms(4,5) Diff_iff Int_commute Overlap_def SigmaI Un_iff  converse_iff insertCI le_iff_inf subsetCE
 inf.right_idem inf.strict_order_iff l52
by (smt)
qed
*)

proposition l55: "inj_on (\<lambda>X. Y-X) (Pow Y)" unfolding inj_on_def by blast 

proposition l52a: assumes "finite (X \<union> Y)" "inj_on f X" "inj_on f Y" "f`X \<subseteq> Y" "f`Y \<subseteq> X" 
shows "f`X=Y & f`Y=X" using assms(1,2,3,4,5) card_image card_inj_on_le card_seteq finite_Un
by (metis)

lemma l53a: assumes "F=(\<lambda>R. (unRel' D - R))" shows "F`
{T|T. Field T \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isFgRepr f D T)} \<subseteq> 
{U|U. Field U \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isRepresentation f D U)}" (is "F`?L \<subseteq> ?R")
proof- let ?F="\<lambda>R. (unRel' D - R)" let ?fd=Field
{
  fix T::"('a \<times> 'a) set" assume 1: "T\<in>?L" then obtain f where 
  0: "isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isFgRepr f D T" by blast
  then have "isRepresentation f D (F T)" unfolding isRepresentation_def assms(1) using l52b by 
  (metis DiffE DiffI SigmaI subsetI) 
  moreover have "?fd (?F T) \<subseteq> ?fd D" unfolding Field_def  using 1 by blast
  ultimately have "F T\<in>?R" using assms 0 by blast
} thus ?thesis by (meson image_subsetI)
qed

lemma l53b: assumes "F=(\<lambda>R. (unRel' D - R))" shows "F`
{U|U. Field U \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isRepresentation f D U)}
\<subseteq>
{T|T. Field T \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isFgRepr f D T)}  
" (is "F`?L \<subseteq> ?R")
proof- let ?F="\<lambda>R. (unRel' D - R)" let ?fd=Field
{
  fix U::"('a \<times> 'a) set" assume "U\<in>?L" then moreover have 
  1: "?fd U \<subseteq> ?fd D" by blast ultimately obtain f where 
  0: "isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isRepresentation f D U" by blast
  then have "isFgRepr f D (F U)" unfolding assms(1) using l52c DiffE DiffI SigmaI subsetI by (metis Sigma_cong)
  moreover have "?fd (?F U) \<subseteq> ?fd D" unfolding Field_def  using 1 by blast
  ultimately have "F U\<in>?R" using assms 0 by blast
} thus ?thesis by (meson image_subsetI)
qed

lemma l54a: assumes "isRepresentation f D U" "({}::nat set)\<notin>Range f" "runiq f" "Domain f = Field D" shows 
"U \<inter> (D\<union>D^-1)={}" 
using assms(1,2,3) unfolding Range_def Field_def apply safe 
apply simp_all
using assms(1) unfolding isRepresentation_def using assms(2,4) Domain.DomainI Field_def Int_commute 
Range.intros Un_iff l37l le_iff_inf apply metis using assms(2,4) Domain.DomainI Field_def 
Range.intros Un_iff  eval_rel.simps eval_runiq_in_Range le_iff_inf by (metis)

corollary l54aa: assumes "isRepresentation f D U" "({}::nat set)\<notin>Range f" "runiq f" "Domain f = Field D" 
"Field U \<subseteq> Field D" shows "U \<subseteq> (Field D \<times> Field D)-(D\<union>D^-1)" 
using l54a assms unfolding Field_def by auto

lemma l54b: assumes "isFgRepr f D T"  "Domain f = Field D" shows 
"T \<inter> (D\<union>D^-1)={}" using assms(1) unfolding isFgRepr_def Range_def Field_def apply safe apply simp_all 
using assms(2) Domain.DomainI Field_def Int_commute Range.intros Un_iff inf.strict_order_iff l52 
le_iff_inf apply (metis ) using assms(2) Domain.DomainI Field_def Overlap_def Range.intros Un_iff 
insertCI le_iff_inf by (metis )

lemma l54bb: assumes "isFgRepr f D T"  "Domain f = Field D" "Field T \<subseteq> Field D" shows 
"T \<subseteq> (Field D \<times> Field D)-(D\<union>D^-1)" using l54b assms unfolding Field_def by auto

theorem bijection: assumes "F=(\<lambda>R. ((Field D \<times> Field D) - (D \<union> D^-1)- R))" "finite D"
"X={T|T. Field T \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isFgRepr f D T)}"
"Y={U|U. Field U \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isRepresent f D U)}"
shows "F`X=Y & F`Y=X & inj_on F X & inj_on F Y & card X=card Y"
proof- let ?i=inj_on let ?F="(\<lambda>R. ((Field D \<times> Field D) - (D \<union> D^-1)-R))"
let ?X="{T|T. Field T \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isFgRepr f D T)}"
let ?Y="{U|U. Field U \<subseteq> Field D & (\<exists> f. isInjection f & ({}::nat set)\<notin>Range f & Domain f=Field D & isRepresentation f D U)}"
have "X \<union> Y\<subseteq>Pow(Field D \<times> Field D)" unfolding assms(3,4) apply safe apply simp_all using Field_def 
contra_subsetD apply fastforce using FieldI2 contra_subsetD apply fastforce using Field_def 
contra_subsetD apply fastforce by (meson FieldI2 contra_subsetD) then have 
1: "finite (X \<union> Y)" using assms(2) by (meson finite_Field finite_Pow_iff finite_SigmaI finite_subset)
have "F`?X \<subseteq> ?Y" using assms(1) by (rule l53a[of F]) then have 
4: "F`X\<subseteq>Y" unfolding assms(3,4) by blast have "F`?Y \<subseteq> ?X" using assms(1) by (rule l53b[of F]) then have
5: "F`Y\<subseteq>X" unfolding assms(3,4) by blast
have "?X\<subseteq>Pow((Field D \<times> Field D)-(D\<union>D^-1))"
apply safe apply simp_all using Field_def contra_subsetD apply fastforce
using FieldI2 apply fastforce using l54b apply fastforce
using l54bb DiffD2 UnCI converseD converse_converse rev_subsetD by blast then have 
2: "?i F X" unfolding assms(1,3) using l55 by (metis (no_types, lifting) subset_inj_on) have 
"?Y\<subseteq>Pow((Field D \<times> Field D)-(D\<union>D^-1))" apply safe apply simp_all using Field_def contra_subsetD 
apply fastforce using FieldI2 apply fastforce using l54a apply fastforce using l54aa DiffD2 UnCI 
converseD converse_converse rev_subsetD by blast then have 
3: "?i F Y" unfolding assms(1,4) using l55 by (metis (no_types, lifting) subset_inj_on)
have "F`X=Y & F`Y=X" using 1 2 3 4 5 by (rule l52a) 
thus ?thesis using card_image 2 3 by fastforce
qed

thm_deps representation
thm_deps bijection

proposition
"{(D,U)| D U. Field D=E & Field U \<subseteq> Field D & (\<exists> f. isInjection f & Domain f = Field D 
& ({}::nat set)\<notin>Range f & finite ((Union o Range) f) & isRepresentation f D U)}=
Union {{D} \<times>
{U|U. (Field U \<subseteq> Field D & (\<exists> f. isInjection f & Domain f=Field D 
& ({}::nat set)\<notin>Range f &  finite ((Union o Range) f) & isRepresentation f D U))}
|D. Field D=E
}"
by auto


























section{*Unused*}

lemma l12: assumes "(Union (A`XX)) \<inter> Union XX={}" "g=(\<lambda>X. X \<union> A X)" shows
"(g|||XX)^-1``YY = (\<lambda>Y. Y-(Union (A`XX)))`(YY \<inter> (g`XX))" (is "?L=?R") 
proof -
let ?g="\<lambda>X. X \<union> A X" let ?G="?g|||XX" let ?h="\<lambda>Y. Y-(Union (A`XX))" let ?H="?h|||(?g`XX)"
have 
0: "Domain ?G=XX" unfolding graph_def by blast moreover have
1: "?g=g" using assms(2) by presburger 
{ 
 fix X assume "X\<in>?L" then obtain Y where "X\<in>XX & ?g X=Y & Y\<in>YY" using IntD1 IntD2 assms(2) vimageE 
 l06 by metis then moreover have "Y\<in>?g`XX" by fast ultimately moreover have"X\<in>{Y-(Union (A`XX))}" 
 using l10 assms(1) 0 1 by fastforce ultimately have "X\<in>?R" using "1" by blast 
} then have 
2: "?L \<subseteq> ?R" by auto
{
 fix X assume "X\<in>?R" then moreover obtain Y where "Y\<in>YY\<inter>(?g`XX) & X=?h Y" using 0 1 by blast
 ultimately moreover have "X\<in>{Y-(Union(A`XX))}" by fast ultimately moreover have "X\<in>?G^-1``{Y}" 
 using l10 using Int_iff assms(1) by auto ultimately have "X\<in>?L" using 0 1 ImageI Image_singleton_iff IntD1 by auto
} thus ?thesis using 2 by blast
qed

lemma assumes "runiq f" "runiq g" "\<forall>x \<in> f^-1``YY \<inter> Domain g. g,,x\<in>YY" shows "f^-1``YY \<subseteq> (f +* g)^-1``YY"
proof -
let ?h="f+*g" let ?l="f^-1``YY" let ?r="?h^-1``YY" have 
0: "?l \<subseteq> Domain f \<union> Domain g" using paste_def by blast
{
  fix x assume "x\<in>?l \<inter> Domain g" then moreover have "f,,x\<in>YY" using assms(1) ImageE converse.cases RelationProperties.l31 by fastforce
  ultimately moreover have "g,,x\<in>YY" using assms(3) by blast ultimately have "x\<in>?r" using 
  assms(2) ImageI IntD2 Un_iff converse.intros eval_runiq_rel paste_def by fastforce
} then have 
1: "?l \<inter> Domain g  \<subseteq> ?r" by fast
{ 
  fix x assume "x\<in>?l - Domain g" then have "x\<in>Domain f - Domain g & f,,x\<in>YY" using 
  assms(1,3) 0 DiffD1 DiffD2 ImageI Un_iff eval_runiq_rel ll71 subsetCE by fastforce then moreover 
  have "?h,,x = f,,x" using 0 by (simp add: MiscTools.lm19b) ultimately have "x\<in>?r" using 
  assms(1,2) DiffD1 Image_iff Un_iff converse_iff eval_runiq_rel paste_Domain runiq_paste2 by metis
  }
then have "?l - Domain g \<subseteq> ?r" by blast then show ?thesis using 1 by blast
qed

lemma assumes "U=Union o Range" "h=f +* (((\<lambda>y. y\<union>A y)o(f,,))|||C)" "Union (A`(f``C)) \<inter> Union(f``C)={}" 
"runiq f" "C \<subseteq> Domain f" shows "h^-1``(Pow(U h - (h,,x))) \<subseteq> f^-1``(Pow(U f - (f,,x)))" (is "?L \<subseteq> ?R")
proof -
let ?f="f,," let ?g="\<lambda>y. y \<union> A y" let ?h="f +* ((?g o ?f)|||C)" let ?G="?g|||(f``C)"
let ?i="(\<lambda>Y. Y - (Union (A`(f``C))))" let ?XX="f``C" let ?U="Union o Range" have  
3: "U=?U" using assms(1) by blast moreover have 
4: "f,,x \<subseteq> (?h,,x)" using assms(5) l15 by force ultimately moreover have 
9: "f^-1``(Pow(U ?h - (?h,,x))) \<subseteq> f^-1``(Pow(U f - (f,,x)))" using 3 by fastforce
moreover have "(\<lambda>x. x \<inter> ?U f)`Pow(?U ?h - ?h,,x)\<subseteq>(\<lambda>x. x \<inter> ?U f)`Pow(?U ?h - f,,x)" using 4 by auto
moreover have "... \<subseteq>Pow(?U f - f,,x)" by blast ultimately have 
2: "(\<lambda>x. x \<inter> ?U f)`Pow(?U ?h - ?h,,x) \<subseteq> Pow(?U f - f,,x)" by simp have 
"Union (A`?XX) \<inter> Union(?XX)={}" using assms(3) by blast moreover have "?g=?g" by simp
ultimately have  "?G^-1``(Pow (U ?h - (?h,,x))) = ?i`(Pow(U ?h - (?h,,x))\<inter>(?g`(?XX)))" by (rule l12)
moreover have "... \<subseteq> ?i`(Pow(U ?h - (?h,,x)))" by blast moreover have 
"...\<subseteq> Pow(U ?h - (?h,,x))" by blast ultimately have 
1: "?G^-1``(Pow (U ?h - (?h,,x)))\<subseteq>Pow(U ?h - (?h,,x))" by blast 
(*then have "f^-1``(?G^-1``(Pow (U ?h - (?h,,x)))) \<subseteq>
f^-1``(Pow(U ?h - (?h,,x)))" by blast moreover have "... = f^-1``((\<lambda>x. x \<inter> ?U f)`Pow(U ?h - (?h,,x)))" by auto
ultimately have 
8: "f^-1``(?G^-1``(Pow (U ?h - (?h,,x)))) \<subseteq> f^-1``(Pow(?U f - f,,x))" using 2 
by (smt Image_iff PowI UnionI Union_Pow_eq assms(1) subsetI)*) have 
7: "?L \<subseteq> f^-1``(Pow (U ?h - (?h,,x))) \<union> (((?g o ?f)|||C)^-1``(Pow (U ?h - (?h,,x))))"
using assms(2) paste_sub_Un Un_Image converse_Un subset_Un_eq by smt
have "(?g o ?f)|||C = (f||C) O ?G" using assms(4,5) MiscTools.ll37 
MiscTools.lm05 fullGraph.l14 fullGraph.l16 l11 lll85c by (metis (no_types, lifting))
then have "((?g o ?f)|||C)^-1=?G^-1 O (f||C)^-1" by blast then have 
"?L \<subseteq> f^-1``(Pow (U ?h - (?h,,x))) \<union> (?G^-1 O (f||C)^-1)``(Pow (U ?h - (?h,,x)))" using 7 by auto
moreover have "(?G^-1 O (f||C)^-1)``(Pow (U ?h - (?h,,x))) = (f||C)^-1``(?G^-1``(Pow (U ?h - (?h,,x))))" by blast
ultimately have "?L \<subseteq> f^-1``(Pow (U ?h - (?h,,x))) \<union> (f||C)^-1``(?G^-1``(Pow (U ?h - (?h,,x))))" by simp 
then show ?thesis using 9 1 Image_mono Un_absorb2 contra_subsetD converse_mono restriction_is_subrel subsetI
by (smt)
qed

lemma l04: assumes "\<forall>Y\<in>XX. Y \<inter> A X={}" "u=(\<lambda>X. X \<union> A X)|||XX" shows 
"u^-1``(Pow(X \<union> A X)) \<subseteq> Pow(X)" 
  proof -
  {fix y assume "y\<in>u^-1``(Pow(A X \<union> X))" then moreover have "y\<in>XX" using assms(2) 
  unfolding graph_def by blast moreover have "runiq u" by (simp add: MiscTools.ll37 assms(2))
  ultimately moreover have "u,,y \<subseteq> (A X \<union> X)" using assms(2) using eval_runiq_rel ll71 by fastforce
  ultimately moreover have "y \<union> A y \<subseteq> X \<union> A X" using assms(2) unfolding graph_def by blast 
  (*moreover have "A y \<inter> X={}" using assms sorry*) 
  moreover have "A X \<inter> y={}" using assms(1) by (simp add: calculation(2) inf.commute)
  ultimately have "y \<subseteq> X" by blast} thus ?thesis by auto
  qed

lemma assumes "x\<in>C" "x\<in>Domain f" "runiq f" "h=f +* ((f||C) O (graph (\<lambda>x. x \<union> A x) (f``C)))" 
"\<forall> y\<in>Range f. y \<inter> A (f,,x)={}" "f^-1``(Pow(f,,x)) \<subseteq>  P``{x}" shows 
"h^-1``(Pow(h,,x)) \<subseteq> P``{x}" (is "?L \<subseteq> ?R")
  proof -
  let ?g="graph (\<lambda>x. x \<union> A x)  UNIV" let ?g'="graph (\<lambda>x. x \<union> A x) (f``C)"
  let ?gg="(f||C) O ?g" let ?gg'="(f||C) O ?g'" let ?h="f +* ?gg" let ?h'="f +* ?gg'" 
  have "?gg = (f||C) O ?g'" unfolding restrict_def graph_def by blast then have 
  4: "?h=?h'" by presburger
  have "x\<in>Domain (f||C)" 
  using assms(1,2) restrict_def by (simp add: IntI RelationProperties.ll41) then have 
  0: "x\<in>Domain ?gg" unfolding graph_def by auto moreover have "runiq ?g" using graph_def 
  by (simp add: MiscTools.ll37) ultimately moreover have 
  1: "runiq ?gg" using assms(3) relcomp.cases restriction_is_subrel runiq_basic subrel_runiq by smt 
  ultimately moreover have "runiq ?h" by (simp add: assms(3,4) runiq_paste2) ultimately have 
  "?h,,x=?gg,,x" using assms(4) sup_commute MiscTools.ll50 inf.absorb_iff2 Int_iff eval_runiq_rel 
  inf_sup_absorb RelationProperties.l31 paste_def by (smt) then have "?L = ?h^-1``(Pow((f,,x) \<union> A (f,,x)))" using 
  assms(3) graph_def eval_runiq_rel RelationProperties.l31 mem_Collect_eq prod.simps(1) relcompE restrict_def IntE 0 1 
  by (smt "4" assms(4))
  moreover have "... = ?h'^-1``(Pow((f,,x) \<union> A (f,,x)))" using 4 assms(4) by presburger
  moreover have "... \<subseteq> f^-1``(Pow((f,,x) \<union> A (f,,x))) \<union> ?gg'^-1``(Pow((f,,x) \<union> A (f,,x)))" 
  using Image_mono PowD Pow_top Un_Image assms(4) converse_Un converse_mono paste_sub_Un
  by (metis (no_types, lifting)) moreover have "...=
  f^-1``(Pow((f,,x) \<union> A (f,,x))) \<union> (?g'^-1 O (f||C)^-1)``(Pow((f,,x) \<union> A (f,,x)))" by blast 
  moreover have "...=f^-1``(Pow((f,,x) \<union> A (f,,x))) \<union> 
  ((f||C)^-1)``(?g'^-1``(Pow((f,,x) \<union> A (f,,x))))" by auto ultimately have 
  2: "?L\<subseteq>f^-1``(Pow((f,,x) \<union> A (f,,x)))\<union>((f||C)^-1)``(?g'^-1``(Pow((f,,x)\<union>A (f,,x))))" by auto have 
  3: "f^-1``(Pow((f,,x) \<union> A (f,,x))) = f^-1``(Pow(f,,x))" using assms(5) by fast have 
  "\<forall> Y\<in>f``C. Y \<inter> A (f,,x)={}" using assms(5) by blast moreover have "?g'=graph (\<lambda>X. X \<union> A X) (f``C)" 
  by simp ultimately have "?g'^-1``(Pow((f,,x) \<union> A (f,,x))) \<subseteq> Pow(f,,x)" by (rule l04) then have
  "((f||C)^-1)``(?g'^-1``(Pow((f,,x) \<union> A (f,,x)))) \<subseteq> f^-1``(Pow(f,,x))" unfolding restrict_def by auto
  then have "?L \<subseteq> f^-1``(Pow(f,,x))" using 2 3 by blast then show ?thesis using assms(6) by auto
  qed

lemma assumes "x \<notin> C" "h= f +* ((f||C) O (graph (\<lambda> s. s \<union> A) UNIV))" "\<forall>y \<subseteq> f,,x. A-y\<noteq>{}" 
"f^-1``(Pow(f,,x)) \<subseteq> P``{x}" shows "h^-1``(Pow(h,,x)) \<subseteq> P``{x}" (is "?L \<subseteq> ?R")
  proof - 
  let ?g="graph (\<lambda> s. s \<union> A) UNIV" let ?gg="(f||C) O ?g" have "\<forall>y\<in>Range ?gg. y\<supseteq>A" unfolding 
  graph_def by fast moreover have "\<forall>y\<in>Pow(f,,x). A-y \<noteq> {}" using assms(3) by blast ultimately 
  moreover have "Pow(f,,x) \<inter> Range ?gg={}" by force ultimately have 
  0: "?gg^-1``(Pow(f,,x))={}" by force have "x \<notin> Domain ((f||C) O ?g)" using assms(1)
  restrict_def RelationProperties.ll41 by auto moreover have "h=f +* (?gg)" 
  using assms(2) by blast ultimately moreover have "h,,x=f,,x" using paste_def 
  by (simp add: MiscTools.lm19b eval_rel.simps) then moreover have "?L=h^-1``(Pow(f,,x))" 
  by simp ultimately moreover have "... \<subseteq> f^-1``(Pow(f,,x)) \<union> ?gg^-1``(Pow(f,,x))" 
  using paste_sub_Un by fastforce ultimately show ?thesis using 0 assms(4) by auto
  qed

lemma assumes "x\<notin>C" "h= f +* ((f||C) O (graph (\<lambda> s. s \<union> A) UNIV))" "f^-1``(Pow(f,,x)) \<inter> C={}"  
"f^-1``(Pow(f,,x)) \<supseteq> P``{x}" shows "h^-1``(Pow(h,,x)) \<supseteq> P``{x}" (is "?L \<subseteq> ?R")
  proof -
  let ?g="graph (\<lambda> s. s \<union> A) UNIV" let ?gg="(f||C) O ?g" have 
  0: "Domain ?gg \<subseteq> C" using RelationProperties.ll41 by fastforce then 
  have "f^-1``(Pow(f,,x)) \<inter> Domain ?gg={}" using assms(3) by fast then have
  "h^-1``(Pow(f,,x)) \<supseteq> f^-1``(Pow(f,,x))" using l00 assms(2) by metis moreover have "h,,x=f,,x" using 
  assms(1,2) 0 Diff_disjoint Diff_insert_absorb MiscTools.lm19b contra_subsetD eval_rel.simps
  by (metis (no_types, lifting)) ultimately show ?thesis using assms(4) by auto
  qed

lemma assumes "x\<in>C" "runiq f" "h=f +* graph (\<lambda>xx. f,,xx \<union> A xx) C" "\<forall>y\<in>f^-1``(Pow(f,,x)). A y \<subseteq> A x" 
shows "f^-1``(Pow(f,,x)) \<subseteq> h^-1``(Pow(h,,x))" (is "?L \<subseteq> ?R")
proof -
let ?g="\<lambda>xx. f,,xx \<union> A xx" let ?G="graph ?g C" let ?H="f +* ?G" have 
5: "x\<in>Domain ?G" using assms(1) unfolding graph_def by blast have 
0: "h=?H" using assms(3) by blast have 
1: "runiq ?H" by (simp add: MiscTools.ll37 assms(2) runiq_paste2) have "?H,,x = ?G,,x" using 0 1 5  
by (metis (no_types, lifting) Un_iff eval_runiq_rel paste_Domain paste_def runiq_basic) moreover have
"... = f,,x \<union> A x" using 0 1 5 assms(1) by (meson MiscTools.ll33) ultimately have 
6: "?H,,x=f,,x \<union> A x" by blast
{ 
  fix y assume 
  3: "y\<in>?L" then moreover have 
  4: "y\<in>Domain f & f,,y \<subseteq> f,,x" using assms(2) Domain.DomainI ImageE converse_iff PowD RelationProperties.l31 by fastforce
  ultimately have "?H,,y \<subseteq> f,,y \<union> A y" using assms(2) 0 1 
  Domain.DomainI MiscTools.ll33 MiscTools.ll37 Un_iff eval_runiq_rel RelationProperties.l31 paste_sub_Un subset_eq
by (smt paste_Domain) moreover
  have "f,,y \<union> A y \<subseteq> f,,x \<union> A x" using 3 4 assms(4) by blast ultimately have "?H,,y \<subseteq> f,,x \<union> A x" by blast
  then have "?H,,y \<subseteq> ?H,,x" using 6 by blast then have "y\<in>?R" using 0 1 4 Image_iff PowI Un_iff 
  converse.intros eval_runiq_rel paste_Domain by (metis (no_types, lifting) )
} thus ?thesis by blast
qed

lemma assumes (*"x\<notin>C"*) "h=f+*((\<lambda>xx. f,,xx \<union> A xx)|||C)" "f^-1``(Pow(f,,x)) \<inter> C={}"
(* this assumption will be equivalent to C downward-closed as soon as x\<notin>C*)
shows "f^-1``(Pow(f,,x)) \<subseteq> h^-1``(Pow(f,,x))"
proof - let ?g="\<lambda>x. f,,x \<union> A x" let ?G="?g|||C" let ?H="f +* ?G" have 
"f^-1``(Pow(f,,x)) \<inter> Domain ?G={}" using assms(2) by (simp add:MiscTools.ll37) then have 
"?H^-1``(Pow(f,,x))\<supseteq>f^-1``(Pow(f,,x))" by(metis(no_types)l00) thus ?thesis using assms(1) by fast
qed

lemma assumes "x \<notin> C" "h= f +* graph (\<lambda> x. f,,x \<union> A x) C" "\<forall>xx\<in>C. A xx - (f,,x) \<noteq>{}" shows 
"h^-1``(Pow(h,,x)) \<subseteq> f^-1``(Pow(f,,x))" (is "?L \<subseteq> ?R")
proof -
let ?g="\<lambda>x. f,,x \<union> A x" let ?G="graph ?g C" let ?H="f +* ?G" have 
0: "Domain ?G=C & x \<notin> Domain ?G" unfolding graph_def using assms(1) by blast then have 
"?H,,x = f,,x" using MiscTools.lm19b eval_rel.simps inf_bot_left insert_disjoint(2)
by (metis (no_types, lifting)) then have "?L=?H^-1``(Pow(f,,x))" using assms(2) by presburger
moreover have "... \<subseteq> f^-1``(Pow(f,,x)) \<union> ?G^-1``(Pow(f,,x))" using paste_sub_Un by fastforce
moreover have "\<forall>xx\<in>Domain ?G. ?G,,xx \<notin> Pow(f,,x)" by (metis (no_types, lifting) PowD 0 assms(3) 
Diff_eq_empty_iff MiscTools.ll33 le_sup_iff) ultimately moreover have "?G^-1``(Pow(f,,x))={}" 
using MiscTools.ll37 disjoint_iff_not_equal l08 vimageE by (smt) ultimately show ?thesis by auto
qed

lemma assumes "x\<in>C" "C\<subseteq>Domain f" "runiq f" "h=f+*(graph (\<lambda>x. f,,x\<union>A x) C)"(is "_=_+* graph ?g _") 
"\<forall> y\<in>Range f. y \<inter> A x={}" shows "h^-1``(Pow(h,,x)) \<subseteq> f^-1``(Pow(f,,x))" (is "?L \<subseteq> ?R")
proof - let ?G="graph ?g C" let ?h="f+*?G" 
have "\<forall>x\<in>C. ?G,,x=f,,x \<union> A x" using MiscTools.ll33 by (metis (no_types, lifting)) moreover have 
"Domain ?G=C" unfolding graph_def by blast then moreover have "?h,,x=?G,,x" using assms(1) 
paste_def Diff_disjoint Int_insert_right_if1 MiscTools.ll50 eval_rel.simps by smt ultimately have 
"?L = ?h^-1``(Pow((f,,x) \<union> A x))" using assms(1,4) by simp  moreover have 
"... \<subseteq> f^-1``(Pow((f,,x) \<union> A x)) \<union> ?G^-1``(Pow((f,,x) \<union> A x))" using Image_mono PowD Pow_top 
Un_Image converse_Un converse_mono paste_sub_Un by (metis (no_types, lifting)) ultimately have 
2: "?L \<subseteq> f^-1``(Pow((f,,x) \<union> A x)) \<union> (?G^-1``(Pow((f,,x) \<union> A x)))" by auto have 
3: "f^-1``(Pow((f,,x) \<union> A x)) = f^-1``(Pow(f,,x))" using assms(5) by fast have 
"\<forall>Y\<in>C. f,,Y \<inter> A x={}" using assms(2,3,5) by (meson Range.intros contra_subsetD eval_runiq_rel) 
moreover have "?G^-1``(Pow((f,,x) \<union> A x))=C \<inter> ?g-`(Pow((f,,x) \<union> A x))" by (simp add: l06)
ultimately moreover have "... \<subseteq> C \<inter> (eval_rel f)-`(Pow (f,,x))" using l07 by fast moreover have 
"...=C\<inter>f^-1``(Pow(f,,x))" using assms(1,2,3) l08 inf.orderE inf_assoc by (metis(no_types)) 
ultimately have "?L \<subseteq> f^-1``(Pow(f,,x))" using 2 3 Int_left_commute Un_Int_distrib2 Un_absorb 
inf.absorb_iff2 inf_sup_absorb sup_commute by smt thus ?thesis by simp
qed

lemma "drop 1 l=rev (take (size l - 1) (rev l))" by (metis drop_rev length_rev rev_rev_ident)

lemma "l=take n l@(drop n l)" by force

corollary assumes "F=(\<lambda>f A. f +< ((\<lambda>x. f,,,x \<union> A,,,x)|||(Domain A)))" (is "F=?F")
"\<forall>A\<in>set l. Domain A\<subseteq>Domain f" shows "Domain (foldl F f l)\<subseteq>Domain f" using assms l27ee 
[of conj "\<lambda>x. Domain x \<subseteq> Domain f" f F] by (simp add: MiscTools.ll37 Un_subset_iff order_refl paste_Domain)

lemma assumes "i<size l" "j<size l" "i\<noteq>j" "distinct l" shows "l!i \<noteq> l!j" using assms 
using ll24 by blast

lemma "f,,,x\<subseteq>(Union o Range)f" by auto

lemma assumes "isDownClosed' D (D^-1``{s})" shows "isDownClosed' (D--s) (D^-1``{s}-{s})"
using assms unfolding Outside_def by blast

proposition "(Union o Range) (X\<times>{y}) \<subseteq> y" by auto

proposition "UNIV\<noteq>{}" by simp
lemma "(P |~ X Y)=biRestr'' P X Y" unfolding biRestr_def by fast

proposition assumes "XX\<noteq>{}" shows "Inter XX \<subseteq> Union XX" using assms by blast

proposition assumes "luniq P" "luniq Q" "Range P \<inter> Range Q={}" shows "luniq (P\<union>Q)" using assms by (simp add: lll77 luniq_def)
proposition assumes "trivial P" shows "luniq' P" using assms 
using RelationProperties.lm022 lm004 by blast


lemma assumes "CC\<subseteq>F" "F-(D\<union>U)\<subseteq>CC" "U\<inter>CC={}" "D\<subseteq>CC" shows "CC=F-U" using assms(1,2,3,4) by auto

lemma assumes "CC\<subseteq>F-U" shows "U\<inter>CC={}" using assms(1) by blast
lemma assumes "CC\<subseteq>F" "CC\<supseteq>F-U" "D\<inter>U={}" shows "F-(D\<union>U)\<subseteq>CC" using assms(1,2,3) by blast
lemma assumes "CC\<subseteq>F" "CC\<supseteq>F-U" "D\<inter>U={}" "D\<subseteq>F" shows "D\<subseteq>CC" using assms(1,2,3,4) by blast

(*proposition "constList m l = map (\<lambda>(x,y). x\<times>{{y}}) (zip l [m..<m+size l])" using l37t l37s 
map_zip_map2 map_zip_map sorry*)

proposition assumes "C\<subseteq>A" shows "(A-B)\<union>C = A-(B-C)" using assms by blast
proposition  "(A-B)\<union>C = A-(B-C) \<union> C" by fast
proposition "irrefl (P||A) & irrefl (P||B) \<longleftrightarrow> irrefl (P||(A \<union> B))" unfolding irrefl_def restrict_def by blast

proposition assumes "antisym P" shows "antisym (P-Q)" 
unfolding bouthside_def using assms unfolding antisym_def by blast

proposition assumes "trans P" shows "trans (P\\X Y)" 
unfolding bouthside_def using assms unfolding trans_def by blast

proposition assumes "reflex P" shows "reflex (P\\X X)" 
using assms reflex_def refl_on_def l45i mm01g by (metis)

proposition assumes "isMonotonicOver U D" shows "isMonotonicOver (U\\X X) (D\\X X)"
unfolding singlebouthside_def using assms by (simp add: l45c)

proposition assumes "isMonotonicOver U D" shows "isMonotonicOver (U---s s) (D---s s)"
unfolding singlebouthside_def using assms by blast

proposition "list2Pair2' o pair2List'=id" using l47b l47c by fastforce

proposition "isMonotone' (Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q)) Q f" by fast

proposition assumes "isMonotone' P Q f" shows 
"isMonotone' (P\<union>(Union((%(x,y). (f-`{x})\<times>(f-`{y}))`Q))) Q f" using assms by fast

proposition "fixPts P \<subseteq> Field P" by (simp add: l45f(2) mono_Field)

proposition "concurrency D (concurrency D U)\<subseteq>U"  by blast

proposition assumes "Field U \<subseteq> Field D" "sym U" "U\<inter>D={}" shows "concurrency D (concurrency D U)\<supseteq>U" 
using assms(1,2,3) unfolding sym_def Field_def by auto

(*lemma assumes "runiq f" "{}\<notin>Range f" "Field U \<subseteq> Field D" "Field D=Domain f" "U\<inter>D={}" "sym U" 
"isRepresentation2' f D (concurrency D U)" shows "isRepresentation' f D U" 
using assms 
Diff_cancel Diff_eq_empty_iff Diff_iff Domain.DomainI Field_converse Field_def 
Range_converse UnCI converse.cases converse_iff insert_iff mem_Sigma_iff sorry*)

proposition "X\<in>finPow' Y = (X\<subseteq>Y & finite X & X\<noteq>{})" using insert_Diff by fastforce
proposition l19a: "(P O P \<subseteq> P)=trans P" by (meson relcomp.relcompI subsetCE transI trans_O_subset)

proposition "sym P=(P^-1 \<subseteq> P)" unfolding sym_def by blast
proposition "irrefl P=(P \<inter> Id={})" unfolding irrefl_def by blast
proposition "antisym P=(P \<inter> P^-1\<subseteq>Id)" unfolding antisym_def by blast

proposition "S``(R``X)=(R O S)``X" by auto


lemma assumes "sym U" shows "sym (concurrency D U)" using assms unfolding sym_def by fast

abbreviation "isRepresentation2' f D U == \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((((x, y)\<in>D)=(f,,x \<supseteq> f,,y)) & (((x,y)\<in>U) = ({}\<notin>{f,,x\<inter>f,,y, f,,x-f,,y, f,,y-f,,x}))))"

lemma assumes "{}\<notin>Range f" "isRepresentation' f D U" shows "isRepresentation2' f D (concurrency D U)"
using assms Diff_cancel Diff_eq_empty_iff Diff_iff Domain.DomainI Field_converse Field_def 
Range_converse UnCI converse.cases converse_iff insert_iff mem_Sigma_iff by (smt)

lemma assumes (*"finite B"*) "(B::(nat set))\<noteq>{}" shows "Inf B\<in>B" using assms by (metis Inf_nat_def LeastI2_wellorder ex_in_conv)

lemma assumes "x\<notin>Domain P" shows "P,,,x={}" using assms by blast

lemma l41p: assumes "(x,x)\<in>D-U" "isMonotonicOver U (D||D^-1``{x})" "sym (U||(D^-1``{x})) \<or> sym U" 
(*"\<exists> X\<subseteq>Field U. D^-1``{x} \<subseteq> X & sym (U||X)": better candidate hypothesis*)
shows "isConflictFree2' U (D^-1``{x})" proof-let ?p="D^-1``{x}" let ?V="U||?p"
{assume "~ isConflictFree2' U ?p" then obtain y z where 
  0: "y\<in>?p & z\<in>?p & z\<in>U``{y}" by blast then moreover have "U``{y} \<subseteq> U``{x}" using assms(2) 
  unfolding restrict_def Outside_def by fast 
  ultimately moreover have "z\<in>?V``{x}" using assms(1) 0 unfolding  restrict_def Outside_def by fast 
  ultimately have "x\<in>?V``{z}" using assms(3) unfolding restrict_def Outside_def sym_def by blast  
  then have "x\<in>U``{z}" unfolding restrict_def Outside_def by fast moreover have "U``{z} \<subseteq> U``{x}" 
  using 0 assms(2) Image_singleton_iff converseD unfolding restrict_def Outside_def by blast
  ultimately have "x\<in>U``{x}" by blast
} thus ?thesis using assms(1) by fast
qed

lemma (*l41q:*) assumes "(s,m)\<notin>U" "isMonotonicOver U (D||(D^-1``{m,s}))" "sym U" "(m,m)\<in>D-U" "(s,s)\<in>D-U" 
shows "isConflictFree2 U (D^-1``{m}\<union>D^-1``{s})"
proof-let ?cf'="isConflictFree2' U"let ?E="D^-1"let ?X="?E``{m}"let ?Y="?E``{s}"let ?Z="?X\<union>?Y" have 
2: "isMonotonicOver U (D||?X)" using assms(2) unfolding restrict_def Outside_def by blast  have 
3: "isMonotonicOver U (D||?Y)" using assms(2) unfolding restrict_def Outside_def by blast have 
0: "?cf' ?X" using l41p assms(1,3,4,5) 2 by metis have
1: "?cf' ?Y" using l41p assms(2,3,5) using l41p assms(1,3,4,5) 3 by metis { assume "\<not>?thesis" then obtain x y where 
  "x\<in>?X & y\<in>?Y & (x,y)\<in>U" unfolding isConflictFree2_def using assms(1,2,3,4,5) 0 1 DiffD2 SigmaE 
  UnE disjoint_iff_not_equal lm32h mem_Sigma_iff symE by (smt) then moreover have "(m,y)\<in>U" using assms 
by (smt "2" Image_singleton_iff IntI Range.RangeI contra_subsetD converse.cases mem_Sigma_iff restrict_def) 
ultimately have "(m,s)\<in>U" using assms 3 Image_singleton_iff IntI Range.RangeI converse.cases 
mem_Sigma_iff restrict_def subsetCE symE by smt } 
thus ?thesis using assms(1,3) unfolding sym_def by meson
qed














lemma (*l42q:*) assumes "X\<subseteq>Range D" "Y\<subseteq>Range D" "sym U" "wf(strict (D^-1))" "isConflictFree2' U X" 
"isConflictFree2' U Y" "isMonotonicOver U (D||X)" "isMonotonicOver U (D||Y)"
"isConflictFree2' U ((D maximals X) \<union> (D maximals Y))" shows "isConflictFree2' U (X \<union> Y)" using mm05l
proof- let ?Z="X\<union>Y" have 
0: "D maximals (X\<inter>U^-1``Y) \<subseteq> (U^-1``Y)\<inter>(D maximals X)" using assms(7) l43d by metis
{
  assume "X\<inter>(U^-1``Y)\<noteq>{}" then have "D maximals (X\<inter>(U^-1``Y))\<noteq>{}" using l42p assms(1,4) inf_assoc 
  inf_bot_right le_iff_inf inf_commute by (metis (no_types, lifting)) then obtain a0 where
  2: "a0\<in>D maximals (X\<inter>U^-1``Y)" by blast then moreover have 
  "a0\<in>X\<inter>U^-1``Y" by (metis Int_iff l42c) ultimately have 
  3: "Y\<inter>U``{a0} \<noteq>{}" by blast then have "Y\<inter>U^-1``{a0}\<noteq>{}" using assms(3) unfolding sym_def 
  by fast then have "{}\<noteq>D maximals (U^-1``{a0}\<inter>Y)" using l42p assms(2,4) inf_assoc inf_bot_right 
  le_iff_inf inf_commute by (metis (no_types, lifting)) then obtain b0 where 
  5: "b0\<in>D maximals (U^-1``{a0}\<inter>Y)" by blast then have 
  4: "b0\<in>U^-1``{a0}\<inter>Y" by (metis IntD1 l42c) have
  1: "D maximals (Y\<inter>U^-1``{a0}) \<subseteq> U^-1``{a0} \<inter> (D maximals Y)" using assms(8) l43d by metis then
  have "a0\<in>(D maximals X) & b0\<in>(D maximals Y) & (a0,b0)\<in>U" using assms(3) 2 0 4 5 IntE inf_commute 
  subsetCE Image_singleton_iff sym_conv_converse_eq by metis then have False using assms(9) by auto
} then have 
6: "U \<inter> (X\<times>Y)={}" by blast have "U \<inter> ?Z\<times>?Z= U\<inter>(X\<times>X\<union>X\<times>Y\<union>Y\<times>X\<union>Y\<times>Y)" by fastforce moreover have 
"... = U\<inter>(X\<times>Y \<union> Y\<times>X)" using assms(5,6) by blast ultimately have "U\<inter>(?Z\<times>?Z)=U\<inter>(X\<times>Y \<union> Y\<times>X)" 
by presburger then show ?thesis using assms(3) unfolding sym_def using 6 by blast
qed











lemma (*l41s:*) assumes "sym U" "isConflictFree2' U (X\<union>Y)" "isMonotonicOver U (D||(D^-1``X \<union> D^-1``Y))"
shows "isConflictFree2' U (D^-1``X \<union> D^-1``Y)"
proof- let ?cf=isConflictFree2' let ?E="D^-1"  let ?X="?E``X" let ?Y="?E``Y" let ?Z="?X \<union> ?Y" have
"?cf U X" using assms(2) by blast moreover have 
0: "isMonotonicOver U (D||?X)" using assms(3) unfolding restrict_def Outside_def by simp ultimately have 
1: "?cf U ?X" using assms(1) unfolding sym_def using 0 unfolding restrict_def Outside_def by blast have
"?cf U Y" using assms(2) by blast moreover have 
2: "isMonotonicOver U (D||?Y)" using assms(3) unfolding restrict_def Outside_def by simp ultimately have
3: "?cf U ?Y" using assms(1) unfolding sym_def using 2 unfolding restrict_def Outside_def by blast have
5: "isMonotonicOver U (D||(?X\<union>?Y))" using assms(3) by blast
{
  assume "\<not>?thesis" then obtain x0 y0 where 
  4: "x0\<in>?X & y0\<in>?Y & (x0,y0)\<in>U" using assms(1) 1 3 DiffD2 SigmaE UnE disjoint_iff_not_equal lm32h 
  mem_Sigma_iff symE by (smt) then obtain x1 y1 where 
  7: "(x0,x1)\<in>D & (y0,y1)\<in>D & x1\<in>X & y1\<in>Y" by blast then have
  6: "x0\<in>?Z & y0\<in>?Z & (x0,x1)\<in>D||?Z & (y0,y1)\<in>D||?Z" unfolding restrict_def Outside_def using 4 by blast have
  "(x1,y1)\<in>U" using assms(1) unfolding sym_def using 6 assms(3) 4 by blast then have False using assms(2) 7 by blast  
} thus ?thesis by presburger
qed

lemma assumes "x\<in>X" "runiq f" "X\<subseteq>Domain f" "F=(\<lambda>g h. g +< ((\<lambda>xx. g,,xx \<union> h,,xx)|||(Domain h)))" (is "F=?F")
"\<forall>A\<in>set l. runiq A & X \<subseteq> Domain A & Union(Range A)\<subseteq>Y" shows
"runiq (foldl F f l) & X \<subseteq> Domain (foldl F f l) & (foldl F f l),,x \<subseteq> f,,x \<union> Y"
proof - let ?D=Domain let ?R=Range let ?U=Union let ?UU="?U o ?R" let ?PP="\<lambda> g. 
runiq g & X \<subseteq> ?D g & g,,x \<subseteq> f,,x \<union> Y" have "conj=disj \<or> conj=conj" by simp moreover  have 
"?PP f" using assms by blast moreover have "\<forall>g h. (?PP g & ?PP h) \<longrightarrow> ?PP (?F g h)" using assms 
UnE MiscTools.ll33 MiscTools.ll37 UnCI eval_runiq_rel paste_Domain paste_def runiq_basic 
runiq_paste2 subset_iff by (smt) moreover have "\<forall>x\<in>set l. ?PP x" using assms(1,5) by (meson UnionI 
eval_runiq_in_Range subsetCE subsetI sup_ge2) ultimately have "?PP (foldl ?F f l)" by (rule l27ee) 
then show ?thesis using assms(4) by fast
qed









(* End of unused *)





























section{*examples*}

datatype mbc00 = x1|x2|x3|x4|x5|x6|x7|x8
definition "mbc00DD={(x1,x8),(x1,x3),(x1,x6),(x1,x7),(x2,x3),(x2,x4),(x2,x5),(x2,x6),(x2,x7),(x2,x8),
(x3,x6),(x3,x7),(x3,x8)}"
definition "mbc00D=mbc00DD \<union> Id_on (Field mbc00DD)"
definition "mbc00T=set(symCl[(x1,x2),(x4,x5),(x6,x7),(x6,x8)])"
definition "mbc00f={(x1,{1,2,4,5,7,9::nat}),(x2,{2,3,4,5,6,7,8,9}),(x3,{2,4,5,7,9})
,(x4,{3,8}),(x5,{3,6}),(x6,{4,7}),(x7,{2,7}),(x8,{4,5,9})}"
value "let f = mbc00f in let D=mbc00D in \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((((x, y)\<in>D) \<longrightarrow>(f,,x \<supseteq> f,,y))))"
value "let f = mbc00f in let D=mbc00D in \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((((x, y)\<in>D) = (f,,x \<supseteq> f,,y))))"
value "let f = mbc00f in let D=mbc00D in {(x,y) \<in> Domain f \<times> Domain f|
((((x, y)\<notin>D) & (f,,x \<supseteq> f,,y)))}"
value "let f=mbc00f in Domain f \<times> Domain f"
value "let f = mbc00f in let T=mbc00T in \<forall>x\<in>Domain f. (\<forall>y\<in>Domain f. 
((x,y)\<in>T = ((f,,x) overlaps (f,,y)))
)"
value "isFgRepr mbc00f mbc00D mbc00T"
value "isRepresentation mbc00f mbc00D (unRel mbc00D - mbc00T)"
value "card(mbc00T)"

value "mbc00T^-1 = mbc00T"
value "((unRel mbc00D) - mbc00T) = 
{(x1,x4), (x1,x5), (x3,x4), (x3,x5), (x4,x1), (x4,x3), (x4,x6), (x4,x7), (x4,x8), (x5,x1), (x5,x3), (x5,x6), (x5,x7), (x5,x8), (x6,x4), (x6,x5), (x7,x4), (x7,x5), (x7,x8), (x8,x4), (x8,x5), (x8,x7)}"

value "let 
R = {(x1,x4), (x1,x5), (x3,x4), (x3,x5), (x4,x6), (x4,x7), (x4,x8), (x5,x6), (x5,x7), (x5,x8), (x7,x8)} in 
(unRel mbc00D - mbc00T = R \<union> R^-1, R\<inter>R^-1)"
value "Domain mbc00D"

(* end of examples *)

















section{* Essence/conjure interlude *}
abbreviation "together' st pt==List.list_ex (\<lambda>x. st\<subseteq>x) pt"
abbreviation "apart' st pt == Not (together' st pt)"
notepad
begin
text{* find R : relation of (int(0..1) * int(0..1))
such that toSet(R) = {(0,0),(0,1),(1,1)} *}
let ?RR="{(0::int, 0::int),(0,1),(1,1)}" let ?R="pairset2rel ?RR"
value "?R 1 0"

text{*find f : function int(0..1) --> int(0..1)
such that toSet(f) = {(0,0),(1,1)}*}
let ?ff="{(0::int,0::int),(1,1)}" let ?f="?ff,,"
value "?f 0"

text{*find g : function int(0..1) --> int(0..1)
such that toRelation(g) = relation((0,0),(1,1))*}
let ?gg="{(0::int, 0::int),(1,1)}" let ?g="?gg,,"
value "?g 1"

text{*find a : bool such that a = allDiff([1,2,4,1]) $ false*}
let ?l="[1::nat,2,4,1]" let ?a="distinct ?l"
value "?a"

text{*letting P be partition({1,2},{3},{4,5,6})
find a : bool such that a = apart({3,5},P) /\ !together({1,2,5},P) $ true*}
let ?P="[{1::nat,2},{3},{4,5,6}]"
value "apart' {3,5} ?P \<and> \<not>together' {1,2,5} ?P"
end


section{*Scraps*}
(*
lemma assumes "runiq f" "C\<subseteq>Domain f" "x\<in>C \<longrightarrow> C\<inter>f^-1``(Pow(U f - (f,,x)))={}" "U=Union o Range" 
"h=f +< (\<lambda>x'. f,,x' \<union> A x')|||C" "\<forall>x'\<in>Domain f - C. \<forall>y\<in>C. A y \<inter> (f,,x')={}"
shows
"h^-1``(Pow(U h - (h,,x))) \<subseteq> f^-1``(Pow(U f - (f,,x)))" using assms sorry

lemma assumes "runiq f" "C \<subseteq> Domain f" "Y\<in>Range f" shows 
"Y \<subseteq> (Union (Range (f+*((\<lambda>x. (f,,x) \<union> A x)|||C))))" using assms l20 
sorry

lemma assumes "runiq f" "C \<subseteq> Domain f" "Y\<in>A`C" shows 
"Y \<subseteq> (Union (Range (f+*((\<lambda>x. (f,,x) \<union> A x)|||C))))" using assms 
MiscTools.ll33 MiscTools.ll37 Range_Un_eq UnCI UnionI eval_runiq_in_Range imageE paste_def subsetI
sorry

lemma assumes "runiq f" "C \<subseteq> Domain f" shows "(Union (Range (f+*((\<lambda>x. (f,,x) \<union> A x)|||C)))) \<supseteq> 
(Union (Range f)) \<union> (Union (A`C))" 
using assms sorry

lemma assumes "U=Union o Range" "runiq f" "h=f+*(\<lambda>x'. f,,x' \<union> A x')|||C" "C\<subseteq>Domain f" shows 
"h^-1``(Pow(U h - (h,,x))) \<subseteq> f^-1``(Pow(U f - f,,x))" (is "?L\<subseteq>?R")
proof- let ?u=Union let ?U="?u o Range" let ?AA="?u (A`C)" let ?g="\<lambda>x'. (f,,x') \<union> A x'" let ?G="?g|||C" let ?h="f +* ?G" 
have 0: "runiq h" sorry have 3: "h=?h" unfolding assms by simp 
have 1: "?AA \<inter> ?u(f``(UNIV-C))={}" sorry have 2: "\<forall>B\<in>A`C. \<forall>y\<in>(Domain f)-C. B\<inter>f,,y={}" sorry 
have 4: "C \<subseteq> Domain f" sorry have 5: "C=Domain ?G" by (simp add: MiscTools.ll37)
have "?U ?h \<subseteq> ?U f \<union> ?AA" using assms(2,4) by (rule l17f) then have "?U ?h - ?AA \<subseteq> ?U f - ?AA" by blast
moreover have "?AA \<inter> ?U (f outside C)={}" sorry
ultimately have "?U ?h - ?AA \<supseteq> ?U f" using assms 1 2 3 4 5 sorry
qed

(* l19, l17 *)
lemma assumes "h=f +* (((\<lambda>y. y\<union>A y)o(f,,))|||C)" "U=Union o Range" "runiq f" "x\<in>Domain f" shows 
"(x \<notin> C \<or> (x\<in>C \<and> (f^-1``Pow(U f - f,,x))\<inter>C={})) \<longrightarrow> f^-1``Pow(U f - (f,,x)) \<subseteq> h^-1``Pow(U h - (h,,x))"
(is "_ \<longrightarrow> ?L \<subseteq> ?R")
proof -
let ?g="\<lambda>x. x\<union>A x" let ?f="f,," let ?G="(?g o ?f)|||C" let ?h="f +* ?G" 
term A
have 
0: "runiq ?G & Domain ?G=C" using assms by (simp add: MiscTools.ll37) have 
1: "C \<subseteq> Domain f" sorry have 
2: "U f \<subseteq> U ?h" using 1 assms l17 by fastforce have 
4: "h=?h" using assms by blast
{
  assume "x\<notin>C" then have 
  3: "f,,x=?h,,x" using 0 assms l19 by (metis (no_types, lifting) DiffI)
  fix x' assume "x'\<in> ?L" then have "x'\<in>Domain f & f,,x'\<subseteq>U f - f,,x" using assms(3) l31 by fastforce
  moreover have "(?h,,x') \<subseteq> ((f,,x') \<union> A x')" using assms sorry
  then have "?h,,x' - f,,x' \<subseteq> (U ?h - ?h,,x)" using 3 2  
  then have "x'\<in>
} then have "x\<notin>C \<longrightarrow> ?L \<subseteq> ?R" try0
qed
*)



(*
Definition of representation f:
e1 \<longrightarrow>* e2          <==>        f(e1) \<supseteq> f(e2)
AND
e1 # e2             <==>        f(e1) \<inter> f(e2) = {}

Theorem: (D,U) has a representation if and only if it is an
Event Structure 

Basically, this means that \<longrightarrow>* is the same thing as \<supseteq>
and # is the same thing as empty intersection.
*)
lemma "P +< Q = (P - (Domain Q \<times> Range P)) \<union> Q" sledgehammer
by (simp add: Outside_def paste_def)
end
