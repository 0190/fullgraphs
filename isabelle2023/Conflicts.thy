(*
Author:
* Marco B. Caminati http://caminati.co.nr
Dually licenced under
* Creative Commons Attribution (CC-BY) 3.0
* ISC License (1-clause BSD License)
See LICENSE file for details
(Rationale for this dual licence: http://arxiv.org/abs/1107.3212)
*)

theory Conflicts

imports les
"./Sandbox/ListUtils"
preorder

begin

section{* Preliminaries *}

lemma mm20: "Union (set (map (%Or.{set Or}\<times>(set (f Or))) (l)))=
             set(concat (map (%Or. (map (%x. (set Or, x)) (f Or))) (l)))" by auto

abbreviation "strictSuccessorsForPartialOrder' order list ==
map snd (filter (% (x,y). x \<in> set list & y\<noteq>x) order)"
definition "strictSuccessorsForPartialOrder = strictSuccessorsForPartialOrder'"

abbreviation "nextForPartialOrder' order list == 
filter (%x. x \<notin> set (strictSuccessorsForPartialOrder order (strictSuccessorsForPartialOrder order list))) 
(strictSuccessorsForPartialOrder order list)"
definition "nextForPartialOrder = nextForPartialOrder'"

(* applies a function only on determined values*)
abbreviation "optApp' f x==if x=None then None else (Some (f (the x)))" definition "optApp=optApp'"

abbreviation "Mins' A=={a\<in>A. a\<le>Min A}" definition "Mins=Mins'"

abbreviation "Maxs' A=={a\<in>A. a=Max A}" definition "Maxs=Maxs'"


lemma mm13: "refl_on A P=(Field P=A & reflex P)" using assms unfolding reflex_def Field_def refl_on_def by auto

lemma lm07a: assumes "isMonotonicOver C Po" "sym C" shows "C``{m} \<supseteq> Po``(C``{m})"
using assms Image_singleton_iff subset_eq symE ImageE by smt
(*
proof -
  have f1: "\<forall>r a aa. \<not> sym r \<or> (a\<Colon>'a, aa) \<notin> r \<or> unset r (aa, a)"
    by (meson symE)
  have "\<forall>a aa. (a, aa) \<notin> Po \<or> C `` {a} \<subseteq> C `` {aa}"
    using assms(1) by presburger
  thus ?thesis
    using f1 assms(2) by blast
qed
*)

lemma lm07b: assumes "isMonotonicOver C Po" "sym C" "reflex' Po" "Range C \<subseteq> Domain Po" shows 
"C``{m} = Po``(C``{m})" (is "?L=?R")
proof -
have "?L \<subseteq> ?R" using assms(3,4) refl_on_def by blast
moreover have "?L \<supseteq> ?R"using assms(1,2) by (rule lm07a)
ultimately show ?thesis by blast
qed

lemma lm07c: assumes "isMonotonicOver C Po" "irrefl C" "Range C \<subseteq> Domain Po" shows 
"C``{m} \<subseteq> Field Po - Po``{m}" 
proof -
{
  fix y assume "(m,y) \<in> C" then have 
  "(m,y) \<in> Po \<longrightarrow> (y,y) \<in> C" using assms lm33a by blast
  then have "(m,y) \<notin> Po" using assms irrefl_def by fast
}
then have "C``{m} \<inter> Po``{m}={}" by blast
moreover have "C``{m} \<subseteq> Field Po" using assms Field_def by fast
ultimately show ?thesis by fast
qed

lemma mm02: assumes "isMonotonicOver C Po" "C``{m} \<subseteq> U" shows "C``{m} \<subseteq> 
(U - (U-\<Inter> {C``{M}| M. M\<in>(order2strictCover' Po)``{m}}))"
using assms(1,2) unfolding next1_def by blast

lemma lm30: assumes "m\<in>Domain P" shows "(order2strictCover' P)``{m} = next1 P {m}" using assms by auto

lemma mm01a: assumes "sym P" shows "sym ((P - (X\<times>UNIV)) - (UNIV\<times>X))" using assms 
using sym_def by auto

lemma mm01b: assumes "irrefl Q" "P\<subseteq>Q" shows "irrefl P" using assms irrefl_def by blast 

lemma mm01c: assumes "antisym Q" "P\<subseteq>Q" shows "antisym P" using assms antisym_def using antisym_subset by auto

lemma lm28f: "(reflex P) = (Id_on (Field P) \<subseteq> P)" using reflex_def les.lm28e by metis

lemma mm01d: assumes "reflex' P" shows "\<forall> x y. (x,y) \<in> P \<longrightarrow> ((x,x)\<in>P & (y,y)\<in>P)" 
using assms refl_onD refl_onD1 refl_onD2 by (metis)

lemma mm01e: assumes "\<forall> x y. (x,y) \<in> P \<longrightarrow> ((x,x)\<in>P & (y,y)\<in>P)" shows "Id_on (Field P) \<subseteq> P" 
using assms Field_def Domain.cases Id_on_iff Range.cases Un_iff subrelI by (metis) 

lemma mm01f: "reflex' P = (\<forall> x y. (x,y) \<in> P \<longrightarrow> ((x,x)\<in>P & (y,y)\<in>P))" 
using Field_def les.lm28e by fastforce

lemma mm01g: assumes "reflex' Q" shows "reflex' (Q - X\<times>UNIV - UNIV\<times>X)"
proof -
let ?P="Q - X\<times>UNIV - UNIV\<times>X" have "\<forall> x y. (x,y) \<in> Q \<longrightarrow> ((x,x)\<in>Q & (y,y)\<in>Q)" 
using assms mm01f by metis then have "\<forall> x y. (x,y) \<in> ?P \<longrightarrow> ((x,x)\<in>?P & (y,y)\<in>?P)" by blast 
thus ?thesis using mm01f by blast
qed

lemma mm01h: assumes "reflex Q" shows "(Domain Q)- X \<subseteq> Field (Q-(X\<times>UNIV \<union> UNIV\<times>X)) & 
                                        (Range Q)- X \<subseteq> Field (Q-(X\<times>UNIV \<union> UNIV\<times>X))"
using assms(1)  Field_def unfolding reflex_def mm01f by force

lemma mm01i: assumes "reflex Q" shows "(Field Q)- X \<subseteq> Field (Q-(X\<times>UNIV \<union> UNIV\<times>X))" (is "?L \<subseteq> ?R")
proof - have "?L = (Domain Q) - X \<union> (Range Q - X)" using Field_def by force 
thus ?thesis using assms mm01h by blast qed

lemma mm01j: "Field (Q-(X\<times>UNIV \<union> UNIV\<times>X)) \<subseteq> Field Q - X"
using Field_def unfolding Field_def by force 

lemma mm01k: assumes "reflex Q" shows "(Field Q)- X = Field (Q-(X\<times>UNIV \<union> UNIV\<times>X))"
using assms mm01i mm01j by fast

lemma mm01l: assumes "reflex' Q" "X \<subseteq> Domain Q" shows "X \<subseteq> Q``X" 
using assms(1,2) ImageI les2.lm31 refl_onD subsetCE subsetI by fastforce

lemma mm04: assumes "irrefl C" "isMonotonicOver C Or" "sym C" "M \<in> Or``{m} - {m}" "y\<in>C``{M}" shows 
"y\<in> (C - ({m}\<times>UNIV \<union> UNIV\<times>{m}))``{M}" 
proof -
have "(M,y) \<in> C" using assms(5) by fast
moreover have "(M,y) \<notin> {m}\<times>UNIV" using assms(4) by blast
ultimately moreover have "y \<noteq> m" using assms(1,2,3,4)
Image_singleton_iff irrefl_def subsetCE symE DiffE by (smt)
ultimately show ?thesis by blast
qed

lemma mm04b: assumes "irrefl C" "isMonotonicOver C Or" "sym C" "M \<in> Or``{m} - {m}"  
shows "C``{M}\<subseteq> (C - ({m}\<times>UNIV \<union> UNIV\<times>{m}))``{M}" (is "?L\<subseteq>?R")  
proof -
have "\<forall>y\<in>?L. y\<in>?R" using assms mm04 by metis thus ?thesis by auto
qed

lemma mm03a: "(order2strictCover' Or)``{m} \<subseteq> Or``{m} - {m}" unfolding next1_def by auto 

lemma mm04c: assumes "irrefl C" "isMonotonicOver C Or" "sym C" "M \<in> (order2strictCover Or)``{m}"  
shows "C``{M}\<subseteq> (C - ({m}\<times>UNIV \<union> UNIV\<times>{m}))``{M}" 
proof -
have 
4: "M\<in>Or``{m}-{m}" using assms(4) mm03a unfolding order2strictCover_def next1_def by fast
show ?thesis using assms(1,2,3) 4 by (rule mm04b)
qed

lemma mm04d: assumes "irrefl C" "isMonotonicOver C Or" "sym C" shows 
"\<forall>M\<in> (order2strictCover Or)``{m} . C``{M}\<subseteq> (C - {m}\<times>UNIV - UNIV\<times>{m})``{M}" 
proof -
{fix M assume "M \<in> (order2strictCover Or)``{m}" then have "C``{M} \<subseteq>
  (C - ({m}\<times>UNIV \<union> UNIV\<times>{m}))``{M}" using assms(1,2,3) mm04c by metis} thus ?thesis by blast
qed

lemma mm05a: "irrefl (strict' P)" 
using DiffD1 DiffD2 Domain.DomainI irreflI mem_Collect_eq by (metis (mono_tags, lifting))

lemma mm05b: assumes "trans P" "antisym P" shows "trans (strict' P)"
using assms(1,2) unfolding trans_def antisym_def by blast

lemma mm05c: "(trans P)=(P=P^+)" using assms by (metis trancl_id trans_trancl)

lemma mm05d: assumes "trans P" "antisym P" shows "acyclic (strict' P)" 
using assms acyclic_irrefl mm05a mm05b mm05c by fastforce

lemma mm05k: assumes "finite (strict P)" "trans P" "antisym P" shows "wf (strict P)" using 
assms mm05d finite_acyclic_wf unfolding strict_def by fastforce

lemma mm05e: assumes "wf (strict' Or)" "(x,z)\<in>(strict' Or)" shows
"EX y. (y,z)\<in>(strict' Or) & (\<forall>t. (x,t)\<in>(strict' Or) \<longrightarrow> (t,y) \<notin> (strict' Or))" using assms wf_def  
by (meson wf_not_sym)

lemma mm05f: assumes "wf (strict' Or)" "(x,z)\<in>(strict' Or)" shows
"EX y. (y,z)\<in>Or & (\<forall>t. (x,t)\<in>( Or) \<longrightarrow> (t,y) \<notin> (strict' Or))" 
using assms(1,2) wf_not_sym Pair_inject mem_Collect_eq Diff_iff surj_pair by (smt)

lemma mm05g: assumes "wf (strict' Or)" "(x,z)\<in>(strict' Or)" shows
"EX y. (x,y)\<in>(strict' Or) & (\<forall>t. (t,z)\<in>(strict' Or) \<longrightarrow> (y,t) \<notin> (strict' Or))" using assms wf_def  
by (meson wf_not_sym)

lemma mm05h: assumes "reflex' Or" "trans Or" "antisym Or" "wf (strict' Or)" "(x,z)\<in>(strict' Or)" 
shows "EX y. (y,z)\<in>Or & (x,y)\<in>Or & (\<forall>t. (x,t)\<in> Or \<longrightarrow> (t,y) \<notin> (strict' Or))" 
using assms(1,3,4,5) wf_not_sym DiffD1 antisymD mm01d by (metis (no_types, lifting))

lemma mm05i: assumes "trans S" "wf S" "(x,y)\<in>S" "(y,z)\<in>S" shows 
"EX xx. (xx,z)\<in>S & (x,xx)\<in>S & (\<forall>t. (x,t)\<in>S \<longrightarrow> (t,xx)\<notin>S)"
proof - let ?Q="S``{x} \<inter> (S^-1``{z})" have "?Q \<noteq> {}" using assms(3,4) by blast then 
obtain a where "a\<in>?Q" by blast then obtain xx where 0: "xx\<in>?Q" and 
1: "\<forall>y. (y,xx)\<in>S \<longrightarrow> y\<notin>?Q" using assms(2) wfE_min by metis have "(xx,z)\<in>S & (x,xx)\<in>S" 
using 0 by fast then moreover have "\<forall>t. (x,t)\<in>S \<longrightarrow> (t,xx)\<notin>S" using 0 1 assms(1) 
by (meson Image_singleton_iff IntI converse.intros transE) ultimately show ?thesis by blast
qed

lemma mm05j: assumes "(x,xx)\<in>(strict' P)" "\<forall>t. (x,t)\<in>(strict' P) \<longrightarrow> (t,xx)\<notin>(strict' P)" 
shows "xx \<in> next1' P {x}" using assms by blast

lemma mm05l: assumes "reflex Or" "trans Or" "antisym Or" "wf (strict Or)" "(x,z)\<in>Or" "z\<noteq>x" shows
"EX y. ((y,z) \<in> Or & y \<in> next1' Or {x})" 
proof - let ?S="strict Or" let ?SS="strict' Or" have 
0: "trans ?S" using assms(2,3) strict_def mm05b by metis
{
  assume "~ (EX y. ((x,y)\<in>?S & (y,z)\<in>?S))" then have "z\<in>next1' Or {x}" 
  using assms(5,6) unfolding strict_def by blast 
  then have ?thesis using assms(1) assms(5) mm01d unfolding reflex_def by auto
}
then have 
2: "~ (EX y. ((x,y)\<in>?S & (y,z)\<in>?S)) \<longrightarrow> ?thesis" by blast
{
  assume "EX y. ((x,y)\<in>?S & (y,z)\<in>?S)" then obtain y where "(x,y) \<in> ?S" and "(y,z)\<in>?S" by meson
  then obtain xx where 
  1: "(xx,z)\<in>?S & (x,xx)\<in>?S & (\<forall>t. (x,t)\<in>?S \<longrightarrow>(t,xx)\<notin>?S)" 
  using assms(4) 0 mm05i by (metis(no_types,lifting))
  then have "(xx,z)\<in>Or & xx\<in>next1' Or {x}" using 1 mm05j unfolding strict_def by blast
  then have "EX y. (y,z)\<in>Or & y\<in>next1' Or {x}" by meson
}
then show ?thesis using 2 by blast
qed

lemma mm05m: assumes "reflex Or" "trans Or" "antisym Or" "wf (strict Or)" shows 
"(next1' Or {m}={}) = ((Or``{m})-{m}={})" using assms mm05l by fast

lemma mm07: "set [d\<union>({m}\<times>Y)\<union>(Y\<times>{m}). Y <- map f l] = {d\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> f`(set l)}" by fastforce

lemma mm05o: "(List.find f l=None) = (True \<notin> f`(set l))" unfolding find_None_iff by auto

lemma mm05n: assumes "Or\<noteq>{}" "wf Or" shows "Domain Or - (Range Or) \<noteq> {}" using 
assms Diff_eq_empty_iff Range_empty_iff subset_empty Image_empty wfE_pf lll85b nn30a by (metis (no_types))

(* from Universes *)
lemma lm02: "set (injections_alg [] Z)={{}}" by simp

abbreviation "irrefl01' A N == \<forall>i<N. A i i = False" definition "irrefl01=irrefl01'"

abbreviation "antisym01' A N == \<forall> i j. i<N & j<N & A i j & A j i \<longrightarrow> i=j" definition "antisym01=antisym01'"

lemma mm09: assumes "isRectangular4 N A" "size A=N" shows "irrefl01 (matr2Fun (setDiag False A)) N"
using assms unfolding setDiag_def matr2Fun_def irrefl01_def isRectangular4_def by force

lemma mm09b: assumes "N>0" shows "set (allPreorders2 N) = 
    {a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N}"
using assms correctnessThm unfolding matr2Fun_def by presburger

lemma mm09e: assumes "(i,j)\<in>set (indicesOf A v)" shows "A!i!j=v"
using assms unfolding indicesOf_def indicize_def by fastforce

lemma mm09c: "set (indicesOf matr val)=fst`{z| z. z\<in>set (indicize' matr (size matr) (size matr)) & snd z=val}" 
unfolding indicesOf_def indicize_def by simp

lemma mm09d: assumes "size A=N" "i<N" "j<N"  "A!i!j=v" shows "(i,j)\<in>set (indicesOf A v)"
using assms unfolding mm09c by force

lemma mm09f: assumes "size A=N" "i<N" "j<N" shows  "(A!i!j=v)=((i,j)\<in>set (indicesOf A v))"
using assms mm09d mm09e by metis

lemma mm09h: "symmetricEntries strictAdjMatr == 
(set (indicesOf strictAdjMatr True)) \<inter> (flip `( set(indicesOf strictAdjMatr True)))"
unfolding symmetricEntries_def by simp

lemma mm09i: "set (indicesOf A True) \<subseteq> {0..<size A}\<times>{0..<size A}" 
unfolding indicize_def indicesOf_def by auto  

lemma mm09g: assumes "size A=N" "i<N" "j<N" shows 
"((i,j)\<in>symmetricEntries' A) = (A!i!j = True & A!j!i=True)" using assms mm09f fst_conv 
image_eqI mm09d set_map snd_conv Int_iff flip_def imageE prod.collapse by (smt)

lemma mm09j: assumes "size A=N" "(i,j)\<in>symmetricEntries A" shows "i<N & j<N" 
using assms mm09i unfolding symmetricEntries_def by fastforce

lemma mm09k: assumes "size A=N" shows 
"((i,j)\<in>symmetricEntries A) = (i<N & j<N & A!i!j = True & A!j!i=True)" 
using assms mm09g symmetricEntries_def using mm09j by (metis(no_types,lifting))

lemma mm09l: "((i,j)\<in>symmetricEntries A) = (i<size A & j<size A & A!i!j = True & A!j!i=True)" 
using mm09k by presburger

lemma mm09m: "(symmetricEntries A={}) = (\<forall> i j. i<size A & j<size A & A!i!j \<longrightarrow> A!j!i=False)"
using mm09l by auto

lemma mm09n: "allStrictPartialOrders2 N = 
filter (\<lambda> A. (\<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False))) (allStrictPreorders N)"
unfolding allStrictPartialOrders2_def using assms mm09m by presburger

lemma mm09p: assumes "size A=N" "\<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False)"
shows "antisym01' (matr2Fun' A) N" using assms by blast

lemma mm09q: assumes "size A=N" "antisym01' (matr2Fun' A) N" "irrefl01' (matr2Fun' A) N"
shows "\<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False)" using assms by blast

lemma mm09o: assumes "N>0" shows "set (allStrictPreorders N)\<subseteq> 
{a| a. N=size a & isRectangular4 N a & irrefl01 (matr2Fun a) N}" (is "?L\<subseteq>?R")
proof -
{
  fix a assume "a\<in>?L" then obtain b where 
  0: "a=setDiag' False b & b \<in> set (allPreorders2 N)" 
  unfolding allStrictPreorders_def setDiag_def by auto then moreover have 
  1: "N=size b & isRectangular4' N b & trans01 (matr2Fun b) N & refl01 (matr2Fun b) N"
  using assms mm09b unfolding isRectangular4_def by force ultimately moreover have "isRectangular4 N a"
  unfolding isRectangular4_def using 0 by force ultimately moreover have "size a = N" using 0 by auto
  ultimately moreover have "isRectangular4 N b" unfolding isRectangular4_def by meson
  then moreover have "irrefl01 (matr2Fun a) N" using 1 mm09 unfolding setDiag_def using 0 by blast
  ultimately have "isRectangular4 N a & size a = N & irrefl01 (matr2Fun a) N" by blast 
  }
thus ?thesis by blast
qed

lemma mm09v: assumes "N>0" shows "set (allStrictPreorders N)=
{setDiag False a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N}"
using assms mm09b unfolding allStrictPreorders_def by auto

lemma assumes "N=size a" "isRectangular4' N a" "irrefl01' (matr2Fun' a) N" shows "a=setDiag' False a" 
using assms(1,2,3) length_map list_update_id map_nth nth_equalityI nth_map by smt

lemma mm09u: assumes "N=size a" "isRectangular4' N a" "irrefl01 (matr2Fun a) N" shows 
"a=setDiag False (setDiag True a)" using assms(1,2,3) length_map list_update_id map_nth nth_map 
nth_equalityI list_update_overwrite unfolding setDiag_def matr2Fun_def irrefl01_def by (smt)

lemma mm09s: assumes "size a=N" "isRectangular4 N a" shows "refl01 (matr2Fun (setDiag True a)) N"
using assms(1,2) unfolding isRectangular4_def setDiag_def matr2Fun_def refl01_def by fastforce

lemma mm09t: assumes "N=size a" "isRectangular4 N a" "irrefl01 (matr2Fun a) N" "trans01 (matr2Fun a) N"
"refl01 (matr2Fun (setDiag True a)) N" shows "trans01 (matr2Fun (setDiag True a)) N" 
using assms(1,2,3,4) length_map map_nth nth_list_update_neq nth_map setDiag_def 
unfolding matr2Fun_def irrefl01_def isRectangular4_def trans01_def refl01_def by (smt)

lemma mm09r: assumes "size a=N" "isRectangular4 N a" "trans01 (matr2Fun a) N" 
"antisym01 (matr2Fun (setDiag False a)) N" shows "trans01 (matr2Fun (setDiag False a)) N"
using assms(1,2,3,4) length_map map_nth nth_list_update_neq nth_map
unfolding matr2Fun_def setDiag_def antisym01_def isRectangular4_def trans01_def by smt
(*
proof -
let ?b="setDiag' False a" let ?B="matr2Fun' ?b" let ?A="matr2Fun' a" have 
3: "isRectangular4' N ?b" using assms(1,2) unfolding isRectangular4_def by force
{
  fix i j k assume "i<N" moreover assume "j<N" moreover assume "k<N" 
  moreover assume "?B i j" moreover assume "?B j k"
  ultimately moreover have "i\<noteq>j & j\<noteq>k \<longrightarrow> ?B i j=?A i j & ?B j k = ?A j k" using assms(1,2) 3
  length_map map_nth nth_list_update_neq nth_map unfolding isRectangular4_def
by (smt )
  ultimately moreover have "i\<noteq>j & j\<noteq>k \<longrightarrow> ?A i k" using assms(3) unfolding trans01_def matr2Fun_def
  by blast
  ultimately moreover have "i \<noteq> k \<longrightarrow> ?A i k = ?B i k" using assms(1,2) 3 length_map 
  map_nth nth_list_update_neq nth_map unfolding isRectangular4_def by (smt )
  ultimately moreover have "i\<noteq>j & j\<noteq>k & i\<noteq>k \<longrightarrow> ?B i k" by blast
  ultimately moreover have "i=j \<or> j=k \<longrightarrow> ?B i k" by meson
  ultimately moreover have "i=k \<longrightarrow> j=i" using assms(4) unfolding matr2Fun_def setDiag_def antisym01_def by blast
  ultimately have "?B i k" by force
}
thus ?thesis unfolding setDiag_def matr2Fun_def by blast
qed
*)

lemma mm09w: "isRectangular4 N a=isRectangular4 N (setDiag x a)" 
unfolding setDiag_def isRectangular4_def by force 

lemma mm09nn: "set (allStrictPartialOrders2 N) = 
{A. \<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False)} \<inter> set (allStrictPreorders N)"
using mm09n by force

lemma mm09tt: assumes "N=size a" "isRectangular4 N a" "irrefl01 (matr2Fun a) N" 
"trans01 (matr2Fun a) N" shows "trans01 (matr2Fun (setDiag True a)) N" 
using assms mm09s mm09t by blast 

lemma mm10: assumes "N>0" shows "set (allStrictPartialOrders2 N) = {a. N=size a & isRectangular4 N a & 
trans01 (matr2Fun a) N & irrefl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}" (is "?L=?R")
proof - 
{
  fix a assume "a\<in>?L" then have 
  0: "a\<in>set (allStrictPreorders N) & (\<forall> i j. ((i<size a & j<size a & a!i!j=True) \<longrightarrow> a!j!i=False))" 
  using mm09n by simp then moreover have "size a = N & isRectangular4 N a &  irrefl01 (matr2Fun a) N" 
  using assms mm09o by auto ultimately moreover have "antisym01 (matr2Fun a) N" 
  using 0 mm09p unfolding antisym01_def matr2Fun_def by blast moreover obtain b where 
  "a=setDiag False b & b \<in>set (allPreorders2 N)" using 0 unfolding allStrictPreorders_def by auto 
  then moreover have "N=size b & isRectangular4 N b & trans01 (matr2Fun b) N & refl01 (matr2Fun b) N" 
  using assms mm09b by blast ultimately moreover have "trans01 (matr2Fun a) N" 
  using mm09r assms by metis ultimately have "N=size a & isRectangular4 N a & trans01 (matr2Fun a) N & 
  irrefl01 (matr2Fun a) N & antisym01 (matr2Fun a) N" unfolding trans01_def by blast 
  } then have 
3: "?L \<subseteq> ?R" by fast
{
  fix a let ?b="setDiag True a" assume 
  1: "a\<in>?R" then moreover have 
  "(\<forall> i j. ((i<(size a) & j<(size a) & a!i!j=True) \<longrightarrow> a!j!i=False))" unfolding mem_Collect_eq antisym01_def
  matr2Fun_def isRectangular4_def irrefl01_def by blast then moreover have 
  2: "a\<in>{A. \<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False)}" by blast ultimately
  moreover have "refl01 (matr2Fun ?b) N" using mm09s unfolding refl01_def by auto 
  ultimately moreover have "trans01 (matr2Fun ?b) N" using 1 mm09tt by simp 
  ultimately moreover have "a=setDiag False ?b" using mm09u unfolding isRectangular4_def by force
  ultimately moreover have "size ?b=N" by (smt mem_Collect_eq preorder.lm35 setDiag_def)
  ultimately moreover have "isRectangular4 N ?b" using mm09w by fast
  ultimately have "a\<in> set(allStrictPreorders N)" using assms mm09v by auto then have 
  "a\<in>{A. \<forall> i j. ((i<size A & j<size A & A!i!j=True) \<longrightarrow> A!j!i=False)} \<inter> set(allStrictPreorders N)" 
  using 2 by blast then have "a\<in>?L" using mm09nn by blast
}
then show ?thesis using 3 by blast
qed

abbreviation "allReflPartialOrders' n == map (setDiag True) (allStrictPartialOrders2 n)"
definition "allReflPartialOrders = allReflPartialOrders'"

lemma mm10a: assumes "N=size a" "irrefl01 (matr2Fun a) N" "trans01 (matr2Fun a) N" shows 
"antisym01 (matr2Fun (setDiag True a)) N" using assms length_map map_nth nth_list_update_neq 
nth_map unfolding matr2Fun_def setDiag_def antisym01_def trans01_def irrefl01_def by smt

lemma mm09ttt: assumes "N=size a" "isRectangular4' N a" "antisym01' (matr2Fun' a) N" "trans01' (matr2Fun' a) N"
"refl01' (matr2Fun' (setDiag' True a)) N" shows "trans01' (matr2Fun' (setDiag' True a)) N" 
apply auto
(* using assms nth_list_update_neq nth_map nth_upt  preorder.lm35 list_update_id  map_nth map_nth mem_Collect_eq add.left_neutral length_map
by smt *)
proof -
  fix i :: nat and j :: nat and k :: nat
  assume a1: "i < N"
  assume a2: "matr2Fun' (setDiag' True a) i j"
  assume a3: "matr2Fun' (setDiag' True a) j k"
  assume a4: "j < N"
  assume a5: "k < N"
  have f6: "\<forall>n ns f. \<not> n < length ns \<or> (map f  ns) ! n = (f (ns ! n::nat)::bool list)"
    using nth_map by blast
  have f7: "length [0..<length a] = N"
    by (simp add: assms(1))
  hence f8: "i < length [0..<length a]"
    using a1 by meson
  hence f9: "(a ! ([0..<length (setDiag' True a)] ! i))[[0..<length a] ! i := True] = setDiag' True a ! i"
    by fastforce
  hence f10: "(a ! ([0..<length (setDiag' True a)] ! i))[[0..<length a] ! i := True] ! j"
    using a2 by presburger
  have f11: "\<forall>n na bs b. n = na \<or> bs[n := b::bool] ! na = bs ! na"
    by fastforce
  have f12: "i < length [0..<length (setDiag' True a)]"
    using f8 by simp
  hence f13: "[0..<length a] ! i = j \<longrightarrow> matr2Fun' (setDiag' True a) i k"
    using a3 by force
  have f14: "tolist (matr2Fun' a i) (length (setDiag' True a)) = a ! i"
    using a1 by (metis assms(1) assms(2) map_nth preorder.lm35)
  have f15: "tolist (op ! a) (length (setDiag' True a)) ! i = a ! ([0..<length (setDiag' True a)] ! i)"
    using f12 f6 by meson
  have f16: "tolist (op ! a) (length (setDiag' True a)) = a"
    by (simp add: map_nth)
  hence f17: "a ! ([0..<length (setDiag' True a)] ! i) = a ! i"
    using f15 by presburger
  have f18: "tolist (matr2Fun' a i) (length (setDiag' True a)) ! j = matr2Fun' a i ([0..<length (setDiag' True a)] ! j)"
    using f7 a4 by force
  have f19: "setDiag' True a ! i = (a ! ([0..<length a] ! i))[[0..<length a] ! i := True]"
    using f8 f6 by meson
  have f20: "matr2Fun' (setDiag' True a) i i"
    using a1 assms(5) by presburger
  have f21: "\<not> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k \<longrightarrow> \<not> matr2Fun' a ([0..<length a] ! i) k"
    by simp
  { assume "k \<notin> Collect (matr2Fun' (setDiag' True a) i)"
    { assume "[0..<length a] ! j \<notin> Collect (matr2Fun' a i)"
      hence "\<not> matr2Fun' a i ([0..<length (setDiag' True a)] ! j)"
        by simp }
    moreover
    { assume "unset (Collect (matr2Fun' (setDiag' True a) i)) k \<noteq> unset (Collect (matr2Fun' a i)) ([0..<length a] ! j)"
      { assume "[0..<length a] ! j \<noteq> k"
        hence "matr2Fun' a j k"
          using f7 a4 a3 by force
        hence "matr2Fun' a i j \<longrightarrow> matr2Fun' a i k"
          using a5 a4 a1 assms(4) by blast
        hence "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<and> setDiag' True a ! i = a ! i \<longrightarrow> matr2Fun' (setDiag' True a) i k"
          using f17 by presburger }
      moreover
      { assume "setDiag' True a ! i \<noteq> a ! i"
        hence "(a ! ([0..<length a] ! i))[[0..<length a] ! i := True] \<noteq> (a ! ([0..<length (setDiag' True a)] ! i)) [[0..<length a] ! i := matr2Fun' a ([0..<length (setDiag' True a)] ! i) ([0..<length a] ! i)]"
          using f19 f16 f15 by simp
        hence "\<not> matr2Fun' a ([0..<length (setDiag' True a)] ! i) ([0..<length a] ! i)"
        using  preorder.lm35 by smt
          (* by (metis)  > 1.0 s, timed out *)
        hence "\<not> matr2Fun' a ([0..<length (setDiag' True a)] ! i) i"
          using f12 by simp
        moreover
        { assume "[0..<length a] ! i = i \<and> matr2Fun' a i ([0..<length a] ! j)"
          moreover
          { assume "[0..<length a] ! i = i \<and> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k \<noteq> matr2Fun' a i ([0..<length a] ! j)"
            hence "[0..<length a] ! i = i \<and> [0..<length a] ! j \<noteq> k"
              by force
            hence "[0..<length a] ! i = i \<and> matr2Fun' a j k"
              using f7 a4 a3 by force
            moreover
            { assume "\<not> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k \<and> [0..<length a] ! i < N \<and> matr2Fun' a j k"
              hence "\<not> matr2Fun' a ([0..<length a] ! i) j"
                using f21 a5 a4 assms(4) by blast
              hence "\<not> matr2Fun' a ([0..<length (setDiag' True a)] ! i) j"
                by simp }
            ultimately have "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<longrightarrow> (a ! ([0..<length (setDiag' True a)] ! i))[[0..<length a] ! i := True] ! k = matr2Fun' a ([0..<length (setDiag' True a)] ! i) k \<and> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k \<or> [0..<length a] ! i = i \<and> (a ! ([0..<length (setDiag' True a)] ! i))[[0..<length a] ! i := True] ! k \<noteq> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k"
              using f8 assms(1) by force }
          moreover
          { assume "[0..<length a] ! i = i \<and> (a ! ([0..<length (setDiag' True a)] ! i))[[0..<length a] ! i := True] ! k \<noteq> matr2Fun' a ([0..<length (setDiag' True a)] ! i) k"
            hence "[0..<length a] ! i = i \<and> [0..<length a] ! i = k"
              using f11 by blast
            hence "matr2Fun' (setDiag' True a) i k"
              using a1 assms(5) by auto }
          ultimately have "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<longrightarrow> matr2Fun' (setDiag' True a) i k"
            using f9 by (metis (no_types)) }
        ultimately have "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<and> matr2Fun' a i ([0..<length (setDiag' True a)] ! j) \<longrightarrow> matr2Fun' (setDiag' True a) i k"
          using f20 f11 f9 by (metis preorder.lm35) }
      ultimately have "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<and> matr2Fun' a i ([0..<length (setDiag' True a)] ! j) \<longrightarrow> matr2Fun' (setDiag' True a) i k"
        by fastforce }
    ultimately have "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<and> matr2Fun' a i ([0..<length (setDiag' True a)] ! j) \<longrightarrow> matr2Fun' (setDiag' True a) i k"
      by blast
    hence "matr2Fun' a ([0..<length (setDiag' True a)] ! i) j \<longrightarrow> matr2Fun' (setDiag' True a) i k"
      using f18 f17 f14 by fastforce
    hence "matr2Fun' (setDiag' True a) i k"
      using f13 f11 f10 by blast }
  thus "matr2Fun' (setDiag' True a) i k"
    by blast
qed

lemma mm09t4: assumes "N=size a" "isRectangular4 N a" "antisym01 (matr2Fun a) N" 
"trans01 (matr2Fun a) N" shows "trans01 (matr2Fun (setDiag True a)) N" 
proof -
let ?m="matr2Fun'" let ?b="setDiag' True a" let ?r="isRectangular4' N" have "N=size a" 
using assms(1) by auto moreover have "?r a" using assms(2) isRectangular4_def by metis 
moreover have "antisym01' (?m a) N" using assms unfolding antisym01_def matr2Fun_def by blast 
moreover have "trans01' (?m a) N" using assms(4) unfolding trans01_def matr2Fun_def by blast
moreover have "refl01' (?m ?b) N" using mm09s unfolding refl01_def matr2Fun_def setDiag_def using assms by blast 
ultimately have "trans01' (?m ?b) N" by (rule mm09ttt)
thus ?thesis unfolding trans01_def  matr2Fun_def setDiag_def by blast
qed

lemma mm10b: assumes "N>0" shows "set (allReflPartialOrders' N) = (setDiag True)`{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & irrefl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
using assms mm10 by simp

lemma mm10c: assumes "N>0" shows "(setDiag True)`{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & irrefl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}
\<subseteq> {a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
apply auto 
apply (simp add: setDiag_def)
apply (meson mm09w)
apply (simp add: mm09tt)
apply (simp add: mm09s)
apply (simp add: mm10a)
done

lemma mm09uu: assumes "N=size a" "isRectangular4 N a" "refl01 (matr2Fun a) N" shows 
"a=setDiag True (setDiag False a)" using assms length_map list_update_id map_nth nth_equalityI nth_map 
list_update_overwrite unfolding setDiag_def matr2Fun_def refl01_def isRectangular4_def by smt

lemma mm09x: assumes "N=size a" "isRectangular4 N a" "antisym01 (matr2Fun a) N" shows
"antisym01 (matr2Fun (setDiag False a)) N" 
using assms(1,2,3) length_map map_nth nth_map nth_list_update_neq 
unfolding setDiag_def matr2Fun_def antisym01_def isRectangular4_def by (smt)

lemma mm09rr: assumes "size a=N" "isRectangular4 N a" "trans01 (matr2Fun a) N" 
"antisym01 (matr2Fun a) N" shows "trans01 (matr2Fun (setDiag False a)) N"
using assms mm09x apply auto using assms mm09r mm09x by (metis)

lemma mm10d: assumes "N>0" shows "{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N} \<subseteq>
(setDiag True)`{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & irrefl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
(is "?L \<subseteq> ?R")
proof -
let ?m=matr2Fun let ?d=setDiag
{
  fix a assume "a\<in>?L" then have 
  0: "N=size a & isRectangular4 N a & trans01 (matr2Fun a) N & 
  refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N" by fast
  let ?b="setDiag False a" have "a=?d True ?b" using 0 mm09uu by blast
  moreover have "N=size ?b" using 0 unfolding setDiag_def by simp
  moreover have "trans01 (?m ?b) N" using 0 mm09rr by blast
  moreover have "irrefl01 (?m ?b) N" using 0 mm09 by blast
  moreover have "antisym01 (?m ?b) N" using 0 mm09x by blast
  ultimately have "a \<in> ?R" using "0" mm09w by fastforce
  }
thus ?thesis by blast
qed

theorem mm10e: assumes "N>0" shows "set (allReflPartialOrders N) = {a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
using assms mm10b mm10c mm10d subset_antisym unfolding allReflPartialOrders_def
by (metis (no_types, lifting))

abbreviation "isPo' P == trans P & reflex P & antisym P" definition "isPo = isPo'"
abbreviation "posUniv'==isPo-`{True}" definition "posUniv=posUniv'" 
abbreviation "posOver' X=={P. Field P=X & isPo P}" definition "posOver = posOver'"

lemma mm10l: "posOver X=posUniv \<inter> Field-`{X}" unfolding posOver_def posUniv_def by auto

lemma mm11: "card o (%Y. {f Y}\<times>(g Y))=card o g" 
proof - {fix Y have "card ({f Y}\<times>(g Y))=card (g Y)" 
  using Groups_Big.card_cartesian_product_singleton by force} thus ?thesis by fastforce 
qed
(* from Universes *)
corollary setsum_associativity: assumes "finite x" "X partitions x" shows
"setsum f x = setsum (setsum f) X" using assms(1,2) lll41 setsum.Union_disjoint 
unfolding is_non_overlapping_def is_partition_of_def by fastforce

lemma mm10j: "finite (Field P) = finite P" using Field_def finite_Un lm57 by (metis )

lemma assumes "inj_on f A" shows "setsum (g o f) A= setsum g (f`A)" using assms 
by (simp add: setsum.reindex)

lemma mm11b: "card ({x}\<times>Y)=card Y" using Groups_Big.card_cartesian_product_singleton by fast

lemma mm10m: "adj2Pairs1=set o adj2PairList" 
proof -
{fix a have "adj2Pairs1' a = set (adj2PairList' a)" by fastforce
  then have "adj2Pairs1 a = (set o adj2PairList) a" unfolding adj2Pairs1_def adj2PairList_def by force
  }
thus ?thesis by presburger
qed

lemma mm14: "inj_on adj2PairList {a| a. size a=M & isRectangular4 N a}" 
proof -
let ?A="{a| a. size a=M & isRectangular4 N a}"
have "inj_on adj2Pairs1 ?A" unfolding lm04bb using lm93b by blast
thus ?thesis unfolding mm10m using inj_on_imageI2 by blast
qed

lemma mm10p: "inj_on (setDiag False) (set(allPreorders2 N))"
proof -
let ?A="{a| a. N=size a & isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N}"
let ?f="setDiag False" let ?t="setDiag True" let ?B="set(allPreorders2 N)"
{
  fix a b assume "a\<in>?A" moreover assume "b\<in>?A" moreover assume "?f a = ?f b"
  ultimately moreover have "a=?t (?f a) & b=?t (?f b)" using mm09uu matr2Fun_def by blast
  ultimately have "a=b" by presburger
}
then have "\<forall> a b. a\<in>?A & b\<in>?A & ?f a = ?f b \<longrightarrow> a=b" by meson
moreover have "N>0 \<longrightarrow>?A=?B" unfolding matr2Fun_def using assms correctnessThm by presburger
ultimately have "N>0 \<longrightarrow>?thesis" unfolding inj_on_def by blast
thus ?thesis unfolding inj_on_def by fastforce
qed

corollary mm10q: "distinct (allStrictPreorders N)" unfolding allStrictPreorders_def using mm10p distinct_map lm91n by blast
corollary mm10r: "distinct (allStrictPartialOrders2' N)" using mm10q distinct_filter by blast

lemma mm10s: assumes "size a=N" "(antisym01' (matr2Fun' a) N)" shows 
"antisym (adj2Pairs3' a)" unfolding antisym_def using assms(1,2) unfolding mem_Collect_eq prod.inject 
by metis

lemma mm10ss: assumes "size a=N" "isRectangular4' N a" "antisym (adj2Pairs3' a)"
"i<N" "j<N" "a!i!j" "a!j!i" shows "i=j"
using assms(1,2,3,4,5,6,7) unfolding antisym_def by fastforce

lemma mm10sss: assumes "size a=N" "isRectangular4 N a" shows 
"(antisym01 (matr2Fun a) N) = antisym (adj2Pairs3 a)" using assms mm10s mm10ss 
unfolding isRectangular4_def matr2Fun_def adj2Pairs3_def antisym01_def by meson

lemma assumes "N>0" shows "isRectangular4' N (pairs2Adj' N a)" using assms(1) by auto

lemma mm09u3: assumes "N=size a" "isRectangular4 N a" "irrefl01 (matr2Fun a) N" shows 
"a=setDiag False (setDiag True a)" using assms unfolding isRectangular4_def by (rule mm09u) 

lemma mm15: assumes "N>0" shows "inj_on (setDiag True) (set (allStrictPartialOrders2 N))" 
using assms inj_onI mem_Collect_eq mm09u3 mm10 by (smt)
corollary mm15b: assumes "N>0" shows "distinct (allReflPartialOrders N)"
unfolding allReflPartialOrders_def
using assms mm15 
by (metis (no_types, lifting) distinct_filter distinct_map mm09n mm10q)

lemma mm10t: assumes "(\<forall> i j. i<N & j<N & (setDiag False a)!i!j \<longrightarrow> (setDiag False a)!j!i=False)"
shows "antisym01 (matr2Fun (setDiag False a)) N" 
using assms unfolding setDiag_def matr2Fun_def antisym01_def by blast

lemma mm10t2: assumes "size (setDiag False a)=N" "isRectangular4 N (setDiag False a)" 
"i<N" shows "i=j \<longrightarrow>(setDiag False a)!j!i=False" using assms 
unfolding setDiag_def isRectangular4_def by fastforce 

lemma mm10t3: assumes "antisym01 (matr2Fun (setDiag False a)) N"
"i<N" "j<N" "(setDiag False a)!i!j" shows "i\<noteq>j\<longrightarrow>(setDiag False a)!j!i=False"
using assms(1,2,3,4) unfolding setDiag_def matr2Fun_def antisym01_def by blast

lemma mm10t4: assumes "size (setDiag False a)=N" "isRectangular4 N (setDiag False a)" shows
"(\<forall> i j. i<N & j<N & (setDiag False a)!i!j \<longrightarrow> (setDiag False a)!j!i=False)=
(antisym01 (matr2Fun (setDiag False a)) N)" (is "?l=?r") using assms mm10t mm10t2 mm10t3 by blast

lemma mm09r3: assumes "size a=N" "isRectangular4 N a" "isRectangular4 N (setDiag False a)" 
"refl01 (matr2Fun a) N" "antisym01 (matr2Fun (setDiag False a)) N" 
"trans01 (matr2Fun (setDiag False a)) N" shows 
"trans01 (matr2Fun a) N" using assms length_map map_nth matr2Fun_def setDiag_def mm09t4 mm09uu by smt  

lemma mm09r4: assumes "size (setDiag False a)=N"  "isRectangular4 N (setDiag False a)" 
"refl01 (matr2Fun a) N" "antisym01 (matr2Fun (setDiag False a)) N" shows
"trans01 (matr2Fun (setDiag False a)) N = trans01 (matr2Fun a) N" 
proof -
  have "length a = length (setDiag False a)" by (simp add: setDiag_def)
  thus ?thesis using assms(1,2,3,4) mm09r mm09r3 mm09w by (metis (no_types) )
qed 

lemma assumes "N=size a" "isRectangular4 N a" "refl01 (matr2Fun a) N" shows 
"a=setDiag True a" using assms length_map list_update_id map_nth 
nth_equalityI nth_map list_update_overwrite
unfolding setDiag_def matr2Fun_def refl01_def isRectangular4_def by smt

theorem mm10k: assumes "N>0" shows 
"set (map (set o adj2PairList) (allReflPartialOrders N)) = posOver {0..<N}" (is "?l=?r") 
proof -
have "set (map (set o adj2PairList') (allReflPartialOrders N)) = 
adj2Pairs1'`( set (allReflPartialOrders N))" by auto
moreover have "... = adj2Pairs1'`{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
using assms mm10e by presburger
moreover have "{a. N=size a & 
isRectangular4 N a & trans01 (matr2Fun a) N & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}=
{a. N=size a & 
isRectangular4 N a & trans (adj2Pairs1 a) & refl01 (matr2Fun a) N & antisym01 (matr2Fun a) N}"
using lm92gg unfolding matr2Fun_def by blast
moreover have "... = {a. N=size a & 
isRectangular4 N a & trans (adj2Pairs1 a) & refl_on {0..<N} (adj2Pairs1 a) & antisym01 (matr2Fun a) N}"
using lm92gg unfolding matr2Fun_def by blast moreover have "...= 
{a. N=size a & 
isRectangular4 N a & trans (adj2Pairs1 a) & refl_on {0..<N} (adj2Pairs1 a) & antisym (adj2Pairs3 a)}"
using mm10sss unfolding matr2Fun_def by blast ultimately have
4: "set (map (set o adj2PairList') (allReflPartialOrders N))=adj2Pairs1'`{a. N=size a & 
isRectangular4 N a & trans (adj2Pairs1 a) & refl_on {0..<N} (adj2Pairs1 a) & antisym (adj2Pairs1 a)}"
unfolding lm04bb by presburger
let ?A="{a. N=size a & 
isRectangular4 N a & trans (adj2Pairs1 a) & refl_on {0..<N} (adj2Pairs1 a) & antisym (adj2Pairs1 a)}"
let ?B="{P. trans P & refl_on {0..<N} P & antisym P}" 
{
  fix P let ?a="pairs2Adj N P" assume "P\<in>?B" then have 
  0: "trans P & refl_on {0..<N} P & antisym P" by fastforce then have 
  1: "adj2Pairs3 ?a=P" using lm92m refl_on_def ll59 by metis then have 
  2: "adj2Pairs1 ?a=P" by (simp add: lm04bb) then moreover have 
  "trans (adj2Pairs1 ?a) & antisym (adj2Pairs1 ?a)" using 0 by force ultimately moreover have 
  3: "size ?a=N" using lm92j by blast then have
  "isRectangular4 N ?a" by (simp add: 2 1 0 assms isRectangular4_def lm92j)
  ultimately have "?a\<in>?A" by (simp add: "0" "2" 3) then have "P\<in>adj2Pairs1`?A" using 2 by blast
}
then have "adj2Pairs1`?A = ?B" by blast then have 
"set (map (set o adj2PairList) (allReflPartialOrders N))=?B" using 4 adj2Pairs1_def
unfolding adj2PairList_def by simp moreover have 
"{P. trans P & refl_on {0..<N} P & antisym P} = posOver {0..<N}" using posOver_def reflex_def isPo_def mm13 by (smt Collect_cong)
ultimately show ?thesis by blast
qed

lemma mm16: "f`(set l)= set (map f l)" by simp

lemma mm16c: "sublists l \<noteq> []" using length_sublists list.distinct(1) sublists.simps(1) 
 length_0_conv log_exp log_zero by (metis)

lemma mm16d: assumes "A\<noteq>{}" shows "(Image P)`A\<noteq>{}" using assms by fast

lemma mm16b: assumes "inj_on f A" "{}\<notin>g`A" shows "inj_on (%x. (f x)\<times>(g x)) A" 
using assms(1,2) unfolding inj_on_def by fastforce

lemma mm17a: "set=set o remdups & card o set o remdups=size o remdups" 
using card_length distinct_card eq_iff by fastforce

lemma lm00: "Domain (P-(A\<times>UNIV)) \<inter> A={}" unfolding Domain_def by auto

lemma lm01: "Range (P-(UNIV\<times>A)) \<inter> A={}" by auto

lemma mm17c: "Field (P-A\<times>UNIV-UNIV\<times>A)\<inter>A={}" unfolding Field_def by blast

lemma mm20b: assumes "finite A" shows "finite (Field-`{A}) & (\<forall>P\<in>Field-`{A}. finite P)" 
proof -
have "Field-`{A} \<subseteq>Pow(A\<times>A)" using Field_def by fast moreover have 
"finite (Pow(A\<times>A))" using assms by blast ultimately show ?thesis 
using vimage_singleton_eq finite_subset assms mm10j by (metis)
qed


abbreviation "enumeratePo' N == map adj2PairList (allReflPartialOrders N)"
definition "enumeratePo = enumeratePo'"

theorem poCorrectness: assumes "N>0" shows "set (map set (enumeratePo N)) = posOver {0..<N}" 
unfolding enumeratePo_def using assms mm10k by auto








































section{* Conflicts *}

subsection {* non-computable, proof-theoretical definitions *}

abbreviation "conflictsFor' Or == {C| C. isLes Or C & Field C \<subseteq> Field Or}"
definition "conflictsFor = conflictsFor'"

abbreviation "generateConflictsSet' Or m c == {c\<union>({m}\<times>Y)\<union>(Y\<times>{m})| Y. Y\<in>(Image(Or-{m}\<times>UNIV-UNIV\<times>{m}))`
Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> (order2strictCover Or)``{m}}))}"
definition "generateConflictsSet=generateConflictsSet'"

lemma mm03: "C \<in> generateConflictsSet Or m c = (
\<exists> Y \<in> (Image (Or - {m}\<times>UNIV - UNIV\<times>{m}))`
Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> (order2strictCover' Or)``{m}}))
. C=c\<union>{m}\<times>Y\<union>Y\<times>{m}
)" unfolding generateConflictsSet_def order2strictCover_def by auto

lemma mm06a: "\<Union> {generateConflictsSet Or m c | c. c\<in>conflictsFor (Or - ({m}\<times>UNIV) - (UNIV\<times>{m}))}
\<supseteq> conflictsFor Or" (is "?L \<supseteq> ?R") 
proof -
let ?or="Or- {m}\<times>UNIV - UNIV\<times>{m}" let ?or2="Or-({m}\<times>UNIV \<union> UNIV\<times>{m})" have 
2: "?or=?or2" by fast
{
  fix C  let ?c="C - {m}\<times> UNIV - UNIV\<times>{m}" let ?c2="C - ({m}\<times>UNIV \<union> UNIV\<times>{m})" have 
  0: "?c \<subseteq> C " by blast assume "C \<in> ?R" then have 
  1: "isLes Or C & Field C \<subseteq> Field Or" unfolding conflictsFor_def by fast then moreover have 
  "trans ?or" using 1 trans_def Diff_iff UNIV_I mem_Sigma_iff by (smt Un_iff)
  ultimately moreover have "isMonotonicOver  ?c ?or & sym ?c" using 1 sym_def by force
  ultimately moreover have "irrefl ?c" using mm01b 0 by blast
  ultimately moreover have "antisym ?or" using mm01c Diff_subset by metis
  ultimately moreover have "reflex' ?or" using mm01g unfolding reflex_def by fast
  ultimately have 
  8: "isLes ?or ?c" unfolding reflex_def by meson 
  moreover have 
  6: "Field ?c \<subseteq> Field C - {m}" using mm01k 1 unfolding Field_def by fast
  moreover have "... \<subseteq> Field ?or2" using mm01k 1 unfolding reflex_def by blast
  ultimately moreover have "Field ?c \<subseteq> Field ?or" unfolding 2 by auto
  ultimately have 
  3: "?c \<in> conflictsFor ?or" unfolding conflictsFor_def by force
  let ?Y="C``{m}" let ?U="Field Or" let ?cOr = "order2strictCover Or" have 
  7: "Range C \<subseteq> Domain Or" using reflex_def "1" Field_def Un_upper2 dual_order.trans les2.lm31 by (metis )
  then have "?Y = Or``?Y" using lm07b 1 reflex_def by metis
  then have 
  5: "?Y \<supseteq> ?or``?Y" using  1 by fast 
  moreover have "?Y \<subseteq> Range C - {m}" using 1 irrefl_def by fastforce
  ultimately have "?Y \<subseteq> Range C - {m}" using 6 1 Range_def Field_def irrefl_def by force then have 
  "?Y \<subseteq> Domain Or - {m}" using 7 by blast then have 
9: "?Y \<subseteq> Field Or - {m}" using 1 les2.lm31 unfolding reflex_def by blast moreover have 
   "... = Field ?or2" using 1 mm01k reflex_def by metis ultimately have "?Y \<subseteq> Field ?or" 
   using 2 by simp then have "?Y \<subseteq> Domain ?or" using reflex_def 8mm01f les2.lm31 
   unfolding reflex_def by (simp add: "8" les2.lm31) 
   then have "?Y \<subseteq> ?or `` ?Y" using 8 mm01l unfolding reflex_def by (simp add: mm01l)
then have 
10: "?Y = ?or``?Y" using 5 by fast
then have "?Y \<subseteq> ?U" using Diff_subset 9 dual_order.trans by (metis) 
  then have 
  12: "?Y \<subseteq> (?U - (?U - \<Inter>{C``{M}|M. M \<in> ?cOr``{m}}))" using 1 mm02 unfolding order2strictCover_def next1_def by blast
  have "\<forall>M\<in>?cOr``{m}. C``{M} \<subseteq> ?c``{M}" using 1 mm04d by metis 
  then
  have "\<Inter>{C``{M}|M. M \<in> ?cOr``{m}} \<subseteq> \<Inter>{?c``{M}|M. M \<in> ?cOr``{m}}" by blast
  then have "?Y \<in>Pow (?U - (?U - \<Inter>{?c``{M}|M. M \<in> ?cOr``{m}}))" using 12 by blast
  then have "?or``?Y \<in> (Image ?or)`Pow(?U - (?U - \<Inter>{?c``{M}|M. M \<in> ?cOr``{m}}))"  
by force then have 
  11: "?Y \<in> (Image ?or)`Pow(?U - (?U - \<Inter>{?c``{M}|M. M \<in> ?cOr``{m}}))" using 10 by simp
  moreover have "C = ?c \<union> {m}\<times>?Y \<union> (C^-1``{m})\<times>{m}" by fast moreover have 
  "C=C^-1" using 1 sym_def by fast ultimately moreover have "C=?c \<union> {m}\<times>?Y \<union> ?Y\<times>{m}" by auto
  ultimately have "C\<in>generateConflictsSet Or m ?c & ?c \<in> conflictsFor ?or" using 3
  unfolding generateConflictsSet_def 
  by auto
  }
then have "\<forall> C\<in>?R. C\<in>generateConflictsSet Or m (C - {m}\<times> UNIV - UNIV\<times>{m}) & 
C - {m}\<times> UNIV - UNIV\<times>{m} \<in> conflictsFor ?or" by meson
thus ?thesis using conflictsFor_def by auto
qed

lemma mm06b: assumes "m \<notin> Range (Or-{(m,m)})" "c\<in>conflictsFor (Or-({m}\<times>UNIV)-(UNIV\<times>{m}))" "trans Or" 
"reflex Or" "antisym Or" "C\<in>generateConflictsSet Or m c" "m \<in> Domain Or" "wf (strict Or)"
shows "C\<in> conflictsFor Or"  
proof -
let ?or="Or-({m}\<times>UNIV)-(UNIV\<times>{m})" let ?U="Field Or" let ?op=order2strictCover let ?cOr="?op Or" have 
3: "isLes ?or c & Field c \<subseteq> Field ?or" using assms(2) unfolding conflictsFor_def by fast obtain Y where 
0: "Y\<in>(Image ?or)`Pow (?U - (?U - \<Inter>{c``{M}| M. M\<in>?cOr``{m}})) & C=c \<union> {m}\<times>Y \<union> Y\<times>{m}"
using assms(6) unfolding generateConflictsSet_def by auto let ?C="c \<union> ({m}\<times>Y \<union> Y\<times>{m})" have 
1: "Y\<in>(Image ?or)`Pow (?U - (?U - \<Inter>{c``{M}| M. M\<in>?cOr``{m}}))" using 0 by blast have
2: "C=c \<union> {m}\<times>Y \<union> Y\<times>{m}" using 0 by blast moreover have "sym c & sym ({m}\<times>Y \<union> Y\<times>{m})" using 3 
unfolding sym_def by fastforce ultimately have 
15: "sym C" using 0 by (simp add: sym_Un Un_assoc) have 
7: "m \<notin> Y" using 0 by auto then have "irrefl ({m}\<times>Y \<union> Y\<times>{m})" using irrefl_def by blast then have 
14: "irrefl C" using 3 0 by (simp add: irrefl_def Un_assoc) obtain X where 
4: "Y=?or``X & X \<subseteq>?U-(?U-\<Inter>{c``{M}|M. M\<in>?cOr``{m}})" using 0 by blast 
{
  fix x1 x2 y assume "(x1,x2)\<in>?or" moreover assume "(x1,y) \<in> ?C"
  ultimately moreover have "x1 = m \<longrightarrow>(x2,y)\<in>?C" using 0 1 2 3  by auto
  ultimately moreover have "x2=m \<longrightarrow> (x2,y)\<in>?C" using 0 1 2 3  by simp
  ultimately moreover have "((x1,x2)\<in>?or & x1\<in>Y) \<longrightarrow> x2\<in>Y" using 3 4 by (meson ImageE ImageI trans_def)
  ultimately moreover have "((x1,x2)\<in>?or & x1\<in>Y) \<longrightarrow> (x2,m)\<in>?C" by simp ultimately moreover have 
  "(x1 \<noteq> m & x2 \<noteq>m & y \<noteq> m) \<longrightarrow> (x2,y)\<in>?C" by (smt "3" DiffE Image_singleton_iff UNIV_I Un_iff mem_Sigma_iff subsetCE)
  ultimately have "(x2,y)\<in>?C" by (smt "3" DiffE Image_singleton_iff UNIV_I Un_iff mem_Sigma_iff subsetCE)
  }
then have 
  5: "isPropagatingOver4' C ?or" using 0 Un_assoc by auto then have
  6: "isMonotonicOver C ?or" using les.lm33a by blast have 
  10: "m \<notin> Y & Field ?or \<subseteq> Field Or" using Diff_subset 7 mono_Field subset_eq by (metis (no_types, lifting)) have 
  9: "\<forall> x z. (x,z)\<in>Or & x\<noteq>m \<longrightarrow> C``{x} \<subseteq> C``{z}" using 6 7 assms(1) by blast
  have "\<forall>X. c``X \<subseteq> ?U" using 3 10 FieldI2 ImageE subset_iff by (metis (no_types, lifting)) then 
  have "{c``{M}|M. M\<in>?cOr``{m}} \<noteq> {} \<longrightarrow> \<Inter>{c``{M}|M. M\<in>?cOr``{m}} \<subseteq> ?U" by blast then have
  "{c``{M}|M. M\<in>?cOr``{m}}\<noteq>{}\<longrightarrow>(?U-(?U-\<Inter>{c``{M}|M. M\<in>?cOr``{m}})=\<Inter>{c``{M}|M. M\<in>?cOr``{m}})" by auto
  have "m \<notin> Field ?or" using 3 DiffE SigmaI UNIV_I refl_onD singletonI unfolding reflex_def 
  by (metis(no_types,lifting)) then have 
  11: "m \<notin> Field ?or & m \<notin> Field c & m \<notin> Domain c" using 3 unfolding Field_def by blast have 
  12: "X \<subseteq>\<Inter>{c``{M}|M. M\<in>?cOr``{m}}" using 4 DiffE DiffI subsetCE subsetI by blast
{
  fix z assume "(m,z)\<in>Or" moreover assume "z\<noteq>m" ultimately moreover obtain y where 
  "(y,z) \<in> Or & y\<in>next1 Or {m}" using assms(3,4,5,8) mm05l unfolding next1_def by fast then moreover have
  8: "(y,z) \<in> Or & (m,y)\<in>?cOr" unfolding order2strictCover_def using assms(7) 0 1 3 by auto
  then have "X\<subseteq>c``{y}" using 12 by blast then have "Y \<subseteq> ?or``(c``{y})" using 4 by auto
  moreover have "?or``(c``{y}) \<subseteq> c``{y}" using 3 by (simp add: lm07a)
  ultimately moreover have "Y \<subseteq> c``{y}" by simp moreover have "C``{m} \<subseteq> Y" using 0 11 by auto  
  ultimately have "C``{m} \<subseteq> c``{y}" by simp also have
  "y \<in> next1 Or {m}" using 8 unfolding order2strictCover_def by fast then 
  have "(y,z) \<in> ?or" using assms(1) 8 unfolding next1_def by auto then
  have "c``{y} \<subseteq> c``{z}" using 3 by presburger then have "c``{y} \<subseteq> C``{z}" using 0 by auto
  ultimately have "C``{m} \<subseteq> C``{z}" by simp
}
  then have "\<forall>z. (m,z) \<in> Or \<longrightarrow> C``{m} \<subseteq> C``{z}" by blast then have
  "isMonotonicOver C Or" using 9 by blast then have 
  13: "isLes Or C" by (meson `irrefl C` `sym C` assms(3,4,5) reflex_def)
  have "Y \<subseteq> Range ?or" using 0 by fast moreover have 
  "Domain C = (Domain c) \<union> Y \<union> (Domain ({m}\<times>Y))" using assms(3) 0 by fast
  then moreover have "Domain C \<subseteq> (Domain c) \<union> Y \<union> {m}" by blast moreover have "Domain c \<subseteq> Domain Or"
  using 10 3 Field_def UnI1 contra_subsetD les2.lm31 subsetI 13 14 15  unfolding reflex_def by (metis)
  ultimately have "Domain C \<subseteq> {m} \<union> (Domain Or) \<union> Range ?or" using Un_iff subsetCE subsetI by blast 
  then have "Domain C \<subseteq> Domain Or \<union> Range ?or" using assms(7) by auto
  then have "Domain C \<subseteq> Field Or" using Field_def 10 Diff_eq_empty_iff Un_Diff_cancel 
  Un_upper2 dual_order.trans les2.lm31 13 unfolding reflex_def by (metis (no_types, lifting))
  moreover have "Domain C = Field C" using Field_def `sym C` sym_conv_converse_eq by fastforce
  ultimately show ?thesis using 13 unfolding conflictsFor_def by fastforce
qed

lemma mm06c: assumes "m \<notin> Range (Or-{(m,m)})"  "trans Or" 
"reflex Or" "antisym Or" "m \<in> Domain Or" "wf (strict Or)" shows
"\<Union> {generateConflictsSet Or m c | c. c\<in>conflictsFor (Or - ({m}\<times>UNIV) - (UNIV\<times>{m}))}
\<subseteq> conflictsFor Or" (is "?L \<subseteq> ?R")
proof - let ?G="generateConflictsSet" let ?C="conflictsFor" let ?or="Or-{m}\<times>UNIV-UNIV\<times>{m}"
{
  fix C assume "C\<in>?L" then obtain c where 
  0: "C\<in>?G Or m c & c\<in>?C ?or" by blast then have "C\<in>?R" using assms mm06b by metis
  }
thus ?thesis by blast
qed

theorem mm06d: assumes "m \<notin> Range (Or-{(m,m)})" "trans Or" "reflex Or" "antisym Or" 
"m \<in> Domain Or" "wf (strict Or)" shows 
"\<Union> {generateConflictsSet Or m c | c. c\<in>conflictsFor(Or-({m}\<times>UNIV)-(UNIV\<times>{m}))}=conflictsFor Or"
(is "?L=?R") proof - have "?L \<subseteq> ?R" using assms mm06c by force 
moreover have "?L \<supseteq> ?R" using mm06a by fast ultimately show ?thesis by blast qed

corollary mm06h: assumes "m \<notin> Range (Or-{(m,m)})" "trans Or" "reflex Or" "antisym Or" 
"m \<in> Domain Or" "finite (strict Or)" shows 
"\<Union> {generateConflictsSet Or m c | c. c\<in>conflictsFor(Or-({m}\<times>UNIV)-(UNIV\<times>{m}))}=conflictsFor Or"
proof -
have 6: "wf (strict Or)" using assms strict_def by (simp add: strict_def finite_acyclic_wf mm05d)
show ?thesis using assms(1,2,3,4,5) 6 by (rule mm06d)
qed

lemma mm05u: "{}\<in>conflictsFor' {}" using irreflI sym_conv_converse_eq transI unfolding reflex_def by fastforce

lemma mm05v: assumes "C\<noteq>{}" shows "C\<notin>conflictsFor' {}" using assms unfolding Field_def by fast

lemma mm05w: "conflictsFor {}={{}}" using mm05u mm05v unfolding conflictsFor_def by auto


abbreviation "esOver' X=={(or, C)| or C. or\<in>posOver X & C\<in>conflictsFor or}"
abbreviation "esOver2' X==\<Union>{{or}\<times>(conflictsFor or) | or. or \<in> posOver X}"
definition "esOver=esOver'" definition "esOver2 = esOver2'"

lemma mm10f: assumes "isPo' or" shows "{}\<in>conflictsFor' or" using assms irreflI sym_conv_converse_eq transI
unfolding reflex_def Field_def antisym_def trans_def sym_def irrefl_def by blast

lemma mm10ff: "\<forall>or\<in>posOver X. {}\<in>conflictsFor or" using assms mm10f 
unfolding isPo_def posOver_def conflictsFor_def by blast

lemma mm10g: "{{or}\<times>(conflictsFor or) | or. or \<in> posOver X} partitions (esOver2 X)" using mm10ff
unfolding is_non_overlapping_def is_partition_of_def esOver2_def by fastforce

lemma mm10h: assumes "finite (esOver2 X)" shows "setsum f (esOver2 X) = setsum (setsum f)
{{or}\<times>(conflictsFor or) | or. or \<in> posOver X}" using assms setsum_associativity mm10g by blast

lemma mm10i: assumes "finite (esOver2 X)" shows 
"card (esOver2 X) = setsum card {{or}\<times>(conflictsFor or) | or. or \<in> posOver X}" 
proof-
let ?f="%x. 1" have "setsum ?f (esOver2 X) = setsum (setsum ?f) 
{{or}\<times>(conflictsFor or) | or. or \<in> posOver X}"
using assms mm10h by blast moreover have "...=setsum card {{or}\<times>(conflictsFor or) | or. or \<in> posOver X}"
by simp ultimately show ?thesis using assms card_eq_setsum by (metis(no_types))
qed

lemma mm20a: assumes "finite or" shows "finite (conflictsFor or)"
proof -
have "conflictsFor' or \<subseteq> Pow (Field or \<times> Field or)" using Field_def by fast
thus ?thesis using assms finite_Pow_iff finite_SigmaI finite_subset mm10j
unfolding conflictsFor_def by (metis(no_types,lifting))
qed

lemma mm21: assumes "finite A" shows "finite (esOver2' A)" using assms mm10l mm20a mm20b
proof -
have "finite (posOver A)"
unfolding mm10l using assms mm20b by force
moreover have "\<forall>or\<in>posOver A. finite or" unfolding mm10l using assms mm20b by force
then moreover have "\<forall> or\<in>posOver A. finite (conflictsFor or)" using mm20a by blast
ultimately show ?thesis by fastforce
qed

lemma mm21b: "esOver2 A=esOver A" unfolding esOver_def esOver2_def by blast























subsection {* computable definitions *}

abbreviation "generateConflicts' Or m c ==  
[c\<union>({m}\<times>Y)\<union>(Y\<times>{m}). Y <- map (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m})
(map set (sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}}))))]"
definition "generateConflicts=generateConflicts'"

lemma mm07b: assumes "l=(sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}})))" shows "set (generateConflicts' Or m c) = 
{c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m})`
set (map set l)}" (is "?L=?R") using assms mm07 
proof -
have "?L=set [c\<union>({m}\<times>Y)\<union>(Y\<times>{m}). Y <- map (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m}) (map set l)]" using 
assms by simp moreover have "...=?R" using mm07 by metis ultimately show ?thesis by presburger
qed

lemma mm07d: assumes "trans Or" "reflex Or" "antisym Or" "m \<in> Domain Or" "Range c \<subseteq> Field Or" 
"finite Or" shows "set (generateConflicts Or m c) = generateConflictsSet Or m c"  
proof -
let ?L="set (generateConflicts' Or m c)" let ?R="generateConflictsSet' Or m c"
have "finite (strict Or)" using assms(6) unfolding strict_def by blast then have 
0: "wf (strict Or)" using assms(1,3) mm05k by blast have
1: "finite (Domain Or)" using assms(6) by (simp add: finite_Domain)
let ?l="sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}}))" let ?U="Field Or"
let ?ll="sublists (if next1 Or {m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}}))" 
have "?l=?l" by auto then have "?L = {c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m})`
(set (map set ?l))}" by (rule mm07b) 
moreover have "next1 Or {m} \<noteq> {} \<longrightarrow>  \<Inter> {c``{M}|M. M \<in> next1 Or {m}} \<subseteq> Field Or" using assms(5) by auto
moreover have "Domain Or = Field Or" using assms(2) reflex_def by (simp add: reflex_def les2.lm31)
ultimately moreover have "set`(set (sublists (sorted_list_of_set (Domain Or)))) = Pow( Domain Or)"
using assms 0 1 sublists_powset by (simp add: sublists_powset)
ultimately moreover have "next1 Or {m} \<noteq> {} \<longrightarrow>set`(set(sublists(sorted_list_of_set(\<Inter> {c``{M}|M. M \<in> next1 Or {m}}))))= 
Pow (\<Inter> {c``{M}|M. M \<in> next1 Or {m}})" using assms(6) 1 MiscTools.lm64 rev_finite_subset 
sublists_powset by (metis (no_types, lifting)) ultimately moreover have "next1 Or {m}={}\<longrightarrow>
Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> next1 Or {m}}))= set`(set ?ll)" by auto
ultimately moreover  have "next1 Or {m} \<noteq> {} \<longrightarrow>
Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> next1 Or {m}}) = (\<Inter> {c``{M}|M. M \<in> next1 Or {m}})" 
 by (simp add: double_diff) ultimately moreover have 
"Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> next1 Or {m}})) = set`(set ?ll)" by force
ultimately moreover have "(order2strictCover' Or)``{m}=next1 Or {m}" using assms(4) by auto 
moreover have "?l=?ll" using assms(1,2,3) 0 mm05m unfolding next1_def by fastforce 
ultimately have "?L = {c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m})`
Pow(Field Or - (Field Or - \<Inter> {c``{M}|M. M \<in> (order2strictCover Or)``{m}}))}" 
unfolding order2strictCover_def by force moreover have 
"{z\<in>Or. fst z\<noteq>m & snd z\<noteq>m} = Or-{m}\<times>UNIV-UNIV\<times>{m}" by force 
ultimately show ?thesis unfolding generateConflictsSet_def generateConflicts_def by force
qed

lemma mm07bb: assumes "l=(sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}})))" shows "set (generateConflicts Or m c) = 
{c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image {z\<in>Or. fst z\<noteq>m & snd z\<noteq>m})`set (map set l)}"  
unfolding generateConflicts_def using assms by (rule mm07b)

lemma mm16e: "remdups (generateConflicts Or m c)\<noteq>[]" using mm07bb mm16c mm16d  
proof -
let ?l="(if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) 
else sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}}))" let ?or="{z\<in>Or. fst z\<noteq>m & snd z\<noteq>m}"
let ?G=generateConflicts obtain L where 
0: "L=sublists ?l" by blast have "set (map set L)\<noteq>{}" using 0 mm16c by fast then have 
"(Image ?or)`set (map set L)\<noteq>{}" using mm16d by blast moreover have "set (?G Or m c)=
{c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image ?or)`set (map set L)}" using 0 by (rule mm07bb) 
ultimately show ?thesis by fastforce
qed






















(*
abbreviation "ReducedRelation' arrayStrictPartialOrder == 
(let m = hd (filterpositions2 (%x. set x = {False}) arrayStrictPartialOrder) in 
(m, filter (% (x,y). x \<noteq> m) (adj2PairList arrayStrictPartialOrder)))"
definition "ReducedRelation = ReducedRelation'" 
*)
abbreviation "ReducedRelationV0' P ==let min = List.find (%x. x \<notin> (snd`((set P)-{(x,x)}))) (map fst P)
in (min, filter (%(x,y). x \<noteq> the min & y \<noteq> the min) P)"

definition "ReducedRelationV0 = ReducedRelationV0'"

lemma lm06: assumes "(Some ya, y) = ReducedRelationV0 pOrder" shows "length y < length pOrder"
proof -
have "ya \<in> set (map fst pOrder) & ya \<notin> (snd`((set pOrder)-{(ya,ya)}))" using assms 
ReducedRelationV0_def find_Some_iff Pair_inject nth_mem by smt
then moreover have "y=filter (%(x,y). x \<noteq> ya & y \<noteq> ya) pOrder" using assms ReducedRelationV0_def 
by (smt case_prod_unfold filter_cong fst_conv option.sel snd_conv)
ultimately show ?thesis using assms ReducedRelationV0_def 
by (smt Pair_inject case_prodE imageE length_filter_less list.set_map prod.collapse)
qed

lemma lm07: assumes "(ReducedRelationV0' Rel) = (M,list)" "m=the M" shows 
"set list = (set Rel)-({m} \<times> UNIV)-(UNIV \<times> {m})" using assms(1,2) apply auto
using mem_Collect_eq set_filter snd_conv apply (metis (no_types, lifting))
using mem_Collect_eq prod.sel(1) prod.simps(2) set_filter snd_conv apply (metis (mono_tags, lifting))
using Pair_inject mem_Collect_eq old.prod.case set_filter apply (metis (mono_tags, lifting))
using mem_Collect_eq prod.sel(1) prod.simps(2) set_filter snd_conv by (metis(mono_tags,lifting))

lemma lm07d: assumes "(ReducedRelationV0' Rel) = (M,list)" "m=the M" "M \<noteq> None" shows
"m \<in> Domain (set Rel) - (Range (set Rel - {(m,m)}))"
proof -
have "m \<in> Domain (set Rel)" using assms apply auto 
using Domain_fst Pair_inject find_Some_iff nth_mem set_map by (metis (no_types, lifting))
then moreover have "m \<notin> Range (set Rel - {(m,m)})" 
using assms apply auto 
using Pair_inject find_Some_iff DiffE DiffI Diff_empty image_eqI insert_iff snd_conv 
by (metis(no_types,lifting)) ultimately show ?thesis by blast
qed

abbreviation "conflictsIterator' f F Arg == (let (M,or)=f Arg in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Arg) m c). c <- F or])"
definition "conflictsIterator = conflictsIterator'"

function conflictsFor1 where "conflictsFor1 Or=(let (M,or)=ReducedRelationV0 Or in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor1 or])" 
apply simp apply simp by blast
termination apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 
by (smt ReducedRelationV0_def case_prod_unfold filter_cong option.sel prod.sel(1))

lemma mm07c: assumes "(M,or)=ReducedRelationV0 Or" "M\<noteq>None" shows 
"set (conflictsFor1 Or)=\<Union>{set (generateConflicts (set Or) (the M) c)| c. c\<in>set(conflictsFor1 or)}"
proof-
have "conflictsFor1 Or=(let (M,or)=ReducedRelationV0 Or in if M=None then [{}] 
else let m=the M in concat [remdups (generateConflicts (set Or) m c). c <- conflictsFor1 or])" by simp
moreover have "...=concat [remdups (generateConflicts (set Or) (the M) c). c <- conflictsFor1 or]"
using assms old.prod.case by (smt map_eq_conv) 
moreover have "...=concat (map (%x. remdups (generateConflicts (set Or) (the M) x)) (conflictsFor1 or))"
by blast ultimately have "set (conflictsFor1 Or) = 
  (set o concat) (map (generateConflicts (set Or) (the M)) (conflictsFor1 or))" by simp moreover have 
"... = Union ((set o (generateConflicts (set Or) (the M)))`(set (conflictsFor1 or)))" by auto
moreover have "{set (generateConflicts (set Or) (the M) c)| c. c\<in>set (conflictsFor1 or)}=
set`((generateConflicts (set Or) (the M))`(set (conflictsFor1 or)))"by blast
moreover have "...=(set o (generateConflicts (set Or) (the M)))`(set (conflictsFor1 or))" 
by fastforce ultimately show ?thesis by presburger
qed

lemma mm05p: assumes  "(M,or)=ReducedRelationV0 Or" shows 
"(M=None) = ((Domain (set Or))-Range (strict (set Or))={})"
proof -
let ?f="(%x. x \<notin> (snd`((set Or)-{(x,x)})))" let ?ff="\<lambda>x. x\<notin>Range (strict' (set Or))"
let ?fff="\<lambda>x. x\<notin>Range (strict (set Or))" have "?ff=?f" by force then have 
0: "M=List.find ?ff (map fst Or)" using assms unfolding ReducedRelationV0_def 
by (metis (no_types, lifting) Pair_inject find_cong) 
then have "M=None = (True \<notin> ?ff`(set (map fst Or)))" by (simp add: mm05o)
moreover have "?ff`(set (map fst Or))=?ff`(Domain (set Or))" by force moreover have
"\<forall>X. ((True \<in> ?fff`X)=(X  - (Range (strict (set Or))) \<noteq> {}))" by blast
ultimately show ?thesis unfolding strict_def by presburger
qed
lemma mm05r: assumes "List.find f l \<noteq> None" shows "the (List.find f l) \<in> set l
& f (the (List.find f l))" using assms 
by (metis find_Some_iff nth_mem option.collapse)

(*overlaps with lm07d*)
lemma mm05q: assumes "finite (strict (set P))" "trans (set P)" "antisym (set P)"
"P\<noteq>[]" "(M,p)=ReducedRelationV0 P" shows "M\<noteq>None & the M \<in> Domain (set P) - Range (strict (set P))"
proof-
let ?P="set P" let ?PP="strict ?P" let ?m="the M" have "Domain ?P\<noteq>{}" using assms(4) by (simp add: Domain_empty_iff) 
then have "?PP={} \<longrightarrow> Domain ?P - Range ?PP \<noteq>{}" by simp then have 
1: "?PP={} \<longrightarrow> M\<noteq>None" using assms mm05p by blast have "wf ?PP" using assms mm05k by blast 
then have "?PP\<noteq>{} \<longrightarrow> Domain ?PP - Range ?PP \<noteq>{}" using mm05n by auto moreover have 
"Domain ?PP - Range ?PP \<noteq> {} \<longrightarrow> M\<noteq>None" using assms mm05p unfolding strict_def by blast 
ultimately moreover have "M\<noteq>None" using 1 by fast ultimately moreover have "the M\<in>set(map fst P)" 
using assms mm05r by (metis (no_types, lifting) ReducedRelationV0_def fst_conv) ultimately have
2: "M\<noteq>None & the M \<in> Domain (set P)" by auto
{ assume "?m \<in> Range ?PP"
  then obtain x where "(x,?m)\<in>?PP" by blast moreover have "?m \<notin> snd`(?P-{(?m,?m)})" 
  unfolding ReducedRelationV0_def
  using 2 mm05r
by (metis (no_types, lifting) ReducedRelationV0_def assms(5) prod.sel(1))
  then moreover have "?m \<notin> Range (?P-{(?m,?m)})" by force
  moreover have "(?P-{(?m,?m)}) \<supseteq> ?PP" unfolding strict_def by blast
  ultimately have False by blast
  }
then show ?thesis using 2 by blast
qed

lemma mm05t: assumes "(M,or)=ReducedRelationV0 []" shows "or=[] & M=None" using assms ReducedRelationV0_def
filter.simps(1) snd_conv by (smt find.simps(1) fst_conv list.map_disc_iff)

lemma mm05x: "conflictsFor1 []=[{}]" using  mm05t by (simp add: ReducedRelationV0_def)

lemma mm05s: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)"  
"\<forall>c\<in>set(conflictsFor1 or). Range c \<subseteq> Field (set Or)" 
"(M,or)=ReducedRelationV0 Or" "Or\<noteq>[]" shows 
"set (conflictsFor1 Or)=\<Union>{generateConflictsSet (set Or) (the M) c| c. c\<in>set(conflictsFor1 or)}"
proof -
let ?m="the M" let ?O="set Or" let ?OO="strict ?O" 
let ?g3="generateConflictsSet ?O ?m" let ?g5="generateConflicts ?O ?m"
let ?r3="{?g3  c| c. c\<in>set(conflictsFor1 or)}" let ?r5="{set (?g5 c)| c. c\<in>set(conflictsFor1 or)}" 
have "finite ?OO" unfolding strict_def by simp then moreover have 
0: "M\<noteq>None & ?m \<in> Domain ?O" using assms mm05q by blast moreover have 
"finite (set Or)" by blast ultimately have 
1: "?r5=?r3" using assms(1,2,3,4) mm07d by (metis (no_types, lifting)) have
2: "M\<noteq>None" using 0 by blast have "set (conflictsFor1 Or)=\<Union> ?r5" using assms(5) 2 by (rule mm07c)
then show ?thesis using 1 by presburger
qed

lemma mm06e: assumes "(M,or)=ReducedRelationV0 Or" "trans (set Or)" shows "trans (set or)"
using assms(1,2) trans_def mem_Collect_eq old.prod.case set_filter snd_conv
unfolding ReducedRelationV0_def by (metis(no_types,lifting))

lemma mm06f: assumes "(M,or)=ReducedRelationV0 Or" "antisym (set Or)" shows "antisym (set or)" 
using assms antisym_def mem_Collect_eq set_filter snd_conv 
unfolding ReducedRelationV0_def by (metis(no_types,lifting))

lemma mm06g: assumes "(M,or)=ReducedRelationV0 Or" "reflex (set Or)" shows "reflex (set or)" 
using assms reflex_def mem_Collect_eq set_filter snd_conv les.lm16 mm01f
unfolding ReducedRelationV0_def by (metis(no_types,lifting))

lemma mm05y: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)"  
"\<forall>c\<in>set(conflictsFor1 or). Range c \<subseteq> Field (set Or)" 
"(M,or)=ReducedRelationV0 Or" shows 
"set (conflictsFor1 Or)=\<Union>{generateConflictsSet (set Or) (the M) c| c. c\<in>set(conflictsFor1 or)}"
(is "?L=?R") proof - have 
0: "set (conflictsFor1 [])={{}}" by (metis empty_set list.simps(15) mm05x)
{
  assume "Or=[]" then moreover have "or=[]" using assms(5) mm05t by blast then moreover have 
  "set (conflictsFor1 or)={{}}" using 0 by blast moreover have 
  "generateConflictsSet {} (the M) {}={{}}" unfolding generateConflictsSet_def by auto moreover have 
  "\<Union>{{{}}} = {{}}" by simp ultimately have "?R={{}}" by auto
} then have "Or=[] \<longrightarrow> ?R={{}}" by blast then have 
1: "Or=[] \<longrightarrow> ?thesis" using 0 by force
{assume 6: "Or\<noteq>[]" have ?thesis using assms(1,2,3,4,5) 6 by (rule mm05s)} 
then have "Or\<noteq>[] \<longrightarrow> ?thesis" by blast thus ?thesis using 1 by force
qed

lemma mm08b: assumes "size Or=N+1" "p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor1 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" "\<forall> Or. size Or \<le> N \<longrightarrow> P Or" "p (set Or)" shows "q Or"
proof -
let ?p="\<lambda> or. trans or & reflex or & antisym or" let ?c="conflictsFor" let ?c2="conflictsFor1"
let ?q="\<lambda> Or. (set (?c2 Or) = ?c (set Or))" let ?P="\<lambda> Or. (?p (set Or) \<longrightarrow> ?q Or)"
let ?r="ReducedRelationV0 Or" let ?or="snd ?r" let ?M="fst ?r" let ?m="the ?M" let ?O="set Or"
let ?OO="strict ?O" have "Or\<noteq>[]" using assms by force moreover have 
0: "(?M,?or)=?r" by simp moreover have 
3: "finite ?OO" unfolding strict_def by simp ultimately have 
1: "?M\<noteq>None & ?m \<in> Domain ?O & ?M=Some ?m" using assms mm05q by (smt DiffD1 option.exhaust_sel) then have
25: "?m \<in> Domain ?O" by blast have "?p ?O" using assms by fast then have "?p (set ?or)" using 
mm06e mm06f mm06g 0 by blast moreover have "size ?or < N+1" using assms 0 1 lm06 by force ultimately have 
2: "?q ?or" using assms by auto moreover have "\<forall>c \<in> ?c (set ?or). Range c \<subseteq> Field (set ?or)" 
unfolding conflictsFor_def using FieldI2 by fastforce ultimately have 
"\<forall>c \<in> set (?c2 ?or). Range c \<subseteq> Field (set ?or)" by presburger moreover have 
"Field (set ?or) \<subseteq> Field ?O" using Domain_fst Range_snd Un_mono filter_is_subset image_mono snd_eqD 
unfolding Field_def ReducedRelationV0_def by (metis (no_types, lifting)) ultimately have 
4: "\<forall>c \<in> set (?c2 ?or). Range c \<subseteq> Field ?O" by fast have 
11: "trans ?O" using assms by meson have
12: "reflex ?O" using assms by blast have
13: "antisym ?O" using assms by blast have 
"set (?c2 Or)=\<Union>{generateConflictsSet ?O ?m c| c. c\<in>set(?c2 ?or)}" using 11 12 13 4 0 by (rule mm05y)
moreover have "...=\<Union>{generateConflictsSet ?O ?m c| c. c\<in>?c (set ?or)}" using 2 by presburger
moreover have "set ?or=?O-{?m}\<times>UNIV-UNIV\<times>{?m}" using 0 lm07 ReducedRelationV0_def by (smt Sigma_cong)
ultimately have
5: "set (?c2 Or) = \<Union>{generateConflictsSet ?O ?m c| c. c\<in>?c (?O-{?m}\<times>UNIV-UNIV\<times>{?m})}" by presburger
have "?m\<notin>Range(?OO)" using assms mm05q 3 using 0 `Or \<noteq> []` by blast then have 
21: "?m \<notin> Range (?O-{(?m,?m)})" unfolding strict_def by blast
have "\<Union> {generateConflictsSet ?O ?m c | c. c\<in>?c(?O-({?m}\<times>UNIV)-(UNIV\<times>{?m}))}=?c ?O"
using 21 11 12 13 25 3 by (rule mm06h) then show ?thesis using 5 assms(3) by presburger
qed

lemma mm08bb: assumes "p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor1 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" "Q=(\<lambda>n. \<forall> Or. size Or=n \<longrightarrow> P Or)"
shows "\<forall> N. ((\<forall> n\<le>N. Q n) \<longrightarrow> Q (Suc N))" using assms mm08b 
proof -
{
  fix N assume "\<forall> n\<le>N. Q n" then have 
  "\<forall> n\<le>N. (\<forall>Or. size Or = n \<longrightarrow> P Or)" unfolding assms(4) 
  using assms by blast then have  "\<forall> Or. size Or \<le> N \<longrightarrow> P Or" by blast
  then have "\<forall> Or. ((size Or=N+1 & p (set Or)) \<longrightarrow> q Or)" using assms mm08b by blast
  then have "Q (Suc N)" using assms by simp
  }
thus ?thesis by blast
qed

lemma mm08c: assumes 
"p=(\<lambda> or. trans or & reflex or & antisym or)" 
"q=(\<lambda> Or. set (conflictsFor1 Or) = conflictsFor (set Or))"
"P=(\<lambda> Or. p (set Or) \<longrightarrow> q Or )" 
"Q=(\<lambda>n. \<forall> Or. size Or=n \<longrightarrow> P Or)"
shows "\<forall> N. Q N"
proof -
have "Q 0" using assms mm05w mm05x lm02 injections_alg.simps(1) length_0_conv set_empty 
by (metis (no_types)) moreover have "\<forall> N. ((\<forall> n\<le>N. Q n) \<longrightarrow> Q (Suc N))" using assms by (rule mm08bb) 
ultimately have "\<forall> N. Q N" using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust) thus ?thesis using assms by blast
qed

theorem conflictCorrectness: assumes "trans (set Or)" "reflex (set Or)" "antisym (set Or)" shows 
"set (conflictsFor1 Or)=conflictsFor (set Or)" 
proof -
let ?p="(\<lambda> (or::('a \<times> 'a) set). trans or & reflex or & antisym or)" 
let ?q="(\<lambda> (Or::('a \<times> 'a)list). set (conflictsFor1 Or) = conflictsFor (set Or))"
let ?P="(\<lambda> Or. ?p (set Or) \<longrightarrow> ?q Or)" let ?Q="(\<lambda>(n::nat). (\<forall> Or. (size Or=n \<longrightarrow> ?P Or)))"
obtain p where 1: "p=?p" by simp obtain q where 2: "q=?q" by presburger  obtain P where
3: "P=?P" by blast obtain Q where 4: "Q=?Q" by blast have "\<forall>N. Q N" using 1 2 3 4 mm08c by fast
thus ?thesis using assms 1 2 3 4 by blast
qed

lemma mm10u: assumes "N>(0::nat)" shows 
"set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N)))
partitions (esOver2 {0..<N})"
proof -
have "{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}=
(%or. {or}\<times>(conflictsFor or))`
{or. trans or & reflex or & antisym or & or \<in> posOver {0..<N}}"
unfolding posOver_def isPo_def by blast moreover have "...=(%or. {or}\<times>(conflictsFor or))`
{or. trans or & reflex or & antisym or & or \<in> set (map (set o adj2PairList) (allReflPartialOrders N))}" 
using assms mm10k by blast moreover have "...=
(%or. {or}\<times>(conflictsFor or))`{set Or|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by fastforce moreover have "...=
{{set Or}\<times>(conflictsFor (set Or))|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by auto moreover have "...=
{{set Or}\<times>(set (conflictsFor1 Or))|Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using conflictCorrectness by blast
moreover have "...=(%Or. {set Or}\<times>(set (conflictsFor1 Or)))`
{Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" by blast
moreover have "{Or. trans (set Or) & reflex (set Or) & antisym (set Or) &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))} = 
{Or. trans (set Or) & reflex (set Or) & antisym (set Or) & set Or\<in>posOver {0..<N} &
Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using assms mm10k
Collect_cong image_comp image_eqI list.set_map by (metis (no_types, lifting) ) moreover have "...=
{Or. set Or\<in>posOver {0..<N} & Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" 
unfolding posOver_def isPo_def mm13 by fast moreover have "...=
{Or. Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}" using assms mm10k 
by (metis (no_types, lifting) Collect_cong image_comp image_eqI list.set_map)
ultimately have "{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}=
(%Or. {set Or}\<times>(set (conflictsFor1 Or)))`{Or. Or \<in> set (map (adj2PairList) (allReflPartialOrders N))}"
by presburger moreover have "...= 
(%Or. {set Or}\<times>(set (conflictsFor1 Or)))`(set (map (adj2PairList) (allReflPartialOrders N)))" by blast 
moreover have "...=set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N)))" 
unfolding mm16 by presburger ultimately have  
0: "set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N)))=
{{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}}" by presburger moreover have 
"... partitions (esOver2 {0..<N})" by (rule mm10g) ultimately show ?thesis by presburger
qed

lemma mm10h2: assumes "N>0" "finite (esOver2 {0..<N})" shows "setsum f (esOver2 {0..<N}) = setsum (setsum f)
(set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N))))" 
using assms setsum_associativity mm10u by blast

lemma mm16g: assumes "(M,or)=ReducedRelationV0 Or" "M\<noteq>None" "conflictsFor1 or\<noteq>[]"
shows "conflictsFor1 Or\<noteq>[]" 
proof- let ?c="conflictsFor1" have "set (?c or)\<noteq>{}" using assms(3) by fastforce then obtain c where 
0: "c\<in>set (?c or)" using all_not_in_conv by blast then have "set (generateConflicts (set Or) (the M) c) \<subseteq>
\<Union>{set (generateConflicts (set Or) (the M) c)| c. c\<in>set(conflictsFor1 or)}" by blast
moreover have "...=set (conflictsFor1 Or)" using assms mm07c by blast ultimately show ?thesis 
using mm16e bot.extremum_unique remdups_eq_nil_iff set_empty by (metis (no_types, lifting))
qed

lemma mm16h: assumes "P=(%n. \<forall>(or::(('a::linorder \<times> 'a) list)). size or=n \<longrightarrow> conflictsFor1 or\<noteq>[])" 
"\<forall>n\<le>N. P n" shows "P (Suc N)"
proof - let ?r="ReducedRelationV0"
{
  fix Or::"(('a \<times> 'a) list)" let ?M="fst (?r Or)" let ?or="snd (?r Or)"
  let ?m="the ?M" have "?M=None \<longrightarrow> conflictsFor1 Or=[{}]" using ReducedRelationV0_def conflictsFor1.simps fst_conv prod.case by (smt)
  assume "size Or=(Suc N)" moreover assume "?M\<noteq>None" then moreover have "?M=Some ?m" by force 
  ultimately moreover have "size ?or \<le>N" using lm06 by (metis Suc_leI leD leI prod.collapse)
  then moreover have "conflictsFor1 ?or\<noteq>[]" using assms by blast ultimately have 
  "conflictsFor1 Or\<noteq>[]" using mm16g by (metis option.distinct(1) prod.collapse)
} then have "\<forall> Or::(('a \<times> 'a) list). 
((size Or=Suc N & fst (?r Or)\<noteq>None) \<longrightarrow> conflictsFor1 Or \<noteq>[])" by blast moreover have 
"\<forall> Or::(('a \<times> 'a) list). fst (?r Or)=None \<longrightarrow> conflictsFor1 Or=[{}]" using ReducedRelationV0_def conflictsFor1.simps fst_conv prod.case by (smt)
ultimately show ?thesis using assms by fastforce
qed

lemma mm16i: assumes "P=(%n. \<forall>(or::(('a::linorder \<times> 'a) list)). size or=n \<longrightarrow> conflictsFor1 or\<noteq>[])"
shows "\<forall>N. P N"
proof -
have "P 0" unfolding assms using  length_0_conv list.distinct(2) mm05x by (smt)
moreover have "\<forall> N. (\<forall>n\<le>N. P n)\<longrightarrow>P (Suc N)" using assms mm16h by auto
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

lemma mm16j: "conflictsFor1 Or\<noteq>[]" using mm16i by fast

lemma mm10i2: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = setsum card 
(set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N))))"
using assms mm10h2 card_eq_setsum by (smt setsum.cong)

lemma mm10i3: assumes "N>0" shows "distinct 
(map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N)))"
proof - let ?f="%x. {set x}" let ?g="%x. set (conflictsFor1 x)" let ?l="allReflPartialOrders N" 
let ?h="%x. (?f x)\<times>(?g x)" have 
0: "set ?l \<subseteq>{a| a. size a=N & isRectangular4 N a}" using assms mm10e by auto then have 
"inj_on adj2PairList (set(allReflPartialOrders N))" using assms mm14 using subset_inj_on by blast then have 
1: "distinct (map (adj2PairList) (allReflPartialOrders N))" using assms mm15b distinct_map by blast
have "inj_on adj2Pairs1 (set ?l)" using assms 0 lm04bb lm93b subset_inj_on by (metis(no_types))
then have "inj_on set ((adj2PairList)`(set ?l))" unfolding mm10m 
by (simp add: inj_on_imageI) then have "inj_on set (set (map (adj2PairList) ?l))" by fastforce
then have "inj_on ?f (set (map (adj2PairList) ?l))" using inj_onI inj_on_contraD singleton_inject 
by (metis (mono_tags, lifting)) 
then have "inj_on ?f (adj2PairList`(set ?l))" by fastforce
moreover have "{}\<notin>?g`(adj2PairList` (set ?l))" using mm16j by blast
ultimately have "inj_on ?h (adj2PairList` (set ?l))" using mm16b by blast
then have "inj_on ?h (set (map (adj2PairList) ?l))" by fastforce then show ?thesis using 1
distinct_map by blast
qed

lemma mm17b: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = listsum 
(map (card o set o conflictsFor1) (map (adj2PairList) (allReflPartialOrders N)))"
(is "?L=?R")
proof -
let ?l="map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (map (adj2PairList) (allReflPartialOrders N))"
let ?M="listsum 
(map (card o (%Or.{set Or}\<times>(set (conflictsFor1 Or)))) (map (adj2PairList) (allReflPartialOrders N)))"
have "?L=setsum card (set ?l)" using assms mm10i2 by blast moreover have
"distinct ?l" using assms mm10i3 by blast then moreover have 
"listsum (map card ?l)=setsum card (set ?l)" by (rule listsum_distinct_conv_setsum_set) ultimately have 
1: "?L=?M" by (metis (no_types, lifting) List.map.compositionality)
 have "card o (%Or. {set Or}\<times>(set (conflictsFor1 Or))) = 
card o (%Or. ((%x. {set x}) Or)\<times>((set o conflictsFor1) Or))" by auto
moreover have "... = card o (set o conflictsFor1)" by (rule mm11) ultimately have 
"card o (%Or. {set Or}\<times>(set (conflictsFor1 Or))) = card o (set o conflictsFor1)"
by (metis(no_types)) moreover have "...= card o set o conflictsFor1" by (simp add: fun.map_comp) ultimately have
0: "card o (%Or. {set Or}\<times>(set (conflictsFor1 Or))) = card o set o conflictsFor1" by (metis(no_types)) 
have "?M=?R" unfolding 0 by blast then show ?thesis using 1 by presburger
qed

theorem cardCorrectness: assumes "N>0" "finite (esOver2 {0..<N})" shows "card (esOver2 {0..<N}) = 
listsum (map (size o remdups o conflictsFor1 o adj2PairList) (allReflPartialOrders N))" (is "?L=?R")
proof -
let ?M="map (card o set o conflictsFor1) (map (adj2PairList) (allReflPartialOrders N))" have 
0: "card o set o conflictsFor1 = size o remdups o conflictsFor1" using mm17a fun.map_comp
by (metis(no_types)) have 
"?M=map (size o remdups o conflictsFor1) (map (adj2PairList) (allReflPartialOrders N))"
unfolding 0 by blast moreover have "?L=listsum ?M" using assms by (rule mm17b) ultimately have 
"?L=listsum (map (size o remdups o conflictsFor1) (map (adj2PairList) (allReflPartialOrders N)))"
by presburger moreover have "...=
listsum (map (size o remdups o conflictsFor1 o adj2PairList) (allReflPartialOrders N))" by auto
ultimately show ?thesis by presburger
qed






















section{*Distinctness of conflictsFor1 mm07bb ll41i quasiDistinctMap ll41c*}

lemma mm12: assumes "C\<in>set(generateConflicts Or m c)" shows "C\<subseteq>c\<union>({m}\<times>(Range Or))\<union>((Range Or)\<times>{m})"
proof-
let ?d="if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> next1 Or {m}})" let ?l="sublists ?d" 
let ?or="{z\<in>Or. fst z\<noteq>m & snd z\<noteq>m}" obtain l where "l=?l" by blast then have 
"set(generateConflicts Or m c)={c\<union>({m}\<times>Y)\<union>(Y\<times>{m})|Y. Y \<in> (Image ?or)`set (map set l)}"
by (rule mm07bb) then obtain Y where "C=c\<union>({m}\<times>Y)\<union>(Y\<times>{m}) & Y\<in>(Image ?or)`set (map set l)" 
using assms by fastforce then moreover have "Y\<subseteq>Range ?or" by blast moreover have 
"Range ?or \<subseteq> Range Or" by blast ultimately show ?thesis by blast
qed

lemma mm18: assumes "m\<notin>Field c1 \<union> Field c2" "C\<in>set (generateConflicts Or m c1)\<inter> set(generateConflicts Or m c2)"
shows "c1=c2"
proof - 
let ?l1="sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c1``{M}|M. M \<in> next1 Or {m}}))" 
let ?l2="sublists (if (Or``{m})-{m}={} then (sorted_list_of_set (Domain Or)) else 
sorted_list_of_set (\<Inter> {c2``{M}|M. M \<in> next1 Or {m}}))"
obtain l1 where "l1=?l1" by blast then obtain Y1 where 
1: "C=c1\<union>{m}\<times>Y1\<union>Y1\<times>{m}" using assms(2) mm07bb by force 
obtain l2 where "l2=?l2" by blast then obtain Y2 where 
2: "C=c2\<union>{m}\<times>Y2\<union>Y2\<times>{m}" using assms(2) mm07bb by force 
have "m\<notin>Field c1 & m\<notin>Field c2" using assms(1) by blast then moreover have 
"c1=C-{m}\<times>UNIV-UNIV\<times>{m}" using 1 unfolding Field_def by fast ultimately moreover have 
"c2=C-{m}\<times>UNIV-UNIV\<times>{m}" using 2 unfolding Field_def by auto ultimately show ?thesis by fast
qed

lemma lm07e: assumes "ReducedRelationV0 Or = (M,or)" "m=the M" shows 
"set or = (set Or)-({m} \<times> UNIV)-(UNIV \<times> {m})" using assms unfolding ReducedRelationV0_def
by (rule lm07)

lemma lm06b: assumes "(Some m,or)=ReducedRelationV0 Or" shows "set or \<subseteq> set Or"
using assms(1) ReducedRelationV0_def Pair_inject filter_is_subset by (metis(no_types,lifting))

lemma mm12a: "Union (Field`set(conflictsFor1 []))\<subseteq>Field (set [])" using assms
unfolding mm05x by simp

lemma mm12b: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor1 Or))\<subseteq>Field (set Or))))"
"\<forall>n\<le>N. P n" shows "P (Suc N)"
proof -
{
  fix Or::"('a \<times> 'a)list" 
  let ?r="ReducedRelationV0 Or" let ?M="fst ?r" let ?or="snd ?r" let ?m="the ?M"
  assume 
  1: "size Or=(Suc N)" have "?M=None \<longrightarrow> conflictsFor1 Or=[{}]" using ReducedRelationV0_def 
    conflictsFor1.simps prod.case prod.sel(1) by (smt)
  then have "?M=None \<longrightarrow>Union (Field`set(conflictsFor1 (Or)))={}" by simp
  then have 
  3: "?M=None \<longrightarrow>Union (Field`set(conflictsFor1 (Or)))\<subseteq>Field(set Or)" by blast
  {
    assume 
    4: "?M\<noteq>None" then have 
    2: "(Some ?m,?or)=?r " using 4 by simp then have "(?M,?or)=?r" by auto then have 
    5: "set(conflictsFor1 Or)= \<Union>{set (generateConflicts (set Or) (the ?M) c)| c. c\<in>set(conflictsFor1 ?or)}"
    using 4 by(rule mm07c) fix C assume "C\<in>set(conflictsFor1 Or)" then 
    moreover obtain c where 
    0: "C\<in>set(generateConflicts (set Or) ?m c) & c\<in>set (conflictsFor1 ?or)"
    using 5 by blast    
    ultimately moreover have "?m\<in>Domain(set Or)" using ReducedRelationV0_def 4 
by (smt DiffD1 fst_conv lm07d)
    ultimately moreover have "length ?or<(Suc N)" using lm06 1 2 by metis 
    then moreover have "P (length ?or)" using assms by (simp add: less_Suc_eq_le) then moreover have 
    "Union (Field`set(conflictsFor1 ?or))\<subseteq>Field (set ?or)" using assms by fast
    then moreover have "Field c\<subseteq>Field (set ?or)" using 0 by blast moreover have 
    "C \<subseteq> c \<union> {?m}\<times>(Range (set Or)) \<union>(Range (set Or))\<times>{?m}" using 0 mm12 by metis
    ultimately moreover  have 
    3: "Field C \<subseteq> Field c \<union> Field(set Or) \<union> Field (set Or)"
    unfolding Field_def by blast
    ultimately  have "Field c \<subseteq> Field (set ?or) \<union> Field (set Or)" by blast
    moreover have "(set ?or) \<subseteq> (set Or)" using 2 by (rule lm06b)
    then moreover have "Field(set ?or)\<subseteq>Field (set Or)" unfolding Field_def by blast
    ultimately have "Field C \<subseteq> Field (set Or)" using 3 by blast
    }
  then have "?M\<noteq>None \<longrightarrow> (\<forall>C\<in>set(conflictsFor1 Or). Field C \<subseteq> Field (set Or) )" by fast
  then have "?M\<noteq>None \<longrightarrow> (Union (Field`set(conflictsFor1 Or)) \<subseteq> Field (set Or) )" by fast
  then have "(Union (Field`set(conflictsFor1 Or)) \<subseteq> Field (set Or) )" using 3 by meson
  }
then show ?thesis using assms by blast
qed

lemma mm12bb: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor1 Or))\<subseteq>Field (set Or))))"
shows "\<And>N.(\<forall>n\<le>N. P n) \<longrightarrow>P (Suc N)"
proof - {fix N assume 2: "(\<forall>n\<le>N. P n)" have "P (Suc N)" using assms(1) 2 by (rule mm12b)} 
then show "\<And>N.(\<forall>n\<le>N. P n) \<longrightarrow>P (Suc N)" by meson 
qed

lemma mm12c: assumes "P=(%n. (\<forall>Or::(('a::linorder \<times> _) list). 
((size Or=n) \<longrightarrow> Union (Field`set(conflictsFor1 Or))\<subseteq>Field (set Or))))" shows "\<forall>n. P n"
proof -
have "P 0" using assms mm12a by blast
moreover have "\<And> N. ((\<forall>n\<le>N. P n)\<longrightarrow> P (Suc N))" using assms(1) by (rule mm12bb) 
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

lemma mm12d: "Union (Field`set(conflictsFor1 Or))\<subseteq>Field (set Or)" using mm12c by fast

lemma mm18a: assumes "L=[remdups (generateConflicts Or m c). c <- l]" "distinct l"
"\<forall> c\<in>set l. m\<notin>Field c" shows 
"\<forall>i j. i<size L & j<size L & i\<noteq>j  \<longrightarrow> (set (L!i) \<inter> set (L!j)={})"
proof -
let ?g="generateConflicts Or m" let ?n="size l" let ?L="map (remdups o ?g) l" have 
0: "size L=size l" using assms by force have "L=?L" using assms by force
{
  fix i1 i2 let ?c1="l!i1" let ?c2="l!i2" assume
  1: "i1<size L" then moreover have "L!i1=remdups (?g (l!i1))" using assms by fastforce
  moreover assume 
  2: "i2<size L" then moreover have "L!i2=remdups (?g (l!i2))" using assms by fastforce
  moreover assume "i1\<noteq>i2" 
  ultimately moreover have "?c1\<noteq>?c2" using assms 1 2 by (simp add: nth_eq_iff_index_eq)
  ultimately moreover have "m \<notin> Field ?c1 \<union> Field ?c2" using assms "1" "2" by auto 
  ultimately have "set (L!i1) \<inter> set (L!i2)={}"
  using assms 1 2 mm18 by (smt disjoint_iff_not_equal inf.right_idem set_remdups)
}
thus ?thesis by presburger
qed

lemma mm19b: assumes "P=(%n. \<forall>(or::('a::linorder\<times>'a) list). size or=n \<longrightarrow> distinct(conflictsFor1 or))" "\<forall>n\<le>N. P n"
shows "P (Suc N)"
proof -
let ?f=conflictsFor1 have 
4: "\<forall> (Or::('a \<times> 'a) list). 
(fst (ReducedRelationV0 Or)=None \<longrightarrow> distinct (?f Or))" using ReducedRelationV0_def 
  assms conflictsFor1.simps fst_conv le0 list.size(3) mm05x prod.case  by (smt distinct_singleton)
{
  fix Or::"('a \<times> 'a) list" let ?r="ReducedRelationV0 Or" let ?M="fst ?r" let ?or="snd ?r" let ?m="the ?M"
  let ?L="[remdups (generateConflicts (set Or) ?m c). c <- ?f ?or]" obtain L where
  1: "L=?L" by auto assume "size Or=Suc N" moreover assume "?M\<noteq>None" ultimately moreover have 
  0: "?f Or=concat [remdups (generateConflicts (set Or) ?m c). c <- ?f ?or]" using 
  ReducedRelationV0_def conflictsFor1.simps map_eq_conv prod.case prod.sel(1) prod.sel(2) by (smt)
  ultimately have "size ?or\<le>N" using ReducedRelationV0_def lm06 leD not_less_eq_eq option.collapse 
  prod.collapse prod.sel(1) prod.sel(2) by (smt) then have "P(size ?or)" using assms by blast then have 
  2: "distinct (?f ?or)" using assms by fast
  have "ReducedRelationV0 Or=(?M, ?or)" by simp moreover have "?m=the ?M" by force
  ultimately have "set ?or = (set Or)-({?m} \<times> UNIV)-(UNIV \<times> {?m})" by (rule lm07e)
  then have "?m \<notin> Field (set ?or)" using mm17c by force moreover have 
  "\<forall>c\<in>set (?f ?or). Field c\<subseteq> Field (set ?or)" using mm12d by blast ultimately have 
  3: "\<forall>c\<in>set (?f ?or). ?m\<notin>Field c" by blast have 
  "\<forall>i j. i<size L & j<size L & i\<noteq>j  \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" using 1 2 3 by (rule mm18a)
  then have 
  "\<forall>i j. i<size L & j<size L & i\<noteq>j & L!i\<noteq>[] & L!j\<noteq>[] \<longrightarrow> (set (L!i) \<inter> set (L!j)={})" by presburger
  moreover have "\<forall>l\<in>set L. distinct l" using 1 by fastforce
  ultimately have "distinct (concat L)" using ll41c by blast then have 
  "distinct (?f Or)" using 0 1 by fastforce
}
then have "\<forall> (Or::('a \<times> 'a) list). 
((size Or=Suc N & fst (ReducedRelationV0 Or)\<noteq>None) \<longrightarrow> distinct (?f Or))" 
by blast then show ?thesis using assms 4 by meson
qed

lemma mm19a: assumes "P=(%n. \<forall>(or::('a::linorder\<times>'a) list). size or=n \<longrightarrow> distinct(conflictsFor1 or))"
shows "\<forall>N. P N"
proof -
have "P 0" using assms distinct_singleton mm05x by (smt length_0_conv)
moreover have "\<forall>N. ((\<forall>n\<le>N. P n)\<longrightarrow>P (Suc N))" using assms
mm19b by auto
ultimately show ?thesis using Suc_n_not_le_n less_induct not_le order_trans
by (metis(lifting) old.nat.exhaust)
qed

theorem distinctness: "remdups o conflictsFor1 = conflictsFor1" using mm19a by fastforce


theorem cardCorrectness2: assumes "N>0" shows "card (esOver {0..<N}) = 
listsum (map (size o conflictsFor1 o adj2PairList) (allReflPartialOrders N))"
proof -
have "finite (esOver2 {0..<N})" unfolding esOver2_def using mm21 by blast then 
have "card (esOver2 {0..<N}) = 
listsum (map (size o remdups o conflictsFor1 o adj2PairList) (allReflPartialOrders N))"
using assms cardCorrectness by blast
moreover have "...= listsum (map (size o (remdups o conflictsFor1) o adj2PairList) (allReflPartialOrders N))"
by (simp add: o_assoc) 
moreover have "...= listsum (map (size o conflictsFor1 o adj2PairList) (allReflPartialOrders N))"
unfolding distinctness by presburger
ultimately show ?thesis unfolding mm21b by presburger
qed


abbreviation "enumerateEs' N==concat (map (%Or. (map (%x. (set Or, x)) (conflictsFor1 Or))) (enumeratePo N))"
definition "enumerateEs = enumerateEs'"
abbreviation "countEs' N == listsum (map (size o conflictsFor1) (enumeratePo N))"
definition "countEs=countEs'"

lemma mm19: assumes "N>(0::nat)" shows 
"set (map (%Or.{set Or}\<times>(set (conflictsFor1 Or))) (enumeratePo N)) partitions (esOver2 {0..<N})" 
unfolding enumeratePo_def using assms mm10u by blast

theorem esCorrectness: assumes "N>(0::nat)" shows "set (enumerateEs N)= (esOver {0..<N})" 
using assms mm19 unfolding mm21b enumerateEs_def mm20 is_partition_of_def by blast

theorem assumes "N>0" shows "card (esOver {0..<N}) = countEs N" 
using assms cardCorrectness2 unfolding enumeratePo_def countEs_def by force
























































section{*scraps*}
(*
lemma "C=(C-({m} \<times> UNIV)-(UNIV\<times>{m})) \<union> ({m} \<times> C``{m}) \<union> (C^-1``{m} \<times> {m})" by fast
lemma assumes "c = (C-({m} \<times> UNIV)-(UNIV\<times>{m}))" "irrefl C" "isMonotonicOver C Po"
shows "C``{m} \<in> Pow ( \<Inter> {c``{M}| M. M \<in> (order2strictCover' Po)``{m}})"
using assms irrefl_def unfolding next1_def by force


abbreviation "generateConflicts4' Or m c == {c\<union>({m}\<times>Y)\<union>(Y\<times>{m})| Y. Y\<in>(Image({(x,y)\<in>Or. x\<noteq>m & y\<noteq>m}))`
Pow(if Or``{m}-{m} = {} then Domain Or else \<Inter> {c``{M}|M. M \<in> next1 Or {m}})}"
definition "generateConflicts4=generateConflicts4'"

abbreviation "successorsForStrictPartialOrder' order list == 
map snd (filter (% (x,y). (List.find (%z. z=x) list) \<noteq> None) order)"
definition "successorsForStrictPartialOrder = successorsForStrictPartialOrder'"

abbreviation "nextForStrictPartialOrder' order list == 
filter (%x. x \<notin> set (successorsForStrictPartialOrder order (successorsForStrictPartialOrder order list))) 
(successorsForStrictPartialOrder order list)"
definition "nextForStrictPartialOrder = nextForStrictPartialOrder'"


abbreviation "ListDomain' listRel == map fst listRel" definition "ListDomain = ListDomain'" 
abbreviation "ListRange' listRel == map snd listRel" definition "ListRange = ListRange'"
abbreviation "ListField' listRel == (ListDomain listRel) @ (ListRange listRel)"
definition "ListField = ListField'"
abbreviation "ListImage' Listrel List == map snd (filter (% (x,y). x \<in> set List) Listrel)"
definition "ListImage = ListImage'"

abbreviation "NewConflicts' minimalList reducedPartialOrder oldConflict == ListIntersection 
[successorsForStrictPartialOrder oldConflict [e]. e <- nextForStrictPartialOrder reducedPartialOrder minimalList]"
abbreviation "NewConflicts2' minimalList partialOrder oldConflict == ListIntersection 
[ListImage oldConflict [e]. e <- nextForPartialOrder partialOrder minimalList]"
definition "NewConflicts2 = NewConflicts2'"


function GenerateConflicts where
(*"GenerateConflicts [(m,m),(n,n)] = [[(m,n),(n,m)],[]]"| "GenerateConflicts [(m,m)] = [[]]"|  "GenerateConflicts [] = [[]]"| *)
"GenerateConflicts pOrder = (let (M,porder)=(ReducedRelationV0 pOrder) in (
if M=None then [[]] else let m=the M in
let olds = GenerateConflicts porder in (olds @ (remdups o concat) 
[
  if (set (ListImage pOrder [m]) - {m}={}) then
    (let unrelatedPowList = 
    (sublists o remdups) (filter(%z. z\<notin>set(map snd (filter (%(x,y). x=m) pOrder))) (ListField pOrder)) in
    [(map (%x. (m,x)) eventList)@(map (%x. (x,m)) eventList)@old. 
    eventList <- (*remdups*) (map (
    (*quicksort o*) (ListImage porder)) unrelatedPowList)])
  else 
    [let newconflicts = NewConflicts2 [m] pOrder old in 
    (*(15,m)#(map (%x. (15,x)) newconflicts)@*)
    (map (%x. (m, x)) newconflicts) @ (map (%x. (x, m)) newconflicts) @ old]. 
old <- olds]
)))"
apply simp by meson
termination GenerateConflicts
apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 by fastforce

function GenerateConflicts2 where
"GenerateConflicts2 pOrder = (let (M,porder)=(ReducedRelationV0 pOrder) in (
if M=None then [[]] else let m=the M in
let olds = GenerateConflicts2 porder in (olds @ ( concat) 
[
  remdups (if (set (ListImage pOrder [m]) - {m}={}) then
    (let unrelatedPowList = 
    (sublists o remdups) (filter(%z. z\<notin>set(map snd (filter (%(x,y). x=m) pOrder))) (ListField pOrder)) in
    [(map (%x. (m,x)) eventList)@(map (%x. (x,m)) eventList)@old. 
    eventList <- (*remdups*) (map (
    (*quicksort o*) (ListImage porder)) unrelatedPowList)]) (*could use pOrder instead of porder? *)
  else 
    [
    (map (%x. (m, x)) newconflicts) @ (map (%x. (x, m)) newconflicts) @ old
    . newconflicts <- (* remdups *) (map (ListImage porder) (sublists (NewConflicts2 [m] pOrder old)))
    ]). 
old <- olds]
)))"
apply simp by meson
termination 
apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 by fastforce

function GenerateConflicts3 where
"GenerateConflicts3 pOrder = (let (M,porder)=(ReducedRelationV0 pOrder) in (
if M=None then [[]] else let m=the M in let olds = GenerateConflicts3 porder in 
(olds @ ((*remdups o*) concat) 
[ remdups
    [(map (%x. (m, x)) newconflicts) @ (map (%x. (x, m)) newconflicts) @ old. 
      newconflicts <- (* remdups *) (map (ListImage porder) (sublists 
      (if (set (ListImage pOrder [m]) - {m}={}) then 
      remdups (filter(%z. z\<notin>set(map snd (filter (%(x,y). x=m) pOrder))) (ListField pOrder))
      else (NewConflicts2 [m] pOrder old))))
    ].old <- olds
])))" apply simp by meson termination 
apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 by fastforce

abbreviation "generateConflicts6' Or or m c == remdups 
[c\<union>({m}\<times>Y)\<union>(Y\<times>{m}). Y <- map (set o (ListImage or))
(*(map set *) 
(sublists (if ((set Or)``{m})-{m}={} then (map fst Or) else 
sorted_list_of_set (\<Inter> {c``{M}|M. M \<in> set (nextForPartialOrder Or [m])})))
(*)*)
]"
definition "generateConflicts6=generateConflicts6'"

lemma assumes "isMonotonicOver C Po" "irrefl C" "sym C" "reflex' Po" "Range C \<subseteq> Domain Po" shows
"C``{m} \<in> {Po``X| X. X \<subseteq> Field Po - Po``{m}}" apply auto using assms lm07b lm07c apply metis done

lemma assumes "irrefl C" shows "irrefl (C-({m} \<times> UNIV)-(UNIV\<times>{m}))" 
using assms unfolding irrefl_def by blast
lemma assumes "isMonotonicOver C Po" "m \<notin> Range (Po - {(m,m)})" shows 
"isMonotonicOver (C-({m}\<times>UNIV)-(UNIV\<times>{m})) Po & isMonotonicOver (C-({m} \<times> UNIV)-(UNIV\<times>{m})) (Po--m)"
apply auto
using assms(1) apply blast 
using assms(2) apply blast 
using assms(1) nn29 apply fastforce
using assms(2) nn29 apply fastforce
done

lemma assumes "isMonotonicOver C Po" shows "C``{m} \<subseteq> \<Inter>{C``{M}| M. M\<in>Po``{m}}" using assms by blast

lemma assumes "isMonotonicOver C Po" shows "C``{m} \<subseteq> \<Inter> {C``{M}| M. M\<in>(order2strictCover' Po)``{m}}"
using assms unfolding next1_def by blast

(*
lemma "setsum card {{or}\<times>(conflictsFor or) | or. or \<in> posOver {0..<N}} = 
setsum size {List.product [Or] (conflictsFor1 Or)|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))}" 
proof -
let ?f="%or. {or}\<times>(conflictsFor or)" let ?H="%or. {set or}\<times>((conflictsFor o set) or)" 
let ?h="%or. {or}\<times>((conflictsFor o set) or)" let ?g="%Or. List.product [Or] (conflictsFor1 Or)"
let ?O="posOver {0..<N}" 
let ?o="map adj2PairList (allReflPartialOrders N)" have
0: "\<forall>C\<in>?g`set ?o. size C= (card o set) C" sorry have
1: "inj_on set (?g`set ?o)" sorry have 
2: "\<forall>Or. card({Or}\<times>(conflictsFor (set Or))) = card ({set Or}\<times>conflictsFor (set Or))"
using Groups_Big.card_cartesian_product_singleton by metis have 
3: "inj_on ?h (set ?o)" sorry have 
5: "inj_on ?H (set ?o)" sorry have 
"set`(set ?o) \<subseteq> posUniv'" using mm10k sorry 
then have "\<forall>Or\<in>set ?o. trans (set Or) & reflex (set Or) & antisym (set Or)" unfolding posUniv_def isPo_def by blast
then have 
7: "\<forall>Or\<in>set ?o. set (conflictsFor1 Or)=conflictsFor (set Or)" using conflictCorrectness by blast
have "card o ?h=card o (conflictsFor o set)" using mm11 by fast moreover have
"card o ?H=card o (conflictsFor o set)" using mm11 by fast ultimately have
4: "card o ?h = card o ?H" by metis have "setsum size {List.product [Or] (conflictsFor1 Or)|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} = 
setsum (card o set) { (List.product [Or] (conflictsFor1 Or))|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))}" using 0 setsum.cong
image_iff mem_Collect_eq by smt
moreover have "...= setsum card {set (List.product [Or] (conflictsFor1 Or))|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))}" 
using 1 setsum.reindex Collect_cong Setcompr_eq_image imageE image_eqI by smt moreover have 
"{set (List.product [Or] (conflictsFor1 Or))|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))}= 
{set [Or]\<times> set(conflictsFor1 Or)|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))}" using Sigma_cong set_product by fast
moreover have "...=
{{Or}\<times> set(conflictsFor1 Or)|Or. Or \<in> set ?o}" by auto moreover have 
"... = {{Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o}" using 7 by blast ultimately have 
6: "setsum size {List.product [Or] (conflictsFor1 Or)|
Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} =
setsum card {{Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o }" by presburger have 
"setsum card {{Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o} = 
setsum card {?h Or| Or. Or \<in> set ?o}" by fastforce moreover have
"{?h Or| Or. Or \<in> set ?o} = (?h`(set ?o))" by blast
ultimately moreover have "setsum card {{Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o} = 
 setsum (card o ?h) (set ?o)" using 3 setsum.reindex by fastforce
moreover have "... = setsum (card o ?H) (set ?o)" using 4 by metis
moreover have "setsum card (?H`(set ?o)) = setsum (card o ?H) (set ?o)" using 5 by (rule setsum.reindex) 
moreover have "?H`(set ?o) = {{set Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o}" by auto
ultimately have "setsum card {{Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o} =setsum card 
{{set Or}\<times>conflictsFor (set Or)| Or. Or \<in> set ?o}" by metis then have 
"setsum size {List.product [Or] (conflictsFor1 Or)|Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} 
= setsum card {{set Or}\<times>conflictsFor (set Or)| Or. Or \<in> set (map adj2PairList (allReflPartialOrders N)) }"
using 6 by presburger moreover have 
"{{set Or}\<times>conflictsFor (set Or)| Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} =
{{or}\<times>conflictsFor (or)| or. or \<in> set (map (set o adj2PairList) (allReflPartialOrders N))}" by fastforce 
moreover have "...=(%or. {or}\<times>conflictsFor (or))`(set (map (set o adj2PairList) (allReflPartialOrders N)))"  try0
by blast moreover have "...=(%or. {or}\<times>conflictsFor or)`posOver{0..<N}" unfolding mm10k by meson
moreover have "...={ {or}\<times>conflictsFor or| or. or\<in>posOver{0..<N}}" by blast
ultimately show ?thesis by presburger
qed
*)

(*
lemma "setsum size {List.product [Or] (conflictsFor1 Or)| Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} 
= listsum [size (conflictsFor1 Or) . Or <- map adj2PairList (allReflPartialOrders N)]"
using listsum_distinct_conv_setsum_set 
proof -
let ?l="map adj2PairList (allReflPartialOrders N)" let ?f="% Or. List.product [Or] (conflictsFor1 Or)"
have "\<forall> C\<in>set ?l. conflictsFor1 C\<noteq>[]" sorry then
have "inj_on ?f (set ?l)" sorry
moreover have "distinct ?l" sorry ultimately
have "distinct (map ?f ?l)" using distinct_map by blast
then moreover have "setsum size (set (map ?f ?l))=listsum (map size (map ?f ?l))"
using listsum_distinct_conv_setsum_set by (metis (no_types, lifting))
moreover have "{?f Or| Or. Or \<in> set ?l}=(set (map ?f ?l))" by force 
moreover have "map size (map ?f ?l)=[size (List.product [Or] (conflictsFor1 Or)). Or <- ?l]"
by simp moreover have "...=[size (conflictsFor1 Or). Or <- ?l]" by simp
ultimately show ?thesis by presburger
qed
*)

lemma "{conflictsFor1 Or| Or. Or \<in> set (map adj2PairList (allReflPartialOrders N))} =
(set (map conflictsFor1 (map adj2PairList (allReflPartialOrders N))))" by force

abbreviation "conflictsFor4' == remdups o conflictsFor1" definition "conflictsFor4=conflictsFor4'"
*)

(*
lemma assumes "(ReducedRelationV0 Rel) = (M,list)" "m=the M" "M \<noteq> None" shows
"m \<notin> Range (set list)" using assms ReducedRelationV0_def Field_def
by (smt Pair_inject Range_iff mem_Collect_eq prod.simps(2) set_filter)

lemma assumes "(ReducedRelationV0 Rel) = (M,list)" "m=the M" "M \<noteq> None" shows
"m \<notin> Domain (set list)" using assms ReducedRelationV0_def Field_def
Pair_inject Range_iff mem_Collect_eq prod.simps(2) set_filter DomainE
by (smt )

lemma "distinct(conflictsFor1 [])" by (metis distinct_singleton mm05x)
*)

(*GenerateConflicts2: 6=3882708 ~ 15minutes
11 March: 6=3528258, 5=41099, 4=916, 3=41*)
(* value "(size o concat) (map (allConflictsCompatibleWith) (map adj2PairList (allReflPartialOrders 6)))"
1=1
2=4
3=41
4=888
5=36814
6=2769020
*)

(*lemma mm09w: assumes "isRectangular4 N a" shows "isRectangular4 N (setDiag x a)"*)

(*

(* SLOWER than conflictsFor1 *)
function conflictsFor4 where "conflictsFor4 Or=(let (M,or)=ReducedRelationV0 Or in if M=None then [{}] 
else let m=the M in (remdups o concat) [generateConflicts (set Or) m c. c <- conflictsFor4 or])" 
apply simp apply simp by blast
termination apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 
by (smt ReducedRelationV0_def case_prod_unfold filter_cong option.sel prod.sel(1))
*)

(*

(* DOES NOT WORK *)
function conflictsFor3 where "conflictsFor3 Or=(let (M,or)=ReducedRelation3 Or in if M=None then [{}] 
else let m=the M in concat [(generateConflicts (set Or) m c). c <- conflictsFor3 or])" 
apply simp apply simp by blast
termination apply (relation "measure (\<lambda> rel. size rel)") apply auto[1] apply auto using lm06 
sorry

abbreviation "enumerateEs2' N == 
Union (set (map (%Or.{set Or}\<times>(set (conflictsFor3 Or))) (enumeratePo N)))"

lemma "enumerateEs2' N = 
set (concat (map (%Or. (map (%x. (set Or, x)) (conflictsFor3 Or))) (enumeratePo N)))"
unfolding mm20 by blast
lemma "set (conflictsFor3 Or)=set (conflictsFor1 Or)" sorry
(*value "size (enumeratePo 7)"*)


abbreviation "optionHd' l==if l=[] then None else (Some o hd) l" definition "optionHd=optionHd'"

abbreviation "optionMin' A==if A={} then None else (Some o Min) A" definition "optionMin=optionMin'"

*)

end
