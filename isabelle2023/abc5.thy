theory abc5
                           
imports (*abc4*) "~~/src/HOL/Library/Prefix_Order"
"~~/src/HOL/Imperative_HOL/ex/List_Sublist"
"preorder"
abc2
(*"~/Combinatorics_Words/CoWBasic"*)
"~/afp/Randomised_Social_Choice/Missing_Permutations"

begin

value "''abc(A Bcac)c caB(ac''"
value "allInterleavings [0,5,7,11::nat]"
value "map (%x. (x, isAlternationFeasible 3 x)) (alternations [0,5,7,11::nat] [[1, 3::nat], [6], []])"
value "map (%x. (x, isAlternationFeasible 3 x)) (alternations [0,5,7,11::nat] [[], [6], [9, 10::nat]])"

value "compact2fullFixedSplit ''aaB Abc''"
value "allAlternations [0,4,7,11::nat]"
value "isAlternationFeasible 2 [(False,[1,3]), (True,[])]"

find_consts "char => char list"
term "CHR ''a''"

value "let s=''a(b(AB'' in  (breakListPositions (filterpositions2 (\<lambda>x. x\<in>set '' ()'') s) s)"
value "map (\<lambda>l. ((hd l=CHR ''(''), filter (\<lambda>x. x\<in>set ''aAbBcC'') l) ) (let s=''a(b(AB'' in  (breakListPositions (filterpositions2 (\<lambda>x. x\<in>set '' ()'') s) s))"



section{*start*}
notation power (infixr "^" 80)

lemma lm14e: assumes "map size L = map size M" shows "size (concat L) = size (concat M)" using assms 
by (simp add: length_concat)

lemma lm08: "[i. i<-l, P i]=filter P l" apply (induct l) apply simp by simp

lemma lm00: assumes "i< length L" shows "L!i = 
take (length (L!i)) (drop (length (concat (take (i) L))) (concat L))" using assms Cons_nth_drop_Suc 
append_eq_conv_conj append_take_drop_id concat.simps(2) concat_append by (metis )

lemma lm01: assumes "l\<noteq>[]" shows "concat((map concat) l)=concat(concat l)"
using assms apply (induct l) apply auto[1] by fastforce

lemma lm02: assumes "L\<noteq>[]" "[]\<notin>set L" shows "hd (concat L)=hd(hd L)" using assms 
by (metis concat.simps(2) hd_Cons_tl hd_append2 list.set_sel(1))

lemma lm03: assumes "j< length l" shows "(concat (L1@[l]@L2))!((length (concat L1))+j) = l!j" 
  using assms by (simp add: nth_append)

lemma lm04: assumes "n< length (concat L)" "length (concat (take i L))\<le>n" 
"n - length(concat(take i L)) < length (L!i)" shows 
"(concat L)!n = (L!i)!(n-length(concat(take i L)))" 
proof - (*shammer-generated*)
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2. x0 = x1 + v2) = (x0 = x1 + nn x0 x1)" by moura
  then have "\<forall>n na. \<not> n \<le> na \<or> na = n + nn na n" using le_Suc_ex by presburger
  then have "n = length (concat (take i L)) + nn n (length (concat (take i L)))"
    using assms(2) by blast
  then show ?thesis using assms(1,2,3) lm00 add_diff_cancel_left' 
  less_imp_le_nat not_le nth_drop nth_take take_all by (metis (no_types))
qed

lemma assumes "l\<noteq>[]" shows "listsum l = foldr (op +) l 0" using assms 
by (simp add: listsum.eq_foldr)


abbreviation "factor0' m n l == drop m (take n l)"
abbreviation "factor1' m n l == take (n-m) (drop m l)"
abbreviation "sublist3' list A == map (nth list) (filter (\<lambda>i. i\<in>A) [0..<size list])" definition "sublist3=sublist3'"
definition "sublist2=sublist2'"

lemma lm14a: "sublist2 = sublist3" unfolding sublist2_def sublist3_def by (simp add: ll10b)

lemma lm14b: "sublist2 l N = sublist l N" unfolding sublist2_def using preorder.lm23 by metis

lemma lm11a: "(let A={m..<n} in filter (\<lambda>i. i\<in>A) [0..<N])=[m..<min n N]" (is "?L=?R") 
proof-
have "sorted [0..<N]" by simp then have "sorted ?L" using filter_sort sorted_sort sorted_sort_id 
by metis moreover have "set ?L = set ?R" by auto moreover have "distinct ?L" by fastforce
moreover have "distinct ?R" by auto moreover have "sorted ?R" by fastforce
ultimately show ?thesis using sorted_distinct_set_unique by blast
qed

lemma lm11b: "sublist3' l {m..<n} = map (nth l) [m..<min n (length l)]" using lm11a by presburger

lemma lm14c: "length (sublist3' l X) \<le> length l" by (metis diff_zero length_filter_le length_map length_upt)

lemma lm14: "size (sublist3' l X) = card (X \<inter> set [0..<size l])" 
proof -
have "set (filter (\<lambda>i. i\<in>X) [0..<size l]) = X \<inter> {0..<size l}" by auto 
moreover have "distinct (filter (\<lambda>i. i\<in>X) [0..<size l])" by simp ultimately moreover 
have "(card o set)(filter (\<lambda>i. i\<in>X) [0..<size l])=size (filter (\<lambda>i. i\<in>X) [0..<size l])" 
by (metis comp_def distinct_card) ultimately show ?thesis by simp
qed

lemma lm14d: "sorted (sorted_list_of_set l)" 
by (metis sorted.Nil sorted_list_of_set sorted_list_of_set.infinite)

(* A list partition is a list of lists. How to define the notion of finer?*)
value "([[1::nat,3,5,6], [5,8]], [[1::nat,3],[5,6],[5,8]])"
abbreviation "allParts' l == {L|L. concat L=l}" definition "allParts=allParts'"
term "{L|L. {} \<in> set L}"
abbreviation "allProperParts' l==allParts' l - {L|L. []\<in>set L}" definition "allProperParts=allProperParts'"
fun app where "app []=[]"|"app [x] = [[[x]]]"|"app (x#xs) = [[x]#L. L<-app xs]@[(x#(hd L))#(tl L). L<-app xs]"
value "app [0::nat, 1, 1, 2,1,2]"

abbreviation "isFiner' L2 L1 == (\<exists> LL2\<in>allProperParts L2. L1=map concat LL2)"

lemma "(map concat L)=(map concat) L" by simp
lemma "concat (map ((map g) o F) l) =(map g) (concat (map F l))"
using map_concat map_map by (metis(no_types))
value "let l = [[[1,2::nat],[14,17]],[[22,21,22], []]] in ((concat o concat) l, (concat o (map concat)) l)"
value "take 1 [1::nat]"
value "(1::nat) - 1"
value "let L = [[1,2],[], [14,17],[99,99,11,14],[22,21,22::nat]] in 
let I= [listsum (map length (take s L)). s<-[0..<1+length L]]
in (concat L, I, let J=the (findFirstIndex (%x. x\<ge>(5+1)) I) in (J - 1, 5-(I!(J-1))))
"
term "[0..(3::nat)]"
find_consts "nat => int"
abbreviation "findFirstIndex00' P l == length (takeWhile (Not o P) l)" 
definition "findFirstIndex00=findFirstIndex00'"
value "[i. i<-rev[1..<4]]"
value "last []"
abbreviation "findLastIndex00' P l == (let ll= [i. i<-[0..<length l], P(l!i)] in 
if ll=[] then length l else last ll)"
abbreviation "findLastIndex01' P l == let ll= filter (P o (nth l)) [0..<length l] in 
if ll=[] then length l else last ll"
value "findFirstIndex00' (%x. x>111) [0..<10]"
(*Given a list of lists, we can concatenate it and ask where any of the concatenated list's element index 
can be found in the original, unconcatenated string. The function below gives the answer, returning
two indices: the one locating the list hosting index within unConcatenated, 
and the index locating the given index in the latter*)
(*abbreviation "invConcat' unConcatenated ind == let L=unConcatenated in 
let I = [listsum (map length (take (nat s) L)). s<-[0..int(length L)]] in 
let J=the (findFirstIndex (\<lambda>i. i>ind) I) in (J-1, ind-(I!(J-1)))" definition "invConcat=invConcat'"
value "let L = [[1,2],[], [14,17],[99,99,11,14],[22,21,22::nat]] in (concat L, invConcat L 3)"

abbreviation "invCat0' unConcatenated ind == let L=unConcatenated in 
let I = [foldr (op +) (map length (take (nat s) L)) 0. s<-[0..int(length L)]] in 
let J=the (findFirstIndex (\<lambda>i. i>ind) I) in (J-1, ind-(I!(J-1)))" 
*)
abbreviation "invCat1' unConcatenated ind == let L=unConcatenated in 
let I = [listsum (map length (take s L)). s<-[0..<1+ length L]] in 
let J=(findFirstIndex00 (\<lambda>i. i>ind) I) in (J-1, ind-(I!(J-1)))" 

abbreviation "invCat2' unConcatenated ind == let L=unConcatenated in 
let I = [listsum (map length (take s L)). s<-[0..<1+ length L]] in 
let J=(findLastIndex00' (\<lambda>i. i\<le>ind) I) in (J, ind-(I!(J)))" 

abbreviation "indicizeList' l == zip [0..<size l] l" definition "indicizeList=indicizeList'"

value "let unc=[[10::nat, 13],[9,4],[6,1,3,9]] in let conc=concat unc in
let middle=map (\<lambda> (i, l). map (\<lambda>e. (i, e)) l) (indicizeList unc) in
(conc, indicizeList unc, middle, map (map snd) middle, (map indicizeList middle))"

(* invCat3' L has the useful property that (concat L)!n = (L!(snd(invCat3' L!n)))!(fst(invCat3' L!n)).
It therefore permits to go from concat L back to L*)
abbreviation "invCat3' unConcatenated == let middle=
map (\<lambda> (i, l). map (\<lambda>e. i) l) (indicizeList unConcatenated) in concat (map indicizeList middle)"

value "let unc=[[10::nat, 13],[9,4],[],[6,1,3,9]] in (unc, concat unc, invCat3' unc)"

lemma lm14f: "l = map snd (indicizeList l) & size l = size (indicizeList l)" 
unfolding indicizeList_def by auto
(*
lemma lm14g: "size (concat L) = size (invCat3' L)"  
proof-
let ?i=indicizeList let ?m="map (\<lambda> (i, l). map (\<lambda>e. i) l) (?i L)" have "map size L = map size ?m" 
using lm14f sorry
moreover have "...=map (size o ?i) ?m" using lm14f by force 
ultimately have "map size L = map size (map ?i ?m)" by auto then show ?thesis using lm14e by metis
qed
*)

value "let L = [[1,2],[], [14,17],[99,99,11,14],[22,21,22::nat]] in (concat L, invCat1' L 3)"
value "let L = [[1,2],[], [14,17],[99,99,11,14],[22,21,22::nat]] in (concat L, invCat2' L 3)"

lemma lm06a: assumes "findFirstIndex00' P l< length l" shows "P(l!findFirstIndex00' P l)" using assms 
 comp_apply nth_length_takeWhile by force
lemma lm06b: assumes "findFirstIndex00' P l< length l" "i < findFirstIndex00' P l" shows "\<not>P(l!i)" using assms 
using comp_apply nth_length_takeWhile by (metis nth_mem set_takeWhileD takeWhile_nth)
lemma lm06c: assumes "i< length l" "P(l!i)" shows "findFirstIndex00' P l \<le>i" using assms 
by (metis comp_apply not_le_imp_less nth_append nth_mem set_takeWhileD takeWhile_dropWhile_id)
lemma lm06d: assumes "l\<noteq>[]" "findFirstIndex00' P l=length l" shows "True \<notin> P`(set l)" using assms lm06a lm06b lm06c 
by (metis imageE in_set_conv_nth not_le)
lemma lm06e: assumes "i=findFirstIndex00' P l" shows "i\<le> length l & ((i= length l) = (True \<notin> P`(set l)))"
unfolding assms(1) using lm06a lm06d 
by (metis (no_types, lifting) True_in_image_Bex empty_iff length_takeWhile_le less_linear list.set(1) not_le nth_mem)
lemma lm06f: assumes "j=findFirstIndex00' P l" shows "j\<le>length l & ((True \<in> P`(set l)) = (j<length l & (\<forall>i<j. \<not>P(l!i))))"
unfolding assms(1) using lm06a lm06b lm06c lm06d lm06e 
by (metis order.not_eq_order_implies_strict)
lemma lm06g: "let j=findFirstIndex00 P l in 
j\<le>length l & ((True \<in> P`(set l)) = (j<length l & (\<forall>i<j. \<not>P(l!i))))"
unfolding findFirstIndex00_def using lm06a lm06b lm06c lm06d lm06e 
by (metis order.not_eq_order_implies_strict)

lemma lm07: "let I = [listsum (map length (take s L)). s<-[0..<1+length L]] in 
(findFirstIndex00 (\<lambda>i. i>ind) I)\<ge>1" (*using findFirstIndex00_def lessI lm06a preorder.lm35
Nat.add_0_right One_nat_def add_gr_0 length_map lessI list.simps(8) nth_map nth_upt take_eq_Nil
 less_nat_zero_code less_one linorder_not_le listsum.Nil by (metis (no_types, lifting))*)
proof-
let ?I="[listsum (map length (take s L)). s<-[0..<1+length L]]" let ?f=findFirstIndex00 
let ?P="\<lambda>i. i>ind"
{assume "?f ?P ?I=0" then have "?P(?I!0)" using 
  One_nat_def add_gr_0 findFirstIndex00_def lessI lm06a preorder.lm35
by metis
moreover have "?I!0=listsum []" using Nat.add_0_right One_nat_def add_gr_0 length_map lessI
list.simps(8) nth_map nth_upt preorder.lm35 take_eq_Nil by (metis(no_types))
  ultimately have False by simp
} thus ?thesis by fastforce
qed

(* in the list orLst, removes the char of index m (inclusive) up to index n-1 and replaces them 
with addLst: *)
abbreviation "posRepl0' m n addLst orgLst == (take m orgLst) @ addLst @ (drop n orgLst)" 
(* when m=n, this means injecting addLst BEFORE the character of index m into orgLst, without
throwing anything away; when n=m+1, the character of index m is substituted by addLst. *)
lemma lm10a: assumes "m < length L" shows "length (posRepl0' m n l L) = (m+(length L - n) + length l)"
using assms(1) by fastforce
lemma lm10b: "length (posRepl0' m n l L) = (min (length L) m) + length l + (length L - n)"
 using lm10 by auto
lemma lm10c: assumes "m < length L" shows "let LL=(posRepl0' m n l L) in 
l = drop m (take (m+length l) LL)" using assms(1) by fastforce
lemma lm10d: assumes "m < length L" shows "let LL=(posRepl0' m n l L) in
take m L = (take m LL) & drop n L = (drop (m+length l) LL)" using assms(1) by auto

value "posRepl0' 2 2 ''injected'' ''0123456''"

lemma lm09: "findLastIndex00' P l == findLastIndex01' P l" using lm08 comp_apply map_eq_conv by (smt )

abbreviation "Iota00' == map (flip o (\<lambda>(x,y). (x, hd y))) 
[(1::int,''a''),(-1,''A''),(2,''b''),(-2,''B''),(3,''c''),(-3,''C''),(4,''d''),(-4,''D'')]"
abbreviation "iota00' == set Iota00'"
value Iota00'

value "let s=''adcAdcDddDD'' in let ss=map ((eval_rel o set) (Iota00')) (s) in let 
top = Max (abs`set ss) in 
(*let dom=((set o remdups) (0#ss@(map uminus ss))) in remdups superfluous*)
let dom = {-top..top} in
let allbals=graph ((count o mset) ss) dom in (ss, allbals,
map (\<lambda>(x,y). int x - int y) (zip (rel2List ((\<lambda>(x,y). (nat x, y))`(allbals||{0..Max (Domain allbals)}))) 
(rel2List((\<lambda>(x,y). (nat \<bar>x\<bar>, y))`(allbals||{Min(Domain allbals)..0}))))
)"

(* Computes the balance vector of a string: *)
abbreviation "bal' s == let ss=map ((eval_rel o set) (Iota00')) (s) in 
let top = Max (abs`set ss) in let dom = {-top..top} in let allbals=graph ((count o mset) ss) dom in 
map (\<lambda>(x,y). int x - int y) (zip 
  (rel2List ((\<lambda>(x,y). (nat x, y))`(allbals||{0..Max (Domain allbals)}))) 
  (rel2List((\<lambda>(x,y). (nat \<bar>x\<bar>, y))`(allbals||{Min(Domain allbals)..0}))))"
value "bal' ''abBAAAbba''"
value "bumps ''abbbA''"
abbreviation "Conj00' letter == (iota00')^-1,, (- (iota00',,letter))"
lemma assumes "n>m" shows "factor0' m n l = factor1' m n l" using assms using drop_take by blast

lemma lm11c: "(factor0' m n l) = sublist3' l {m..<n}"
proof - (*quasi-shammer-generated*)
  have f1: "[0..<min n (length l)] = take (min n (length l)) [0..<length l]" by simp
  have "l = take (length l) (tolist (op ! l) (length l))" by (simp add: map_nth)
  then show ?thesis using f1 drop_map drop_upt plus_nat.add_0 take_map take_take lm11b by (metis)
qed

value "Conj00' (CHR ''a'') = (CHR ''B'')"

abbreviation "bumps00' x y strg == [[i..<1+j].i<-[0..< length strg-1], j<-[i+1..<length strg], let 
(l,r)=(strg!i,strg!j) in l\<in>x & strg!(i+1)\<in>y & l = Conj00' r & 
(let middle=set (factor0' (i+1) j strg) in 
  middle \<inter> {l, r}={} & middle \<inter> Conj00'`(middle)={})]"

definition "bumps00=bumps00'" 

lemma lm12: "[f i j. i<-I, j<-J i, P i j] = concat (map (\<lambda>i. map (f i) (filter (P i) (J i))) I)" 
by (simp add: ll10b)

lemma lm12b: "[f i j. i<-I, j<-J i] = concat (map (\<lambda>i. map (f i) (J i)) I)" using lm12 by blast

lemma lm12a: "bumps00 x y strg = (let f=(\<lambda>i j. [i..<1+j]) in let I=[0..< length strg-1] in 
let J=(\<lambda>i. [i+1..<length strg]) in let P=(\<lambda> i j. let 
(l,r)=(strg!i,strg!j) in l\<in>x & strg!(i+1)\<in>y & l = Conj00' r & 
(let middle=set (factor0' (i+1) j strg) in 
  middle \<inter> {l, r}={} & middle \<inter> Conj00'`(middle)={})
) in concat (map (\<lambda>i. map (f i) (filter (P i) (J i))) I))" 
unfolding bumps00_def using lm12 [of _ "(\<lambda>i j. [i..<1+j])" _ "[0..< length strg-1]"] by (metis)

abbreviation "bumps01' x strg==bumps00 (set x) UNIV strg" definition "bumps01=bumps01'"

value "bumps01' ''aA'' ''bBbabbaBA''"
value "bumps01' ''aA'' ''bCcabCA''"

value "factor0' 1 2 ''abc''"

proposition lm13a: assumes "[m..<1+n] \<in> set (bumps00' x y p)" shows "n>m & n<length p" 
proof-
let ?n="length p" obtain i j where 
0: "i\<in>set [0..< ?n-1] & j\<in>set [(i+1)..<?n] & [m..<1+n]=[i..<1+j]" using assms 
by fastforce have 
1: "m=i & n=j" using 0 One_nat_def Suc_eq_plus1_left add_Suc_right 
add_diff_inverse_nat atLeastLessThan_iff diff_Suc_Suc diff_add_inverse2 diff_zero 
le0  length_upt lessI less_antisym less_imp_diff_less linorder_not_le  
not_less_eq nth_upt set_upt take_upt zero_less_diff
by (metis) show ?thesis using 0 1 by simp
qed

section{*2D*}
(*lemma assumes "set p \<subseteq> (set ''aAbB'')" "[i..<1+j] \<in> set (bumps01' x p)" obtains a b n where 
"(*b\<notin>{a, Conj00' a} & *) factor0' i (1+j) p=[(a::char)]@([b] ^ n)@[Conj00'(a)]" using assms 
sorry


lemma assumes "[i, i+k] \<in> set (bumps00' {a} {b} q0)" "set (q0@q1) \<subseteq> set ''aAbB''"
"\<forall> p0. \<forall>p1. (p0@p1) \<in> set (sublists (q0@q1))\<longrightarrow> True"
"length [i, i+k] \<le> Inf (length`(set (bumps01' [a, Conj00' a] q0) \<union> (set (bumps01' [a, Conj00' a] q1))))" 
obtains j q0' where
"j\<ge>1 & j<k & i=0 & q1=''b'' ^ j@q0'"
sorry
*)

value "let unc=[[10::nat, 13],[9,4],[6,1,3,9]] in 
[zip (zip (replicate (size (unc!i)) i) [0..<size(unc!i)]) (unc!i). i<-[0..<size unc]]"

abbreviation "invCat4' unc == [zip (zip (replicate (size (unc!i)) i) [0..<size(unc!i)]) (unc!i). i<-[0..<size unc]]" definition "invCat4=invCat4'"
value "let unc=[[10::nat, 13],[9,4],[6,1,3,9]] in invCat4' unc"
lemma lm14i: "map (map snd) ((invCat4 L))= L" 
proof- let ?i=invCat4' let ?LL="?i L" { fix i assume "i<size L" then have "?LL!i = zip (zip (replicate (size (L!i)) i) [0..<size(L!i)]) (L!i)" by fastforce then have "map snd(?LL!i)=L!i" by force } thus ?thesis 
using list_eq_iff_nth_eq unfolding invCat4_def by fastforce
qed
lemma lm14r: "size (invCat4 L)=size L" unfolding invCat4_def by simp
lemma lm14ii:  "map snd (concat (invCat4 L)) = concat L" using lm14i by (metis (no_types, lifting) map_concat map_eq_conv)
lemma lm14h: "take (size (concat (take i L))) (concat L)=concat (take i L)" using append_eq_conv_conj append_take_drop_id concat_append by (metis)
lemma lm14k: assumes "i<size L" "j<size (L!i)" shows "take ((size (concat (take i L)))+j) (concat L) = concat (take i L) @ (take j (L!i))" 
using assms lm14h Sefm.lm23 abc5.lm00 le_Suc_ex less_imp_le_nat take_add by (smt )
lemma lm14l: assumes "i<size L" "j<size (L!i)" shows "(concat L)!((size (concat (take i L)))+j)=(L!i)!j"
using assms(1,2) by (metis abc5.lm00 append_eq_conv_conj append_take_drop_id concat_append nth_append_length_plus nth_take)
lemma lm14n: "concat(map (map fst) L) = map fst (concat L)" by (simp add: map_concat) 
lemma lm14o:  "map fst (concat L)=(concat LL) & m < size (concat LL) \<longrightarrow> fst ((concat L)!m)=(concat LL)!m"
using list_update_id map_update nth_list_update_eq by (metis (no_types, lifting) )

lemma lm14j: "let f=invCat4 in (\<forall>i< size (f L). \<forall>j< size((f L)!i). fst (((f L)!i)!j) = (i,j))" unfolding invCat4_def by fastforce
lemma lm14m: assumes "distinct (concat L)" "i<size L" "j<size(L!i)" "m<size (concat L)" "(concat L)!m=(L!i)!j" shows 
"m=size (concat (take i L))+j"  
proof-
have "(concat L)!((size (concat (take i L)))+j)=(L!i)!j" using assms(2,3) lm14l by blast then have 
"(concat L)!((size (concat (take i L)))+j)=(concat L)!m" using assms(5) by presburger moreover have 
"((size (concat (take i L)))+j)< size (concat L)" using assms(2,3) abc5.lm00 add_less_cancel_left append_eq_conv_conj 
append_take_drop_id length_append lm14k not_le take_all trans_less_add1 by fastforce 
ultimately have "(size (concat (take i L)))+j=m" using assms(1,4) ll24 by blast then show ?thesis by blast
qed
print_statement lm14m

lemma lm14q: "(size (concat (take i (invCat4 L))) = size (concat (take i L)))" using lm14i by (smt length_map map_concat map_eq_conv take_map)
lemma lm14p: "size (concat (take i (map (map f) L)))= size (concat (take i L))" using length_map map_concat take_map by (metis )

lemma lm14s: assumes "x\<in>set (invCat4 L)" shows "distinct x"  using assms unfolding invCat4_def 
by (metis (no_types, lifting) distinct_upt distinct_zipI1 distinct_zipI2 ex_map_conv)
find_theorems distinct 

lemma lm14t: assumes "i<size L" shows "map fst ((invCat4 L)!i) = zip (replicate (size (L!i)) i) [0..<size(L!i)]"
unfolding invCat4_def using assms(1) by auto

lemma lm14u: assumes "xs\<in>set (invCat4 L)" "ys\<in>set(invCat4 L)" "xs \<noteq> ys" shows "set xs \<inter> set ys={}" 
proof- let ?f'=invCat4' let ?f=invCat4 let ?s=size let ?r=replicate obtain i where 
0: "i< ?s (?f L) &  xs=(?f L)!i" using assms by (metis in_set_conv_nth) obtain j where 
1: "j< ?s (?f L) &  ys=(?f L)!j" using assms by (metis in_set_conv_nth)
have "i\<noteq>j" using 0 1 assms by blast have "map fst (xs) = zip (?r (?s (L!i)) i) [0..<size(L!i)]" unfolding invCat4_def using lm14t lm14r 0 
by metis moreover have 
"map fst ys = zip (?r (?s (L!j)) j) [0..<size(L!j)]" unfolding invCat4_def using lm14t 1 lm14r by metis
moreover have "i\<noteq>j" using 0 1 assms by blast
ultimately have "set (map fst (map fst xs)) \<inter> (set (map fst (map fst ys)))={}" using 0 1 assms 
by fastforce then have "set ((map fst xs)) \<inter> (set ((map fst ys)))={}" by force 
thus ?thesis by auto
qed

lemma lm14v: assumes "[]\<notin>set L" (* assms(1) could be weakened *) shows "distinct (invCat4 L)" using assms 
proof- let ?f'=invCat4' let ?f=invCat4 let ?r=replicate let ?s=size
{fix i j let ?xs="(?f L)!i" let ?ys="(?f L)!j" assume 
  0: "i\<noteq>j & i<size (?f L) & j<size(?f L)" then have 
  1: "(L!i) \<noteq> [] & (L!j) \<noteq>[]" using assms by (simp add: in_set_conv_nth lm14r) 
  have "map fst ?xs = zip (?r (?s (L!i)) i) [0..<size(L!i)]" using 0 lm14t lm14r by metis
  then have 
  2: "map fst (map fst ?xs) = (?r (?s (L!i)) i)" by force
  have "map fst ?ys = zip (?r (?s (L!j)) j) [0..<size(L!j)]" using lm14t lm14r 0 by metis then
  have "map fst (map fst ?ys) = (?r (?s (L!j)) j)" by force moreover have "... \<noteq> ?r (?s (L!i)) i"
  using 0 1 by simp ultimately have "?xs \<noteq> ?ys" using 2 by auto 
} 
thus ?thesis by (simp add: distinct_conv_nth)
qed

lemma lm14w: assumes "[]\<notin>set L" shows "distinct (concat (invCat4 L))" using assms distinct_concat 
lm14s lm14v lm14u by blast

lemma lm14y: assumes "[]\<notin>set L" shows "distinct (map (map fst) (invCat4 L))" using assms
in_set_conv_nth lm14r lm14t
diff_zero distinct_conv_nth length_greater_0_conv length_map length_replicate length_upt map_fst_zip nth_map nth_replicate
by (smt)

lemma lm14x: assumes "L'=(map (map fst) (invCat4 L))" "xs\<in>set L'" "ys\<in>set L'" shows 
"distinct xs & ((xs \<noteq> ys) \<longrightarrow>set xs \<inter> set ys={})"   
proof- let ?f'=invCat4' let ?f=invCat4 let ?s=size let ?r=replicate let ?L="map (map fst) (?f L)" obtain i where 
0: "i< ?s ?L &  xs=?L!i" using assms(1,2)  in_set_conv_nth by (metis) obtain j where 
1: "j< ?s ?L &  ys=?L!j" using assms(1,3) by (metis in_set_conv_nth) have 
2: "i<?s L & j<?s L" using lm14r by (metis "0" 1 length_map) then have 
3: "xs = zip (?r (?s (L!i)) i) [0..<size(L!i)]"  using lm14t lm14r 0 by auto moreover have 
"ys = zip (?r (?s (L!j)) j) [0..<size(L!j)]" using lm14t lm14r 1 2 by auto then have 
"map fst ys = (?r (?s (L!j)) j)" by auto then have "set (map fst ys) \<subseteq> {j}" by fastforce
moreover have "set (map fst xs) \<subseteq> {i}" using 3 by fastforce moreover have "xs \<noteq> ys \<longrightarrow> i\<noteq>j" using 
0 1 by blast ultimately have "xs \<noteq> ys \<longrightarrow> set (map fst xs) \<inter> set (map fst ys)={}" by blast moreover 
have "distinct xs" using "3" distinct_upt distinct_zipI2 by blast ultimately show ?thesis by auto 
qed

lemma lm14z: assumes "[]\<notin>set L" shows "distinct (concat (map (map fst) (invCat4 L)))" using assms 
proof- let ?L="map (map fst) (invCat4 L)"
have "\<forall>xs\<in>set ?L. \<forall>ys\<in>set ?L. xs \<noteq> ys \<longrightarrow> set xs \<inter> set ys={}" using lm14x by blast
moreover have "distinct ?L" using assms lm14y by blast 
moreover have "\<forall>xs\<in>set ?L. distinct xs" using assms lm14x by blast
ultimately show ?thesis using distinct_concat by fast
qed

lemma lm14iii: assumes "i<size L" shows "size(L!i) = size ((invCat4 L)!i)" using assms lm14i
by (metis length_map nth_map)

lemma lm14jj: assumes "l\<in>set (invCat4 L)" "((i,j),k) \<in> set l" shows "i<size L & j<size (L!i) & k=(L!i)!j" 
proof- let ?r=replicate let ?c=concat let ?s=size obtain i' where 
0: "i'<?s L & l=zip (zip (?r (?s (L!i')) i') [0..<?s(L!i')]) (L!i')" 
using assms unfolding invCat4_def by fastforce then moreover have 
2: "i'=i" using assms 
by (metis (no_types, lifting) in_set_replicate set_zip_leftD) obtain j' where
1: "j'<size(l) & ((i,j),k) = l!j'" using assms 0 by (metis in_set_conv_nth) then have 
"((i,j),k) = ((i', j'), (L!i')!j')" using 0 by force then moreover have "... = ((i, j), (L!i)!j)" by fast
ultimately have "i<?s L & j<?s l & k=(L!i)!j" using 0 1 by fast moreover have 
"?s l = ?s (L!i)" using 0 2 by fastforce ultimately show ?thesis by auto
qed

corollary lm14jjj: assumes "m<size (concat L)" "(i,j)=fst ((concat(invCat4 L))!m)" shows "i<size L & j<size (L!i)"
proof- let ?r=replicate let ?c=concat let ?s=size let ?f=invCat4 
have "(?c (?f L))!m \<in> set (?c (?f L))" using assms by (metis length_map lm14ii nth_mem) then
obtain l where "l\<in>set (?f L) & (?c (?f L))!m\<in>set l" using assms by auto moreover have 
"((i,j), snd ((?c (?f L))!m))= ((?c (?f L))!m)" using assms by simp ultimately show ?thesis using lm14jj by metis
qed

lemma lm15: assumes "m < size (concat L)" "(i,j)=fst ((concat(invCat4 L))!m)" "[] \<notin> set L" shows
"m=size (concat (take i L))+j"
proof-
let ?s=length let ?c=concat let ?f'=invCat4' let ?f=invCat4 let ?L="?f L" let ?LL="map (map fst) ?L"
have "?s (?c ?LL) = ?s (map fst (?c ?L))" using lm14n by metis moreover have "... = ?s (?c ?L)" 
by simp moreover have "... = ?s (?c L)" using length_map lm14ii by (metis) ultimately have 
2: "?s (?c ?LL) = ?s (?c L)" by presburger have 
8: "i<?s L & j<?s (L!i)" using assms invCat4_def lm14jjj by metis then have 
1: "i<size ?L" using assms lm14r by metis then moreover have 
0: "map fst (?L!i)=(?LL!i)" by simp ultimately moreover have
7: "j<size (?LL!i)" using assms  8 lm14iii by force ultimately have 
6: "fst ((?L!i)!j)= (?LL!i)!j" using list_update_id map_update nth_list_update_eq 0 by metis
have "fst ((?c ?L)!m)=fst ((?L!i)!j)"  using assms(1,2,3) 
by (simp add: "0" "6" "8" add.left_neutral diff_zero invCat4_def length_replicate length_upt lm14t map_eq_conv nth_replicate nth_upt nth_zip) 
moreover have "...=(?LL!i)!j" using 6 by blast  moreover have 
3: "m<?s (?c ?LL) " using assms 2 by presburger moreover have "map fst (?c ?L)=?c ?LL" using lm14n 
by metis ultimately moreover have "(?c ?LL)!m=fst((?c ?L)!m)" using lm14o by metis ultimately have 
4: "(?c ?LL)!m = (?LL!i)!j" by presburger have "distinct (concat ?LL)" using lm14z assms by blast 
moreover have "i<size ?LL" using 1 by simp moreover have  "j<size(?LL!i)" using 7 by blast 
moreover have "m<size (concat ?LL)" using 3 by simp moreover have "(?c ?LL)!m = (?LL!i)!j" using 4 by blast ultimately have 
5: "m=?s(?c (take i ?LL))+j" by (rule lm14m) 
have "?s (?c (take i ?LL))=?s (?c (take i ?L))" using lm14p by fast moreover have 
"?s (?c (take i (?f' L))) = ?s (?c (take i L))" using lm14q by (metis invCat4_def)
ultimately show ?thesis using 5 unfolding invCat4_def by presburger
qed

lemma assumes "m=size (concat (take i L)) + j" "j<size (L!i)" "i<size L"  
shows "take m (concat L) = (concat (take i L)) @ (take j (L!i))"  using assms lm14k by blast


lemma assumes "i<size l" shows "l=(take i l) @ [l!i] @ drop (Suc i) l" 
by (simp add: Cons_nth_drop_Suc assms)


(* A generalisation of filterpositions deciding based not only on the entry, 
but also on its position in the list: *)
abbreviation "fullFilter' P l == [i. i<-[0..< length l], P i (l!i)]"

lemma "fullFilter' (\<lambda>i. P) l == filterpositions2 P l" by (simp add: filterpositions2_def)
find_consts "'a set => 'a set"
(* Conditions whereby Finer and X form a balanced decomposition of a given tuple Coarser *)
abbreviation "isBDecomp0' Finer X Coarser == isFiner' Finer Coarser & [] \<notin> set Finer & 
(let one=sublist Finer X in set (bal' (concat one)) \<subseteq> {0} & length one \<le> length Coarser) & 
(let other=sublist Finer (-X) in set (bal' (concat other)) \<subseteq> {0} & length other \<le> length Coarser)"

section{*scraps*}
(*  
lemma "let middle=map (\<lambda> (i, l). map (\<lambda>e. (i, e)) l) (indicizeList' L) in  
let LL=map indicizeList' middle in map (map trd) LL=L" 
proof-
let ?i=indicizeList
let ?middle="map (\<lambda> (i, l). map (\<lambda>e. (i, e)) l) (?i L)" let ?LL="map ?i ?middle"
have "size ?middle = size (?i L)" by simp moreover have "... = size L" by (metis abc5.lm14f) 
ultimately have 
0: "size ?middle = size (?i L) & size ?middle = size L" by simp
{ fix i assume "i<size L" then moreover have "size (L!i) = size (?LL!i)" sorry
  ultimately have "?LL!i=
  zip [0..<size (?LL!i)] (zip (replicate (size (L!i)) i)  (L!i))" using 0 sorry
}
thus ?thesis sorry
qed


lemma assumes "i< length L" "concat L = l" "(d=(0::nat)) \<or> (d=1)" "(j,i) = (invCat3' L)!m"
"(i, j) = invCat1' L m" "(i, j) = invCat2' L m" "(i,j)=fst (concat (invCat4' L)!m)"
"m < length l" "m + d < length l"
 "[] \<notin> set L" "L\<noteq>[]" "l\<noteq>[]"  "j < length (L!i)" "j < length l" "length L \<le> length l" "i< length l" 
shows
"take m (concat L) = (concat (take i L)) @ (take j (L!i))"
"posRepl0' m (m+d) injected l = concat (posRepl0' i (i+1) [(posRepl0' j (j+d) injected (L!i))] L)"
using assms print_state 
proof-
let ?c=concat let ?rp=posRepl0' let ?seg="?rp j (j+d) injected (L!i)" let ?L="?rp i (i+1) [?seg] L" have
2: "j<size (L!i)" sorry have 
1: "(j+d) < size (L!i)" sorry have 
0: "i<size ?L " sorry then
have "(?c ?L) = (?c (take i ?L)) @ (?L!i) @ ?c(drop (i+1) ?L)" using  Cons_nth_drop_Suc assms 
 One_nat_def add.right_neutral add_Suc_right append_take_drop_id concat.simps(2) concat_append
by (smt)
moreover have "?L!i = ?seg" using 0  abc5.lm10d append_Cons append_take_drop_id assms(1) nth_via_drop same_append_eq 
by smt
moreover have "... = take j (L!i) @ injected @ drop (j+d) (L!i)" by force
moreover have "?c (take i ?L)= ?c (take i L)" using assms(1) by auto moreover have 
"?c (drop (i+1) ?L) = ?c (drop (i+1)L)" 
by (metis One_nat_def Suc_eq_plus1 abc5.lm10d assms(1) list.size(3) list.size(4))
ultimately have "?c ?L = (?c (take i L)) @ take j (L!i) @ injected @ drop (j+d) (L!i) @ (?c (drop (i+1) L))" 
by force moreover have "(?c (take i L)) @ take j (L!i) = take m (?c L)" try0 (*KEY STEP !!!!!!*)
have "take m (?c ?L) = concat (take i ?L) @ (take j ?seg)" sorry moreover have 

unfolding indicizeList_def by simp moreover have "size L = size ?middle" sorry 

have "\<forall>k<size L. map snd (?middle!k)= L!k" using indicizeList_def sorry
have "map trd' ?l=concat L" using assms sorry
let ?rp=posRepl0' let ?l'="?rp m (m+d) injected l" let ?newSegment="?rp j (j+d) injected (L!i)" 
let ?L'="?rp i (i+1) [?newSegment] L" have 
0: "take i L=take i ?L'" using assms(1) by auto moreover
have "drop (i+1) L = drop (i+1) ?L'" using assms(1) One_nat_def Suc_eq_plus1 abc5.lm10d list.size(3) list.size(4)
by (metis) ultimately have "concat ?L' = (concat (take i L)) @ (?L'!i) @ concat (drop (i+1) L)"
using append_Cons append_take_drop_id  nth_via_drop
by (smt append_Nil concat.simps(2) concat_append same_append_eq)
have "?L'!i = ?newSegment" using 0 append_Cons append_take_drop_id  nth_via_drop 
using same_append_eq by force
have "take m ?l' = take m l" using assms(7) by simp have "drop (m+d) l=drop (m+size injected) ?l'"
using assms lm10d by metis
have "drop m (take (m+size injected) (concat ?L')) = injected" sorry
have "take m (concat L) = (concat (take i L)) @ (take j (L!i))" 
using  less_not_refl take_Nil sorry
qed

lemma assumes "isBDecomp0' L X M" shows "length (sublist L X) + length (sublist L (-X)) \<ge> length M" 
using assms sorry
lemma "sublist l X = sublist l (X \<inter> {0..< length l})" sorry

lemma "isBDecomp0' L X M = isBDecomp0' L (X \<inter> {0..<length L}) M" sorry
term isBDecomp0'
term "a partitions b"
lemma "set `(allProperParts' [0..<n]) = (% part. sorted_list_of_set`part)` (all_partitions {0..<n})"
unfolding all_partitions_def is_partition_of_def
sorry

lemma assumes "r\<noteq>[]" "[]\<notin>set r" "isFiner' p r"  shows  
"isFiner' (let (i,j)= (invCat2' (hd r) n) in posRepl0' i (i+1) [posRepl0' j (j) r' (hd p)] p) 
(posRepl0' 0 1 [posRepl0' n (n) r' (hd r)] r)" using assms sorry

lemma assumes "r\<noteq>[]" "[]\<notin>set r" "isFiner' p r" "\<delta>=0 \<or> \<delta>=1" shows  
"isFiner' (let (i,j)= (invCat2' (hd r) n) in posRepl0' i (i+1) [posRepl0' j (j+\<delta>) r' (hd p)] p) 
(posRepl0' 0 1 [posRepl0' n (n+\<delta>) r' (hd r)] r)" using assms sorry 

proposition assumes "r\<noteq>[]" "[]\<notin>set r" "isBDecomp0' p X r" "\<delta>=0 \<or> \<delta>=1" "bal' r' = bal' (posRepl0' i (i+\<delta>) r' (hd r))" 
obtains q Y where "isBDecomp0' q Y (posRepl0' 0 1 [posRepl0' i (i+\<delta>) r' (hd r)] r)"
proof-
let ?i'=isBDecomp0' 
obtain 
qed

value "map id (bumps01' ''aAbB'' ''aAbaBabbbA'')"
value "let strg=''abBA'' in factor0' 1 2 strg"

lemma assumes "iot=set Iota00'" "i\<in>Range iot" "lowcase=((iot^-1),,\<bar>i\<bar>)" "upcase = ((iot^-1),,(-\<bar>i\<bar>))" 
shows "int((count o mset) ss lowcase) - ((count o mset) ss upcase)=(bal' s) ! (nat \<bar>i\<bar>)"
using assms sorry

value "bal' ''abcb''"
term prefixeq
find_consts "'a list => 'a list"
value "map (%(x,y). x-y) [(1::int, 2::nat)]"
value "rel2List {(0,0),(1,10::nat),(2,3)}"
find_consts "(nat \<times> 'a) set => ('a list)"
value "let s=''abAb'' in mset s"
value "count (mset [0::nat,1,0]) 3"

lemma assumes "L\<noteq>[]" "[] \<notin> set L" "ind < length(concat L)" 
"let I=[listsum (map length (take s L)). s<-[0..<1+length L]] in findFirstIndex (\<lambda>i. i\<ge>1+ind) I  \<noteq> None"
shows "let I=[listsum (map length (take s L)). s<-[0..<1+length L]] in 
let J=the (findFirstIndex (\<lambda>i. i\<ge>1+ind) I) in
take (length (L!(J-1))) (drop (length (concat (take (J-1) L))) (concat L) )
 = L!(the (findFirstIndex (\<lambda>i. i\<ge>1+ind) I)-1)"
apply (induct L) 
print_state apply (induct ind)
apply (induct ind)
using assms lm00 lm01 findFirstIndex_def 
sorry

lemma assumes "(i,j) = invCat1' L n" "L\<noteq>[]" "[] \<notin> set L" "n< length (concat L)"  
"j=0" 
shows "take (length (L!i)) (concat (drop i L))=L!i" 
unfolding assms(1) 
print_state using assms lm00 lm01 apply (induct L)
try0
print_state apply blast
print_state using assms lm00 lm01 sorry

lemma "P(l!i)= (P o nth l) i" sorry

lemma assumes "m< length l" "m+n < length l" shows "sublist l {m..<m+n} = drop m (take (m+n) l)"
sorry

lemma assumes "m<n" "m < length L" "n < length L" shows 
"sublist (posRepl0' m n l L) {m..<m+length l} = l" using assms lm10a lm10b sorry 

lemma assumes "i=fst (invCat2' L n)" "n<length (concat L)" 
"[] \<notin> set L" "\<forall>j<length L. L!j\<noteq>[]" "L\<noteq>[]" "concat L \<noteq> []" "L!i \<noteq> []" "i<length L"
"length (concat (take i L))\<le>n" shows
"n-length(concat(take i L)) < length (L!i)"
(* "n-length(concat(take i L))=j" *)using assms unfolding assms(1) 
print_state using assms lm06g lm06a lm06b lm06c lm06d lm06e lm06f 
print_state sorry

(*lemma assumes "n<length (concat L)" "(i,j) = invConcat' L n" 
"length (concat (take i L))\<le>n" 
"n-length(concat(take i L)) < length (L!i)"
shows "n-length(concat(take i L))\<ge>j"
using assms sorry
*)
(*
lemma lm04: assumes "[] \<notin> set L" "L\<noteq>[]" "concat L \<noteq> []" (*unused*) 
"i<length L" "n<length (concat L)" "length (concat (take i L))\<le>n" 
"length (concat (take (Suc i) L))\<ge>n+1" "L!i \<noteq> []" (*unused*) 
"n-length(concat(take i L)) < length (L!i)" shows
"(concat L)!n = (L!i)!(n-length(concat(take i L)))" using assms(4,5,6,9) 
 (* by (smt (verit, del_insts) Cons_nth_drop_Suc append_take_drop_id concat.simps(2) concat_append le_antisym nat_less_le nth_append nth_append_length_plus)*)
   (*add_diff_cancel_left' le_Suc_ex le_trans less_imp_le_nat nth_drop nth_take lm00 *)
proof -
  obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
    "\<forall>x0 x1. (\<exists>v2. x0 = x1 + v2) = (x0 = x1 + nn x0 x1)"
    by moura
  then have "\<forall>n na. \<not> n \<le> na \<or> na = n + nn na n"
    using le_Suc_ex by presburger
  then have f1: "n = length (concat (take i L)) + nn n (length (concat (take i L)))"
    using assms(6) by blast
  have "L ! i = take (length (L ! i)) (drop (length (concat (take i L))) (concat L))"
    using abc4.lm00 assms(4) by blast
  then show ?thesis
    using f1 by (metis (no_types) add_diff_cancel_left' assms(5) assms(9) less_imp_le_nat nth_drop nth_take)
qed 
(*
  by (metis Cons_nth_drop_Suc append.assoc append_Cons append_take_drop_id concat.simps(2) concat_append list.inject nth_append same_append_eq take_all)
*)

lemma lm05: assumes "[] \<notin> set L" "L\<noteq>[]" "concat L \<noteq> []" (*unused*) 
 "n<length (concat L)" "length (concat (take i L))\<le>n" 
"length (concat (take (Suc i) L))\<ge>n+1" "L!i \<noteq> []" (*unused*) 
"n-length(concat(take i L)) < length (L!i)" shows "i<length L" using assms(4,5) 
 Suc_le_lessD le_Suc_ex not_add_less1 not_less_eq_eq take_all by fastforce 
*)



lemma assumes  "(i,j) = invCat1' L n" "L\<noteq>[]" "[] \<notin> set L" "n<length (concat L)" "j<length (L!i)"
"length (concat (drop i L)) \<le> length (concat L)"
"j<length (concat (drop i L))" 
"let I=[listsum (map length (take s L)). s<-[0..<1+length L]] in findFirstIndex (\<lambda>i. i\<ge>1+n) I  \<noteq> None"
shows 
"let (i,j) = invCat1' L n in (concat (drop i L))!j=(concat L)!n"
using assms lm06g sorry

lemma assumes "n=length L" "i<length (concat L)" "concat L\<noteq>[]" "L\<noteq>[]" "[] \<notin> set L"shows 
"(concat L)!i = (L!(fst (invConcat' L i)))!(snd (invConcat' L i))"
sorry

lemma assumes "l\<noteq>[]" shows "concat((map concat) l)=concat(concat l)"
proof-
let ?s=set let ?c=concat let ?m=map let ?coarser="?m ?c l" let ?finer="?c l"
have 
00: "?s o ?c o (?m ?c)=?s o ?c o ?c" by force then have
01: "?s(?c(?coarser)) = ?s(?c(?finer))" by force then have
"length (?c ?coarser) \<ge> length (?c ?finer)" sorry
{
  fix i assume "i<length (?c ?finer)" then have "(?c ?coarser)!i = (?c ?finer)!i" 
  using assms 00 01 sorry
  }
show ?thesis sorry
qed

lemma assumes "L1=map concat LL2" shows "concat (map concat LL2)=(concat o concat) LL2"
  sorry

lemma "filterpositions2 = filterpositions" sorry
lemma "sublist l (set (filterpositions P l)) = filter P l" sorry

lemma assumes "i<card X" "i<length l" "Sup X < length l" "finite X" "X\<noteq>{}" shows 
"(sublist l X)!i = l!((sorted_list_of_set X)!i)"
using assms sorted.Nil sorted_list_of_set sorted_list_of_set.infinite sorry

lemma assumes "l \<in> set (sublists m)" "m \<in> set (sublists l)" shows "length l \<le> length m" using
assms sorry
*)
end 