theory Sefm

imports myDigraph les les2 les3a Conflicts 
(*"~/afp/Graph_Theory/Arc_Walk"*)
begin

abbreviation "IsRoot Po r==(\<forall> n. (Po r n \<longrightarrow> n=r))"
abbreviation "IsLeaf Po l==(\<forall> n. (Po n l \<longrightarrow> l=n))"
abbreviation "Inj f == (\<forall> x1 x2. f x1=f x2 \<longrightarrow> x1=x2)"
(*datatype mbc00 = e0 | e1 | e2 | e3 | e4 | g0 | g1 | g2 | g3 | g4 | ff0 | ff1 | ff2 | ff3*)

term "reflcl"

abbreviation "isConfiguration2' P Cf X == isConflictFree2 Cf X & isDownwardClosed P X"

abbreviation "isTrace' P Cf X == isConfiguration2' P Cf X & (\<forall> Y. Y \<supset> X \<longrightarrow> 
\<not> (isConfiguration2' P Cf Y))"
definition "isTrace=isTrace'"
abbreviation "isTrace2' P Cf X == isConfiguration2' P Cf X & 
(\<forall> y\<in> (events P)-X.  \<not> (isConfiguration2' P Cf (X\<union>{y})))"
definition "isTrace2=isTrace2'"

lemma lm01: assumes "isConflictFree2 Cf Y" "X\<subseteq>Y" shows "isConflictFree2 Cf X" using assms 
unfolding isConflictFree2_def by blast

lemma lm08b: assumes "isTrace' P Cf X" shows "isTrace2' P Cf X" using assms by blast

lemma lm01f:  assumes "wf (strict P)" "A\<noteq>{}" "A\<subseteq>Field (strict P)" shows 
"\<exists> m\<in>A. \<forall> x\<in>A. (x,m)\<in>P \<longrightarrow> x=m" using assms wf_eq_minimal2 DiffI mem_Collect_eq 
prod.inject unfolding strict_def by (smt)

lemma lm01g: assumes "wf (strict P)" "A\<noteq>{}" shows "\<exists> m\<in>A. \<forall> x\<in>A. (x,m)\<in>P \<longrightarrow> x=m"
proof -
let ?A="A \<inter> Field (strict P)" have 
0: "?A={} \<longrightarrow> ?thesis" using assms(1,2) unfolding Field_def strict_def by blast
{
  assume "?A\<noteq>{}" moreover have 3: "?A\<subseteq>Field(strict P)" by auto ultimately
  obtain m where 
  1: "m\<in>?A & (\<forall> x\<in>?A. (x,m)\<in>P \<longrightarrow> x=m)" using assms(1) lm01f by (metis)
  {
    fix x assume "x\<in>A-?A" moreover assume "(x,m)\<in>P"
    ultimately have "x=m" unfolding strict_def Field_def by blast     
  }
  then have "\<exists> m\<in>?A. (\<forall> x\<in>A. (x,m)\<in>P \<longrightarrow> x=m)" using 1 by blast
}
thus ?thesis using 0 by blast
qed

(*minimal element existence*)
corollary lm01h: assumes "finite (strict P)" "trans P" "antisym P" "A\<noteq>{}" shows 
"\<exists> m\<in>A. \<forall> x\<in>A. (x,m)\<in>P \<longrightarrow> x=m" using assms(1,2,3,4) mm05k lm01g by blast

lemma lm08a: assumes "isTrace2' P Cf X" "wf (strict P)"
shows "isTrace' P Cf X" 
proof - have
2: "isDownwardClosed' P X" using assms(1) unfolding isDownwardClosed_def by fast
{
  assume "\<not> ?thesis" then obtain Y where 
  0: "Y \<supset> X & (isConfiguration2' P Cf Y)" using assms(1) by meson obtain y where 
  1: "y\<in>Y-X & (\<forall> z\<in>Y-X. (z,y)\<in>P \<longrightarrow> z=y)" using 0 assms(2) equals0D psubset_imp_ex_mem lm01g 
by (metis ) let ?Y="X\<union>{y}" have
  "?Y \<subseteq> Y" using 1 0 by blast moreover have "isConflictFree2 Cf Y" using 0 by blast
  ultimately moreover have "isConflictFree2 Cf ?Y" using lm01 by metis
  moreover have "isDownwardClosed P Y" using 0 by simp
  ultimately moreover have "isDownwardClosed P ?Y" using assms(1) 0 1 2 DiffD2 Un_iff 
  insert_Diff insert_iff subset_trans mm01d unfolding isDownwardClosed_def by blast 
  ultimately have "isConfiguration2' P Cf ?Y" by blast then have 
  "\<exists> y\<in>(events P)-X.  (isConfiguration2' P Cf (X\<union>{y}))" using 0 1 
  unfolding isDownwardClosed_def by auto then have  "\<not>(isTrace2' P Cf X)" using assms(1) by blast
}
thus ?thesis using assms(1) by fast
qed



lemma lm08: assumes "trans P" "antisym P" "isTrace2' P Cf X" "finite (strict P)" shows 
"isTrace' P Cf X" using assms lm08a mm05k by blast

(*
lemma (*lm00a:*) assumes "X\<subseteq>events P" "isLes P Cf" "Domain Cf \<subseteq> Domain P" "isTrace' P Cf X" 
"j \<in> (next1 P {i})" "k\<in> next1 P {i}" "i\<in>X" "j \<in> X" "k \<notin> X" shows "(j,k)\<in>Cf"
proof-
{
assume "(j,k) \<notin>Cf" moreover have "(P^-1``{k}) \<subseteq> events P" unfolding Field_def by fast 
moreover have "(\<forall>e f. e \<in> P^-1``{k} & (f, e) \<in> P \<longrightarrow> f \<in> P^-1``{k})"
using assms trans_def antisym_def Image_singleton_iff converse_iff by (smt)
moreover have "isConflictFree2' Cf X" using assms unfolding isConflictFree2_def by fast
ultimately moreover have "isConflictFree2' Cf (X \<union> P^-1``{k})" using assms 
sorry
ultimately moreover have "isConfiguration2' P Cf (X\<union>P^-1``{k})" using assms
unfolding isTrace_def isDownwardClosed_def isConflictFree2_def by auto
ultimately moreover have "k\<in>P^-1``{k}" using assms refl_on_def Field_def
next1_def DiffD1 Image_singleton_iff Range.RangeI Un_iff converse.intros
by (smt)
ultimately have "\<not> isTrace' P Cf X" by (metis Sigma_cong UnCI Un_upper1 assms(9) psubsetI)
}
thus ?thesis using assms unfolding isTrace_def by blast
qed
*)

lemma lm00b: assumes "isTrace P Cf X" "j \<in> X" "(j,k)\<in>Cf" shows "k\<notin>X" using assms
unfolding isTrace_def isConflictFree2_def by blast

(*
lemma (*lm00c:*) assumes "X\<subseteq>events P" "isLes P Cf" "Domain Cf \<subseteq> Domain P" "isTrace P Cf X"
"j \<in> (next1 P {i})" "k\<in> next1 P {i}" "i\<in>X" "j\<in>X" 
shows "(j,k)\<in>Cf = (k \<notin> X)" (is "?l=?r") 
proof -
{ assume 9: ?l have ?r using assms(4,8) 9 by (rule lm00b) } then have 0: "?l \<longrightarrow> ?r" by simp
{ assume 9: ?r have ?l using assms 9 sorry } thus ?thesis using 0 by blast
qed
*)

lemma assumes "isPo' P" "l\<in>Range P - (Domain (P-{(l,l)}))" "i\<in>Domain P" "finite P"
"j \<in> next1' P {i}" "j\<noteq>l" "l\<notin>next1' P {i}" shows "j\<in> next1' (P-UNIV\<times>{l}) {i}"
using assms mm05l  trans_def antisym_def reflex_def refl_on_def by blast

lemma lm01a: "next1 P {i} \<subseteq> {l}\<union>next1(P-UNIV\<times>{l}) {i}" unfolding next1_def by auto

lemma lm01b: assumes "l \<notin> Domain (P-{(l,l)})" shows "next1 (P-UNIV\<times>{l}) {i} \<subseteq> next1 P {i}"
using assms unfolding next1_def by blast
lemma lm03: "l \<notin> Domain (P-{(l,l)}) = (P``{l}\<subseteq>{l})" by blast

lemma lm03a: "isDownwardClosed P X = (X\<union>P^-1``X \<subseteq> X\<inter>Field P)" unfolding isDownwardClosed_def by blast

lemma lm01c: assumes "l \<notin> Domain (P-{(l,l)})" "l\<notin>X" "reflex P" 
"isDownwardClosed P X" shows "isDownwardClosed (P-UNIV\<times>{l}) X" using assms 
proof - let ?p="P-UNIV\<times>{l}"
have "X\<subseteq>events ?p" using assms(1,2,3,4) Diff_iff Field_def Range.RangeI RangeE 
Range_Diff_subset contra_subsetD mem_Sigma_iff refl_onD reflex_def rev_subsetD 
subsetI sup_ge2 singletonD unfolding isDownwardClosed_def by (smt) thus ?thesis using assms 
unfolding isDownwardClosed_def by blast
qed

lemma lm01d: assumes "l\<notin>X" "isDownwardClosed (P-UNIV\<times>{l}) X" shows
"isDownwardClosed P X" using assms(1,2) unfolding isDownwardClosed_def 
Field_def by blast

lemma lm01e: assumes "reflex P" "l\<notin>X \<union> Domain (P-{(l,l)})" shows
"isDownwardClosed (P-UNIV\<times>{l}) X = isDownwardClosed P X" (is "?l=?r")
proof -
have "?l \<longrightarrow> ?r" using assms lm01d by fast moreover have 
"?r \<longrightarrow> ?l" using assms lm01c by force ultimately show ?thesis by blast
qed

lemma lm05: assumes "l\<in>Range P" "l \<notin> Domain (P-{(l,l)})" "reflex P" shows "reflex (P-UNIV\<times>{l})"
proof - let?p="P-UNIV\<times>{l}"
{
  fix x assume "x\<in>Field ?p" then moreover have "(x,x)\<in>P" using assms unfolding reflex_def refl_on_def Field_def by blast
  ultimately moreover have "x\<noteq>l" using assms unfolding Field_def by blast
  ultimately have "(x,x)\<in>?p" using assms by blast
}
thus ?thesis unfolding reflex_def using assms Domain.DomainI FieldI2 Field_def UnI1 mm01f by (smt)
qed

lemma lm02: assumes "next1 P {x} \<noteq> {}" shows "\<not>(P``{x} \<subseteq> {x})"
using assms unfolding next1_def by blast

lemma mm05ll: assumes "reflex Or" "trans Or" "antisym Or" "wf (strict Or)" 
"(x,z)\<in>Or" "z\<noteq>x" shows
"EX y. ((y,z) \<in> Or & y \<in> next1 Or {x})" using assms mm05l unfolding next1_def by fastforce 

lemma lm04: assumes "isPo' P" "wf (strict P)" "x\<in>P^-1``X-X" shows 
"\<exists> y\<in>next1 P {x}. y\<in>P^-1``X" using assms mm05ll DiffD1 DiffD2 ImageE ImageI converse.intros converseD
by (metis (no_types, lifting))

corollary lm04a: assumes "isPo' P" "finite (strict P)" "x\<in>P^-1``X-X" shows 
"\<exists> y\<in>next1 P {x}. y\<in>P^-1``X" using assms lm04 mm05k by metis

lemma assumes "isDownwardClosed' P X" shows "(P^-1``X-X={})" using assms by blast

lemma lm06: assumes "j\<in>next1 P {i}" shows "i\<in>next1 (P^-1) {j}" using assms unfolding next1_def by auto

lemma lm07: assumes "isPo' (P^-1)" "wf (strict (P^-1))" "l\<in>Range (P-{(l,l)})" 
shows "\<exists> p. p\<in>next1 (P^-1) {l}" using assms mm05ll by fastforce

lemma lm07b: assumes "isPo' (P^-1)" "finite (strict (P^-1))" "l\<in>Range (P-{(l,l)})" 
shows "\<exists> p. p\<in>next1 (P^-1) {l}" using assms lm07 by (simp add: Sefm.lm07 mm05k)

lemma lm04c: "strict (P^-1)=(strict P)^-1" unfolding strict_def by blast

theorem lm04b: assumes "isPo' P" "finite (strict P)"  
"\<forall> i. i\<in>Domain (P-{(i,i)}) & next1 P {i} \<inter> X\<noteq>{} \<longrightarrow> i\<in>X"
"X\<subseteq>Field P" shows "isDownwardClosed P X"
proof- let ?A="P^-1``X-X" have 
"finite (strict (P^-1)) & finite (strict P)" using assms(2) lm04c strict_def 
by (smt finite_converse) then have 
2: "wf (strict (P^-1))" using assms(1) by (simp add: mm05k) have
3: "wf (strict P)" using assms(1,2) by (simp add: mm05k)
{
assume "?A\<noteq>{}" then have "\<exists> m\<in>?A. \<forall> x\<in>?A. (x,m)\<in>P^-1 \<longrightarrow> x=m" 
using 2 lm01g by metis then obtain m where 
0: "m\<in>?A & (\<forall> x\<in>?A. (m,x)\<in>P\<longrightarrow>x=m)" by auto have "next1 P {m} \<noteq>{}" 
using assms(1) 3 0 Sefm.lm04 by fastforce then obtain x where
1: "x\<in>next1 P {m} & x\<in>P^-1``X" using assms(1) 3 lm04 0 by metis then moreover have "(m,x)\<in>P" 
unfolding next1_def by blast ultimately moreover have "x\<noteq>m" unfolding next1_def by blast
ultimately moreover have "x\<notin>P^-1``X-X" using 0 by blast ultimately moreover have "x\<in>X" by blast 
ultimately have "m\<in>X" using assms(3) 1 by blast
then have False using 0 by blast
}
then show ?thesis using assms(4) unfolding isDownwardClosed_def by blast
qed

lemma lm04d: assumes "isPo' P" "\<forall>l\<in>Range P. l\<notin>Domain (P-{(l,l)}) \<longrightarrow> l\<in>X" 
"isDownwardClosed P X" "wf (strict (P^-1))" shows "Domain P \<subseteq> X" 
proof - 
{
  fix x let ?A="P``{x}" assume "x\<in>Domain P" then have "?A\<noteq>{}" by fast then obtain l where
  1: "l\<in>?A & (\<forall>z\<in>?A. (z,l)\<in>P^-1 \<longrightarrow> z=l)" using assms(4) lm01g by metis have 
  "\<forall>z\<in>?A. (l,z)\<in>P \<longrightarrow> z=l" using 1 by blast then have "l\<notin>Domain (P-{(l,l)})" using assms(1) 1 
  Diff_iff DomainE Image_singleton_iff singletonI transD by (metis) then moreover have "l\<in>X" 
  using assms using "1" by blast ultimately moreover have "P^-1``{l}\<subseteq>X" using assms(3)
  unfolding isDownwardClosed_def by blast moreover have "x\<in>P^-1``{l}" using 1 by blast ultimately have "x\<in>X" by blast
}
then show "Domain P \<subseteq>X" by blast
qed

abbreviation "p0' X P Cf == isConflictFree2 Cf (X\<inter>Domain P - (Range (strict P)))
& (\<forall>Y. Y\<supset>X\<inter>Domain P - (Range (strict P)) & Y\<subseteq>Domain P - (Range (strict P))\<longrightarrow>\<not>(isConflictFree2 Cf Y))"
abbreviation "p1' X P == \<forall>i. i\<in>Domain (P-{(i,i)}) \<longrightarrow> (i\<in>X = (X\<inter>next1 P {i} \<noteq> {}))"
abbreviation "p2' X P Cf == 
              \<forall>i j k. j\<in>next1 P {i} & k\<in> next1 P {i} & i\<in>X & j\<in>X \<longrightarrow> ((j,k)\<in>Cf = (k\<notin>X))"

theorem assumes "isPo' P" "finite (strict P)"  
"\<forall> i. i\<in>Domain (P-{(i,i)}) & next1 P {i}\<inter>X \<noteq> {} \<longrightarrow> i\<in>X"
"\<forall>l\<in>Range P. l\<notin>Domain (P-{(l,l)}) \<longrightarrow> l\<in>X" "X\<subseteq>Field P" shows "X=Field P"
proof -
have "isDownwardClosed P X" using assms(1,2,3,5) by (rule lm04b) moreover have 
"wf (strict (P^-1))" using assms(1,2) finite_acyclic_wf_converse lm04c mm05d strict_def by (metis)
ultimately have "Domain P \<subseteq> X" using assms(1,4) lm04d by metis
thus ?thesis using assms by (simp add: `X \<subseteq> events P` antisym_conv les2.lm31 reflex_def)
qed

theorem assumes "isLes P Cf" "finite P" "p1' X P" "p2' X P Cf" 
"l\<in>Range(P-{(l,l)})-Domain (P-{(l,l)}) - X"
"\<forall> p cf. card p < card P & isLes p cf & p1' X p & p2' X p cf \<longrightarrow> isTrace p cf X"
shows "isTrace P Cf X"
proof - have 
  0: "l\<in>Range P & l \<in> Range(P-{(l,l)}) & l\<notin> Domain (P-{(l,l)}) & l \<notin> X" using assms(5) by blast
  let ?p="P-UNIV\<times>{l}" let ?cf="Cf-{l}\<times>UNIV-UNIV\<times>{l}"   
  have "trans ?p" using assms(1) 0 unfolding trans_def by blast moreover have "antisym ?p" 
  using assms(1) 0 unfolding antisym_def by auto moreover have "reflex P" 
  unfolding reflex_def using assms(1) unfolding reflex_def by blast then moreover have "reflex ?p" using 0 lm05 by metis   
  ultimately moreover have "isPo' ?p" by blast
  ultimately moreover have "sym ?cf" using assms(1) unfolding sym_def by blast
  ultimately moreover have "irrefl ?cf" using assms(1) unfolding irrefl_def by blast
  moreover have "isPropagatingOver4' ?cf ?p" using assms(1) by blast then moreover have 
  "isMonotonicOver ?cf ?p" using lm33a by blast ultimately have 
  23: "isLes ?p ?cf" using assms(1) 
  unfolding reflex_def by blast have "\<forall>i. next1 P {i} \<subseteq> {l} \<union> next1 ?p {i}" using lm01a by metis
  then moreover have "\<forall> i. next1 ?p {i} \<inter> X \<supseteq> next1 P {i} \<inter> X" using 0 by fast 
  ultimately moreover have "\<forall> i. next1 ?p {i} \<subseteq> next1 P {i}"  using 0 lm01b by metis
  ultimately moreover have "\<forall>i. next1 ?p {i} \<inter> X = next1 P {i} \<inter> X" by blast moreover have 
  "\<forall> i\<in>Domain ?p. i\<in>Domain(?p-{(i,i)})\<longrightarrow>(i\<in>X=(next1 P {i}\<inter>X \<noteq> {}))" using assms(3,4) 0 by blast 
  ultimately have "\<forall> i \<in> Domain ?p. i\<in>Domain (?p-{(i,i)}) \<longrightarrow> (i\<in>X = (next1 ?p {i} \<inter> X\<noteq>{}))" 
  using 0 Domain_def by simp then have 
  21: "p1' X ?p" by blast
  {
    fix i j k assume "j\<in>next1 ?p {i}" moreover assume "k\<in>next1 ?p {i}" 
    moreover assume "i\<in>X & j\<in>X"          
    ultimately moreover have "j\<in>next1 P {i}" using 0 
    lm01b by force
    ultimately moreover have "k\<in>next1 P {i}" using 0 lm01b by force
    ultimately moreover have "(j,k)\<in>?cf \<longrightarrow> k \<notin> X" using assms(4) 0 by force
    ultimately moreover have "k \<notin> X\<longrightarrow>(j,k)\<in>?cf" using assms(4) 0 Diff_iff 
    Image_singleton_iff UNIV_I mem_Sigma_iff next1_def by (metis (no_types, lifting))
    ultimately have "(j,k)\<in>?cf = (k \<notin> X)" by meson
  }
  then have 
  22: "p2' X ?p ?cf" by force have "?p \<subset> P" using 0 by blast then
  have "card ?p<card P" using assms(2) by (simp add: psubset_card_mono) then have 
  9: "isTrace' ?p ?cf X" using assms(6) 21 22 23 unfolding isTrace_def by blast
  then have "isDownwardClosed ?p X" by blast moreover have "l\<notin>X\<union>Domain(P-{(l,l)})" 
  using 0 by fast moreover have 
  2: "reflex P" using assms(1) unfolding reflex_def by blast
  ultimately have "isDownwardClosed P X" using lm01e by metis moreover have 
  "isConflictFree2' ?cf X" using 9 unfolding isConflictFree2_def by fast then moreover have 
  "isConflictFree2 Cf X" unfolding isConflictFree2_def using 0 by auto ultimately have 
  12: "isConfiguration2' P Cf X" by auto have
  1: "isTrace2' ?p ?cf X" using 9 by auto
  {
    fix y let ?Y="X\<union>{y}" assume "y\<in>(Field P)-X" moreover assume "isConfiguration2' P Cf ?Y"
     moreover assume "y\<noteq>l" then moreover have "l\<notin>?Y\<union>Domain(P-{(l,l)})" using 0 by fast
     ultimately moreover have "isDownwardClosed ?p ?Y" using 2 lm01e by metis 
     ultimately moreover have "isConflictFree2' Cf ?Y" unfolding isConflictFree2_def by fast
     ultimately moreover have "isConflictFree2 ?cf ?Y" unfolding isConflictFree2_def by blast
     ultimately moreover have "isConfiguration2' ?p ?cf ?Y" by blast
     ultimately have False using 1 
     `isConfiguration2' (P - UNIV \<times> {l}) (Cf - {l} \<times> UNIV - UNIV \<times> {l}) X \<and> 
     (\<forall>Y. X \<subset> Y \<longrightarrow> \<not> isConfiguration2' (P - UNIV \<times> {l}) (Cf - {l} \<times> UNIV - UNIV \<times> {l}) Y)` by blast
  } then have 
  11: "\<forall>y\<in>Field P - X. isConfiguration2' P Cf (X\<union>{y}) \<longrightarrow> y=l" by blast
  {
   fix y let ?Y="X\<union>{y}" assume "y\<in>(Field P)-X" moreover assume 
   8: "isConfiguration2' P Cf ?Y" moreover assume 
   7: "y=l" have "isPo' (P^-1)" using assms(1) by (simp add: reflex_def)
   moreover have "finite (strict (P^-1))" using assms(2) finite_converse lm04c by (simp add: strict_def)
   ultimately obtain i where "i\<in>next1 (P^-1) {l}" using 0 lm07b by metis then
   have "l\<in>next1 P {i} " using lm06 by fastforce then moreover have "i\<in>?Y" using 7 8 
   unfolding next1_def isDownwardClosed_def by auto ultimately have
   5: "l\<in>next1 P {i} & i\<in>?Y" by meson then moreover have 
   3: "i\<in>X" using Diff_iff ImageI Image_singleton_iff UnE 7 next1_def by (smt)
   ultimately have "\<not>(P``{i} \<subseteq>{i})" using lm02 by fast then moreover have 
   4: "i\<in>Domain (P-{(i,i)})" unfolding lm03 by blast
   have "next1 P {i} \<inter> X \<noteq> {}" using assms(3) 3 4 by auto then obtain j where
   6: "j \<in> next1 P {i} \<inter> X" by blast then
   have "(j,l)\<in>Cf = (l \<notin> X)" using assms(4) 5 3 by force then have "(j,y)\<in>Cf" using 0 7 by blast
   then have "\<not> (isConflictFree2 Cf ?Y)" unfolding isConflictFree2_def using 0 6 by fast 
   then have False using 8 by blast
  } then have 
  "\<forall>y\<in>Field P - X. isConfiguration2' P Cf (X\<union>{y}) \<longrightarrow> y\<noteq>l" by blast
  then have "\<forall>y\<in>Field P - X. (\<not>(isConfiguration2' P Cf (X\<union>{y})))" using 11 by blast
  then have "isTrace2' P Cf X" using 12 by blast 
  then have "isTrace' P Cf X" using assms(1,2) lm08 unfolding strict_def by auto
  thus ?thesis unfolding isTrace_def by blast
qed

lemma lm09a: "isTrace {} Cf {}" unfolding isTrace_def isDownwardClosed_def isConflictFree2_def by auto 











































section{*ifm*}
term leaves
abbreviation "sources==roots" abbreviation "sinks==leaves"
abbreviation "isWalk' dag w == w\<noteq>[] \<longrightarrow> (\<forall>i\<in>{1..<size w}. (w!(i-1),w!i)\<in>dag)"
definition "isWalk=isWalk'"
value "isWalk ({(2,1)}::((nat\<times>nat) set)) []"
abbreviation "isTrail' dag w ==isWalk dag w & distinct w" 
lemma "swap unset=Set.member" unfolding swap_def by simp

(* abbreviation "isSink' Dag node == node \<in> sinks (pred2set Dag)"*)
abbreviation "isSink' Dag node == \<forall> child. Dag node child \<longrightarrow> node=child"
definition "isSink=isSink'"
(*abbreviation "isSource' Dag node == node \<in> sources (pred2set Dag)"*)
abbreviation "isSink2' Dag node == (isSink' Dag node) & (\<exists>p. Dag p node)"
abbreviation "isSource' Dag node == \<forall> parent. Dag parent node \<longrightarrow> node=parent"
definition "isSource=isSource'"
abbreviation "isSource2' Dag node == (isSource' Dag node)&(\<exists> c. Dag node c)"

abbreviation "conditionOne' Dag w == (\<forall> source. isSource' Dag source \<longrightarrow> w source)"
abbreviation "conditionOne2' Dag w== (\<forall> source. isSource2' Dag source \<longrightarrow> w source)"
abbreviation "conditionSix' Dag w == (\<forall> parent child1 child2. (w parent & w child1 & w child2 & 
  Dag parent child1 & Dag parent child2) \<longrightarrow> child1=child2)"
abbreviation "conditionSeven' Dag w == \<forall> parent. w parent & (\<not> isSink' Dag parent) \<longrightarrow>
(\<exists> child. (Dag parent child & w child))"
abbreviation "conditionThree' Dag w == \<forall> child. (\<not> isSource' Dag child & (\<forall>parent. Dag parent child \<longrightarrow> \<not> w parent))
\<longrightarrow> (\<not> w child)"
definition "conditionOne=conditionOne'" definition "conditionSix=conditionSix'"
definition "conditionSeven=conditionSeven'" definition "conditionThree=conditionThree'"

lemma lm11a: assumes "irrefl P" "antisym (trancl P)" shows "irrefl (trancl P)" using assms(1,2)
trancl.trancl_into_trancl converse_tranclE unfolding
antisym_def irrefl_def rtrancl_trancl_trancl tranclD by (metis(no_types,lifting)) 

lemma lm11b: assumes "irrefl (trancl P)" shows "antisym (trancl P)" using assms(1) 
trancl.trancl_into_trancl antisym_def irrefl_def rtrancl_trancl_trancl tranclD by (metis(no_types,lifting)) 

lemma lm11c: assumes "irrefl P" shows "antisym (trancl P) = irrefl (trancl P)" 
using assms lm11a lm11b by blast

lemma lm10: assumes "isWalk' dag w" shows "{n. m+(Suc n)<size w & (w!m, w!(m+Suc n))\<notin>trancl dag}={}"
proof -
let ?X="{n. m+(Suc n)<size w & (w!m, w!(m+Suc n))\<notin>trancl dag}" have "?X \<subseteq>{n. m+(Suc n)<size w}" 
by blast moreover have "... \<subseteq> {0..<size w}" using assms by fastforce moreover have "finite ..." 
by simp ultimately have 0: "finite ?X" by (simp add: finite_subset) let ?N="Min ?X"
{
  assume "?X\<noteq>{}" then have 1: "?N\<in>?X" using 0 using Min_in by blast then moreover have "?N>0" 
  using assms by force ultimately moreover have "?N-1 \<notin>?X" using "0" Min_le diff_less 
  less_one not_le by (metis (no_types, lifting) ) ultimately moreover have 
  "(w!m, w!(m+Suc (?N - 1)))\<in>trancl dag" by fastforce ultimately moreover have 
  "(w!(m+Suc ?N - 1),w!(m+Suc ?N))\<in>dag" using assms by fastforce ultimately have 
  "(w!m, w!(m+Suc ?N))\<in>trancl dag" using Nat.add_diff_assoc One_nat_def Suc_diff_1 Suc_leI 
  diff_Suc_1 trancl.r_into_trancl trancl_trans zero_less_Suc by (metis (no_types, lifting))
  then have False using 1 by blast
}
thus ?thesis by fast
qed

lemma lm10a: assumes "\<forall> m n. m+Suc n<size l \<longrightarrow> l!m \<noteq> l!(m+Suc n)" shows "distinct l" using assms(1) 
add_Suc_right distinct_conv_nth less_iff_Suc_add linorder_neqE_nat by (metis (no_types))
lemma lm10b: assumes "distinct l" shows "\<forall> m n. m+Suc n<size l \<longrightarrow> l!m \<noteq> l!(m+Suc n)" using assms(1) 
distinct_conv_nth add_diff_cancel_left' cancel_comm_monoid_add_class.diff_cancel nat.distinct(1)
add_lessD1 by (metis (no_types))
lemma lm10c: "distinct l = (\<forall> m n. m+Suc n<size l \<longrightarrow> l!m \<noteq> l!(m+Suc n))" using lm10a lm10b by blast 
lemma lm10d: "distinct l = (\<forall> m n. m+n+1<size l \<longrightarrow> l!m \<noteq> l!(m+n+1))" unfolding lm10c by auto 

theorem lm12: assumes "isWalk dag w" "irrefl dag" "antisym (trancl dag)" shows "distinct w" 
proof -
{
  fix m n assume "m+(Suc n)<size w" then have "(w!m,w!(m+Suc n)) \<in> trancl dag" using assms(1) 
  lm10 unfolding isWalk_def by blast moreover have "irrefl (trancl dag)" using assms(2,3) 
  lm11c by blast ultimately have "w!m \<noteq>w!(m+Suc n)" unfolding irrefl_def by fastforce
}
thus ?thesis unfolding lm10c by blast
qed

abbreviation "transred' Or==order2strictCover' Or" definition "transred=transred'"
abbreviation "isHasseDiagram' P == irrefl (trancl P) & transred (trancl P)=P"
abbreviation "isWalkFromSource' G w == w\<noteq>[] & w!0\<in>sources G & isWalk G w"

lemma lm13a: assumes "\<forall> parent. (w parent & \<not> isSink' G parent) \<longrightarrow> (\<exists>! child. (G parent child & w child))"
shows "conditionSeven G w" unfolding conditionSeven_def using assms(1) by blast

lemma lm13b: assumes "\<forall> parent. (w parent & \<not> isSink' G parent) \<longrightarrow> (\<exists>! child. (G parent child & w child))" shows
"conditionSix G w" unfolding conditionSix_def using assms(1) by metis

lemma lm13c: assumes "conditionSix G w" "conditionSeven G w" shows
"\<forall> parent. (w parent & \<not> isSink' G parent) \<longrightarrow> (\<exists>! child. (G parent child & w child))"
using assms unfolding conditionSix_def conditionSeven_def by blast

abbreviation "conditionTwo' G w == 
(\<forall> parent. (w parent & \<not> isSink' G parent) \<longrightarrow> (\<exists>! child. (G parent child & w child)))"
definition "conditionTwo=conditionTwo'"

lemma lm37: "(conditionSix G w & conditionSeven G w) = conditionTwo G w"
(is "?l=?r")
proof -
have "?l \<longrightarrow> ?r" unfolding conditionTwo_def using lm13c by blast 
thus ?thesis unfolding conditionTwo_def using lm13a lm13b by auto
qed

lemma assumes "n\<in>Domain (P-UNIV\<times>(sinks P))" "n\<notin>sinks P" shows "n\<notin>sinks(P-UNIV\<times>(sinks P))"  
 using assms unfolding Field_def leaves_def by blast

fun mbc01 where 
"mbc01 G W 0 = [the_elem (W\<inter>sources G)]" |
"mbc01 G W (Suc n) = (let X=W\<inter>G``{last (mbc01 G W n)} in if (X={} \<or> mbc01 G W n=[]) then [] else 
                      (mbc01 G W n)@[the_elem X])"

lemma "size (mbc01 G W n)\<le>n+1" apply (induct n) 
apply simp using Suc_eq_plus1 add_le_cancel_right length_append list.size(3,4) 
mbc01.simps(2) nat_le_linear trans_le_add2 One_nat_def by (smt)

lemma lm14: assumes "P 0" "\<forall>n. ((\<forall>m<Suc n. P m) \<longrightarrow> P(Suc n))" shows "\<forall>n. P n" using assms lm13a 
by (metis infinite_descent0 old.nat.exhaust)

lemma lm14a: assumes "P=(%n. size (mbc01 G W n)>0 \<longrightarrow>size (mbc01 G W n)=n+1)"
shows "\<forall>k. P k"
proof -
let ?f="mbc01 G W"
let ?P="(%n. size (mbc01 G W n)>0 \<longrightarrow>size (mbc01 G W n)=n+1)" have 
0: "?P 0"  by simp
{ fix n
  assume "\<forall> m<Suc n. ?P m" moreover assume "size (?f (Suc n))>0" ultimately moreover have 
  "?f n \<noteq>[] & W \<inter> G``{last (?f n)} \<noteq> {} & ?f (Suc n)= ?f n@[the_elem (W \<inter> G``{last (?f n)})]" 
  using length_greater_0_conv mbc01.simps(2) by (smt )
  ultimately moreover have "size (?f n)=n+1" by blast
  ultimately have "size (?f (Suc n))=(Suc n)+1" by auto
}
then have 
1: "\<forall>n. ((\<forall>m<Suc n. ?P m) \<longrightarrow> ?P(Suc n))" by blast
have "\<forall>n. ?P n" using 0 1 by (rule lm14) thus ?thesis using assms by blast
qed

lemma lm14b: assumes "(mbc01 G W n)\<noteq>[]" shows "size (mbc01 G W n)=n+1"
using assms lm14a by auto
lemma lm14c: assumes "mbc01 G W (Suc n)\<noteq>[]" shows "size (mbc01 G W n)=n+1" using assms lm14b by force
lemma lm14d: assumes "mbc01 G W (Suc n)\<noteq>[]" shows 
"size (mbc01 G W n)=n+1 & mbc01 G W n=take (n+1) (mbc01 G W (Suc n))" using assms lm14c 
append_eq_conv_conj length_greater_0_conv mbc01.simps(2) by smt
lemma lm14e: assumes "mbc01 G W n=[]" shows "mbc01 G W (Suc n)=[]" using assms by simp

lemma lm15: assumes "l\<noteq>[]" "isWalk' G l" "(last l, x)\<in>G" shows "isWalk' G (l@[x])" using nth_append 
assms(1,2,3) One_nat_def atLeastLessThan_iff diff_le_self diff_less last_conv_nth le_antisym 
le_trans length_append_singleton less_Suc_eq_le nat_less_le neq0_conv nth_Cons_0  zero_less_diff
by (smt)

lemma lm16: "set2pred P=(% x y. (x,y)\<in>P)" by blast

lemma assumes "isSink' (set2pred P) s" "s\<in>Field P" "irrefl P" shows "s\<notin> (Domain P)"
using assms unfolding leaves_def lm16 Field_def irrefl_def by blast

lemma lm17a: "isWalk' G (mbc01 G W 0)"  by simp

lemma lm17b: assumes "W\<inter>sources G={source}" shows "set (mbc01 G W 0)\<subseteq>W\<inter>sources G" 
proof -
have "mbc01 G W 0=[source]" using assms by fastforce then moreover have "source\<in>W \<inter> sources G" 
using assms by simp ultimately show ?thesis by simp
qed

lemma lm17c: assumes "conditionTwo' (set2pred G) (unset W)" "irrefl G"
"isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G" "mbc01 G W (Suc n)\<noteq>[]"
shows "isWalk G (mbc01 G W (Suc n)) & set (mbc01 G W (Suc n)) \<subseteq> W \<inter> Field G"
proof -
let ?f="mbc01 G W" let ?L="?f (Suc n)" have "isWalk' G (?f n) & set (?f n) \<subseteq> W \<inter> Field G"
using assms(3) unfolding isWalk_def by blast moreover have "?L=?f n @ [the_elem (W \<inter> G``{last (?f n)})]" using
 assms(4) mbc01.simps(2) by (smt) then moreover have "?f n\<noteq>[]" 
by fastforce ultimately moreover have "W\<inter>G``{last (?f n)}\<noteq>{}" by force
ultimately moreover have "last (?f n)\<in> W \<inter> Field G-sinks G" unfolding leaves_def by auto
ultimately moreover have "\<not> isSink' (set2pred G) (last (?f n))" using assms(2) unfolding 
irrefl_def by blast ultimately moreover have "\<exists>! c\<in>W. (last (?f n), c)\<in>G" using assms(1) by auto 
then moreover obtain c where "c\<in>W & W\<inter>G``{last (?f n)}={c}" by auto ultimately moreover have 
"isWalk' G ((?f n)@[c])" using lm15 by fast ultimately moreover have "isWalk' G (?f (Suc n))" 
by simp ultimately moreover have "set (?f (Suc n))= set (?f n) \<union> {c}" by auto 
ultimately show ?thesis unfolding Field_def isWalk_def by auto
qed

lemma lm17d: assumes "conditionTwo' (set2pred G) (unset W)" "irrefl G" shows 
"\<forall>n. (isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G \<longrightarrow> 
    isWalk G (mbc01 G W (Suc n)) & set (mbc01 G W (Suc n)) \<subseteq> W \<inter> Field G)"
proof -
let ?f="mbc01 G W" let ?P="%n. isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G"
{
  fix n assume "?P n" then have "?f (Suc n)\<noteq>[]\<longrightarrow>?P (Suc n)" using assms lm17c by blast
  moreover have "?f (Suc n)=[] \<longrightarrow> ?P (Suc n)" unfolding isWalk_def by force
  ultimately have "?P (Suc n)" by fast
}
thus ?thesis by blast
qed

lemma lm17e: assumes "conditionTwo' (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}"
"P=(%n. (isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G))" shows "\<forall>n. P n"
proof -
let ?f="mbc01 G W" let ?P="(%n. (isWalk G (?f n) & set (?f n) \<subseteq> W \<inter> Field G))"
have "?P 0" using assms lm17a lm17b unfolding Field_def roots_def isWalk_def by force then have 
0: "P 0" unfolding assms(4) by blast have "\<forall>n. ?P n \<longrightarrow> ?P (Suc n)" using assms(1,2) by (rule lm17d)
then have "\<forall>n. P n \<longrightarrow> P (Suc n)" unfolding assms(4) by blast
then show ?thesis using 0 using nat_induct by auto
qed

theorem lm17f: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}" 
shows "isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G"
proof - obtain P where 
4: "P=(%n. (isWalk G (mbc01 G W n) & set (mbc01 G W n) \<subseteq> W \<inter> Field G))" by blast have "\<forall>n. P n" 
using assms 4 unfolding conditionTwo_def by (rule lm17e) thus ?thesis using 4 by blast
qed

lemma lm18a: assumes "mbc01 G W (Suc n)\<noteq>[]" shows "mbc01 G W n\<noteq>[] & last (mbc01 G W n) \<in> Domain G-leaves G"
proof -
have "W\<inter>G``{last(mbc01 G W n)}\<noteq>{}" using assms(1) by force
then show ?thesis using assms unfolding leaves_def by auto
qed

lemma lm18b: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}" 
"mbc01 G W n\<noteq>[]" "last (mbc01 G W n) \<in> Domain G-leaves G" shows "mbc01 G W (Suc n)\<noteq>[]"
proof - let ?f="mbc01 G W"
have "isWalk G (?f n) & set (?f n) \<subseteq> W \<inter> Field G" using assms(1,2,3) by (rule lm17f)
moreover have "G``{last(?f n)}\<noteq>{}" using assms(5) unfolding leaves_def by blast 
ultimately moreover have "last(?f n)\<in>W" using assms(4) last_in_set by blast
ultimately have "W\<inter>G``{last(?f n)}\<noteq>{}" using assms(1) unfolding conditionTwo_def 
by fast then show ?thesis using assms by auto
qed

lemma lm18c: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}" shows 
"(mbc01 G W n\<noteq>[] & last (mbc01 G W n) \<in> Domain G-leaves G) = (mbc01 G W (Suc n)\<noteq>[])" (is "?l=?r")
proof -
have "?l\<longrightarrow>?r" using assms lm18b unfolding conditionTwo_def by metis thus ?thesis using lm18a by metis
qed

lemma fixes A::"nat set" assumes "A\<noteq>{}" shows "Inf A\<in>A" using assms 
by (metis Inf_nat_def LeastI2_wellorder all_not_in_conv)

lemma lm19: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}"
"finite (W\<inter>Field G)" "antisym (trancl G)" shows "mbc01 G W (card (W\<inter>Field G))=[]" 
proof -
have "W\<inter>Field G\<noteq>{}" using assms(3) unfolding roots_def Field_def by auto then obtain n where 
0: "card (W\<inter>Field G)=n+1" using assms(4) by (metis Suc_eq_plus1 all_not_in_conv card_Suc_Diff1)
{
  assume "mbc01 G W (n+1)\<noteq>[]" then have "size (mbc01 G W (n+1))=n+2" using lm14b by fastforce
  then moreover have "card (set (mbc01 G W (n+1)))=n+2" using assms(1,2,3,5) lm12 lm17f 
  distinct_card by (metis) moreover have "set (mbc01 G W (n+1))\<subseteq>W \<inter> Field G" 
  using assms lm17f by metis ultimately have False using 0 assms(4) card_seteq by fastforce
  }
thus ?thesis using 0 by presburger
qed

lemma lm14f: assumes "mbc01 G W m=[]" shows "mbc01 G W (m+n)=[]" apply (induct n) using assms by auto

lemma lm20: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}"
"finite (W\<inter>Field G)" "antisym (trancl G)" shows "finite {n. mbc01 G W n\<noteq>[]}"  
proof -
let ?A="{n. mbc01 G W n\<noteq>[]}" let ?n="card (W\<inter>Field G)" let ?f="mbc01 G W" 
{
  fix m assume "m\<in>?A" then have "?f m\<noteq>[]" by simp moreover have "\<forall>n. ?f (?n+n)=[]" using assms(1,2,3,4,5)  
  by (simp add: Sefm.lm14f Sefm.lm19) ultimately have "m<?n" by (metis le_add_diff_inverse not_le)
  then have "m\<in>{0..<?n}" by simp
}
then have "?A\<subseteq>{0..<?n}" by blast thus ?thesis using finite_subset by blast
qed

lemma fixes A::"nat set" assumes "finite A" shows "A\<subseteq>{0..Max A}" using assms by force

lemma lm22: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}"
"mbc01 G W m\<noteq>[]" "mbc01 G W (Suc m)=[]" shows "last (mbc01 G W m)\<in>leaves G"
proof- let ?x="last (mbc01 G W m)"
have "?x \<notin> Domain G - leaves G" using assms(1,2,3,4,5) lm18c by metis moreover have "?x\<in>Field G" 
using assms(1,2,3,4) Int_subset_iff contra_subsetD last_in_set lm17f by (metis)
ultimately show "?x\<in>leaves G" unfolding Field_def leaves_def by fast 
qed

lemma lm23: "take m (take (m+n) l)=take m l" by (simp add: min.absorb1)

lemma lm24a: assumes "P=(%m. (mbc01 G W (n+m)\<noteq>[]\<longrightarrow>mbc01 G W n=take (n+1) (mbc01 G W (n+m))))" 
shows "P m"
proof -
let ?f="mbc01 G W" let ?P="%m. (?f (n+m)\<noteq>[] \<longrightarrow> ?f n=take (n+1) (?f (n+m)))"
{
  fix M assume 
  0: "?P M" moreover assume "?f (n+Suc M)\<noteq>[]" then moreover have "?f (Suc (n+M))\<noteq>[]" by simp 
  then moreover have 
  1: "?f (n+M)=take (n+M+1) (?f (Suc(n+M)))" using assms lm14d by blast
  ultimately  have "?f (n+M)\<noteq>[]" using lm18a by blast
  then have "?f n=take (n+1) (?f (n+M))" using 0 by blast
  then have "?f n=take (n+1) (take (n+1+M) (?f (Suc(n+M))))" unfolding 1 by simp
  moreover have "...=take (n+1) (?f (Suc (n+M)))" unfolding lm23 by blast
  ultimately have "?f n=take (n+1) (?f (n+(Suc M)))" by simp
}
then have "\<forall>M. P M \<longrightarrow> P (Suc M)" unfolding assms(1) by blast
moreover have "P 0" unfolding assms(1)
by (metis One_nat_def Sefm.lm14b Suc_eq_plus1 add.right_neutral append_eq_conv_conj mbc01.elims take.simps(1) take_Suc_Cons take_add)
ultimately have "\<forall>M. P M" using nat_induct by auto thus ?thesis by simp
qed

lemma lm24b: assumes "mbc01 G W (n+m)\<noteq>[]" shows "mbc01 G W n=take (n+1) (mbc01 G W (n+m))" 
using assms lm24a by blast

theorem lm21: assumes "conditionTwo (set2pred G) (unset W)" "irrefl G" "W\<inter>sources G={source}"
"finite (W\<inter>Field G)" "antisym (trancl G)" shows 
"\<exists>!M. {n. mbc01 G W n\<noteq>[]}={0..M} & last(mbc01 G W M)\<in>sinks G & 
      size (mbc01 G W M)=M+1 & M=Max{n. mbc01 G W n\<noteq>[]} & (mbc01 G W M)!0=source"
proof -
let ?f="mbc01 G W" let ?A="{n. ?f n\<noteq>[]}" let ?M="Max ?A" let ?B="{0..?M}"
have "finite ?A" using assms(1,2,3,4,5) lm20 by metis then moreover have 
1: "?A \<subseteq> ?B" by force moreover have "0\<in>?A" by simp ultimately moreover have 
0: "?f ?M\<noteq>[]" using Max_in by blast
ultimately moreover have "?f (Suc ?M)=[]" by fastforce ultimately moreover have 
2: "last(?f ?M)\<in>sinks G" using assms(1,2,3) lm22 by metis ultimately moreover have "size(?f ?M)=?M+1" 
using lm14b by blast ultimately moreover have "(?f 0)=take 1 (?f ?M)" using lm24b add.left_neutral 
by (metis) moreover have "(?f 0)!0=source" using assms   by simp ultimately have 
3: "(?f ?M)!0=source" by fastforce
{
  fix m assume "m\<in>?B" then have "m\<le>?M" by fastforce moreover assume "?f m=[]"
  then moreover have "?f (m+(?M-m))=[]" using lm14f by blast ultimately have False using 0 by simp
}
then have "?B\<subseteq>?A" by blast then have "?A=?B" using 1 by blast thus ?thesis using 2 0 3 lm14b by blast
qed

abbreviation "convertToPath' G W==mbc01 G W (Max {n. mbc01 G W n\<noteq>[]})"
definition "convertToPath=convertToPath'"

lemma lm25: assumes "irrefl G" "n \<in> Field G" shows "n\<in>sources G= isSource' (set2pred G) n"
using assms unfolding irrefl_def Field_def roots_def by blast

lemma lm26: assumes "irrefl (trancl G)" "conditionThree' (set2pred G) (unset W)" 
"conditionTwo (set2pred G) (unset W)" "W\<inter>sources G={source}" "finite G" 
shows "W\<inter>Field G \<subseteq> set (convertToPath' G W)"
proof - have 
0: "irrefl G" by (meson assms(1) irrefl_def r_r_into_trancl) then moreover have 
11: "antisym (trancl G)" using assms(1) using lm11c by blast let ?f="mbc01 G W" let ?A="W \<inter> Field G" 
let ?M="Max {n. ?f n\<noteq>[]}" let ?l="?f ?M" let ?B="?A-set ?l" let ?P="trancl G" have 
1: "finite (strict ?P)" using assms(5) unfolding strict_def by (simp add: assms(3)) have 
2: "trans ?P" by simp have
3: "antisym ?P" using assms(1) lm11b by blast have "finite (W\<inter>Field G)" using assms(5) finite_Int 
mm10j by (metis) then have
9: "last ?l\<in>sinks G & source\<in>set ?l" using assms(2,3,4) 0 11 lm21 Suc_eq_plus1_left add.commute 
le0 le_antisym not_le nth_mem old.nat.distinct(2) by (metis) have
10: "isWalk G ?l & set ?l \<subseteq> W \<inter> Field G" using lm17f 0 assms(3,4) by metis
{
  assume "?A-set (?f ?M)\<noteq>{}" then obtain m where
  4: "m\<in>?B & (\<forall>x\<in>?B. (x,m)\<in>?P\<longrightarrow>x=m)" using 1 2 3 lm01h by metis 
  {
    fix p assume 
    5: "(p,m)\<in>G" then moreover have "p\<noteq>m" by (meson 0 irrefl_def) moreover assume 
    6: "p\<in>W" ultimately moreover have "p\<in>?A" unfolding Field_def by blast ultimately moreover have 
    "p\<in>set(?f ?M)" using 4 by blast ultimately moreover have "p\<notin>sinks G" unfolding 
    leaves_def by blast ultimately moreover obtain i where 
    8: "i<size ?l-1 & p=?l!i" using in_set_conv_nth Sefm.lm14b Suc_eq_plus1 diff_Suc_1 less_Suc_eq 
    last_conv_nth length_greater_0_conv length_pos_if_in_set 9 by (metis (lifting)) ultimately have 
    7: "(?l!i, ?l!(i+1))\<in>G & ?l!(i+1)\<in>W\<inter>set ?l" using 10 isWalk_def IntI Sefm.lm14b add.commute 
    add_diff_cancel_right' atLeastLessThan_iff le_infE length_greater_0_conv length_pos_if_in_set 
    nat_add_left_cancel_less not_add_less2 not_le nth_mem subsetCE by (smt) 
    moreover have "\<exists>! c\<in>W. (p,c)\<in>G" using assms(3) 5 6 unfolding conditionTwo_def by blast 
    moreover have "m\<in>W" using 4 by blast ultimately have "m=?l!(i+1)" using 7 5 8 by blast 
    then have False using 7 4 by blast 
  }
  then have "\<forall>p. (p,m)\<in>G\<longrightarrow>p\<notin>W" by blast moreover have "m\<in>Field G - sources G" using assms(4) 4 
  Field_def roots_def 9 by fast then moreover have  "\<not> isSource' (set2pred G) m" using 0 lm25 
  unfolding irrefl_def Field_def roots_def by blast
  ultimately have "m\<notin>W" using assms(2) by auto then have False using 4 by blast
  }
then have "?B={}" by blast then have "W\<inter>Field G \<subseteq> set (?f ?M)" by blast thus ?thesis by blast
qed

lemma lm11d: "(irrefl P & antisym (trancl P))=irrefl (trancl P)" by (meson irrefl_def lm11a lm11b r_into_trancl')

lemma lm36: assumes "conditionTwo (set2pred G) (unset W)" "irrefl (trancl G)" 
"card (W\<inter>sources G)=1" "finite G" "conditionThree (set2pred G) (unset W)" shows 
"distinct (convertToPath G W) & isWalk G (convertToPath G W) & last(convertToPath G W)\<in>sinks G & 
{hd(convertToPath G W)}=W\<inter>sources G & set (convertToPath G W)=W\<inter>Field G"
proof - let ?f="mbc01 G W" let ?M="Max {n. mbc01 G W n\<noteq>[]}" let ?l="?f ?M" let ?P="trancl G" 
let ?L="convertToPath G W" have "irrefl G & antisym ?P" using assms(2) lm11d by blast 
moreover have "finite (W\<inter>Field G)" unfolding Field_def using assms(4) 
by (simp add: finite_Domain finite_Range) moreover obtain source where "W\<inter>sources G={source}" 
using assms(3) by (meson nn56) ultimately moreover obtain M where 
"{n. ?f n\<noteq>[]}={0..M} & last(?f M)\<in>sinks G & size ?l=M+1 & M=?M & (?f M)!0=source"
using assms(1) lm21 by metis ultimately moreover have "isWalk G ?l & set ?l\<subseteq>W\<inter>Field G" 
using assms(1) lm17f by metis ultimately moreover have "distinct ?l" using lm12 by fast
ultimately moreover have "W\<inter>Field G \<subseteq> set ?l" using assms(1,2,4,5) unfolding conditionThree_def using lm26 
by metis ultimately moreover have "set ?l=W\<inter>Field G" by fast moreover have "?l=?L" unfolding 
convertToPath_def by simp ultimately show ?thesis using hd_conv_nth list.size(3) 
not_one_le_zero order_refl trans_le_add2 by (metis)
qed

lemma lm38: assumes "finite G" "card (sources G)=1" "irrefl (trancl G)" 
"conditionOne (set2pred G) (unset W)" "conditionThree (set2pred G) (unset W)"  
"conditionTwo (set2pred G) (unset W)" shows 
"distinct (convertToPath G W) & isWalk G (convertToPath G W) & last(convertToPath G W)\<in>sinks G & 
{hd(convertToPath G W)}=W\<inter>sources G & set (convertToPath G W)=W\<inter>Field G"
proof -
have "sources G=W\<inter>sources G" using assms(4) unfolding conditionOne_def roots_def by blast then
have "card(W\<inter>sources G)=1" using assms by presburger then show ?thesis using assms lm36 by blast
qed
abbreviation "nodes==Field"
theorem assumes "finite G" "card (sources G)=1" "irrefl (trancl G)" 
"conditionOne (set2pred G) (unset W)" "conditionTwo (set2pred G) (unset W)"  
"conditionThree (set2pred G) (unset W)" shows
"\<exists> w. distinct w & isWalk G w & last w\<in>sinks G & {hd w}=W\<inter>sources G & set w=W\<inter>nodes G" 
using assms lm38 by fast

































lemma lm27: assumes "isWalk' G w" "i+2<size w" "w\<noteq>[]" "distinct w" shows "w!(i+2)\<notin>next1' G {w!i}"
using assms(1,2,3,4) les.lm11 add_diff_cancel_left' add_lessD1 atLeastLessThan_iff diff_Suc_1 le_add1 
distinct_conv_nth n_not_Suc_n one_add_one Collect_cong add.commute add.left_commute add_2_eq_Suc'
by (smt)

lemma lm28a: assumes "isWalk' G w" "P=(%n. m+(Suc n)<size w \<longrightarrow> (w!m, w!(m+Suc n))\<in>G ^^ (n+1))" 
shows "\<forall>n. P n"
proof -
let ?P="%n. m+(Suc n)<size w \<longrightarrow> (w!m, w!(m+Suc n))\<in>G ^^ (n+1)" 
{
  fix n assume "?P n" moreover assume "m+(Suc (Suc n))<size w" ultimately moreover have 
  "(w!m, w!(m+Suc n))\<in>G ^^ (n+1)" by force ultimately moreover have "(w!(m+Suc n),w!(m+Suc(Suc n)))\<in>G"
  using assms by force ultimately have "(w!m, w!(m+Suc(Suc n)))\<in> G ^^ (Suc(n+1))" by auto
}
then have "\<forall>n. P n \<longrightarrow> P (Suc n)" unfolding assms by simp moreover have "P 0" using assms not_less0 
add.left_neutral add_diff_cancel_right' atLeastLessThan_iff list.size(3) not_add_less2 One_nat_def
not_le relpow_1 by (metis (no_types, lifting)) ultimately show ?thesis using nat_induct by metis
qed

lemma lm28b: assumes "isWalk' G w" "m+(Suc n)<size w" shows "(w!m, w!(m+Suc n))\<in>G ^^ (n+1)" using 
assms lm28a by fast

corollary lm28c: assumes "isWalk' G w" "m+(Suc n)<size w" shows "(w!m, w!(m+Suc n))\<in>trancl G"
using assms lm28b unfolding trancl_power by fastforce

(*
lemma assumes "isWalk' G w" "i+2<size w" "w\<noteq>[]" "distinct w" shows "w!(i+2)\<in>trancl G``{w!i}"
using assms(1,2,3,4) Image_singleton_iff One_nat_def add.left_commute add.right_neutral add_2_eq_Suc' 
add_Suc_right atLeastLessThan_iff diff_Suc_1 le_add1 less_Suc_eq_le less_trans one_add_one trancl.simps
by (smt)
*)

lemma lm29: assumes "(x,y)\<in>G" "x\<noteq>y" "(y,z)\<in>G" "y\<noteq>z" shows "z\<notin>next1' G {x}" using assms by blast

lemma lm30: assumes "isWalk G w" "distinct w" "i+2+j<size w" shows "w!(i+2+j)\<notin>next1' (trancl G) {w!i}"
proof -
let ?R="trancl G"
have "(w!i, w!(i+1))\<in>?R" using assms lm28c unfolding isWalk_def by force
moreover have "(w!(i+1),w!(i+2+j)) \<in> ?R" using assms lm28c unfolding isWalk_def
by (metis (no_types, lifting) Suc_1 add.assoc add_2_eq_Suc add_Suc nat.inject)
moreover have "w!i \<noteq> w!(i+1)" using assms 
by (metis One_nat_def Suc_1 add.right_neutral add_Suc_right add_lessD1 lessI less_trans lm10d)
moreover have "w!(i+1)\<noteq>w!(i+2+j)" using assms 
by (metis Suc_1 add.assoc add_Suc_right add_lessD1 lessI ll24 not_add_less1 one_add_one)
ultimately show ?thesis using lm29 by auto
qed

lemma lm25a: assumes "irrefl G" "n \<in> Field G" shows "n\<in>sinks G=isSink' (set2pred G) n"
using assms(1,2) unfolding irrefl_def Field_def leaves_def by blast

lemma lm31: assumes "irrefl (trancl P)" shows "irrefl P" using
assms irrefl_def trancl.r_into_trancl by (metis)

lemma lm35: assumes "isWalk' G w" "irrefl(trancl G)" shows "distinct w" 
unfolding lm10c using assms lm28c unfolding irrefl_def by metis

lemma lm32: assumes "isWalk' G w" "hd w\<in>Field G" shows "set w \<subseteq> Field G" 
proof -
{
  fix x assume "x\<in>set w" then obtain i where "i<size w & x=w!i" by (meson lm84e) then moreover
  have "i>0 \<longrightarrow> (w!(i-1),w!i)\<in>G" using assms by fastforce ultimately moreover have "i>0 \<longrightarrow> x\<in>Field G"
  unfolding Field_def by fast ultimately moreover have "w!0\<in>Field G" using assms hd_conv_nth 
  less_nat_zero_code list.size(3) by (metis) ultimately have "x\<in>Field G" by force
}
then show ?thesis by blast
qed

lemma lm34: assumes "isWalk' G w" "last w \<in> sinks G" "transred (trancl G)\<supseteq>G" 
"set w \<subseteq> Field G" "distinct w" "irrefl (trancl G)" shows 
"conditionTwo (set2pred G) (unset (set w))"
proof - let ?W="set w" let ?P="trancl G" have
0: "antisym ?P" using assms(6) lm11b by fast have
4: "isWalk G w" using assms(1) unfolding isWalk_def by blast have 
7: "irrefl G" using assms(6) lm31 by fast
{
  fix p assume "p \<in> Field G \<inter> ?W - sinks G" then obtain j where 
  2: "j<size w-1 & p=w!j" using assms(2) DiffD1 DiffD2 IntD2 One_nat_def Suc_pred last_conv_nth 
  length_greater_0_conv length_pos_if_in_set less_Suc_eq lm84e by (metis(no_types))
  then have 
  3: "(p,w!(j+1))\<in>G" using assms(1) by force
  {
    fix i assume "i<j" 
    then moreover have "i+Suc(j-i-1)<size w" using 2 by fastforce
    then moreover have "(w!i,w!(i+Suc(j-i-1)))\<in>?P" using assms(1) lm28c by blast 
    ultimately moreover have "(w!i,w!j)\<in>?P" by force 
    ultimately moreover have "w!i \<noteq> w!j" using assms(5) 2 One_nat_def Suc_pred add_lessD1 
    less_SucI ll24 nat_neq_iff not_less0 by (metis)
    ultimately have "(p,w!i)\<notin>?P" using 0 2 unfolding antisym_def by meson
  }
  then have "\<forall>i<j. (p, w!i)\<notin>?P" using 2 by blast moreover have "(p,p)\<notin>G" using 7 
  unfolding irrefl_def by fast ultimately have 
  10: "\<forall>i\<le>j. (p,w!i)\<notin>G" using 2 le_neq_trans by blast
  {
    fix k assume "k>j+1" moreover assume "k<size w" ultimately moreover have 
    6:  "k=j+2+(k-j-2)" by simp ultimately have 
    5: "j+2+(k-j-2)<size w" by presburger have "(w!(j+2+(k-j-2))) \<notin> next1' ?P {w!j}" 
    using 4 assms(5) 5 by (rule lm30)
    moreover have "w!j\<in>Domain ?P" using "2" "3" by blast 
    ultimately moreover have "next1 ?P {w!j} = order2strictCover' ?P``{w!j}" using Conflicts.lm30 by blast
    ultimately have "w!k\<notin>order2strictCover' ?P``{w!j}" using 6 unfolding next1_def by metis
    moreover have "order2strictCover' ?P``{w!j}\<supseteq>G``{w!j}" using assms(3) unfolding transred_def by fast
    ultimately have "(w!j,w!k)\<notin>G" by blast then have "(p,w!k)\<notin>G" using 2 by blast
  }
  then have "\<forall>k>j+1. k<size w \<longrightarrow> (p,w!k)\<notin>G" by blast then moreover have "\<exists> x\<in>set w. (p,x)\<in>G" 
  using 2 3 by auto ultimately have "\<exists>! x\<in>set w. (p,x)\<in>G" using lm84e 10 unfolding One_nat_def 
  add_Suc_right using nat_neq_iff less_trans not_le add.right_neutral less_Suc_eq by smt
}
then have 
11: "\<forall>p\<in>?W-sinks G. (\<exists>! x\<in>set w. (p,x)\<in>G)" using assms(4) by blast 
{
  fix p assume "p\<in>?W" moreover assume "\<not> isSink' (set2pred G) p" ultimately have "p\<in>?W-sinks G" 
  using assms(4) 7 lm25a by fast then have "(\<exists>! x\<in>set w. (p,x)\<in>G)" using 11 by force
}
thus ?thesis unfolding conditionTwo_def by blast
qed

lemma lm33: assumes "isWalk' G w" "w\<noteq>[]" "hd w\<in>sources G" "set w\<subseteq>Field G" "irrefl G" shows 
"conditionThree (set2pred G) (unset(set w))"
proof - let ?W="set w"
{
  fix c assume "c\<in>Field G-sources G" moreover assume "c\<in>set w" then obtain j where
  "j\<ge>1 & j<size w & w!j=c" using assms(2,3) DiffD2 calculation hd_conv_nth less_one lm84e not_le
  by (metis ) then moreover have "(w!(j-1),w!j)\<in>G" using assms(1) by force
  ultimately have "\<exists>p\<in>?W. (p,c)\<in>G" by auto
}
then have "\<forall>c\<in>Field G-sources G. c\<in>?W \<longrightarrow>(\<exists>p\<in>?W. (p,c)\<in>G)" by blast thus ?thesis 
using assms(4,5) lm25 DiffI curryD curryI subsetCE unfolding conditionThree_def by (metis)
qed

lemma assumes "isWalk' G w" "last w \<in> sinks G" "transred (trancl G)\<supseteq>G" "irrefl (trancl G)"
"w\<noteq>[]" "hd w\<in>sources G" shows 
"conditionTwo (set2pred G) (unset(set w)) & conditionThree (set2pred G) (unset(set w))"
proof - have 
10: "set w\<subseteq>Field G" using lm32 assms(1,5,6) unfolding Field_def roots_def by blast have
11: "distinct w" using assms(1,4) lm35 by blast have
12: "irrefl G" using assms(4) Sefm.lm31 by blast
have "conditionTwo (set2pred G) (unset(set w))" using assms(1,2,3,4) 10 11 lm34 by blast
moreover have "conditionThree (set2pred G) (unset(set w))" using assms(1,5,6) 10 12 lm33 by blast
ultimately show ?thesis by blast
qed

lemma assumes "Field G=UNIV" shows "isSource' (set2pred G) n = isSource2' (set2pred G) n" 
using assms(1) unfolding Field_def by blast

lemma assumes "Field G=UNIV" shows "isSink' (set2pred G) n = isSink2' (set2pred G) n" 
using assms(1) unfolding Field_def by blast

lemma assumes "irrefl G" shows "n\<in>sources G=isSource2' (set2pred G) n"   
using assms unfolding irrefl_def roots_def by blast

lemma lm39a: assumes "wf (strict (P^-1))" "z\<in>Field P - C - (Cf``C)" "isTrace2' P Cf C"
"trans P" "antisym P" "reflex P" "sym Cf" "irrefl Cf" shows 
"(next1 (P^-1) {z}) - C \<noteq> {}"
proof- have 
1: "isDownwardClosed' P C" using assms(3) isDownwardClosed_def by metis let ?D="C\<union>{z}" have 
11: "trans (P^-1)" using assms(4) by simp have 12: "antisym (P^-1)" using assms(5) by simp have 
13: "reflex (P^-1)" using assms(6) unfolding reflex_def by simp have "isConflictFree2' Cf C" 
using assms(3) unfolding isConflictFree2_def by fast moreover have "sym Cf & irrefl Cf" 
using assms(7,8) by simp ultimately have "isConflictFree2 Cf ?D" using assms(2) 
unfolding irrefl_def sym_def isConflictFree2_def by blast then have 
"\<not> (isDownwardClosed' P ?D)" using assms(2,3) unfolding isDownwardClosed_def by blast then obtain e e' where 
0: "e\<in>?D & e'\<in>P^-1``{e} & e' \<notin> ?D" using assms(2,3) DiffD1 Image_singleton_iff Un_insert_right 
converse.intros insert_subset isDownwardClosed_def sup_bot.right_neutral by (metis)
have "e\<notin>C" using 1 0 by blast then have "e=z" using 0 by blast then obtain x where 
2: "x\<in>P^-1``{z}-?D" using 0 by blast then have 22: "(z,x)\<in>P^-1 & x\<noteq>z" by fast
{ assume 3: "\<forall>y\<in>next1 (P^-1) {z}. y\<in>C" then have 4: "isDownwardClosed' P (C \<union> (next1 (P^-1){z}))" 
  using 1 UnE UnI1 contra_subsetD subsetI by blast obtain y where "(y,x)\<in>P^-1 & y\<in> next1 (P^-1) {z} "
  using 13 11 12 assms(1) 22 mm05ll by metis then have "x\<in>C\<union>(next1 (P^-1) {z})" using 4 by blast 
  then have False using 2 3 by (simp add: 3)}
then show ?thesis by blast
qed

lemma lm39b: assumes "\<forall>z \<in> Field P - C - Cf``C. (next1 (P^-1) {z})-C \<noteq> {}" "isConfiguration2' P Cf C" 
shows "isTrace2' P Cf C" 
proof -
{
  assume "\<not> ?thesis" then obtain z where 0: "z\<in>Field P - C & isConfiguration2' P Cf (C \<union> {z})"
  using assms(1,2) by fast then moreover have "z \<notin> Cf``C" unfolding isConflictFree2_def 
  sym_def irrefl_def by blast ultimately have "(next1 (P^-1) {z})-C\<noteq>{}" using assms(1) 
  by simp then obtain y where "y\<in>next1' (P^-1) {z} - C" unfolding next1_def by blast then have
  "y\<in>P^-1``{z}-C-{z}" by fast then have "\<not> (isDownwardClosed P (C\<union>{z}))" 
  unfolding isDownwardClosed_def by blast then have False using 0 by blast
}
then show ?thesis by fast
qed

lemma lm39c: assumes "wf (strict (P^-1))" "trans P" "antisym P" "reflex P" "sym Cf" "irrefl Cf" 
"isConfiguration2' P Cf C" shows 
"(isTrace2' P Cf C) = (\<forall>z \<in> Field P - C - Cf``C. (next1 (P^-1) {z})-C \<noteq> {})" (is "?l=?r")
proof -
have "?l \<longrightarrow> ?r" using assms(1,2,3,4,5,6) lm39a by fastforce
moreover have "?r \<longrightarrow> ?l" using assms(7) lm39b by auto ultimately show ?thesis by meson
qed

abbreviation "immediatePredecessors' P X == next1 (P^-1) X"

lemma lm39d: assumes "wf (strict (P^-1))" "trans P" "antisym P" "reflex P" "sym Cf" "irrefl Cf" 
"isConfiguration2' P Cf C" shows 
"(isTrace2 P Cf C) = (\<forall>z \<in> events P - C. z\<in> Cf``C \<or> (next1 (P^-1) {z})-C \<noteq> {})"
unfolding isTrace2_def using assms lm39c by blast

lemma lm08d: assumes "wf (strict P)" shows "isTrace P Cf X =(isTrace2 P Cf X)"
unfolding isTrace_def isTrace2_def using assms lm08a lm08b by metis

lemma lm39e: assumes "wf (strict P)" "wf (strict (P^-1))" "trans P" "antisym P" "reflex P" "sym Cf" "irrefl Cf" 
"isConfiguration2' P Cf C" shows 
"(isTrace P Cf C) = (\<forall>z \<in> events P - C. z \<in> Cf``C \<or> (immediatePredecessors' P {z})-C \<noteq> {})"
using assms lm08d lm39d by blast

abbreviation "isMaximalConfSmt' Ca Cf C == 
(\<forall>z \<in> events Ca - C. z \<in> Cf``C \<or> (immediatePredecessors' Ca {z})-C \<noteq> {})"

abbreviation "IsMaximalConfSmt' immediateca cf c == 
(\<forall>z. (\<not> c z \<longrightarrow> (\<exists> y. ((c y & cf y z )\<or>(immediateca y z & \<not> c y)))))"

(*
lemma assumes "IsMaximalConfSmt' ca cf c" shows False 
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms) sorry
*)

theorem correctness: assumes "finite Ca" "isLes Ca Cf" "isConfiguration2' Ca Cf C" shows 
"(isTrace Ca Cf C) = isMaximalConfSmt' Ca Cf C"
proof-
have "finite (strict Ca)" using assms unfolding strict_def by fast
moreover have "finite (strict (Ca^-1))" using assms unfolding strict_def by fast
ultimately show ?thesis using assms lm39e mm05k by (simp add: lm39e mm05k mm13)
qed



























section extended

(*
abbreviation "isMonotonicIsabelle' P s ==
(\<forall> e. ((s e)::int) \<ge> 1) &
(\<forall> e2. s e2 > 1 \<longrightarrow> (\<exists> e0 e1. s e1=s e2 - 1 & P e0 e1 & P e0 e2 & s e0=1)) &
(\<forall> e1 e2. P e1 e2 & e1 \<noteq> e2 \<longrightarrow> s e2 > s e1) &
(\<forall> e1 e2. (e1 \<noteq> e2 & (\<exists> e0 . (P e0 e1 & P e0 e2)))  \<longrightarrow> s e1 \<noteq> s e2)"
*)

(*27/3/19: *)
abbreviation "isMonotonicIsabelle' P s ==
(\<forall> e1 e2. P e1 e2 & e1 \<noteq> e2 \<longrightarrow> s e2 > s e1) &
(\<forall> e1 e2. (e1 \<noteq> e2 & (\<exists> e0 . (P e0 e1 & P e0 e2)))  \<longrightarrow> s e1 \<noteq> s e2) &
(\<forall> e. ((s e)::int) \<ge> 1) &
(\<forall> e2. s e2 > 1 \<longrightarrow> (\<exists> e0 e1. s e1=s e2 - 1 & P e0 e1 & P e0 e2 & s e0=1))"

(*
lemma assumes "isMonotonicIsabelle' (set2pred P) s" "s e0 < s e1" 
"(e0, e1)\<in>P \<or> (e1,e0)\<in>P" shows "(e0, e1)\<in>P & e0 \<noteq> e1" using assms by fastforce
(* by (metis DiffE DiffI Sefm.lm16 insertCI less_irrefl less_trans mm05ll) *)
lemma assumes "isMonotonicIsabelle' p s" shows False
sledgehammer[provers=z3, minimize=false, timeout=1, overlord=true] (assms) sorry
*)

abbreviation "setMaximals' lst == 
[i. i<-[0..<size lst], {} \<notin> set (map (\<lambda> f. (lst!i) - f) (removeAll (lst!i) lst))]"
(*Finds the maximal entries of a list wrt the partial order \<subseteq>*)
definition "setMaximals = setMaximals'"

abbreviation "isMaximal' XX X == ({} \<notin> (\<lambda> Y. X-Y)`(XX-{X}))" definition "isMaximal=isMaximal'"
abbreviation "maximals' XX== {X\<in>XX. isMaximal XX X}" definition "maximals=maximals'"
lemma lm44a: assumes "X\<in>maximals XX" "Y\<in>XX" "Y\<noteq>X" shows "X\<in>XX & X - Y \<noteq> {}" using assms(1,2,3) isMaximal_def 
image_eqI insertE insert_Diff mem_Collect_eq unfolding maximals_def by (metis(no_types,lifting))
lemma lm44b: assumes "X\<in>XX" "\<forall> Y\<in>XX. Y\<noteq>X \<longrightarrow> X-Y \<noteq>{}" shows "X\<in>maximals XX"
unfolding isMaximal_def maximals_def using assms(1,2) by blast
theorem lm44c: "X\<in>maximals XX \<longleftrightarrow> (X\<in>XX & (\<forall> Y\<in>XX. Y\<noteq>X \<longrightarrow> \<not> X \<subseteq> Y))" using lm44a lm44b maximals_def 
mem_Collect_eq Diff_eq_empty_iff by (metis (no_types, lifting))
abbreviation "traces' cau cfl == maximals (configurations cau cfl)" 
definition "traces = traces'"

abbreviation "findFirstIndex' P l == optApp fst (List.find (% (x,y). P y) 
(map (\<lambda> i. (i,l!i)) (upt 0 (size l))))"
(* Correct by find_dropWhile find_Some_iff *)
definition "findFirstIndex = findFirstIndex'"
lemma lm40a: "(optApp f x=None) = (x=None)" unfolding optApp_def by simp
lemma lm40b: "(optApp f x=Some y)=(x\<noteq>None & f (the x)=y)" unfolding optApp_def by simp
lemma lm41a: assumes "i<length xs" "P (xs!i)" "\<forall>j<i. \<not> P (xs!j)" shows "findFirstIndex P xs = Some i"
proof -
let ?P="\<lambda> (x,y). P y" let ?xs="map (\<lambda> j. (j,xs!j)) (upt 0 (size xs))" let ?x="?xs!i" have 
"i<length ?xs" using assms by simp moreover have "?P (?xs!i)" using assms by auto moreover 
have "\<forall>j<i. \<not> ?P (?xs!j)" using assms by auto ultimately have "Some (i, xs!i)=List.find ?P ?xs" 
using assms(1,2) find_Some_iff add.left_neutral length_map nth_map nth_upt by smt then show ?thesis 
using lm40a lm40b fst_conv option.sel unfolding findFirstIndex_def by (metis)
qed
lemma lm41b: assumes "(findFirstIndex P xs = Some i)" shows 
"i<length xs & P (xs!i) & (\<forall>j<i. \<not> P (xs!j))" using find_Some_iff lm40 
proof -
let ?P="\<lambda> (x,y). P y" let ?xs="map (\<lambda> j. (j,xs!j)) (upt 0 (size xs))" let ?x="(i,xs!i)"
have "List.find ?P ?xs=Some ?x" using assms(1) lm40b find_Some_iff fst_conv 
length_map nth_map option.collapse unfolding findFirstIndex_def by smt then have 
"\<exists>ii<length ?xs. ?P (?xs!ii) \<and> ?x = ?xs!ii \<and> (\<forall>j<ii. \<not> ?P (?xs!j))"
using find_Some_iff by smt then obtain ii where
"ii<length ?xs & ?P (?xs!ii) \<and> ?x = ?xs!ii \<and> (\<forall>j<ii. \<not> ?P (?xs!j))" by presburger
thus ?thesis by force
qed

lemma lm41c: "(findFirstIndex P xs = Some i) = (i<length xs & P (xs!i) & (\<forall>j<i. \<not> P (xs!j)))" 
using lm41a lm41b by metis

lemma lm41d: "(findFirstIndex P xs = None) = (\<not> (\<exists>i<size xs. P (xs!i)))"
proof -
let ?P="\<lambda> (x,y). P y" let ?xs="map (\<lambda> j. (j,xs!j)) (upt 0 (size xs))" have 
"List.find ?P ?xs = None \<longleftrightarrow> \<not> (\<exists>x. x \<in> set ?xs \<and> ?P x)" using find_None_iff by smt moreover have 
"(\<exists>x. x \<in> set ?xs \<and> ?P x)=(\<exists>i<size xs. P (xs!i))" by force moreover have 
"(List.find ?P ?xs = None)=(findFirstIndex P xs = None)" using lm40a unfolding findFirstIndex_def 
by (metis (no_types, lifting)) ultimately show ?thesis by satx
qed

lemma lm41e: assumes "findFirstIndex (\<lambda>x. x=m) l\<noteq>None" "findFirstIndex (\<lambda>x. x=n) l\<noteq>None" shows 
"(m\<noteq>n)=(the (findFirstIndex (\<lambda>x. x=m) l) \<noteq> the (findFirstIndex (\<lambda>x. x=n) l))" 
proof -
let ?f=findFirstIndex let ?i="the (?f (\<lambda>x. x=m) l)" let ?j="the (?f (\<lambda>x. x=n) l)"
have "l!(?i)=m" using assms(1) lm41c  by fastforce moreover have 
"l!(?j)=n" using assms(2) lm41c by fastforce
ultimately show ?thesis using assms by fastforce
qed
lemma lm41f: assumes "findFirstIndex (\<lambda>x. x=m) l\<noteq>None" shows 
"(m\<noteq>n)=((findFirstIndex (\<lambda>x. x=m) l)\<noteq> (findFirstIndex (\<lambda>x. x=n) l))" 
using assms lm41e by metis

abbreviation "isOrderPreserving' G l == (None = (List.find (\<lambda>x. x=True)
[let m=findFirstIndex (\<lambda>x. x=f) l in let n=findFirstIndex (\<lambda>x. x=s) l in 
(m\<noteq>None & n\<noteq>None & the m > the n ). (f, s) <- G]))"
(* Checks whether a list of nodes preserves the order represented by the adjacency graph G *)
definition "isOrderPreserving = isOrderPreserving'"
abbreviation "isOrderPreserving2' G l == ((List.find (\<lambda>x. x=True) (let g=findFirstIndex in map 
(\<lambda> (f,s).(g (\<lambda>x. x=f) l)\<noteq>None & (g (\<lambda>x. x=s) l)\<noteq>None & the (g (\<lambda>x. x=f) l)>the (g (\<lambda>x. x=s) l)) 
    G))) = None"
definition "isOrderPreserving2=isOrderPreserving2'"
lemma lm41g: "isOrderPreserving G l = isOrderPreserving2 G l" 
proof -
  have "pointwise op = (map (\<lambda>(a, aa). [findFirstIndex (\<lambda>aa. aa = a) l \<noteq> None \<and> findFirstIndex (\<lambda>a. a = aa) l \<noteq> None \<and> the (findFirstIndex (\<lambda>a. a = aa) l) < the (findFirstIndex (\<lambda>aa. aa = a) l)])) (map (\<lambda>p. [case p of (a, aa) \<Rightarrow> findFirstIndex (\<lambda>aa. aa = a) l \<noteq> None \<and> findFirstIndex (\<lambda>a. a = aa) l \<noteq> None \<and> 
  the (findFirstIndex (\<lambda>a. a = aa) l) < the (findFirstIndex (\<lambda>aa. aa = a) l)])) G" unfolding pointwise_def
    by (simp add: Ball_def_raw)
  thus ?thesis unfolding isOrderPreserving_def isOrderPreserving2_def pointwise_def 
    by (metis (no_types) concat_map_singleton)
qed

lemma lm41h: assumes "isOrderPreserving G l" "(f,s)\<in>set G" "f\<in>set l" "s\<in>set l" 
"f\<noteq>s" shows "the (findFirstIndex (%x. x=f) l) < the (findFirstIndex (%x. x=s) l)" 
proof -
let ?L="[let m=findFirstIndex (\<lambda>x. x=f) l in let n=findFirstIndex (\<lambda>x. x=s) l in 
(m\<noteq>None & n\<noteq>None & the m > the n ). (f, s) <- G]"
let ?m="findFirstIndex (\<lambda>x. x=f) l" let ?n="findFirstIndex (\<lambda>x. x=s) l" have
0: "?m\<noteq>None" using assms(3) by (simp add: in_set_conv_nth lm41d) moreover have 
1: "?n\<noteq>None" using assms(4) by (simp add: in_set_conv_nth lm41d) ultimately have 
"the ?m>the ?n \<longrightarrow> True \<in> set ?L" using assms(2) by force 
moreover have "True \<notin> set ?L" using assms(1) find_None_iff 
unfolding isOrderPreserving_def by (metis (no_types, lifting))
ultimately have "the ?m \<le> the ?n" by linarith 
thus ?thesis using assms(5) lm41e 0 1 nat_less_le by force
qed

lemma lm41i: assumes "\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l & f\<noteq>s) \<longrightarrow> 
the (findFirstIndex (%x. x=f) l) \<le> the (findFirstIndex (%x. x=s) l)"
shows "isOrderPreserving2 G l" 
proof -
let ?f=findFirstIndex let ?LL="map
(\<lambda> (f,s).(?f (\<lambda>x. x=f) l)\<noteq>None & (?f (\<lambda>x. x=s) l)\<noteq>None & the (?f (\<lambda>x. x=f) l) > the (?f (\<lambda>x. x=s) l)) G"
{
  assume "True \<in> set ?LL" then obtain f s where "(?f (\<lambda>x. x=f) l)\<noteq>None & (?f (\<lambda>x. x=s) l)\<noteq>None & 
  the (?f (\<lambda>x. x=f) l)>the (?f (\<lambda>x. x=s) l) & (f,s)\<in>set G" by auto then moreover have 
  "f\<in>set l & s\<in>set l" by (simp add: lm41d in_set_conv_nth) ultimately have False using assms(1) by force 
}
then have "True \<notin> set ?LL" by blast 
then show ?thesis using lm41g find_None_iff unfolding isOrderPreserving2_def by force
qed

theorem lm41j: "(\<forall> f s. ((f,s)\<in>set G & f\<in>set l & s\<in>set l & f\<noteq>s) \<longrightarrow> 
the (findFirstIndex (\<lambda>x. x=f) l) < the (findFirstIndex (\<lambda>x. x=s) l)) \<longleftrightarrow>
(isOrderPreserving G l)" 
unfolding lm41g using lm41h lm41i less_imp_le_nat by (metis lm41g)

abbreviation "isConflictFree' Co X == (\<forall> e e'. e \<in> X & e' \<in> X \<longrightarrow> (e,e') \<notin> Co)" 
definition "isConflictFree = isConflictFree'"
lemma lm42: "isConflictFree2 = isConflictFree" unfolding isConflictFree_def isConflictFree2_def by blast

abbreviation "allPermutations' l == [perm2 l i. i<-[0..<fact (size l)]]" definition "allPermutations=allPermutations'"
lemma lm43: "(conf \<in> configurations' cau con)=(isConflictFree2 con conf & isDownwardClosed' cau conf)"
unfolding isConflictFree2_def by blast

theorem "(conf \<in> configurations cau con) \<longleftrightarrow> (isConflictFree con conf & isDownwardClosed cau conf)"
unfolding lm42 lm43 configurations_def isDownwardClosed_def by blast

abbreviation "clocks' dephasing \<nu>_2 l == [ (let start=listsum (map \<nu>_2 (take i l)) in 
set [dephasing + start..< dephasing + start + (\<nu>_2 (l!i))]). i<-[0..<size l]]" 
definition "clocks = clocks'"

abbreviation "overlappingIndices' clocks1 clocks2 ==
concat [[(i,j). j<-[0..<size clocks2], clocks1!i \<inter> clocks2!j \<noteq> {}]. i<-[0..<size clocks1]]"
definition "overlappingIndices=overlappingIndices'"

abbreviation "areCompatible' \<mu> \<Gamma> arg1 arg2 == 
let traceList1=fst arg1 in let clocks1=snd arg1 in let traceList2=fst arg2 in let clocks2=snd arg2 in
\<Gamma> \<inter> \<Union> set (map (\<lambda> (i,j). \<mu>(traceList1!i) \<times> \<mu>(traceList2!j)) (overlappingIndices clocks1 clocks2)) = {}"
definition "areCompatible=areCompatible'"

fun isListCompatible:: "('a \<Rightarrow> 'b set) \<Rightarrow> ('b \<times> 'b) set => ('a list \<times> 'c set list) list => bool" where 
"isListCompatible mu ga [] = True" |
"isListCompatible mu ga (x#xs) = listerate (op &) ((isListCompatible mu ga xs)#(map (areCompatible mu ga x) xs))"

abbreviation "trancL' lst == concat [List.product [e] (remdups (e#(memo_list_trancl lst e))). e<-
remdups (remdups (map fst lst)@(remdups (map snd lst)))]" 
definition "trancL = trancL'"
(*compare to les3.TrCl*)
abbreviation "tracesList' cau cfl == let cfgs=remdups (configurationsList cau cfl) in
sublist cfgs (set (setMaximals (map set cfgs)))"
definition "tracesList = tracesList'"
abbreviation "sortedTraces' cau cfl == 
concat [[l. l <- allPermutations trace, isOrderPreserving cau l]. trace <- tracesList cau cfl]"
definition "sortedTraces = sortedTraces'"
abbreviation "tracesClocks' cau cfl dephasing \<nu>_2 == map (\<lambda> l. (l, clocks dephasing \<nu>_2 l)) (sortedTraces cau cfl)"
definition "tracesClocks = tracesClocks'"

abbreviation "allCompatibleTraces' \<mu> \<Gamma> caus cfls dephasings \<nu>_2s == map (\<lambda>l. map fst l)
(filter (isListCompatible \<mu> \<Gamma>) (product_lists [tracesClocks (caus!i) (cfls!i) (dephasings!i) (\<nu>_2s!i). i<-[0..<size caus]]))"
definition "allCompatibleTraces=allCompatibleTraces'"
abbreviation "solutions' \<nu>_1s \<mu> \<Gamma> caus cfls dephasings \<nu>_2s == let allTraceTuples =
[(ts, listsum [listsum (map (\<nu>_1s!i) (ts!i)). i<- [0..<size ts]]).ts <- allCompatibleTraces \<mu> \<Gamma> caus cfls dephasings \<nu>_2s]
in map fst (filter (\<lambda> (traceTuple, score). score=(Max o set) (map snd allTraceTuples)) allTraceTuples)"
definition "solutions=solutions'"

abbreviation "mbcGe==[(0::nat ,1::nat),(1 ,2),(1 ,3),(2 ,4)]" abbreviation "mbcPe==trancL mbcGe"
abbreviation "mbcCe==[(2,3)]"
abbreviation "mbcGf == [(10::nat, 11::nat),(11 ,12),(11 ,13)]" abbreviation "mbcPf==TrCl mbcGf"
abbreviation "mbcCf == [(12,13)]"
abbreviation "mbcGg == [(20::nat,21::nat),(21 ,22),(21 ,23),(22 ,24),(23 ,24)]"
abbreviation "mbcPg == trancL mbcGg"
(*abbreviation "mbcCg == []"*)
abbreviation "ImageList' listRel listArgs==map snd (filter (% (x,y). x \<in> set listArgs) listRel)"
(* This generally needs a further remdups unless |listArgs|<2*)
abbreviation "partialPropagationClosure' cau cfl == (remdups o concat) 
[List.product [f] (ImageList' cau (ImageList' cfl [f])) . f<-DomainList cfl]"
definition "partialPropagationClosure=partialPropagationClosure'"
abbreviation "propagationClosure cau cfl == symCl(partialPropagationClosure cau (symCl cfl))"
abbreviation "mbcDe==propagationClosure mbcPe mbcCe"
abbreviation "mbcDf==propagationClosure mbcPf mbcCf"
abbreviation "mbcDg==propagationClosure mbcPg []"
(*value "(id o remdups) (tracesList mbcPe mbcDe)"*)

abbreviation "clocksDelme' dephasing \<nu>_2 l == [ (let start=listsum (map \<nu>_2 (take i l)) in 
set [dephasing + start..< dephasing + start + (\<nu>_2 (l!i))]) . i<-[0..<size l]]" 
definition "clocksDelme = clocksDelme'"
(*pro1=1, pro2=2, ma1=11, ma2=12, ma3=13, mb1=21, mb2=22, mc1=31, mc2=32*)
abbreviation "mbcGamma == (set o symCl) [(11::nat,31::nat),(12,22)]"
abbreviation "mbcMu == eval_rel ({(2::nat,{}),(3,{2, 13::nat}), (4,{12}), (22,{21}), (23,{22}), (12,{31}), (13,{32})} \<union> {0,1,20,21,24,10,11}\<times>{{}})"
abbreviation "mbcNu1 == eval_rel (({0::nat,1,3,10,11,13,20,21,22,23,24}\<times>{1::nat})\<union>{(2,5),(4,5),(12,3)})"
abbreviation "mbcNu2 == eval_rel ({0::nat,1,10,11,20,21,23}\<times>{1::nat}\<union>{(2,3),(3,3),(4,2),(12,3),(13,2),(22,2),(24,4)})"
abbreviation "listPow' n == compow n (\<lambda>x. x@x)"
abbreviation "repeat' n l==concat [l. i<-[0..<n]]"

lemma lm45a: "Domain o set=set o DomainList & Range o set = set o RangeList" 
unfolding DomainList_def RangeList_def by force
lemma lm45b: "Field (set relList)=set (FieldList relList)" unfolding Field_def set_remdups 
FieldList_def using lm45a comp_eq_dest set_append by (metis (no_types))

lemma lm45c: assumes "set C\<in>set`(set (configurationsList cau cfl))" shows 
"set C\<in>configurations (set cau) (set cfl)" 
proof -
have "set C\<in>set `(set( (sublists (FieldList cau))))" using assms unfolding  configurationsList_def
by fastforce then have "set C \<in> Pow (set (FieldList cau))" using assms sublists_powset by metis
moreover have "set (FieldList cau)=events (set cau)" using lm45b by fast moreover have 
"extension (set cau) (set C)\<subseteq> set C" using assms unfolding  configurationsList_def by force 
moreover have "set C \<subseteq> restriction (set cfl) (set C)" using assms unfolding configurationsList_def 
by force ultimately show ?thesis unfolding configurations_def by blast
qed

lemma lm45d: assumes "set C\<in>configurations (set cau) (set cfl)" shows 
"set C\<in>set`(set (configurationsList cau cfl))" 
proof -
let ?Ca="set cau" let ?Co="set cfl" have "set C \<in> Pow (Field ?Ca)" using assms unfolding 
configurations_def by simp then have "set C \<in> (Pow o set o FieldList) cau" using lm45b by fastforce 
then have "set C \<in> set  ` (set (sublists (FieldList cau)))" using sublists_powset by auto
moreover have "extension ?Ca (set C) \<subseteq> set C" using assms unfolding configurations_def by simp
moreover have "set C \<subseteq> restriction ?Co (set C)" using assms unfolding configurations_def by simp
ultimately show ?thesis unfolding configurationsList_def by force
qed

lemma lm45e: "set C\<in>set`(set (configurationsList cau cfl)) = 
  (set C\<in>configurations (set cau) (set cfl))"  using lm45c lm45d by metis

lemma lm45f: assumes "i\<in>set (setMaximals xs)" "j<size xs" "xs!i \<noteq> xs!j" shows "xs!i - (xs!j)\<noteq>{} & i<size xs" 
proof - let ?g="\<lambda> f. (xs!i) - f" have "{}\<notin>set (map ?g (removeAll (xs!i) xs))" 
using assms(1) unfolding setMaximals_def by auto then show ?thesis  using assms(1,2,3) 
unfolding setMaximals_def by auto
qed

lemma lm45g: assumes "i<size xs" "\<forall>j. j<size xs & xs!i \<noteq> xs!j \<longrightarrow> xs!i - xs!j \<noteq>{}" 
shows "i\<in>set (setMaximals xs)"
proof -
let ?g="\<lambda> f. (xs!i) - f" have "\<forall>y\<in>set xs. y \<noteq> xs!i \<longrightarrow> ?g y \<noteq>{}" using assms(2) in_set_conv_nth by (metis)
thus ?thesis unfolding setMaximals_def using assms(1) by auto
qed

lemma lm45h: 
"(i\<in>set (setMaximals xs))=((i<size xs) & (\<forall>j. j<size xs & xs!i \<noteq> xs!j \<longrightarrow> xs!i - xs!j \<noteq>{}))" (is "?l=?r")
proof -
{ 
  fix j assume "?l" 
  (* then moreover have "i<size xs" unfolding setMaximals_def by force *)  
  moreover assume "j<size xs" moreover assume "xs!i \<noteq> xs!j" ultimately have "xs!i - xs!j \<noteq>{}" 
  unfolding setMaximals_def by auto
}
then have "?l \<longrightarrow> ?r" unfolding setMaximals_def by simp moreover
have "?r \<longrightarrow> ?l" using lm45g by (metis(no_types,lifting)) ultimately show ?thesis by meson
qed

(*value "let n=4 in solutions (repeat' (3*n) [mbcNu1]) mbcMu mbcGamma (repeat' n [mbcPe, mbcPf, mbcPg]) 
(repeat' n [mbcDe, mbcDf, mbcDg]) (repeat' n [0,4,1]) (repeat' (3*n) [mbcNu2])"
*)



























section{*Grading*}

find_consts "'a set => 'a list => 'a list"
abbreviation "sourceList' order == filter (%x. x \<notin> set (RangeList order)) (DomainList order)"
definition "sourceList = sourceList'"
value "sourceList' mbcGe"
fun removeLevels where
"removeLevels cover 0 =  cover"  |
"removeLevels cover (Suc n) = filter (\<lambda> (x,y). x \<notin> set  (sourceList (removeLevels cover n))) (removeLevels cover n)"

end
