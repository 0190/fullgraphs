theory myDigraph

imports Main 
"./Vcg/MiscTools"

(*
"~~/src/HOL/Library/Extended"
"~~/src/HOL/Library/Extended_Real"
"~~/src/HOL/Library/Float"
"~~/src/HOL/IMP/Abs_Int2" *)

begin
value "List.zip [1::nat,2,3] [2::nat,3]"
(* Gives all the paths extending list by one, with respect to the graph rel *)
definition "helper rel list == (%x. list@[x])`(rel``{list!((size list) - 1)})"

(* Gives all paths from a node within a graph, having length exactly n *)
fun pathsFromInWithin where
"pathsFromInWithin start grph 0 = {[start]}"|
"pathsFromInWithin start  grph (Suc n)= Union (helper grph ` (pathsFromInWithin start grph n))"

(* Gives all possible paths in a graph starting from a given node *)
definition "allPathsFromWithin start grph ==
Union ((pathsFromInWithin start grph) ` {0..<(card (Field grph) - 1)})"

definition "labelGraph grph labelsList = 
((graph (nth labelsList) {0..<size labelsList} )^-1) O grph O
(graph (nth labelsList)  {0..<size labelsList})"

definition "roots grph == Domain grph - (Range grph)"
definition "leaves grph == Range grph - (Domain grph)"
 
definition "takeUntilInclusive target list = take 
(Max ({0::nat} \<union> (set (map Suc (filterpositions2 (op = target) list))))) list"

definition "pathsOfFromTo grph start end =
takeUntilInclusive end `(allPathsFromWithin start grph) - {[]}"

abbreviation "allLongestPaths grph == 
Union ((pathsOfFromTo grph (the_elem (roots grph))) ` (leaves grph))"

abbreviation "helper1 edge node == ((fst edge, node), (snd edge, node))"
abbreviation "helper2 node edge == ((node, fst edge), (node, snd edge))"
abbreviation "cartProd G H == ((case_prod helper1) ` (G \<times> (Field H))) \<union> ((case_prod helper2) ` ((Field G) \<times> H))"
value "cartProd {(0::nat,1)} {(10::nat,11)}"
value "((0, 10), 1, 10) = ((0,10), (1, 10))"

(* We can associate to some edges a pair (nat,  nat => bool).
The first element is the index, indicating the variable we are predicating about.
We will then combine with an "AND" all the predicates about the same variable, when combining
possible paths
*)

find_consts "('a => 'a => 'a) => ('a list => 'a)"
find_consts "'a list => 'a"
value "listsum [1::nat, 2]"
 (* definition "listAnd = monoid_list.F (op &) False" *)
value "listAnd [True, False]"
definition "pointwise Op f g = (%x. (Op (f x) (g x)))" 
(*definition "pointwise=pointwise'"*)
fun "Operation2Map" where "Operation2Map Op [x,y] = Op x y" | "Operation2Map Op (x#xs) = Op (Operation2Map Op xs) x"
abbreviation "listAnd == Operation2Map (op &)"
value "listAnd [True]"
find_consts "(_ => bool) => (_ => bool) => (_ => bool)"
definition "predAnd = pointwise (op &)"
(* Given a list of list of predicates, we return a list of predicates being the
[ AND 1st preds, AND 2nd preds, ... ] *)
abbreviation "listlist2list listlist == map (Operation2Map predAnd) (transpose listlist)"
(* This is useful because, assuming that each edge of the path has a list of predicates, one for each
numerical variable, we can just obtain a conjunction of predicates for each numerical variable. 
We then do the same for any finite number of paths: the operation doesn't change. *)

find_consts "_ list => _ list => (_ \<times> _) list"

abbreviation "path2edges path == zip path (tl path)" 
(* edgeLabels associates to each edge of a digraph a list of predicates, each in one of the (e.g., numerical)
varialbes. path is just a list of nodes *)
abbreviation "path2listlist edgeLabels path == map edgeLabels (path2edges path)"

(*
abbreviation "inftyInverse (n::nat) == if n=0 then \<infinity> else ereal (1/n)"

abbreviation "Abs (x::('a::{linordered_ring}) extended) == (if (x\<ge>0) then x else (-x))"
(*abbreviation "Abs (x::int extended) == (if (x\<ge>0) then x else (x))"*)
abbreviation "mylog (n::nat) == (if (n > 0) then (Fin (Discrete.log n)) else (- \<infinity>))"
abbreviation "Card X == Abs (mylog (card (Pow X)))"

lemma lm01: assumes "finite X" shows "Card X = Fin (card X)" (is "?L=?R") using assms 
proof -
have "Card X=Fin (Discrete.log (card (Pow X)))" using assms 
less_eq_extended.simps(1) less_nat_zero_code linorder_neqE_nat lm022 of_nat_0_le_iff zero_extended_def
proof -
  have "0 < card (Pow X)"
    by (meson assms less_nat_zero_code linorder_neqE_nat lm022)
  thus ?thesis
    by (simp add: zero_extended_def)
qed
moreover have "... = Fin (Discrete.log (2^card X))" using assms by (metis (poly_guards_query) card_Pow)
ultimately show ?thesis by fastforce
qed

lemma lm02b: assumes "Card X = \<infinity>" shows "\<not> (finite X)" using assms
Pow_not_empty card_gt_0_iff finite_Pow_iff int_less_0_conv
ereal_less(1) ereal_less(6) not_real_of_nat_less_zero
less_eq_extended.simps(5) order_refl
proof -
  { assume "Fin (int (Discrete.log (card (Pow X)))) \<noteq> \<infinity> \<and> - Fin (int (Discrete.log (card (Pow X)))) \<noteq> Fin (int (card X))"
    hence "- Fin (int (Discrete.log (card (Pow X)))) \<noteq> Fin (int (card X)) \<and> \<not> 0 \<le> Fin (int (Discrete.log (card (Pow X)))) \<or> Fin (int (Discrete.log (card (Pow X)))) \<noteq> \<infinity> \<and> 0 \<le> Fin (int (Discrete.log (card (Pow X))))"
      by meson
    hence "finite X \<and> 0 < card (Pow X) \<longrightarrow> Fin (int (Discrete.log (card (Pow X)))) \<noteq> \<infinity> \<and> 0 \<le> Fin (int (Discrete.log (card (Pow X))))"
      using myDigraph.lm01 by fastforce }
  hence "finite X \<and> 0 < card (Pow X) \<longrightarrow> Fin (int (Discrete.log (card (Pow X)))) \<noteq> \<infinity> \<and> 0 \<le> Fin (int (Discrete.log (card (Pow X))))"
    using assms by force
  hence "\<not> 0 < card (Pow X) \<or> \<not> finite X"
    using assms by force
  thus ?thesis
    by (simp add: Pow_not_empty card_gt_0_iff)
qed

lemma lm02a: assumes "\<not> (finite X)" shows "Card X= \<infinity>" using assms 
by (simp add: assms zero_extended_def)

corollary "(Card X = \<infinity>) = (\<not> finite X)" using lm02a lm02b by blast
find_consts "ereal => ereal"
(* Given a list of predicates in distinct variables, we want to compute the feasibility: i.e., how easy 
it is to find a combination of values for the variables that satisfy all the predicates.
Instead of Min, we could also take the product. *)
abbreviation "feasibility predList == (Min o set) [Card (P-`{True}). P <- predList]"
*)





section {* Application of above to clinical stuff *}
definition "pointsOfContention allIncompatibleSets treatmentsPair == 
(Pow ((set (fst treatmentsPair)) \<union> (set (snd treatmentsPair)))) \<inter> allIncompatibleSets"

definition "allCandidateTreatments grph = Union ((pathsOfFromTo grph (the_elem (roots grph))) ` (leaves grph))"

end
