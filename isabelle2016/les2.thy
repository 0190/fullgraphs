(*
Author:
* Marco B. Caminati http://caminati.co.nr
Dually licenced under
* Creative Commons Attribution (CC-BY) 3.0
* ISC License (1-clause BSD License)
See LICENSE file for details
(Rationale for this dual licence: http://arxiv.org/abs/1107.3212)
*)

theory les2

imports les
(* "~/AuctionMbc2015/Maskin2-3/RelationQuotient" *)  
(* "~/AuctionMbc2015/Vcg/Universes"*)
"./Vcg/MiscTools"
begin

lemma lm31: assumes "reflex' P" shows "Domain P = Field P" using assms 
using Field_def les.lm28e by fastforce

lemma lm32a: assumes "EX C. isConfiguration' ca co C & x1 \<in> C & y1 \<in> C" shows "(x1,y1) \<notin> co"
using assms by blast

lemma lm32b: assumes "trans ca" shows "extension ca ((ca^-1)``{x1}) \<subseteq> ((ca^-1)``{x1})"
using assms(1) trans_def by fastforce

lemma lm32c: assumes "isMonotonicOver co ca" "sym co" "irrefl co" shows
"((ca^-1)``{x1}) \<subseteq> restriction co ((ca^-1)``{x1})" 
using assms irrefl_def sym_def subsetI ImageE symE 
(*"EX C. isConfiguration' ca co C & x1 \<in> C & y1 \<in> C"*) 
proof -
  have f1: "(\<forall>a aa. (a, aa) \<notin> ca \<or> co `` {a} \<subseteq> co `` {aa}) \<and> sym co \<and> irrefl co"
    using assms(1,2,3) by presburger
  have f2: "\<forall>a A Aa. (a::'a) \<notin> A \<or> unset Aa a \<or> unset (A - Aa) a"
    by blast
  have f3: "\<forall>r a aa. \<not> sym r \<or> (a::'a, aa) \<notin> r \<or> unset r (aa, a)"
    by (meson symE)
  obtain aa :: "'a set \<Rightarrow> ('a \<times> 'a) set \<Rightarrow> 'a \<Rightarrow> 'a" where
    f4: "\<forall>x0 x1a x2. (\<exists>v3. unset x1a (v3, x2) \<and> unset x0 v3) = (unset x1a (aa x0 x1a x2, x2) \<and> unset x0 (aa x0 x1a x2))"
    by moura
  obtain aaa :: "'a set \<Rightarrow> 'a set \<Rightarrow> 'a" where
    "\<forall>x0 x1a. (\<exists>v2. unset x1a v2 \<and> v2 \<notin> x0) = (unset x1a (aaa x0 x1a) \<and> aaa x0 x1a \<notin> x0)"
    by moura
  hence f5: "\<forall>A Aa. unset A (aaa Aa A) \<and> aaa Aa A \<notin> Aa \<or> A \<subseteq> Aa"
    by (meson subsetI)
  have "\<forall>a. (a, a) \<notin> co"
    using f1 by (metis (no_types) irrefl_def)
  moreover
  { assume "(aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1)), x1) \<notin> co"
    hence "(x1, aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1))) \<notin> co"
      using f1 by (meson sym_def)
    hence "((aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1)), aaa (restriction co (Seg ca x1)) (Seg ca x1)) \<notin> co \<or> aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1)) \<notin> Seg ca x1) \<or> aaa (restriction co (Seg ca x1)) (Seg ca x1) \<notin> Seg ca x1 \<or> unset (restriction co (Seg ca x1)) (aaa (restriction co (Seg ca x1)) (Seg ca x1))"
      using f3 f1 by blast }
  ultimately have "((aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1)), aaa (restriction co (Seg ca x1)) (Seg ca x1)) \<notin> co \<or> aa (Seg ca x1) co (aaa (restriction co (Seg ca x1)) (Seg ca x1)) \<notin> Seg ca x1) \<or> aaa (restriction co (Seg ca x1)) (Seg ca x1) \<notin> Seg ca x1 \<or> unset (restriction co (Seg ca x1)) (aaa (restriction co (Seg ca x1)) (Seg ca x1))"
    using f1 by blast
  thus ?thesis
    using f5 f4 f2 by (meson ImageE)
qed

lemma lm32d: assumes "trans ca" "isMonotonicOver co ca" "sym co" "irrefl co" 
shows "isConfiguration' ca co ((ca^-1)``{x1})" using assms lm32b lm32c by metis

lemma lm32e: assumes "isConfiguration' ca co C" "isConfiguration' ca co D" shows 
"extension ca (C \<union> D) \<subseteq> (C\<union>D)" using assms by blast

lemma lm32f: assumes "sym co" "isConfiguration' ca co C" "isConfiguration' ca co D" 
"\<forall> c d. c\<in>C & d\<in>D \<longrightarrow> (c,d) \<notin> co" shows "(C\<union>D) \<subseteq> restriction co (C \<union> D) " 
using assms DiffI ImageE UnE lm32a subsetI symE by (smt)

lemma lm32g: assumes "sym co" "isConfiguration' ca co C" "isConfiguration' ca co D" 
"\<forall> c d. c\<in>C & d\<in>D \<longrightarrow> (c,d) \<notin> co" shows "isConfiguration' ca co (C \<union> D)" 
using assms lm32e lm32f by metis

lemma lm32h: assumes "isMonotonicOver co ca" "sym co" "(x1,y1) \<notin> co" shows
"\<forall> c d. c\<in>(ca^-1)``{x1} & d\<in>(ca^-1)``{y1} \<longrightarrow> (c,d) \<notin> co"
using assms(1,2,3) Image_singleton_iff contra_subsetD converse_iff symE by (smt)
(*"EX C. isConfiguration' ca co C & x1 \<in> C & y1 \<in> C"*) 

corollary lm32i: assumes "trans ca" "isMonotonicOver co ca" "sym co" "irrefl co" "(x1,y1) \<notin> co" shows 
"isConfiguration' ca co (((ca^-1)``{x1}) \<union> ((ca^-1)``{y1}))" (is "isConfiguration' ca co (?C \<union> ?D)")
using assms lm32h lm32g lm32d 
proof - have 
2: "isConfiguration' ca co ?C" using assms(1,2,3,4) lm32d by metis moreover have 
3: "isConfiguration' ca co ?D" using assms(1,2,3,4) lm32d by metis moreover have 
4: "\<forall> c d. c\<in>?C & d\<in>?D \<longrightarrow> (c,d) \<notin> co" using assms(2,3,5) by (rule lm32h)
show ?thesis using assms(3) 2 3 4 by (rule lm32g)
qed

lemma lm32j: assumes "trans ca" "isMonotonicOver co ca" "sym co" "irrefl co" 
"reflex' ca" "x1\<in>events ca" "y1\<in>events ca" shows 
"(x1,y1) \<notin> co = (EX C. isConfiguration' ca co C & x1 \<in> C & y1 \<in> C)" (is "?L = ?R")
proof -
{
  assume 5: "?L" let ?C="(ca^-1)``{x1} \<union> (ca^-1)``{y1}"
  have "isConfiguration' ca co ?C" using assms(1,2,3,4) 5 by (rule lm32i)
  moreover have "x1 \<in> ?C" using assms using refl_onD by fastforce
  moreover have "y1 \<in> ?C" using assms using refl_onD by fastforce
  ultimately have "EX C. isConfiguration' ca co C & x1 \<in> C & y1 \<in> C" by meson 
}
moreover have "?R \<longrightarrow> ?L" using assms lm32a by blast
ultimately show "?thesis" by blast
qed

abbreviation "Condition1' ca1 ca2 h == (\<forall> y1\<in>Domain h. (ca2^-1)``{h,,y1} \<subseteq> h``((ca1^-1)``{y1}))"
definition "Condition1=Condition1'"
abbreviation "Condition1'' ca1 ca2 h == (\<forall> y1\<in>Domain h. \<forall> x2. 
  (((x2, h,,y1)\<in>ca2) \<longrightarrow> (\<exists> x1\<in> Domain h. (x2=h,,x1 & (x1,y1)\<in>ca1))))"

lemma lm38a: assumes "runiq h" "Condition1' ca1 ca2 h" "y1\<in>Domain h" "(x2, h,,y1)\<in>ca2" 
shows "\<exists> x1\<in> Domain h. (x2=h,,x1 & (x1,y1)\<in>ca1)" 
proof -
have "(h,,y1, x2)\<in>ca2^-1" using assms by blast
then have "x2\<in>ca2^-1``{h,,y1}" by simp then have "x2\<in>h``((ca1^-1)``{y1})" using assms by blast
then obtain x1 where "x1\<in>Domain h & x1\<in>ca1^-1``{y1} & x2=h,,x1" using assms 
by (metis (no_types, lifting) Domain.DomainI ImageE insertI1 l31b rev_ImageI)
thus ?thesis by blast
qed

lemma lm38b: assumes "runiq h" "Condition1'' ca1 ca2 h"
"y1\<in>Domain h" shows "(ca2^-1)``{h,,y1} \<subseteq> h``((ca1^-1)``{y1})"
using assms Domain.DomainI ImageE insertI1 l31b rev_ImageI 
proof - let ?y2="h,,y1"
{
  fix x2 assume 0: "x2\<in>(ca2^-1)``{?y2}" then have "(x2,?y2)\<in>ca2" by fast 
  then obtain x1 where "x1\<in> Domain h & x2=h,,x1 & (x1,y1)\<in>ca1" using assms 0 by meson
  then have "x2\<in>h``((ca1^-1)``{y1})" using assms insertI1  rev_ImageI by (metis (full_types) converse_iff eval_runiq_rel)
}
thus ?thesis by blast
qed

lemma lm38c: assumes "runiq h" shows "Condition1 ca1 ca2 h \<longrightarrow> Condition1'' ca1 ca2 h" 
using assms lm38a unfolding Condition1_def by metis
lemma lm38d: assumes "runiq h" shows "Condition1'' ca1 ca2 h \<longrightarrow> Condition1 ca1 ca2 h" 
using assms lm38b unfolding Condition1_def by metis
lemma lm38: assumes "runiq h" shows "Condition1 ca1 ca2 h = Condition1'' ca1 ca2 h" 
using assms lm38c lm38d by blast

abbreviation "Condition2' co1 co2 h == (\<forall> x1. \<forall>y1. x1 \<in> Domain h & y1\<in>Domain h & (x1, y1) \<notin> co1
\<longrightarrow> (h,,x1, h,,y1) \<notin> co2)"
definition "Condition2=Condition2'"
abbreviation "Condition3' co1 h == 
(\<forall> x1. \<forall> y1. x1 \<in> Domain h & y1 \<in> Domain h & x1 \<noteq> y1 & (x1,y1)\<notin>co1 \<longrightarrow> h,,x1 \<noteq> h,,y1)"
definition "Condition3=Condition3'"

lemma lm34a: assumes (* "Domain h \<subseteq> Domain ca1" "Range h \<subseteq> Domain ca2" "isLes ca1 co1" "isLes ca2 co2" "Condition2' co1 co2 h" *) 
"runiq h" "Condition1' ca1 ca2 h" "isConfiguration' ca1 co1 C1" shows "extension ca2 (h``C1) \<subseteq> (h``C1)" 
using assms Domain.DomainI ImageE ImageI Image_singleton_iff UnCI  contra_subsetD l31 subsetCE
Un_iff subsetI by smt
(*
proof -
let ?C2="h``C1"
{
  fix y2 assume "y2\<in>?C2" then obtain y1 where 
  1: "y2=h,,y1 & y1\<in>Domain h" using assms(1) l31 by fastforce 
  fix x2 assume "x2\<in>ca2^-1``{y2}" then have "x2\<in>ca2^-1``{h,,y1}" using 1 by blast then have
  2: "x2\<in>h``(ca1^-1``{y1})" using assms(2) 1 by blast then obtain x1 where 
  "x2=h,,x1 & x1\<in>ca1^-1``{y1}" using assms(1) by (meson ImageE l31)
  then have "x2\<in>?C2" using assms(1,2,3) 1 Domain.DomainI ImageE ImageI Image_singleton_iff UnCI   
by (smt `unset (Seg ca2 (h ,, y1)) x2` `unset (h \`\` C1) y2` contra_subsetD l31 subsetCE)
  }
thus ?thesis by blast
qed
*)

lemma lm34b: assumes "runiq h" "Condition2' co1 co2 h" "isConfiguration' ca1 co1 C1" shows
"(h``C1) \<subseteq> restriction co2 (h``C1)" using 
assms ImageE l31 Diff_iff Domain.DomainI Image_def mem_Collect_eq subsetCE subsetI by (smt)  

lemma lm34c: assumes "runiq h" "Condition1' ca1 ca2 h" "Condition2' co1 co2 h" shows 
"isConfPreserving' ca1 co1 ca2 co2 h" using assms lm34a lm34b 
proof -
{
  fix C assume "isConfiguration' ca1 co1 C" then have
  "isConfiguration' ca2 co2 (h``C)" using assms lm34a lm34b by (metis(no_types))
}
thus ?thesis by blast
qed

lemma lm34d: assumes "runiq h" "Domain h \<subseteq> Domain ca1" 
(*"Range h \<subseteq> Domain ca2"*) 
"trans ca1" "isMonotonicOver co1 ca1" "sym co1" "irrefl co1" "reflex' ca1"
(* "isLes ca2 co2" *) 
"isConfPreserving' ca1 co1 ca2 co2 h" shows "Condition1' ca1 ca2 h"
proof -
{
  fix y1 let ?y2="h,,y1" assume 
  0: "y1\<in>Domain h" 
  then have "(ca2^-1)``{h,,y1} \<subseteq> h``((ca1^-1)``{y1})" (is "?L \<subseteq> ?R") 
  proof -
  have "isConfiguration' ca1 co1 ((ca1^-1)``{y1})" using assms(3,4,5,6) lm32d by metis
  then have "isConfiguration' ca2 co2 ?R" using assms(8) by presburger
  moreover have "y1 \<in> ca1^-1``{y1}" using assms(2,7) 0 refl_on_def
  Image_singleton_iff les2.lm31 refl_on_converse subsetCE by (metis )
  then moreover have "?y2\<in>?R" using 0 assms(1) by (meson Image_iff eval_runiq_rel)
  ultimately show "ca2^-1``{?y2} \<subseteq> ?R" by fast 
  qed
}
thus ?thesis by simp
qed

lemma lm34e: assumes "runiq h" "Domain h \<subseteq> Domain ca1" 
(* "Range h \<subseteq> Domain ca2" *)
"trans ca1" "isMonotonicOver co1 ca1" "sym co1" "irrefl co1" "reflex' ca1"
(* "isLes ca2 co2" *) 
"isConfPreserving' ca1 co1 ca2 co2 h" shows "Condition2' co1 co2 h"
proof -
{
  fix x1 fix y1 
  let ?C1="((ca1^-1)``{x1}) \<union> ((ca1^-1)``{y1})" let ?C2="h``?C1"
  assume 1: "x1\<in>Domain h" moreover assume 2: "y1\<in> Domain h" moreover assume 3: "(x1,y1) \<notin> co1"
  have "isConfiguration' ca1 co1 ?C1" using assms(3,4,5,6) 3 by (rule lm32i) 
  then have 
  5: "isConfiguration' ca2 co2 ?C2" using assms(8) by presburger
  have "x1 \<in> ?C1" using assms(2,7) 1 
  by (metis Image_singleton_iff UnI1 les2.lm31 refl_onD refl_on_converse subsetCE)
  then have 
  4: "h,,x1 \<in> ?C2" using assms(1) 1 2 3 by (meson ImageI eval_runiq_rel)
  have "y1 \<in> ?C1" using assms(2,7) 2
  Image_singleton_iff UnI1 les2.lm31 refl_onD refl_on_converse subsetCE UnCI by smt
  then have "h,,y1 \<in> ?C2" using assms(1) 1 2 3 by (meson ImageI eval_runiq_rel)
  then have "(h,,x1, h,,y1) \<notin> co2" using 4 5 by fast 
}
thus ?thesis by blast
qed

lemma lm34f: assumes "runiq h" "Domain h \<subseteq> Domain ca1" 
"trans ca1" "isMonotonicOver co1 ca1" "sym co1" "irrefl co1" "reflex' ca1"
shows "((Condition1 ca1 ca2 h) & (Condition2 co1 co2 h)) = 
(isConfPreserving ca1 co1 ca2 co2 h)" (is "(?L1 & ?L2)=?R")  
proof -
let ?LL1="Condition1' ca1 ca2 h" let ?LL2="Condition2' co1 co2 h" let ?RR="isConfPreserving' ca1 co1 ca2 co2 h"
{
  assume 13: "?RR" have "?LL2" using assms(1,2,3,4,5,6,7) 13 by (rule lm34e) 
} then have "?RR \<longrightarrow> ?LL2" by blast moreover have "?RR \<longrightarrow> ?LL1" using assms(1,2,3,4,5,6,7) lm34d 
by metis ultimately have 
0: "?RR \<longrightarrow> (?LL1 & ?LL2)" by fast
{
  assume 2: "?LL1" assume 3: "?LL2" have "?RR" using assms(1) 2 3 by (rule lm34c)
}
then have "(?LL1 & ?LL2) \<longrightarrow> ?RR" by satx
then show ?thesis using 0 unfolding Condition1_def Condition2_def isConfPreserving_def by satx
qed

lemma lm35a: assumes "isEsMorphism ca1 co1 ca2 co2 h" shows "isConfPreserving ca1 co1 ca2 co2 h & 
(\<forall>C. isConfiguration ca1 co1 C \<longrightarrow> ( isInjection (h||C)))"
using assms isConfiguration_def isConfPreserving_def unfolding isEsMorphism_def 
by metis

lemma lm35b: assumes "isConfPreserving ca1 co1 ca2 co2 h" 
"\<forall>C. isConfiguration ca1 co1 C \<longrightarrow> (isInjection (h||C))"
shows "isEsMorphism ca1 co1 ca2 co2 h" using assms unfolding isConfiguration_def isConfPreserving_def
isEsMorphism_def by metis

lemma lm35c: "isEsMorphism ca1 co1 ca2 co2 h = 
(isConfPreserving ca1 co1 ca2 co2 h & (\<forall>C. isConfiguration ca1 co1 C \<longrightarrow>  isInjection (h||C)))" 
using assms lm35a lm35b by metis 

lemma lm36a: assumes "isInjection f" "x\<in>Domain f" "y\<in>Domain f" "x\<noteq>y" shows "f,,x \<noteq> f,,y"
using assms by (metis Image_runiq_eq_eval converse_Image_singleton_Domain singletonD singletonI)

lemma lm36b: assumes "runiq f" "\<forall> x. \<forall> y. x\<in>Domain f & y \<in> Domain f & x\<noteq>y \<longrightarrow> f,,x \<noteq> f,,y"
shows "isInjection f" using assms 
by (metis Range.intros Range_converse converse_iff l31 runiq_basic)

lemma lm37a: assumes "Condition3' co h" "isConfiguration' ca1 co C" "runiq h" 
shows "isInjection (h||C)"
proof - let ?h="h||C" have 
0: "runiq ?h" using assms(3) by (meson restriction_is_subrel subrel_runiq)
{
  fix x1 fix y1 assume "x1 \<in> Domain ?h" moreover assume "y1\<in>Domain ?h" 
  moreover assume "x1 \<noteq> y1" 
  ultimately moreover have "(x1,y1) \<notin> co" using assms(2) by (metis Int_iff ll41 lm32a)
  ultimately have "?h,,x1 \<noteq> ?h,,y1" using assms(1) by (simp add: ll41 lm61)
}
thus ?thesis using 0 lm36b by metis
qed

lemma lm37b: assumes "trans ca" "isMonotonicOver co ca" "sym co" "irrefl co" "reflex' ca"
"Domain h \<subseteq> events ca" "runiq h"
"\<forall> C. isConfiguration' ca co C \<longrightarrow> isInjection (h||C)" 
shows "Condition3' co h" using assms 
proof -
{
  fix x1 fix y1 assume 
  1: "x1\<in>Domain h" moreover assume 
  2: "y1\<in>Domain h" moreover assume 
  4: "x1\<noteq>y1" ultimately have "x1 \<in> events ca & y1 \<in> events ca" using assms by fast
  moreover assume "(x1,y1) \<notin> co" ultimately obtain C where 
  0: "isConfiguration' ca co C & x1\<in>C & y1\<in>C" using lm32j assms by fastforce let ?h="h||C"
  have "isInjection ?h" using 0 assms by presburger moreover have 
  "x1\<in>Domain ?h" using 0 1 assms by (simp add: ll41) moreover have 
  "y1\<in>Domain ?h" using 0 assms by (simp add: ll41 "2")
  ultimately have "?h,,x1 \<noteq> ?h,,y1" using 4 lm36a by metis
  then have "h,,x1 \<noteq> h,,y1" 
by (simp add: `unset (Domain (h || C)) x1` `unset (Domain (h || C)) y1` lm61)
}
thus ?thesis by presburger
qed

lemma lm37c: assumes 
"runiq h"
"Domain h \<subseteq> events ca" 
"trans ca" "isMonotonicOver co ca" "sym co" "irrefl co" "reflex' ca"
shows "Condition3' co h = (\<forall> C. isConfiguration' ca co C \<longrightarrow> isInjection (h||C))" (is "?L=(?R)")
using assms lm37a lm37b 
proof -
have "?L \<longrightarrow> ?R" using lm37a assms by auto moreover have "?R \<longrightarrow> ?L" using assms lm37b by metis 
ultimately show ?thesis by satx
qed

theorem assumes "runiq h" "Domain h \<subseteq> events ca1" 
"trans ca1" "isMonotonicOver co1 ca1" "sym co1" "irrefl co1" "reflex' ca1"
shows "((Condition1 ca1 ca2 h) & (Condition2 co1 co2 h) & (Condition3 co1 h)) =
((isConfPreserving ca1 co1 ca2 co2 h) & (\<forall> C. isConfiguration' ca1 co1 C \<longrightarrow> isInjection (h||C)))"
(is "(?L1 & ?L2 & ?L3) = (?R1 & ?R2)")
proof - let ?LL3="Condition3' co1 h" have 
2: "Domain h \<subseteq> Domain ca1" using assms by (simp add: les2.lm31)
have "(?L1 & ?L2) = ?R1" using assms(1) 2 assms(3,4,5,6,7) by (rule lm34f)
moreover have "?LL3 = ?R2" using assms by (rule lm37c)
ultimately show ?thesis unfolding Condition3_def by satx
qed








end

