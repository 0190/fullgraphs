(*
Author:
* Marco B. Caminati http://caminati.co.nr
Dually licenced under
* Creative Commons Attribution (CC-BY) 3.0
* ISC License (1-clause BSD License)
See LICENSE file for details
(Rationale for this dual licence: http://arxiv.org/abs/1107.3212)
*)


theory ListUtils

imports Main
"~~/src/HOL/Library/Code_Target_Nat" 

begin
(* Takes a list and breaks it into contiguous sublists at the points specified by indexList.
This gives a specific partition of the original list.
It is responsibility of the user to make sure that indexList is a sorted and remdupsed natlist,
having entries lying in the appropriate range.*)
abbreviation "breakListPositions' indexList list == 
(% il l. let IL=(0#il@[size l]) in [sublist l {IL!(n-1)..<IL!n}. n<-[1..<size IL]]) indexList list"
definition "breakListPositions = breakListPositions'"
value "breakListPositions' [3,6,9] [1..<11::nat]"

abbreviation "breakListEvenly' period list == 
breakListPositions' (map (op * period) [1 ..< 1+((size list - 1) div period)]) list"
definition "breakListEvenly = breakListEvenly'"
value "((breakListEvenly' 3 [0..<11]), transpose (breakListEvenly' 3 [0..<11]))"
abbreviation "pairWise' Op l m == [Op (l!i) (m!i). i <- [0..<min (size l) (size m)]]"
definition "pairWise=pairWise'"
lemma lm01: assumes "n\<in>{0..<min (size l) (size m)}" shows "(pairWise' Op l m)!n = Op (l!n) (m!n)"
using assms by auto
definition "droplast list = take (size list - 1) list"
abbreviation "lastElem' list == list!(size list - 1)"

text{* Yields all the possible pairs (x,y) with x, y in a given list, 
but if and only if x strictly precedes y in the list *}
abbreviation "allPairs' list == map (map (nth list)) [[x,y]. [x,y]<-(List.n_lists 2 [0..<size list]), x<y]"
definition "allPairs = allPairs'"
end
