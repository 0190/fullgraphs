(*
Author:
* Marco B. Caminati http://caminati.co.nr
Dually licenced under
* Creative Commons Attribution (CC-BY) 3.0
* ISC License (1-clause BSD License)
See LICENSE file for details
(Rationale for this dual licence: http://arxiv.org/abs/1107.3212)
*)

theory les

imports Main
(* myDigraph *)
(*"~~/src/HOL/Library/Quotient_Product"*)
(* Real*)
(* Transitive_Closure *) 
(* "~/AuctionMbc2015/Maskin2-3/RelationQuotient" *) 
(* "~/AuctionMbc2015/Vcg/Universes" *) 
"./Vcg/RelationOperators" 

begin

abbreviation "events == Field"
abbreviation "DomainList' rel==remdups (map fst rel)" definition "DomainList = DomainList'"
abbreviation "RangeList' rel == remdups (map snd rel)" definition "RangeList = RangeList'"
abbreviation "FieldList' rel == remdups ((DomainList rel) @ (RangeList rel))"
definition "FieldList = FieldList'"

abbreviation "unset A == (% a. a \<in> A)"
term "curry (unset (R :: ('a \<times> 'b) set))"
abbreviation "set2pred R == (curry (\<lambda> a. a \<in> R))" 
abbreviation "pred2set R == Collect (case_prod R)"
lemma lm01: "set2pred o pred2set = id" by fastforce
lemma lm02: "pred2set o set2pred = id" by fastforce
(* lemma "inj_on set2pred UNIV & inj_on pred2set UNIV" using lm01 lm02 
sorry *)
abbreviation "Trans Ca == \<forall> x y z. ((Ca x y & Ca y z) \<longrightarrow> Ca x z)"

abbreviation "Antisym Ca == (\<forall> x y. (Ca x y & Ca y x \<longrightarrow> x = y))"
abbreviation "Propagation Co Ca == (\<forall> x y z. ((Co x y & Ca y z) \<longrightarrow> Co x z))"
abbreviation "Irrefl Co == (\<forall> x. ~ (Co x x))"
abbreviation "Reflex2 Ca == (\<forall> x. Ca x x)"
abbreviation "Reflex Ca == (\<forall> x. ((\<exists> y. (Ca x y \<or> Ca y x)) \<longrightarrow> Ca x x))"
abbreviation "Sym Co == (\<forall> x y. Co x y \<longrightarrow> Co y x)"
(* lemma (*lm03:*) "swap (Set.member) = unset" (* by (rule lm160) *) sorry *)
abbreviation "IsLes Ca Co == 
( Reflex Ca & Antisym Ca & Trans Ca & Irrefl Co & Sym Co & Propagation Co Ca)"
abbreviation "IsDownClosed Ca C == \<forall> e1 e2. (C e2 & Ca e1 e2 \<longrightarrow> C e1)"
abbreviation "IsConflictFree Co C == \<forall> e1 e2. (C e1 & C e2 \<longrightarrow> \<not> (Co e1 e2))"
abbreviation "IsEventSet Ca C == \<forall> e. C e \<longrightarrow> Ca e e"
abbreviation "IsConfig Ca Co C == IsDownClosed Ca C & IsConflictFree Co C & IsEventSet Ca C"
abbreviation "IsConfigPreserving Ca1 Co1 Ca2 Co2 h == 
(\<forall> D. (EX C. IsConfig Ca1 Co1 C & (\<forall> e2::int. Ca2 e2 e2 \<longrightarrow> D e2 = (EX e1::int. e2=h e1 & C e1))) \<longrightarrow> 
IsConfig Ca2 Co2 D)"
abbreviation "IsInjective Ca1 Ca2 h== (\<forall> e1 e2. Ca1 e1 e1 & Ca2 (h e1) (h e1) & h e2 = h e1 \<longrightarrow> e1 = e2)"
term "IsConfigPreserving"

term "IsConfig Ca1 Co1 C & (\<forall> e2::int. Ca2 e2 e2 \<longrightarrow> (D e2 = (EX e1::int. e2 = h e1 & C e1)))"

(* theorem assumes "IsLes Ca1 Co1" and "Ca1 (1::int) (2::int)" and 
"Ca1 2 3" and "Co1 (1::int) (4::int)" 
"IsInjective Ca1 Ca2 h" "IsConfigPreserving Ca1 Co1 Ca2 Co2 h" shows False  
(* sledgehammer run [provers=z3, minimize=false, overlord=true, timeout=1] (assms) *)
sorry
*)
(*
theorem assumes "IsLes Ca Co" and "Ca e1 e2" and 
"Ca e2 e3" and "Co e1 e4" shows False  
sledgehammer run [provers=z3, minimize=false, overlord=true, timeout=1] (assms)
*)


(*
lemma assumes "Reflex Ca" "Antisym Ca" "Trans Ca" "Irrefl Co" "Sym Co" "Propagation Co Ca"
"Ca (1::int) 2" "Ca 2 3" "Co 1 (4::int)" "(Ca 1 4)"  
shows False  
sledgehammer run [provers=z3, minimize=false, overlord=true] (assms) 
*)

(*
lemma "\<not> (Trans Ca & Antisym Ca & Propagation Co Ca & Irrefl Co & Sym Co 
& Ca (1::int) (2::int) & Ca 2 3 & Co 1 (4::int) & Ca 1 4)" 
sledgehammer [provers=z3, overlord=true, max_facts=0] 
*)

abbreviation "isPropagatingOver conflict causality == 
(\<forall>x.  (causality O conflict)``{x} \<subseteq> conflict `` {x})"
abbreviation "isPropagatingOver2 conflict causality == 
(\<forall>x. conflict `` (causality``{x}) \<subseteq> conflict `` {x})" 
(* abbreviation "isPropagatingOver3 conflict causality == compatible conflict causality Id" *)
abbreviation "isPropagatingOver4' conflict causality == (\<forall> x y z. 
(x,z) \<in> causality & (x,y) \<in> conflict \<longrightarrow> (z,y) \<in> conflict)"
definition "isPropagatingOver4 = isPropagatingOver4'"
abbreviation "isPropagatingOver5 conflict causality == 
(\<forall>x. conflict `` (causality``{x}) \<supseteq> conflict `` {x})" 
abbreviation "isMonotonicOver conflict causality == 
\<forall> x y. (x,y) \<in> causality \<longrightarrow> conflict``{x} \<subseteq> conflict``{y}"
abbreviation "propagation == isMonotonicOver"

abbreviation "isPairwiseBoundedWrt X Y == (\<forall> x. (x \<in> (case_prod (op \<union>)) ` (X\<times>X) \<longrightarrow> (\<exists> y \<in> Y. x \<subseteq> y)))"

abbreviation "isConflictFree2' Co X == ((X \<times> X) \<inter> Co = {})"
definition "isConflictFree2 = isConflictFree2'"

abbreviation "isLeftClosed Ca X == ((Ca^-1) `` X \<subseteq> X)"

abbreviation "allLeftClosed Ca == {X. X \<subseteq> Field Ca & isLeftClosed Ca X}"
abbreviation "Seg Ca e == Ca^-1``{e}"

abbreviation "segMapper Ca0 Ca1 x == {e \<in> Field Ca1. Seg Ca1 e \<inter> Field Ca0 \<subseteq> x}"

lemma assumes "x \<subseteq> y" shows "{e \<in> E1. Seg Ca1 e \<inter> E0 \<subseteq> x} \<subseteq>
{e \<in> E1. Seg Ca1 e \<inter> E0 \<subseteq> y}" using assms by fast

lemma assumes "x \<subseteq> y" shows "segMapper Ca0 Ca1 x \<subseteq> segMapper Ca0 Ca1 y"
using assms by blast






lemma lm33a: "isMonotonicOver co ca = isPropagatingOver4' co ca" by blast

(* lemma "isPropagatingOver3 = isPropagatingOver2" by (simp add: compatible_def)*)
lemma lm33b: "isPropagatingOver = isPropagatingOver2" by fast

(*
lemma assumes "isMonotonicOver co ca" shows "isPropagatingOver5 co ca" 
using assms unfolding Image_def sorry
*)
abbreviation "reflex' P== refl_on (Field P) P" definition "reflex=reflex'"
abbreviation "isLes causality conflict == propagation conflict causality & 
sym conflict & irrefl conflict & trans causality & antisym causality & reflex causality"

(*
lemma assumes "isLes Ca Co" "F={C. isLeftClosed Ca C & isConflictFree2 Co C & C\<subseteq> Field Ca & C \<subseteq> Field Co}" 
"isPairwiseBoundedWrt X F" shows
"(\<Union> X) \<in> F" 
using Field_def
using assms sledgehammer[provers=e]
*)

section{* Equivalence proofs *}

lemma lm12: "Sym Co \<longleftrightarrow>  sym (pred2set Co)" by (simp add: sym_def)

lemma lm13: "Trans Co \<longleftrightarrow> trans (pred2set Co)" using case_prodI mem_Collect_eq trans_def 
prod.sel(1) prod.sel(2) by (smt curryD curryI curry_case_prod)

lemma lm14: "Antisym Ca = antisym (pred2set Ca)" using antisym_def by fastforce

lemma lm15: "Irrefl Co = irrefl (pred2set Co)" using irrefl_def by fastforce

lemma lm16: assumes "Reflex Ca" shows "refl_on (Field (pred2set Ca)) (pred2set Ca)" 
using assms unfolding refl_on_def Field_def by blast

lemma lm17: assumes "refl_on (Field (pred2set Ca)) (pred2set Ca)" shows "Reflex Ca" using assms 
FieldI2 mem_Collect_eq refl_onD refl_onD1
by (metis split_conv)

lemma lm18: assumes "Sym Co" "Propagation Co Ca" shows "propagation (pred2set Co) (pred2set Ca)" 
using assms by blast

lemma lm19: assumes "Sym Co" "propagation (pred2set Co) (pred2set Ca)" shows  "Propagation Co Ca" using assms
lm12 by blast

(* lemma assumes "{(1::nat, 2::nat), (2::nat, 4), (3, 4)} \<subseteq> CAU & {(2,3::nat)} \<subseteq> CFL"
shows "isLes CAU CFL" sorry *)

(*
lemma "EX cau cfl. isLes cau cfl" sledgehammer[provers=z3, overlord=true] 
by (metis antisymI bot_least empty_iff irrefl_distinct isLes_def isPropagatingOver_def order_refl subset_antisym sym_def trans_O_subset trans_def)
*)

definition "myUnderdefinedCausality == {(0::nat,1),(10,11)}"
definition "myUnderdefinedConflict  == {(0::nat,10)}"

(*
lemma "EX cau cfl. isLes cau cfl & myUnderdefinedCausality \<subseteq> cau & myUnderdefinedConflict \<subseteq> cfl"
sorry
*)

(*abbreviation "ca22 == {(2,3),(3::nat,2::nat)}"*)

(* lemma "x \<in> A \<Longrightarrow> (THE y. y \<in> A) \<in> A" sorry *)

(* datatype eventType = ea | eb | ec | ed | ee *)
(*
abbreviation "ca3 == {(2::nat, 4::nat), (3,5), (5,6), (6, 4)}"
abbreviation "co3 == {(2::nat,3::nat)}"
*)
(*
abbreviation "ca11 == {(eb,ed), (ec,ee), (ee,ee), (ed, ed)}"
abbreviation "co11 == {(eb,ec)}"
*)
(*
abbreviation "ca4 == {(ea, eb), (ec,ed), (ea,ee), (ec,ee)}"
abbreviation "co4 == {(ea, ec), (eb,ed)}"
*)

abbreviation "Ca4' == [(1::nat,2::nat), (3,4)]"
abbreviation "Co4' == [(1::nat, 3::nat), (2,4)]"
abbreviation "ca4' == {(1::nat,2::nat), (3,4)}"
abbreviation "co4' == {(1::nat, 3::nat), (2,4)}"
definition "ca4=ca4'" definition "co4=co4'" definition "Ca4=Ca4'" definition "Co4=Co4'"
abbreviation "ca5 == {(1::int, 2::int), (3,4), (1,5), (3,5)}"
abbreviation "co5 == {(1::int, 3::int), (2,4)}"
(* lemma assumes "ca4 \<subseteq> CA" "(co4 - {(e1, e2)}) \<subseteq> CO" shows "\<not> (isLes CA CO)" 
nitpick [card eventType=6, max_potential=0]*)

(*
lemma assumes "ca4 \<subseteq> CA" "(co4 - C) \<subseteq> CO" "card C\<le>1" shows "\<not> (isLes CA CO)" 
nitpick [card eventType=6, max_potential=0]
*)
(*
abbreviation "CA == {(ea, ea), (ea, eb), (ea, ee), (eb, eb), (ec, ec), (ec, ed), (ec, ee), (ed, ed), (ee, ee)}"
abbreviation "CO == {(ea, ec), (ea, ed), (eb, ec), (eb, ed), (ec, ea), (ec, eb), (ed, ea), (ed, eb)}"
*)
(*
lemma assumes "myLes caa coo" 
"caa (1::int) (1::int)"
"caa 1 2" "caa 1 5" "caa 2 2" "caa 3 3" "caa 3 4" "caa 3 5" "caa 4 4" "caa 5 5"
"coo 1 3" "coo 1 4" "coo 2 3" "coo 2 4" "coo 3 1" "coo 3 2" "coo 4 1" "coo 4 2"
shows False sledgehammer run [provers=z3, minimize=false, overlord=true] (assms) *) 

(*
lemma "\<not> (EX e1 e2. (e1, e2) \<in> ca2 & (e2, e1) \<in> ca2)" sorry

lemma "\<not> (EX e1 e2. (e1 \<in> {2::nat, 3, 4} & e2 \<in> {2, 3, 4} & (e1, e2) \<in> {(2::nat ,3:: nat), (3,2)} & 
(e2, e1) \<in> {(2,3), (3,2)}))" sorry
*)

abbreviation "extension Ca X == (X \<union> (Ca^-1``X))"
abbreviation "restriction Co X == X - (Co `` X)"
abbreviation "isConfiguration' Ca Co X == extension Ca X \<subseteq> X & X \<subseteq> restriction Co X"
definition "isConfiguration = isConfiguration'"
abbreviation "configurations' Ca Co == 
{X. X \<in> Pow (events Ca) & extension Ca X \<subseteq> X & X \<subseteq> restriction Co X}"
definition "configurations=configurations'"
abbreviation "configurationsList2' Ca Co ==
[X. X<- sublists (sorted_list_of_set (Field Ca)) , extension Ca (set X) \<subseteq> set X & set X \<subseteq> restriction Co (set X)]"
abbreviation "configurationsList' Ca Co == [X. X <- sublists (FieldList Ca), extension (set Ca) (set X) 
\<subseteq> set X & set X \<subseteq> restriction (set Co) (set X)]"
definition "configurationsList Ca Co = configurationsList' Ca Co"
definition "configurationsList2 Ca Co = configurationsList2' Ca Co"

lemma assumes "C \<in> configurations' Ca Co" "(e1, e2) \<in> Co" "e1 \<in> C" shows "e2 \<notin> C"
using assms by blast

(* All the partial functions from xs into ys *)
fun runiqsAlg where
"runiqsAlg [] ys = [[]]"|
"runiqsAlg (x#xs) ys = runiqsAlg xs ys @ [(x,y)#f. f<-runiqsAlg xs ys, y<-ys]"

(*value "configurations ca5 co5"
value "runiqsAlg [1::nat,2] [11,12::nat]"*)
abbreviation "Ca451a' == [(1::nat,2::nat), (2,3),(2,4),(1,3),(1,4)]"
abbreviation "Co451a' == [(3,4)]"
abbreviation "Ca451b' == [(11::nat, 12::nat), (12,13), (11,13)]"
definition "Ca451a=Ca451a'" definition "Co451a=Co451a'" definition "Ca451b=Ca451b'" definition "Co451b=[]"
abbreviation "runiqAlg' == inj_on fst" definition "runiqAlg=runiqAlg'"
abbreviation "injAlgOn' f X == runiqAlg ((f||X)^-1)" definition "injAlgOn = injAlgOn'"
(*value "inj_on fst ({(1::nat,2::nat),(2,4)}||{1})"
value "injAlgOn {(1::nat,2::nat), (0,2)} {0,1}"
value " (map set (configurationsList Ca4 Co4))"
*)

abbreviation "allEsMorphisms' Ca1 Co1 Ca2 Co2 == 
[f. f <- runiqsAlg (FieldList Ca1) (FieldList Ca2), 
set (map (Image (set f)) (map set (configurationsList Ca1 Co1)))
\<subseteq> set (map set (configurationsList Ca2 Co2)), 
set (map (injAlgOn (set f)) (map set (configurationsList Ca1 Co1))) \<subseteq> {True}]"

definition "allEsMorphisms = allEsMorphisms'"

(*
value "allEsMorphisms Ca451a Co451a Ca451b Co451b"
value "[f. f<-runiqsAlg (configurationsList ca1 co1) (configurationsList ca2 co2)
(*(\<forall> l1 \<in> set f. \<forall> l2 \<in> set f. set (map fst l1) \<subset> set (map fst l2) \<longrightarrow> True)*)
]"
*)

fun injectionListsAlg where 
"injectionListsAlg [] (Y::'a list) = [[]]" |
"injectionListsAlg (x#xs) Y = concat [ 
   [(x,y) # f. y \<leftarrow> (filter (%y. y \<notin> (set o (map snd)) f) Y)]
   .f \<leftarrow> injectionListsAlg xs Y ]"

find_consts "'a list \<Rightarrow> 'b list \<Rightarrow> ('a \<times> 'b) set list"

definition "exportMe = configurations"
(*export_code exportMe in Scala module_name Configurations file "/tmp/Configurations.scala"*)

(*notation map (infix "`" 90)*)


text{* sdaf *}
(*
lemma "\<not> (myTrans Ca & myAntiSym Ca & myPropagation Co Ca & myIrrefl Co & mySym Co 
& Ca 1 2 & Co 2 3 & Ca 2 4 & Ca 3 4)" sorry
*)

(* lemma "EX R. (antisym R & {(x, y)} \<subseteq> R)" sledgehammer [provers=z3, spy=true, overlord=true]
[provers=z3, spy=true, overlord=true, debug=true, max_facts=smart, fact_thresholds=1.0 1.0]
by (metis antisymI dual_order.refl old.prod.inject singletonD) *) 

section{* generating ESs *}

(* partialK is a partial order where we conventionally establish 
that x partialK y means x smaller or equal to y in some sense *)

abbreviation "strict' partialK == partialK - {(x,x)| x. x \<in> Domain partialK}"
definition "strict=strict'"

abbreviation "minimals partialK == (Domain partialK) - (Range (partialK - {(x,x)| x. x\<in>Domain partialK}))"

abbreviation "successors partialK X == ((strict' partialK) `` X)"
abbreviation "predecessors == successors o converse"
abbreviation "next1' partialK X == successors partialK X - (successors partialK (successors partialK X))"
definition "next1 = next1'"
abbreviation "next2 partialK X == successors partialK X - (successors (partialK || (successors partialK X)) (successors partialK X))"
abbreviation "next3 partialK X == minimals (partialK || (successors partialK X)) \<inter> (partialK``X)"
abbreviation "order2strictCover' P == Union {{x}\<times>(next1 P {x})|x. x\<in> (Domain P)}"
definition "order2strictCover = order2strictCover'"

abbreviation "testOrd == {(1::nat,2::nat), (1,1), (2,2), (1,3), (2,3), (3, 3)}"

lemma lm10: "next2 partialK X \<subseteq> next1' partialK X" unfolding restrict_def by blast

lemma lm09: "next3 partialK X \<subseteq> next1' partialK X" unfolding restrict_def by blast

(*
lemma lm08: assumes "refl_on (Field partialK) partialK" shows "next1' partialK X \<subseteq> next3 partialK X" 
using assms(1) unfolding restrict_def refl_on_def sorry 

lemma lm04: "next3 partialK X \<subseteq> next2 partialK X" by (smt DiffE DiffI ImageE IntE Range.RangeI ll41 subsetI) *)

lemma lm05: assumes "antisym K" "refl_on (Field K) K" "x \<noteq> y" "(x,y) \<in> K" 
"\<forall> z. (x,z) \<in> K \<longrightarrow> (y,z) \<in> K" shows "y \<in> next3 K {x}"
using refl_on_def antisym_def assms refl_onD1 by (metis(hide_lams))

lemma lm06: assumes "y \<in> next1' K {x}" shows "(x,y) \<in> K" using assms by blast

lemma lm07: assumes "y \<in> next1' K {x}" "z\<noteq>y" "(z,y) \<in> K" shows "z \<notin> next1' K {x}" using assms by blast

lemma lm11: assumes "y \<in> next1' K {x}" "(x,z) \<in> K" "(z,y) \<in> K" "x \<noteq> z" shows "z=y"
using lm07 assms(1,2,3,4) by blast

(* 
  Consider a causality (partial order) \<le>. Let o be a minimal element (o\<le>o & \<nexists> x. x<o).
  Consider \<le>':=\<le>--{o}, which is still a partial order.
  Let # be a conflict wrt \<le>'.
  Set N := \<Inter>_{e \<in> next_\<le> (o)} #``{e}, and return
  [#, # \<union> ({o}\<times>N) \<union> (N \<times> {o})].
  Ranging over \<le> and #, you obtain exactly all prime event structures by recursion.
*)

section{*labels*}

abbreviation "listApp P x == [(P!j) (x!j). j<- [0..<2]]"
abbreviation "formula1 x == (% x. x 1 \<le> x 2)"
(* datatype mbc = a | b | c *)
abbreviation "ander f1 f2 == (\<lambda> i. \<lambda> n. f1 i n & f2 i n)"

(* lemma "[ (EX x. P x) .P <- listlist2list [[%x. x>10],[%x. x<(5::int)]]] = [True]" *)

section{**}

abbreviation "isInjection R == runiq R & (runiq (R^-1))"

abbreviation "isConfPreserving' Ca1 Co1 Ca2 Co2 h == 
(\<forall>C. isConfiguration' Ca1 Co1 C \<longrightarrow> isConfiguration' Ca2 Co2 (h``C))"
definition "isConfPreserving = isConfPreserving'"

abbreviation "isEsMorphism' Ca1 Co1 Ca2 Co2 h == 
(\<forall>C. isConfiguration' Ca1 Co1 C \<longrightarrow> (isConfiguration' Ca2 Co2 (h``C) & isInjection (h||C)))"
definition "isEsMorphism = isEsMorphism'"
abbreviation "isInjectionOn f X == (\<forall> x1 x2. x1\<in>X & x2\<in>X & f x1=f x2 \<longrightarrow> x1=x2)"
abbreviation "isEsMorphism'' Ca1 Co1 Ca2 Co2 h == 
(\<forall>C. isConfiguration' Ca1 Co1 C \<longrightarrow> (isConfiguration' Ca2 Co2 (h`C) & isInjectionOn h C))"

abbreviation "isCommunicationMorphism Ca1 Co1 Ca2 Co2 h ==
(Domain h=events(Ca1) &
(\<forall> e1\<in>events(Ca1). 
  h``((Ca1 - Id) `` {e1}) \<subseteq> (Ca2-Id)``{h,,e1} &
  h``(Co1 `` {e1}) \<subseteq> Co2``{h,,e1}
))"
(*
lemma assumes "h\<noteq>{}" "Ca1 \<noteq> {}" "Ca2 \<noteq> {}" "Field Ca1 \<supseteq> Field Co1" "Field Ca2 \<supseteq> Field Co2" "isLes Ca1 Co1" "isLes Ca2 Co2" 
"isEsMorphism' Ca1 Co1 Ca2 Co2 h" "(a, b) \<in> Co1" shows 
"isConfPreserving' Ca1 Co1 Ca2 Co2 h" using assms sorry
*)



abbreviation "concurrency ca co == (  (events ca) \<times> (events ca)) - ca - (ca^-1) - co"

lemma assumes "trans P" shows "trancl (P \<union> Q) \<supseteq> P" using assms by auto

abbreviation "reflCl P == P \<union> Id_on (Field P)"
abbreviation "reflTranCl1' == trancl o reflCl"
abbreviation "reflTranCl2' == reflCl o trancl"
abbreviation "reflTranCl3' P == reflCl P \<union> trancl P"

lemma lm28: "reflex' (Id_on X)" by (simp add: Field_def Id_onI Id_on_subset_Times refl_on_def)

lemma lm28b: assumes "reflCl P \<subseteq> P" shows "reflex' P" using assms lm28
by (smt Domain.DomainI FieldI2 Field_def le_sup_iff mem_Sigma_iff mono_Field refl_on_Id_on refl_on_def subrelI subsetCE)

lemma lm28c: "reflCl (reflCl P) = reflCl P" 
by (metis Domain_Id_on Field_Un Field_def Range_Id_on sup.idem sup.right_idem)

lemma lm28d: "reflex' (reflCl P)" using eq_iff lm28b lm28c by (metis )

lemma lm29f: assumes "trans Ca" "reflex' Ca" "e\<notin>events Ca" shows 
"trans (Ca \<union> (Ca^-1``{e'} \<times> {e}) \<union> ({e}\<times>{e}))" using assms 
Image_singleton_iff UnCI UnE converse_iff mem_Sigma_iff refl_on_def singletonD subsetCE trans_def
by (smt)

lemma "Field (P\<union>Q) = (Field P) \<union> Field Q" by simp

lemma lm28e: "(reflex' P) = (Id_on (Field P) \<subseteq> P)" by (metis Id_onE lm28d refl_onD subset_eq sup.orderE)

lemma lm29e: assumes "reflex' Ca" shows "reflex' (Ca \<union> ((Ca^-1``{e'}) \<times> {e}) \<union> ({e}\<times>{e}))" (is "reflex' ?Ca") 
proof -
have "Field ?Ca= Field Ca \<union> Ca^-1``{e'} \<union> {e}" using assms unfolding Field_def by auto
moreover have "Id_on ... = Id_on (Field Ca) \<union> Id_on (Ca^-1``{e'}) \<union> Id_on {e}" by auto
ultimately moreover have "... = Id_on (Field Ca) \<union> Id_on {e}" using assms Field_converse 
Id_on_def Image_subset UN_insert Un_insert_right converse_Id_on converse_Un inf_sup_aci(5) 
lm28d lm28e refl_on_def sup.orderE sup.right_idem sup_bot.right_neutral by (smt)
ultimately have "Id_on (Field ?Ca) \<subseteq> ?Ca" using assms Id_onE UnCI UnE lm28e 
mem_Sigma_iff subrelI subsetCE by (smt) then show ?thesis using lm28e by blast 
qed

lemma lm29d: assumes "e' \<in> events Ca" "antisym Ca" "e\<notin>events Ca" shows 
"antisym (Ca \<union> (Ca^-1``{e'} \<times> {e}) \<union> ({e}\<times>{e}))" (is "antisym ?Ca") using assms 
unfolding Field_def antisym_def by blast

lemma lm29c: assumes "irrefl Co" "e\<notin>Field Co" shows 
"irrefl (Co \<union> ((Co``{e'}) \<times> {e}) \<union> ({e} \<times> Co``{e'}))" using assms Field_def 
unfolding irrefl_def by fast

lemma lm29b: assumes "sym Co" shows "sym (Co \<union> ((Co``{e'}) \<times> {e}) \<union> ({e} \<times> Co``{e'}))" 
using assms unfolding sym_def by blast

lemma lm29a: assumes "e' \<in> events Ca" "isLes Ca Co" "e\<notin>events Ca" shows 
"isMonotonicOver (Co \<union> ((Co``{e'}) \<times> {e}) \<union> ({e} \<times> Co``{e'})) (Ca \<union> (Ca^-1``{e'} \<times> {e}) \<union> ({e}\<times>{e}))"
(is "isMonotonicOver ?co ?ca")
proof -
{
  fix x y assume "(x,y)\<in>?ca & y\<noteq>e" then moreover have "(x,y) \<in> Ca" by blast
  then moreover have "Co``{x} \<subseteq> Co``{y}" using assms(2) by simp
  ultimately have "?co``{x} \<subseteq> ?co``{y}" 
  using assms(2,3) ImageI Image_singleton_iff 
  Un_iff insert_iff mem_Sigma_iff refl_on_def subsetCE subsetI sym_def reflex_def by (smt)
}
then have 
0: "\<forall>x y. (x,y)\<in>?ca & y \<noteq> e \<longrightarrow> ?co``{x} \<subseteq> ?co``{y}" by blast
{
  fix x assume "(x,e)\<in>?ca" moreover assume "x\<noteq>e"
  moreover have "reflex' Ca" using assms(2) reflex_def by metis
  ultimately moreover have 
  1: "x\<in>Ca^-1``{e'}" using reflex_def using assms(2,3) refl_onD2  by fastforce
  ultimately have "Co``{x}\<subseteq>Co``{e'}" using assms(2) by auto
  moreover have "Co``{e'} \<subseteq> ?co``{e'}" by fast
  ultimately moreover have "x \<noteq> e" using assms(3) by (metis FieldI2 Field_converse Image_singleton_iff 1)
  ultimately have "?co``{x} \<subseteq> ?co``{e}" using "0" Field_converse Field_def Image_singleton_iff
  Un_iff 1 assms(2,3) converse_iff insert_iff irrefl_def mem_Sigma_iff subsetCE subsetI by (smt sym_def)
}
thus ?thesis using 0 by (metis set_eq_subset)
qed

abbreviation "addEventToCausality Ca e' e == Ca \<union> (Ca^-1``{e'} \<times> {e}) \<union> ({e}\<times>{e})"
abbreviation "addEventToConflict Co e' e == 
(Co \<union> ((Co``{e'}) \<times> {e}) \<union> ({e} \<times> Co``{e'}))"

theorem lm30: assumes "e' \<in> events Ca" "isLes Ca Co" "e\<notin>events Ca" "e\<notin>events Co" shows 
"isLes (addEventToCausality Ca e' e) (addEventToConflict Co e' e)"
(is "isLes ?Ca ?Co")
proof -
have "isMonotonicOver ?Co ?Ca" using assms lm29a by metis moreover have 
"sym ?Co" using assms lm29b by metis moreover have "irrefl ?Co" using assms lm29c by metis 
moreover have "trans ?Ca" using assms lm29f reflex_def by metis moreover have "antisym ?Ca" 
using assms lm29d by metis moreover have "reflex' ?Ca" using assms lm29e reflex_def by metis
ultimately have "isLes ?Ca ?Co" unfolding reflex_def by fast thus ?thesis by blast
qed

abbreviation "earliestConcurrents Ca Co == concurrency Ca Co - Union {{x}\<times>successors Ca{y}|x y. (x,y)\<in> concurrency Ca Co} - Union{successors Ca {x} \<times> {y} |x y. (x,y)\<in>concurrency Ca Co}"
abbreviation "latestConcurrents Ca Co == concurrency Ca Co - Union {{x}\<times>predecessors Ca {y} |x y. (x,y)\<in>concurrency Ca Co} - Union{predecessors Ca {x} \<times> {y}|x y. (x,y)\<in>concurrency Ca Co}"
abbreviation "earliestConcurrents2 Ca Co X == concurrency Ca Co``X - successors Ca (concurrency Ca Co``X)"
(*value "card (earliestConcurrents composedCa {})"
value "latestConcurrents composedCa {}"
value "composedConcurrency `` {e2} - successors composedCa (composedConcurrency``{e2})"
value " composedConcurrency \<in> a"*)
abbreviation "divergingTriangles4 Ca Co == Union {{{x,y}} \<times> 
(predecessors (order2strictCover' Ca) {x} \<inter> predecessors (order2strictCover' Ca) {y})
| x y. (x,y) \<in> concurrency Ca Co}"
(*value "divergingTriangles4 Ca1 {}"*)
abbreviation "convergingTriangles4 Ca Co == Union {{{x,y}} \<times> 
(successors (order2strictCover' Ca) {x} \<inter> successors (order2strictCover' Ca) {y})
| x y. (x,y) \<in> concurrency Ca Co}"

abbreviation "divergingTriangles2 Ca Co == Union {{(x,y)} \<times>(predecessors (order2strictCover' Ca) {x} \<inter>
predecessors (order2strictCover' Ca) {y})| x y. (x,y) \<in> earliestConcurrents Ca Co}"

abbreviation "rhombuses Ca Co == (divergingTriangles4 Ca Co) || (Domain (divergingTriangles4 Ca Co) 
\<inter> Domain (convergingTriangles4 Ca Co)) \<union> 
(convergingTriangles4 Ca Co) || (Domain (divergingTriangles4 Ca Co) 
\<inter> Domain (convergingTriangles4 Ca Co))"

(*value "Domain (divergingTriangles4 composedCa {})"
value "convergingTriangles4 composedCa {}"
value "{x| x. x\<notin>(events composedCa)}"
lemma "\<not> (isLes extendedComposedCa {})" sledgehammer[provers=z3]*)
(* lemma
"\<not> (EX X. X\<subseteq> mbc04 & Domain X = mbc05)"
unfolding mbc04_def runiq_wrt_ex1
sledgehammer run [provers=z3, minimize=false, overlord=true, timeout=1] (assms)
*)



end
